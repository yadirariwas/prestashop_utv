<?php
/*
* 2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Eduardo Neria <eneria@outlook.com>
*  @copyright  2015 Adimpacto
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AccountsCore extends ObjectModel
{
	public $email; //


	/**
	 * @param $email
	 * @return usuarios con los que cuenta el cliente
	 */
	public function getAccounts($id_customer)
	{
		//return Db::getInstance()->getValue('SELECT * FROM '._DB_PREFIX_.'customer_roku WHERE email = "'.$email.'"');

		//$result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'customer_roku WHERE email = "'.$email.'"');
		$customer = 140;
		$result = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'customer_roku.*, MAX('._DB_PREFIX_.'customer_group.id_group) AS grupo
			FROM '._DB_PREFIX_.'customer_roku
			INNER JOIN '._DB_PREFIX_.'customer_group ON ('._DB_PREFIX_.'customer_group.id_customer = '._DB_PREFIX_.'customer_roku.id_customer)
			WHERE '._DB_PREFIX_.'customer_roku.id_customer = "'.$id_customer.'"
			GROUP BY '._DB_PREFIX_.'customer_roku.user
			ORDER BY '._DB_PREFIX_.'customer_roku.user ASC');
		
		//echo "<pre><small>". print_r($result, true) ."</small></pre>"; exit;

		return $result;		
	}

}
