<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CustomerRokuCore extends ObjectModel
{
	public $id;

	/** @var string e-mail */
	public $email;

	/** @var string Lastname */
	public $user;
	
	public $fecha_pago = null;	
	
	/** @var boolean Status */
	public $active = true;

	public $years;
	public $days;
	public $months;	

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'customer_roku',
		'primary' => 'id_customer_roku',
		'fields' => array(
			'email' => 						array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
			'user' => 		  				array('type' => self::TYPE_STRING, 'required' => true, 'size' => 32),
			'fecha_pago' => 				array('type' => self::TYPE_DATE),
		),
	);
	
	public function __construct($id = null)
	{	
		//$this->id_default_group = (int)Configuration::get('PS_CUSTOMER_GROUP');
		parent::__construct($id);
	}
	
	public function add($autodate = true, $null_values = true)
	{			
		//$this->fecha_pago = (empty($this->years) ? $this->fecha_pago : (int)$this->years.'-'.(int)$this->months.'-'.(int)$this->days);
		
	 	$success = parent::add($autodate, $null_values);

		return $success;
	}

	public function update($nullValues = false)
	{
		$this->fecha_pago = (empty($this->years) ? $this->fecha_pago : (int)$this->years.'-'.(int)$this->months.'-'.(int)$this->days);
			
		if ($this->deleted)
		{
			$addresses = $this->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
			foreach ($addresses as $address)
			{
				$obj = new Address((int)$address['id_address']);
				$obj->delete();
			}
		}

	 	return parent::update(true);
	}




	/**
	 * Check if e-mail is already registered in database
	 *
	 * @param string $email e-mail
	 * @param $return_id boolean
	 * @param $ignore_guest boolean, to exclude guest customer
	 * @return Customer ID if found, false otherwise
	 */
	public static function customerExists($email, $return_id = false, $ignore_guest = true)
	{
		if (!Validate::isEmail($email))
		{
			if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_)
				die (Tools::displayError('Invalid email'));
			else
				return false;
		}
		
		$sql = 'SELECT `id_customer`
				FROM `'._DB_PREFIX_.'customer`
				WHERE `email` = \''.pSQL($email).'\'
					'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).
					($ignore_guest ? ' AND `is_guest` = 0' : '');
		$result = Db::getInstance()->getRow($sql);

		if ($return_id)
			return $result['id_customer'];
		return isset($result['id_customer']);
	}




}
