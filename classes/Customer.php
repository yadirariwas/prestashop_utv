<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CustomerCore extends ObjectModel
{
	public $id;

	public $id_shop;

	public $id_shop_group;

	/** @var string Secure key */
	public $secure_key;

	/** @var string protected note */
	public $note;

	/** @var integer Gender ID */
	public $id_gender = 0;

	/** @var integer Default group ID */
	public $id_default_group;

	/** @var integer Current language used by the customer */
	public $id_lang;

	/** @var string Lastname */
	public $lastname;

	/** @var string Firstname */
	public $firstname;

	/** @var string Birthday (yyyy-mm-dd) */
	public $birthday = null;

	/** @var string e-mail */
	public $email;

	/** @var boolean Newsletter subscription */
	public $newsletter;

	/** @var string Newsletter ip registration */
	public $ip_registration_newsletter;

	/** @var string Newsletter ip registration */
	public $newsletter_date_add;

	/** @var boolean Opt-in subscription */
	public $optin;

	/** @var string WebSite **/
	public $website;

	/** @var string Company */
	public $company;

	/** @var string SIRET */
	public $siret;

	/** @var string APE */
	public $ape;

	/** @var float Outstanding allow amount (B2B opt)  */
	public $outstanding_allow_amount = 0;

	/** @var integer Show public prices (B2B opt) */
	public $show_public_prices = 0;

	/** @var int Risk ID (B2B opt) */
	public $id_risk;

	/** @var integer Max payment day */
	public $max_payment_days = 0;

	/** @var integer Password */
	public $passwd;

	/** @var string Datetime Password */
	public $last_passwd_gen;

	/** @var boolean Status */
	public $active = true;

	/** @var boolean Status */
	public $is_guest = 0;

	/** @var boolean True if carrier has been deleted (staying in database as deleted) */
	public $deleted = 0;

	/** @var string Object creation date */
	public $date_add;

	/** @var string Object last modification date */
	public $date_upd;

	public $years;
	public $days;
	public $months;

	/** @var int customer id_country as determined by geolocation */
	public $geoloc_id_country;
	/** @var int customer id_state as determined by geolocation */
	public $geoloc_id_state;
	/** @var string customer postcode as determined by geolocation */
	public $geoloc_postcode;

	/** @var boolean is the customer logged in */
	public $logged = 0;

	/** @var int id_guest meaning the guest table, not the guest customer  */
	public $id_guest;

	public $groupBox;

	protected $webserviceParameters = array(
		'fields' => array(
			'id_default_group' => array('xlink_resource' => 'groups'),
			'id_lang' => array('xlink_resource' => 'languages'),
			'newsletter_date_add' => array(),
			'ip_registration_newsletter' => array(),
			'last_passwd_gen' => array('setter' => null),
			'secure_key' => array('setter' => null),
			'deleted' => array(),
			'passwd' => array('setter' => 'setWsPasswd'),
		),
		'associations' => array(
			'groups' => array('resource' => 'groups'),
		)
	);

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'customer',
		'primary' => 'id_customer',
		'fields' => array(
			'secure_key' => 				array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
			'lastname' => 					array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
			'firstname' => 					array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
			'email' => 						array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
			'passwd' => 					array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
			'last_passwd_gen' =>			array('type' => self::TYPE_STRING, 'copy_post' => false),
			'id_gender' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'birthday' => 					array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
			'newsletter' => 				array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'newsletter_date_add' =>		array('type' => self::TYPE_DATE,'copy_post' => false),
			'ip_registration_newsletter' =>	array('type' => self::TYPE_STRING, 'copy_post' => false),
			'optin' => 						array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'website' =>					array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
			'company' =>					array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'siret' =>						array('type' => self::TYPE_STRING, 'validate' => 'isSiret'),
			'ape' =>						array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
			'outstanding_allow_amount' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
			'show_public_prices' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'id_risk' =>					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
			'max_payment_days' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
			'active' => 					array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'deleted' => 					array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'note' => 						array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
			'is_guest' =>					array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'id_shop' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'id_shop_group' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'id_default_group' => 			array('type' => self::TYPE_INT, 'copy_post' => false),
			'id_lang' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'date_add' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			'date_upd' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
		),
	);

	protected static $_defaultGroupId = array();
	protected static $_customerHasAddress = array();
	protected static $_customer_groups = array();
	
	public function __construct($id = null)
	{	
		$this->id_default_group = (int)Configuration::get('PS_CUSTOMER_GROUP');
		parent::__construct($id);
	}
	
	public function add($autodate = true, $null_values = true)
	{

	/*	
		$angel["descuento150"][]="mankev@me.com";
		$angel["descuento150"][]="rokudf@gmail.com";
		$angel["descuento50"][]="monicagzzfigueroa@gmail.com";
		$angel["descuento50"][]="aaya@sunedison.com";
		$angel["descuento50"][]="pindterbertha@gmail.com";
		$angel["descuento50"][]="adri1calderon@gmail.com";
		$angel["descuento50"][]="asdz58@hotmail.com";
		$angel["descuento50"][]="gerardjaguar@yahoo.com.mx";
		$angel["descuento50"][]="nirvana_heaven@hotmail.com";
		$angel["descuento50"][]="alesscavazos@gmail.com";
		$angel["descuento50"][]="a_martinez_c@yahoo.com";
		$angel["descuento50"][]="prismav@yahoo.com";
		$angel["descuento50"][]="asg.1973@gmail.com";
		$angel["descuento50"][]="anac.almaguer@hotmail.com";
		$angel["descuento50"][]="alejandro@amrlabogados.mx";
		$angel["descuento50"][]="anita_sada@hotmail.com";
		$angel["descuento50"][]="antonio.herrero@deltagaming.com.mx";
		$angel["descuento50"][]="antonio.herrero@deltagaming.com.mx";
		$angel["descuento50"][]="blanca_blanco2007@hotmail.com";
		$angel["descuento50"][]="kps@chelsearf.com";
		$angel["descuento50"][]="sarkislara@gmail.com";
		$angel["descuento50"][]="liga_te@yahoo.com.mx";
		$angel["descuento50"][]="martcado@hotmail.com";
		$angel["descuento50"][]="cfreigva@televisa.com.mx";
		$angel["descuento50"][]="bibist@hotmail.com";
		$angel["descuento50"][]="taniadiaz777@hotmail.com";
		$angel["descuento50"][]="cecmc@me.com";
		$angel["descuento50"][]="dmz.dany@gmail.com";
		$angel["descuento50"][]="cornago@gmail.com";
		$angel["descuento50"][]="Dandelrio@hotmail.com";
		$angel["descuento50"][]="david.perezsalinas@gmail.com";
		$angel["descuento50"][]="eclavijo@integrapostventa.com";
		$angel["descuento50"][]="Murrieta.emg@gmail.com";
		$angel["descuento50"][]="federicobelden@gmail.com";
		$angel["descuento50"][]="federicobeldensada@gmail.com";
		$angel["descuento50"][]="federicosf@gmail.com";
		$angel["descuento50"][]="fdeovando@hotmail.com";
		$angel["descuento50"][]="fidelraul10191963@gmail.com";
		$angel["descuento50"][]="florerubio90@gmail.com";
		$angel["descuento50"][]="fredo.abrantes@gmail.com";
		$angel["descuento50"][]="jdavila10@gmail.com";
		$angel["descuento50"][]="gbj007@gmail.com";
		$angel["descuento50"][]="gcch@eog.mx";
		$angel["descuento50"][]="gildamgarciag@gmail.com";
		$angel["descuento50"][]="gilberto61c@gmail.com";
		$angel["descuento50"][]="garr231@interhabita.com.mx";
		$angel["descuento50"][]="garciagus2003@yahoo.com";
		$angel["descuento50"][]="pitaglezs@yahoo.com.mx";
		$angel["descuento50"][]="humbertocavazos@yahoo.com";
		$angel["descuento50"][]="humberto@cavazosflores.com";
		$angel["descuento50"][]="hpaliza@gmail.com";
		$angel["descuento50"][]="iabiega@yahoo.com";
		$angel["descuento50"][]="inigomat@yahoo.com.mx";
		$angel["descuento50"][]="inigomat@yahoo.com.mx";
		$angel["descuento50"][]="igrobles@hotmail.com";
		$angel["descuento50"][]="jorgecarlos@inteligencialaboral.com";
		$angel["descuento50"][]="fernanda.delafuente@gmail.com";
		$angel["descuento50"][]="asuntoarreglado@cavazos.com";
		$angel["descuento50"][]="fernanda.delafuente@gmail.com";
		$angel["descuento50"][]="jdavila10@gmail.com";
		$angel["descuento50"][]="juan_antoniohernandez@hotmail.com";
		$angel["descuento50"][]="lopez.jorgej@gmail.com";
		$angel["descuento50"][]="jsauquet@gmail.com";
		$angel["descuento50"][]="joseroman.garzon@gmail.com";
		$angel["descuento50"][]="jorgeeduardo15@hotmail.com";
		$angel["descuento50"][]="jreyes63@mac.com";
		$angel["descuento50"][]="jsauquet@gmail.com";
		$angel["descuento50"][]="japedreroh@gmail.com";
		$angel["descuento50"][]="mestradag18@gmail.com";
		$angel["descuento50"][]="ARQ.KARLARMZ@GMAIL.COM";
		$angel["descuento50"][]="kps@chelsearf.com";
		$angel["descuento50"][]="igavidal@gmail.com";
		$angel["descuento50"][]="alfonso_mtzr@hotmail.com";
		$angel["descuento50"][]="laila.garciag@hotmail.com";
		$angel["descuento50"][]="lugaudi@gmail.com";
		$angel["descuento50"][]="lcasamayor@gmail.com";
		$angel["descuento50"][]="mallitaxoxo93@gmail.com";
		$angel["descuento50"][]="lgmartinez@almex.com.mx";
		$angel["descuento50"][]="Mab3067@gmail.com";
		$angel["descuento50"][]="maragarza@gmail.com";
		$angel["descuento50"][]="mariolozano21@hotmail.com";
		$angel["descuento50"][]="mariza.mejiag@gmail.com";
		$angel["descuento50"][]="manuel.castellanosc@gmail.com";
		$angel["descuento50"][]="mda@chelsearf.com";
		$angel["descuento50"][]="paolatrevino10@gmail.com";
		$angel["descuento50"][]="mgarzah@hotmail.com";
		$angel["descuento50"][]="monicagr22@hotmail.com";
		$angel["descuento50"][]="monicasanz@t-visa.com.mx";
		$angel["descuento50"][]="mtt99@hotmail.com";
		$angel["descuento50"][]="mzubiria@mac.com";
		$angel["descuento50"][]="nataliagarza@gmail.com";
		$angel["descuento50"][]="normacantu@me.com";
		$angel["descuento50"][]="nunziarojodelavega@gmail.com";
		$angel["descuento50"][]="osalinas@tsigns.com.mx";
		$angel["descuento50"][]="osalinas@tsigns.com.mx";
		$angel["descuento50"][]="jflozano@rimmsa.com";
		$angel["descuento50"][]="jdavila10@gmail.com";
		$angel["descuento50"][]="pablozuelos@gmail.com";
		$angel["descuento50"][]="politrevino@hotmail.com";
		$angel["descuento50"][]="Garciabazan@hotmail.com";
		$angel["descuento50"][]="alejandroprats90@gmail.com";
		$angel["descuento50"][]="rafgonz43@yahoo.com";
		$angel["descuento50"][]="ricardodigo@hotmail.com";
		$angel["descuento50"][]="robertofmiles@gmail.com";
		$angel["descuento50"][]="rlopez66@hotmail.com";
		$angel["descuento50"][]="drbetochas@gmail.com";
		$angel["descuento50"][]="Roberto.sanchez@consyrsa.com";
		$angel["descuento50"][]="rtogores@ludicus.com";
		$angel["descuento50"][]="visionk6@gmail.com";
		$angel["descuento50"][]="visionk6@gmail.com";
		$angel["descuento50"][]="caneggle87@gmail.com";
		$angel["descuento50"][]="stephaniemdl@hotmail.com";
		$angel["descuento50"][]="sofia_mtz5@hotmail.com";
		$angel["descuento50"][]="sonia_bdm@hotmail.com";
		$angel["descuento50"][]="pablosozzi@hotmail.com";
		$angel["descuento50"][]="Stephaniemdl@hotmail.com";
		$angel["descuento50"][]="dondelees@gmail.com";
		$angel["descuento50"][]="valeriant@gmail.com";
		$angel["descuento50"][]="vela_f@hotmail.com";
		$angel["descuento50"][]="drvalpuesta@gmail.com";
		$angel["descuento50"][]="dvillarreal@aceromex.com";
		$angel["descuento50"][]="mzubiriam@hotmail.com";
		
		$angel["descuento50"][]="Myrnavalderrama@hotmail.com";
		
		$angel["descuento50"][]="angel.servin@gmail.com";
		$angel["descuento50"][]="sebastian.carry@gmail.com";
		
		//
		
		$angel["anteriores"][]="aaya@sunedison.com";
		$angel["anteriores"][]="c4martell@gmail.com";
		$angel["anteriores"][]="pindterbertha@gmail.com";
		$angel["anteriores"][]="adri1calderon@gmail.com";
		$angel["anteriores"][]="asdz58@hotmail.com";
		$angel["anteriores"][]="mlopez@cpiproductos.com.mx";
		$angel["anteriores"][]="gerardjaguar@yahoo.com.mx";
		$angel["anteriores"][]="alberto@grupofrisa.com";
		$angel["anteriores"][]="patricioalcocer@yahoo.com";
		$angel["anteriores"][]="alejandro@delgado.com";
		$angel["anteriores"][]="alejandro.fernandez@matesa.net";
		$angel["anteriores"][]="nirvana_heaven@hotmail.com";
		$angel["anteriores"][]="alejandroespinosa@correoinfinitum.com";
		$angel["anteriores"][]="alesscavazos@gmail.com";
		$angel["anteriores"][]="rokudf@gmail.com";
		$angel["anteriores"][]="a_martinez_c@yahoo.com";
		$angel["anteriores"][]="marinalejandro@mac.com";
		$angel["anteriores"][]="almuval86@yahoo.com";
		$angel["anteriores"][]="prismav@yahoo.com";
		$angel["anteriores"][]="asg.1973@gmail.com";
		$angel["anteriores"][]="anac.almaguer@hotmail.com";
		$angel["anteriores"][]="javier.amador@me.com";
		$angel["anteriores"][]="Ro_avendano@hotmail.com";
		$angel["anteriores"][]="alejandro@amrlabogados.mx";
		$angel["anteriores"][]="anita_sada@hotmail.com";
		$angel["anteriores"][]="antonio.herrero@deltagaming.com.mx";
		$angel["anteriores"][]="antonio.herrero@deltagaming.com.mx";
		$angel["anteriores"][]="mauro_308@hotmail.com";
		$angel["anteriores"][]="lo8a61@gmail.com";
		$angel["anteriores"][]="adrianaparedeso@hotmail.com";
		$angel["anteriores"][]="aperez1105@gmail.com";
		$angel["anteriores"][]="adelarosa0728@yahoo.com";
		$angel["anteriores"][]="rt@wallakholding.com";
		$angel["anteriores"][]="balta@cavazos.com";
		$angel["anteriores"][]="blanca_blanco2007@hotmail.com";
		$angel["anteriores"][]="kps@chelsearf.com";
		$angel["anteriores"][]="lbenjami@hotmail.com";
		$angel["anteriores"][]="lbenjami@hotmail.com";
		$angel["anteriores"][]="cellobene@hotmail.com";
		$angel["anteriores"][]="sarkislara@gmail.com";
		$angel["anteriores"][]="barbaraizzo@me.com";
		$angel["anteriores"][]="bruno_reynoso@hotmail.com";
		$angel["anteriores"][]="liga_te@yahoo.com.mx";
		$angel["anteriores"][]="misaza55@yahoo.com.mx";
		$angel["anteriores"][]="nizarclemente@gmail.com";
		$angel["anteriores"][]="mariainesprats@gmail.com";
		$angel["anteriores"][]="martcado@hotmail.com";
		$angel["anteriores"][]="karlacapote@hotmail.com";
		$angel["anteriores"][]="carce2000@yahoo.com";
		$angel["anteriores"][]="cfreigva@televisa.com.mx";
		$angel["anteriores"][]="bienmanejado@hotmail.com";
		$angel["anteriores"][]="Credelag@gmail.com";
		$angel["anteriores"][]="sobando@yahoo.com";
		$angel["anteriores"][]="bibist@hotmail.com";
		$angel["anteriores"][]="taniadiaz777@hotmail.com";
		$angel["anteriores"][]="Ccouttolenc@me.com";
		$angel["anteriores"][]="cecmc@me.com";
		$angel["anteriores"][]="queridomario@gmail.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="chabatito@yahoo.com.mx";
		$angel["anteriores"][]="nizarclemente@gmail.com";
		$angel["anteriores"][]="dmz.dany@gmail.com";
		$angel["anteriores"][]="Danespino@gmail.com";
		$angel["anteriores"][]="cornago@gmail.com";
		$angel["anteriores"][]="david.solis@condenast.com.mx";
		$angel["anteriores"][]="Dandelrio@hotmail.com";
		$angel["anteriores"][]="diego.fernandez@matesa.net";
		$angel["anteriores"][]="teransfam@gmail.com";
		$angel["anteriores"][]="danperezsalazar@gmail.com";
		$angel["anteriores"][]="iarmida@gmail.com";
		$angel["anteriores"][]="jokhuysen@gmail.com";
		$angel["anteriores"][]="doporto@doporto.mx";
		$angel["anteriores"][]="guillermo.lts@gmail.com";
		$angel["anteriores"][]="david.perezsalinas@gmail.com";
		$angel["anteriores"][]="direccion@luxorytransfers.com";
		$angel["anteriores"][]="eclavijo@integrapostventa.com";
		$angel["anteriores"][]="edivelazquez@hotmail.com";
		$angel["anteriores"][]="virues0@gmail.com";
		$angel["anteriores"][]="virues0@gmail.com";
		$angel["anteriores"][]="Murrieta.emg@gmail.com";
		$angel["anteriores"][]="fraga.emilio@gmail.com";
		$angel["anteriores"][]="Esberatala@me.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="e.o.vieira@gmail.com";
		$angel["anteriores"][]="federicobelden@gmail.com";
		$angel["anteriores"][]="fabiolahg96@gmail.com";
		$angel["anteriores"][]="federicobeldensada@gmail.com";
		$angel["anteriores"][]="federicosf@gmail.com";
		$angel["anteriores"][]="fdeovando@hotmail.com";
		$angel["anteriores"][]="fidelraul10191963@gmail.com";
		$angel["anteriores"][]="Fjkm33@gmail.com";
		$angel["anteriores"][]="florerubio90@gmail.com";
		$angel["anteriores"][]="carlos.noyola@gmail.com";
		$angel["anteriores"][]="cupcakes.to.go@hotmail.com";
		$angel["anteriores"][]="juan.f.mediavilla@gmail.com";
		$angel["anteriores"][]="fredo.abrantes@gmail.com";
		$angel["anteriores"][]="jdavila10@gmail.com";
		$angel["anteriores"][]="gbj007@gmail.com";
		$angel["anteriores"][]="gcch@eog.mx";
		$angel["anteriores"][]="gemmaburch1@mac.com";
		$angel["anteriores"][]="morancastellot@yahoo.com.mx";
		$angel["anteriores"][]="gildamgarciag@gmail.com";
		$angel["anteriores"][]="gilberto61c@gmail.com";
		$angel["anteriores"][]="giocerritelli@me.com";
		$angel["anteriores"][]="gerardomorancastellot@gmail.com";
		$angel["anteriores"][]="ricardo.kerber@gmail.com";
		$angel["anteriores"][]="moga_16@hotmail.com";
		$angel["anteriores"][]="gretta27@gmail.com";
		$angel["anteriores"][]="gflores@me.com";
		$angel["anteriores"][]="garr231@interhabita.com.mx";
		$angel["anteriores"][]="garciagus2003@yahoo.com";
		$angel["anteriores"][]="pitaglezs@yahoo.com.mx";
		$angel["anteriores"][]="humbertocavazos@yahoo.com";
		$angel["anteriores"][]="humberto@cavazosflores.com";
		$angel["anteriores"][]="helle_jeppsson@hotmail.com";
		$angel["anteriores"][]="hpaliza@gmail.com";
		$angel["anteriores"][]="hazel_sandoval@hotmail.com";
		$angel["anteriores"][]="iabiega@yahoo.com";
		$angel["anteriores"][]="inigomat@yahoo.com.mx";
		$angel["anteriores"][]="inigomat@yahoo.com.mx";
		$angel["anteriores"][]="jcabrera@jaca.com.mx";
		$angel["anteriores"][]="jalazvar@yahoo.com.mx";
		$angel["anteriores"][]="igrobles@hotmail.com";
		$angel["anteriores"][]="Javier@amrlabogados.mx";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="jorgecarlos@inteligencialaboral.com";
		$angel["anteriores"][]="jorgegcastaneda@gmail.com";
		$angel["anteriores"][]="fernanda.delafuente@gmail.com";
		$angel["anteriores"][]="asuntoarreglado@cavazos.com";
		$angel["anteriores"][]="fernanda.delafuente@gmail.com";
		$angel["anteriores"][]="jdavila10@gmail.com";
		$angel["anteriores"][]="javfolch@mac.com";
		$angel["anteriores"][]="juan_antoniohernandez@hotmail.com";
		$angel["anteriores"][]="jibarrola@lico.mx";
		$angel["anteriores"][]="lopez.jorgej@gmail.com";
		$angel["anteriores"][]="genaf@me.com";
		$angel["anteriores"][]="jsauquet@gmail.com";
		$angel["anteriores"][]="rokudf@gmail.com";
		$angel["anteriores"][]="joseroman.garzon@gmail.com";
		$angel["anteriores"][]="joselbq@yahoo.com";
		$angel["anteriores"][]="jorgeeduardo15@hotmail.com";
		$angel["anteriores"][]="jorge@recreoentretenimiento.com";
		$angel["anteriores"][]="jreyes63@mac.com";
		$angel["anteriores"][]="jsauquet@gmail.com";
		$angel["anteriores"][]="japedreroh@gmail.com";
		$angel["anteriores"][]="jrmoreno@morenolaw.com";
		$angel["anteriores"][]="jcarlosgonzalez2000@hotmail.com";
		$angel["anteriores"][]="jamessprowls@gmail.com";
		$angel["anteriores"][]="mestradag18@gmail.com";
		$angel["anteriores"][]="ARQ.KARLARMZ@GMAIL.COM";
		$angel["anteriores"][]="kps@chelsearf.com";
		$angel["anteriores"][]="igavidal@gmail.com";
		$angel["anteriores"][]="alfonso_mtzr@hotmail.com";
		$angel["anteriores"][]="Leogarcia7@gmail.com";
		$angel["anteriores"][]="leyrafael@gmail.com";
		$angel["anteriores"][]="lidibc@hotmail.com";
		$angel["anteriores"][]="bladic@hotmail.com";
		$angel["anteriores"][]="hbladinieres@hotmail.com";
		$angel["anteriores"][]="liliabh29@hotmail.com";
		$angel["anteriores"][]="laila.garciag@hotmail.com";
		$angel["anteriores"][]="lmv@monsalvo.com.mx";
		$angel["anteriores"][]="lazcarraga88@aol.com";
		$angel["anteriores"][]="lazcarraga88@aol.com";
		$angel["anteriores"][]="lazcarraga88@aol.com";
		$angel["anteriores"][]="lugaudi@gmail.com";
		$angel["anteriores"][]="lcasamayor@gmail.com";
		$angel["anteriores"][]="lgonzalez@inpamex.com";
		$angel["anteriores"][]="proinsumos01@gmail.com";
		$angel["anteriores"][]="macahaca@gmail.com";
		$angel["anteriores"][]="centenomaria@gmail.com";
		$angel["anteriores"][]="melisaromo@gmail.com";
		$angel["anteriores"][]="mallitaxoxo93@gmail.com";
		$angel["anteriores"][]="lgmartinez@almex.com.mx";
		$angel["anteriores"][]="Mab3067@gmail.com";
		$angel["anteriores"][]="maragarza@gmail.com";
		$angel["anteriores"][]="mariagaha@me.com";
		$angel["anteriores"][]="mariolozano21@hotmail.com";
		$angel["anteriores"][]="mariza.mejiag@gmail.com";
		$angel["anteriores"][]="lavazquez001@hotmail.com";
		$angel["anteriores"][]="marthagalas@gmail.com";
		$angel["anteriores"][]="matute@hotmail.com";
		$angel["anteriores"][]="mautvtv@hotmail.com";
		$angel["anteriores"][]="maytegfigueroa@gmail.com";
		$angel["anteriores"][]="manuel.castellanosc@gmail.com";
		$angel["anteriores"][]="mda@chelsearf.com";
		$angel["anteriores"][]="paolatrevino10@gmail.com";
		$angel["anteriores"][]="mgarzah@hotmail.com";
		$angel["anteriores"][]="monicagzzfigueroa@gmail.com";
		$angel["anteriores"][]="gerenciawtc@teikit.mx";
		$angel["anteriores"][]="mnhayito@hotmail.com";
		$angel["anteriores"][]="monicagr22@hotmail.com";
		$angel["anteriores"][]="maytegfigueroa@gmail.com";
		$angel["anteriores"][]="monicasanz@t-visa.com.mx";
		$angel["anteriores"][]="mtt99@hotmail.com";
		$angel["anteriores"][]="mzubiria@mac.com";
		$angel["anteriores"][]="icastro@adimpacto.com";
		$angel["anteriores"][]="nataliagarza@gmail.com";
		$angel["anteriores"][]="normacantu@me.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="nizarclemente@gmail.com";
		$angel["anteriores"][]="nunziarojodelavega@gmail.com";
		$angel["anteriores"][]="osalinas@tsigns.com.mx";
		$angel["anteriores"][]="osalinas@tsigns.com.mx";
		$angel["anteriores"][]="plavalleg@hotmail.com";
		$angel["anteriores"][]="plavalleg@hotmail.com";
		$angel["anteriores"][]="jflozano@rimmsa.com";
		$angel["anteriores"][]="patyf2@mac.com";
		$angel["anteriores"][]="jdavila10@gmail.com";
		$angel["anteriores"][]="pedro_alvear@hotmail.com";
		$angel["anteriores"][]="pablozuelos@gmail.com";
		$angel["anteriores"][]="politrevino@hotmail.com";
		$angel["anteriores"][]="ropodo@hotmail.com";
		$angel["anteriores"][]="Garciabazan@hotmail.com";
		$angel["anteriores"][]="alejandroprats90@gmail.com";
		$angel["anteriores"][]="angaha-1001@hotmail.com";
		$angel["anteriores"][]="rafgonz43@yahoo.com";
		$angel["anteriores"][]="ricardodigo@hotmail.com";
		$angel["anteriores"][]="robertofmiles@gmail.com";
		$angel["anteriores"][]="rherrerias@rhcei.mx";
		$angel["anteriores"][]="rherrerias@rhcei.mx";
		$angel["anteriores"][]="rlopez66@hotmail.com";
		$angel["anteriores"][]="drbetochas@gmail.com";
		$angel["anteriores"][]="Roberto.sanchez@consyrsa.com";
		$angel["anteriores"][]="torresl.german@gmail.com";
		$angel["anteriores"][]="rtogores@ludicus.com";
		$angel["anteriores"][]="jrubenhernandez@hotmail.com";
		$angel["anteriores"][]="claude.mongala@gmail.com";
		$angel["anteriores"][]="rmatenorio@outlook.com";
		$angel["anteriores"][]="mrvieira@yahoo.com";
		$angel["anteriores"][]="undertvsa@gmail.com";
		$angel["anteriores"][]="rmatenorio@gmail.com";
		$angel["anteriores"][]="bernard.ruberg@gmail.com";
		$angel["anteriores"][]="Greg.lang@touchhome.co.za";
		$angel["anteriores"][]="claude@seragliojewellers.com";
		$angel["anteriores"][]="salomonmansur@gmail.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="schufani@chufani.com";
		$angel["anteriores"][]="sergiochedraui@gmail.com";
		$angel["anteriores"][]="visionk6@gmail.com";
		$angel["anteriores"][]="visionk6@gmail.com";
		$angel["anteriores"][]="caneggle87@gmail.com";
		$angel["anteriores"][]="diego.fernandez@matesa.net";
		$angel["anteriores"][]="stephaniemdl@hotmail.com";
		$angel["anteriores"][]="sofia_mtz5@hotmail.com";
		$angel["anteriores"][]="sonia_bdm@hotmail.com";
		$angel["anteriores"][]="pablosozzi@hotmail.com";
		$angel["anteriores"][]="Stephaniemdl@hotmail.com";
		$angel["anteriores"][]="susre@hotmail.com";
		$angel["anteriores"][]="dondelees@gmail.com";
		$angel["anteriores"][]="mankev@me.com";
		$angel["anteriores"][]="scullenp@gmail.com";
		$angel["anteriores"][]="ltostado@hotmail.com";
		$angel["anteriores"][]="Ugow@gaam.com.mx";
		$angel["anteriores"][]="affernan@radioformula.com.mx";
		$angel["anteriores"][]="affernan@radioformula.com.mx";
		$angel["anteriores"][]="mauro_plantabaja@hotmail.com";
		$angel["anteriores"][]="mlarregui@hotmail.com";
		$angel["anteriores"][]="valeriant@gmail.com";
		$angel["anteriores"][]="vela_f@hotmail.com";
		$angel["anteriores"][]="rokudf@gmail.com";
		$angel["anteriores"][]="drvalpuesta@gmail.com";
		$angel["anteriores"][]="dvillarreal@aceromex.com";
		$angel["anteriores"][]="e.trejo@me.com";
		$angel["anteriores"][]="josed_rgc@hotmail.com";
		$angel["anteriores"][]="agaskdro@yahoo.com.mx";
		$angel["anteriores"][]="amhilairefraga@hotmail.com";
		$angel["anteriores"][]="yessics@hotmail.com";
		$angel["anteriores"][]="marioab75@yahoo.com.mx";
		$angel["anteriores"][]="mzubiriam@hotmail.com";
		*/
		/*
		foreach($angel["descuento50"] as $key => $value){
			$angel["descuento50"][$key]=strtolower($value);
		}
		
		foreach($angel["descuento100"] as $key => $value){
			$angel["descuento100"][$key]=strtolower($value);
		}
		
		foreach($angel["descuento150"] as $key => $value){
			$angel["descuento150"][$key]=strtolower($value);
		}
		
		foreach($angel["anteriores"] as $key => $value){
			$angel["anteriores"][$key]=strtolower($value);
		}
		*/

		
		$this->id_shop = ($this->id_shop) ? $this->id_shop : Context::getContext()->shop->id;
		$this->id_shop_group = ($this->id_shop_group) ? $this->id_shop_group : Context::getContext()->shop->id_shop_group;
		$this->id_lang = ($this->id_lang) ? $this->id_lang : Context::getContext()->language->id;
		$this->birthday = (empty($this->years) ? $this->birthday : (int)$this->years.'-'.(int)$this->months.'-'.(int)$this->days);
		$this->secure_key = md5(uniqid(rand(), true));
		$this->last_passwd_gen = date('Y-m-d H:i:s', strtotime('-'.Configuration::get('PS_PASSWD_TIME_FRONT').'minutes'));
		
		if ($this->newsletter && !Validate::isDate($this->newsletter_date_add))
			$this->newsletter_date_add = date('Y-m-d H:i:s');
			
		if ($this->id_default_group == Configuration::get('PS_CUSTOMER_GROUP'))
			if ($this->is_guest)
				$this->id_default_group = (int)Configuration::get('PS_GUEST_GROUP');
			else
				$this->id_default_group = (int)Configuration::get('PS_CUSTOMER_GROUP');

		$existe_customer = Db::getInstance()->executes('SELECT descuento FROM '._DB_PREFIX_.'customer_tienda WHERE LOWER(correo) ="'. strtolower($this->email) .'"');
		
		//echo "<pre><small>".print_r($existe_customer,true)."</small></pre>";
		//exit;
		if($existe_customer[0]['descuento']=="descuento50"){
			$this->id_default_group=4;
		}
		elseif($existe_customer[0]['descuento']=="descuento100"){
			$this->id_default_group=5;
		}
		elseif($existe_customer[0]['descuento']=="descuento150"){
			$this->id_default_group=6;
		}
		elseif($existe_customer[0]['descuento']=="anteriores"){
			$this->id_default_group=7;
		}

		/*if (in_array(strtolower($this->email), $angel["descuento50"])) {
			$this->id_default_group=4;
		}	
		elseif (in_array(strtolower($this->email), $angel["descuento100"])) {
			$this->id_default_group=5;
		}	
		elseif (in_array(strtolower($this->email), $angel["descuento150"])) {
			$this->id_default_group=6;
		}	
		elseif (in_array(strtolower($this->email), $angel["anteriores"])) {
			$this->id_default_group=7;
		}	*/
			
		/* Can't create a guest customer, if this feature is disabled */
		if ($this->is_guest && !Configuration::get('PS_GUEST_CHECKOUT_ENABLED'))
			return false;
	 	$success = parent::add($autodate, $null_values);
		$this->updateGroup($this->groupBox);
		
		//Cosultamos si el email del cliente esta en nuestra tabla de control de usuarios
		$customer_roku = Db::getInstance()->executeS('SELECT user FROM '._DB_PREFIX_.'customer_roku WHERE LOWER(email) ="'. strtolower($this->email) .'"');
		//echo print_r($customer_roku, true); exit;
		if(!empty($customer_roku)){
			//Actualizamos el id_cliente
			Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_roku SET id_customer="'.$this->id.'" WHERE LOWER(email) ="'. strtolower($this->email) .'"');
		}			
		
		if($existe_customer){			
			//if (in_array(strtolower($this->email), $angel["descuento50"]) || in_array(strtolower($this->email), $angel["descuento100"]) || in_array(strtolower($this->email), $angel["descuento150"]) || in_array(strtolower($this->email), $angel["anteriores"])) {
			$group=(int)Configuration::get('PS_CUSTOMER_GROUP');
			$row = array('id_customer' => (int)$this->id, 'id_group' => (int)$group);
			Db::getInstance()->insert('customer_group', $row, false, true, Db::INSERT_IGNORE);			
		}
		
		return $success;
	}

	public function update($nullValues = false)
	{
		$this->birthday = (empty($this->years) ? $this->birthday : (int)$this->years.'-'.(int)$this->months.'-'.(int)$this->days);

		if ($this->newsletter && !Validate::isDate($this->newsletter_date_add))
			$this->newsletter_date_add = date('Y-m-d H:i:s');
		if (isset(Context::getContext()->controller) && Context::getContext()->controller->controller_type == 'admin')
			$this->updateGroup($this->groupBox);
			
		if ($this->deleted)
		{
			$addresses = $this->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
			foreach ($addresses as $address)
			{
				$obj = new Address((int)$address['id_address']);
				$obj->delete();
			}
		}

	 	return parent::update(true);
	}

	public function delete()
	{
		if (!count(Order::getCustomerOrders((int)$this->id)))
		{
			$addresses = $this->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
			foreach ($addresses as $address)
			{
				$obj = new Address((int)$address['id_address']);
				$obj->delete();
			}
		}
		Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'customer_group` WHERE `id_customer` = '.(int)$this->id);
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'message WHERE id_customer='.(int)$this->id);
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'specific_price WHERE id_customer='.(int)$this->id);		
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'compare WHERE id_customer='.(int)$this->id);
				
		$carts = Db::getInstance()->executes('SELECT id_cart 
															FROM '._DB_PREFIX_.'cart
															WHERE id_customer='.(int)$this->id);
		if ($carts)
			foreach ($carts as $cart)
			{
				Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart WHERE id_cart='.(int)$cart['id_cart']);
				Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_product WHERE id_cart='.(int)$cart['id_cart']);
			}
		
		$cts = Db::getInstance()->executes('SELECT id_customer_thread 
															FROM '._DB_PREFIX_.'customer_thread
															WHERE id_customer='.(int)$this->id);
		if ($cts)
			foreach ($cts as $ct)
			{
				Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread='.(int)$ct['id_customer_thread']);
				Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread='.(int)$ct['id_customer_thread']);
			}

		CartRule::deleteByIdCustomer((int)$this->id);
		return parent::delete();
	}

	/**
	 * Return customers list
	 *
	 * @return array Customers
	 */
	public static function getCustomers()
	{
		$sql = 'SELECT `id_customer`, `email`, `firstname`, `lastname`
				FROM `'._DB_PREFIX_.'customer`
				WHERE 1 '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
				ORDER BY `id_customer` ASC';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}

	/**
	 * Return customer instance from its e-mail (optionnaly check password)
	 *
	 * @param string $email e-mail
	 * @param string $passwd Password is also checked if specified
	 * @return Customer instance
	 */
	public function getByEmail($email, $passwd = null, $ignore_guest = true)
	{
		if (!Validate::isEmail($email) || ($passwd && !Validate::isPasswd($passwd)))
			die (Tools::displayError());

		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`
				WHERE `email` = \''.pSQL($email).'\'
					'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
					'.(isset($passwd) ? 'AND `passwd` = \''.Tools::encrypt($passwd).'\'' : '').'
					AND `deleted` = 0'.
					($ignore_guest ? ' AND `is_guest` = 0' : '');

		$result = Db::getInstance()->getRow($sql);

		if (!$result)
			return false;
		$this->id = $result['id_customer'];
		foreach ($result as $key => $value)
			if (array_key_exists($key, $this))
				$this->{$key} = $value;

		return $this;
	}

	/**
	 * Retrieve customers by email address
	 *
	 * @static
	 * @param $email
	 * @return array
	 */
	public static function getCustomersByEmail($email)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`
				WHERE `email` = \''.pSQL($email).'\'
					'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);

		return Db::getInstance()->ExecuteS($sql);
	}


	/**
	 * Check id the customer is active or not
	 *
	 * @return boolean customer validity
	 */
	public static function isBanned($id_customer)
	{
		if (!Validate::isUnsignedId($id_customer))
			return true;
		$cache_id = 'Customer::isBanned_'.(int)$id_customer;
		if (!Cache::isStored($cache_id))
		{
			$result = (bool)!Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE `id_customer` = \''.(int)$id_customer.'\'
			AND active = 1
			AND `deleted` = 0');
			Cache::store($cache_id, $result);
		}
		return Cache::retrieve($cache_id);
	}

	/**
	 * Check if e-mail is already registered in database
	 *
	 * @param string $email e-mail
	 * @param $return_id boolean
	 * @param $ignore_guest boolean, to exclude guest customer
	 * @return Customer ID if found, false otherwise
	 */
	public static function customerExists($email, $return_id = false, $ignore_guest = true)
	{
		if (!Validate::isEmail($email))
		{
			if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_)
				die (Tools::displayError('Invalid email'));
			else
				return false;
		}
		
		$sql = 'SELECT `id_customer`
				FROM `'._DB_PREFIX_.'customer`
				WHERE `email` = \''.pSQL($email).'\'
					'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).
					($ignore_guest ? ' AND `is_guest` = 0' : '');
		$result = Db::getInstance()->getRow($sql);

		if ($return_id)
			return $result['id_customer'];
		return isset($result['id_customer']);
	}

	/**
	 * Check if an address is owned by a customer
	 *
	 * @param integer $id_customer Customer ID
	 * @param integer $id_address Address ID
	 * @return boolean result
	 */
	public static function customerHasAddress($id_customer, $id_address)
	{
		$key = (int)$id_customer.'-'.(int)$id_address;
		if (!array_key_exists($key, self::$_customerHasAddress))
		{
			self::$_customerHasAddress[$key] = (bool)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT `id_address`
			FROM `'._DB_PREFIX_.'address`
			WHERE `id_customer` = '.(int)$id_customer.'
			AND `id_address` = '.(int)$id_address.'
			AND `deleted` = 0');
		}
		return self::$_customerHasAddress[$key];
	}

	public static function resetAddressCache($id_customer)
	{
		if (array_key_exists($id_customer, self::$_customerHasAddress))
			unset(self::$_customerHasAddress[$id_customer]);
	}

	/**
	 * Return customer addresses
	 *
	 * @param integer $id_lang Language ID
	 * @return array Addresses
	 */
	public function getAddresses($id_lang)
	{
		$share_order = (bool)Context::getContext()->shop->getGroup()->share_order;
		$cache_id = 'Customer::getAddresses'.(int)$this->id.'-'.(int)$id_lang.'-'.$share_order;
		if (!Cache::isStored($cache_id))
		{ 
			$sql = 'SELECT DISTINCT a.*, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
					FROM `'._DB_PREFIX_.'address` a
					LEFT JOIN `'._DB_PREFIX_.'country` c ON (a.`id_country` = c.`id_country`)
					LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country`)
					LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
					'.($share_order ? '' : Shop::addSqlAssociation('country', 'c')).' 
					WHERE `id_lang` = '.(int)$id_lang.' AND `id_customer` = '.(int)$this->id.' AND a.`deleted` = 0';

			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
			Cache::store($cache_id, $result);
		}
		return Cache::retrieve($cache_id);
	}

	/**
	 * Count the number of addresses for a customer
	 *
	 * @param integer $id_customer Customer ID
	 * @return integer Number of addresses
	 */
	public static function getAddressesTotalById($id_customer)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT COUNT(`id_address`)
			FROM `'._DB_PREFIX_.'address`
			WHERE `id_customer` = '.(int)$id_customer.'
			AND `deleted` = 0'
		);
	}

	/**
	 * Check if customer password is the right one
	 *
	 * @param string $passwd Password
	 * @return boolean result
	 */
	public static function checkPassword($id_customer, $passwd)
	{
		if (!Validate::isUnsignedId($id_customer) || !Validate::isMd5($passwd))
			die (Tools::displayError());
		$cache_id = 'Customer::checkPassword'.(int)$id_customer.'-'.$passwd;
		if (!Cache::isStored($cache_id))
		{
			$sql = 'SELECT `id_customer`
					FROM `'._DB_PREFIX_.'customer`
					WHERE `id_customer` = '.$id_customer.'
					AND `passwd` = \''.$passwd.'\'';
			$result = (bool)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
			Cache::store($cache_id, $result);
		}
		return Cache::retrieve($cache_id);
	}

	/**
	 * Light back office search for customers
	 *
	 * @param string $query Searched string
	 * @return array Corresponding customers
	 */
	public static function searchByName($query)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`
				WHERE (
						`email` LIKE \'%'.pSQL($query).'%\'
						OR `id_customer` LIKE \'%'.pSQL($query).'%\'
						OR `lastname` LIKE \'%'.pSQL($query).'%\'
						OR `firstname` LIKE \'%'.pSQL($query).'%\'
					)'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}

	/**
	 * Search for customers by ip address
	 *
	 * @param string $ip Searched string
	 */
	public static function searchByIp($ip)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT DISTINCT c.*
		FROM `'._DB_PREFIX_.'customer` c
		LEFT JOIN `'._DB_PREFIX_.'guest` g ON g.id_customer = c.id_customer
		LEFT JOIN `'._DB_PREFIX_.'connections` co ON g.id_guest = co.id_guest
		WHERE co.`ip_address` = \''.ip2long(trim($ip)).'\'');
	}

	/**
	 * Return several useful statistics about customer
	 *
	 * @return array Stats
	 */
	public function getStats()
	{
		$result = Db::getInstance()->getRow('
		SELECT COUNT(`id_order`) AS nb_orders, SUM(`total_paid` / o.`conversion_rate`) AS total_orders
		FROM `'._DB_PREFIX_.'orders` o
		WHERE o.`id_customer` = '.(int)$this->id.'
		AND o.valid = 1');

		$result2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
		SELECT MAX(c.`date_add`) AS last_visit
		FROM `'._DB_PREFIX_.'guest` g
		LEFT JOIN `'._DB_PREFIX_.'connections` c ON c.id_guest = g.id_guest
		WHERE g.`id_customer` = '.(int)$this->id);

		$result3 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
		SELECT (YEAR(CURRENT_DATE)-YEAR(c.`birthday`)) - (RIGHT(CURRENT_DATE, 5)<RIGHT(c.`birthday`, 5)) AS age
		FROM `'._DB_PREFIX_.'customer` c
		WHERE c.`id_customer` = '.(int)$this->id);

		$result['last_visit'] = $result2['last_visit'];
		$result['age'] = ($result3['age'] != date('Y') ? $result3['age'] : '--');
		return $result;
	}

	public function getLastConnections()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT c.date_add, COUNT(cp.id_page) AS pages, TIMEDIFF(MAX(cp.time_end), c.date_add) as time, http_referer,INET_NTOA(ip_address) as ipaddress
		FROM `'._DB_PREFIX_.'guest` g
		LEFT JOIN `'._DB_PREFIX_.'connections` c ON c.id_guest = g.id_guest
		LEFT JOIN `'._DB_PREFIX_.'connections_page` cp ON c.id_connections = cp.id_connections
		WHERE g.`id_customer` = '.(int)$this->id.'
		GROUP BY c.`id_connections`
		ORDER BY c.date_add DESC
		LIMIT 10');
	}

	/*
	* Specify if a customer already in base
	*
	* @param $id_customer Customer id
	* @return boolean
	*/
	// DEPRECATED
	public function customerIdExists($id_customer)
	{
		return Customer::customerIdExistsStatic((int)$id_customer);
	}

	public static function customerIdExistsStatic($id_customer)
	{
		$cache_id = 'Customer::customerIdExistsStatic'.(int)$id_customer;
		if (!Cache::isStored($cache_id))
		{
			$result = (int)Db::getInstance()->getValue('
			SELECT `id_customer`
			FROM '._DB_PREFIX_.'customer c
			WHERE c.`id_customer` = '.(int)$id_customer);
			Cache::store($cache_id, $result);
		}
		return Cache::retrieve($cache_id);
	}

	/**
	 * Update customer groups associated to the object
	 *
	 * @param array $list groups
	 */
	public function updateGroup($list)
	{
		if ($list && !empty($list))
		{
			$this->cleanGroups();
			$this->addGroups($list);
		}
		else
			$this->addGroups(array($this->id_default_group));
	}

	public function cleanGroups()
	{
		Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'customer_group` WHERE `id_customer` = '.(int)$this->id);
	}

	public function addGroups($groups)
	{
		foreach ($groups as $group)
		{
			$row = array('id_customer' => (int)$this->id, 'id_group' => (int)$group);
			Db::getInstance()->insert('customer_group', $row, false, true, Db::INSERT_IGNORE);
		}
	}

	public static function getGroupsStatic($id_customer)
	{
		if (!Group::isFeatureActive())
			return array(Configuration::get('PS_CUSTOMER_GROUP'));

		if ($id_customer == 0)
			self::$_customer_groups[$id_customer] = array((int)Configuration::get('PS_UNIDENTIFIED_GROUP'));

		if (!isset(self::$_customer_groups[$id_customer]))
		{
			self::$_customer_groups[$id_customer] = array();
			$result = Db::getInstance()->executeS('
			SELECT cg.`id_group`
			FROM '._DB_PREFIX_.'customer_group cg
			WHERE cg.`id_customer` = '.(int)$id_customer);
			foreach ($result as $group)
				self::$_customer_groups[$id_customer][] = (int)$group['id_group'];
		}
		return self::$_customer_groups[$id_customer];
	}

	public function getGroups()
	{
		return Customer::getGroupsStatic((int)$this->id);
	}

	/**
	 * @deprecated since 1.5
	 */
	public function isUsed()
	{
		Tools::displayAsDeprecated();
		return false;
	}

	public function getBoughtProducts()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT * FROM `'._DB_PREFIX_.'orders` o
		LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
		WHERE o.valid = 1 AND o.`id_customer` = '.(int)$this->id);
	}

	public static function getDefaultGroupId($id_customer)
	{
		if (!Group::isFeatureActive())
			return Configuration::get('PS_CUSTOMER_GROUP');

		if (!isset(self::$_defaultGroupId[(int)$id_customer]))
			self::$_defaultGroupId[(int)$id_customer] = Db::getInstance()->getValue('
				SELECT `id_default_group`
				FROM `'._DB_PREFIX_.'customer`
				WHERE `id_customer` = '.(int)$id_customer
			);
		return self::$_defaultGroupId[(int)$id_customer];
	}

	public static function getCurrentCountry($id_customer, Cart $cart = null)
	{
		if (!$cart)
			$cart = Context::getContext()->cart;
		if (!$cart || !$cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
			$id_address = (int)Db::getInstance()->getValue('
				SELECT `id_address`
				FROM `'._DB_PREFIX_.'address`
				WHERE `id_customer` = '.(int)$id_customer.'
				AND `deleted` = 0 ORDER BY `id_address`'
			);
		else
			$id_address = $cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
		$ids = Address::getCountryAndState($id_address);
		return (int)$ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT');
	}

	public function toggleStatus()
	{
		parent::toggleStatus();

		/* Change status to active/inactive */
		return Db::getInstance()->execute('
		UPDATE `'.pSQL(_DB_PREFIX_.$this->def['table']).'`
		SET `date_upd` = NOW()
		WHERE `'.$this->def['primary'].'` = '.(int)$this->id);
	}


	public function isGuest()
	{
		return (bool)$this->is_guest;
	}

	public function transformToCustomer($id_lang, $password = null)
	{
		if (!$this->isGuest())
			return false;
		if (empty($password))
			$password = Tools::passwdGen();
		if (!Validate::isPasswd($password))
			return false;

		$this->is_guest = 0;
		$this->passwd = Tools::encrypt($password);
		$this->cleanGroups();
		$this->addGroups(array(Configuration::get('PS_CUSTOMER_GROUP'))); // add default customer group
		if ($this->update())
		{
			$vars = array(
				'{firstname}' => $this->firstname,
				'{lastname}' => $this->lastname,
				'{email}' => $this->email,
				'{passwd}' => $password
			);

			Mail::Send(
				(int)$id_lang,
				'guest_to_customer',
				Mail::l('Your guest account has been transformed into a customer account', (int)$id_lang),
				$vars,
				$this->email,
				$this->firstname.' '.$this->lastname,
				null,
				null,
				null,
				null,
				_PS_MAIL_DIR_,
				false,
				(int)$this->id_shop
			);
			return true;
		}
		return false;
	}

	public function setWsPasswd($passwd)
	{
		if ($this->id != 0)
		{
			if ($this->passwd != $passwd)
				$this->passwd = Tools::encrypt($passwd);
		}
		else
			$this->passwd = Tools::encrypt($passwd);
		return true;
	}

	/**
	 * Check customer informations and return customer validity
	 *
	 * @since 1.5.0
	 * @param boolean $with_guest
	 * @return boolean customer validity
	 */
	public function isLogged($with_guest = false)
	{
		if (!$with_guest && $this->is_guest == 1)
			return false;

		/* Customer is valid only if it can be load and if object password is the same as database one */
		if ($this->logged == 1 && $this->id && Validate::isUnsignedId($this->id) && Customer::checkPassword($this->id, $this->passwd))
			return true;
		return false;
	}

	/**
	 * Logout
	 *
	 * @since 1.5.0
	 */
	public function logout()
	{
		if (isset(Context::getContext()->cookie))
			Context::getContext()->cookie->logout();
		$this->logged = 0;
	}

	/**
	 * Soft logout, delete everything links to the customer
	 * but leave there affiliate's informations
	 *
	 * @since 1.5.0
	 */
	public function mylogout()
	{
		if (isset(Context::getContext()->cookie))
			Context::getContext()->cookie->mylogout();
		$this->logged = 0;
	}

	public function getLastCart($with_order = true)
	{
		$carts = Cart::getCustomerCarts((int)$this->id, $with_order);
		if (!count($carts))
			return false;
		$cart = array_shift($carts);
		$cart = new Cart((int)$cart['id_cart']);
		return ($cart->nbProducts() === 0 ? (int)$cart->id : false);
	}

	public function getOutstanding()
	{
		$query = new DbQuery();
		$query->select('SUM(oi.total_paid_tax_incl)');
		$query->from('order_invoice', 'oi');
		$query->leftJoin('orders', 'o', 'oi.id_order = o.id_order');
		$query->groupBy('o.id_customer');
		$query->where('o.id_customer = '.(int)$this->id);
		$total_paid = (float)Db::getInstance()->getValue($query->build());

		$query = new DbQuery();
		$query->select('SUM(op.amount)');
		$query->from('order_payment', 'op');
		$query->leftJoin('order_invoice_payment', 'oip', 'op.id_order_payment = oip.id_order_payment');
		$query->leftJoin('orders', 'o', 'oip.id_order = o.id_order');
		$query->groupBy('o.id_customer');
		$query->where('o.id_customer = '.(int)$this->id);
		$total_rest = (float)Db::getInstance()->getValue($query->build());

		return $total_paid - $total_rest;
	}

	public function getWsGroups()
	{
		return Db::getInstance()->executeS('
			SELECT cg.`id_group` as id
			FROM '._DB_PREFIX_.'customer_group cg
			'.Shop::addSqlAssociation('group', 'cg').'
			WHERE cg.`id_customer` = '.(int)$this->id
		);
	}

	public function setWsGroups($result)
	{
		$groups = array();
		foreach ($result as $row)
			$groups[] = $row['id'];
		$this->cleanGroups();
		$this->addGroups($groups);
		return true;
	}

	/**
	 * @see ObjectModel::getWebserviceObjectList()
	 */
	public function getWebserviceObjectList($sql_join, $sql_filter, $sql_sort, $sql_limit)
	{
		$sql_filter .= Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'main');
		return parent::getWebserviceObjectList($sql_join, $sql_filter, $sql_sort, $sql_limit);
	}

	/**
	 * Función para obtener los usuarios que estan ligados a un cliente
	 */
	public function getUserByEmail($email){
		$sql = 'SELECT user
				FROM '._DB_PREFIX_.'customer_roku
				WHERE email = "'.$email_customer.'"';
		$resultado = Db::getInstance()->executeS($sql);	
		
		return $resultado;		
	}
}
