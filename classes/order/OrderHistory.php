<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrderHistoryCore extends ObjectModel
{
	/** @var integer Order id */
	public $id_order;

	/** @var integer Order status id */
	public $id_order_state;

	/** @var integer Employee id for this history entry */
	public $id_employee;

	/** @var string Object creation date */
	public $date_add;

	/** @var string Object last modification date */
	public $date_upd;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'order_history',
		'primary' => 'id_order_history',
		'fields' => array(
			'id_order' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_order_state' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_employee' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'date_add' => 		array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);

	/**
	 * @see  ObjectModel::$webserviceParameters
	 */
	protected $webserviceParameters = array(
		'objectsNodeName' => 'order_histories',
		'fields' => array(
			'id_order_state' => array('required' => true, 'xlink_resource'=> 'order_states'),
			'id_order' => array('xlink_resource' => 'orders'),
		),
		'objectMethods' => array(
			'add' => 'addWs',
		), 
	);

	/**
	 * Sets the new state of the given order
	 *
	 * @param int $new_order_state
	 * @param int/object $id_order
	 * @param bool $use_existing_payment
	 */
	public function changeIdOrderState($new_order_state, $id_order, $use_existing_payment = false)
	{
		if (!$new_order_state || !$id_order)
			return;

		if (!is_object($id_order) && is_numeric($id_order))
			$order = new Order((int)$id_order);
		elseif (is_object($id_order))
			$order = $id_order;
		else
			return;

		ShopUrl::cacheMainDomainForShop($order->id_shop);

		$new_os = new OrderState((int)$new_order_state, $order->id_lang);
		$old_os = $order->getCurrentOrderState();
		$is_validated = $this->isValidated();
		

		// executes hook
		if (in_array($new_os->id, array(Configuration::get('PS_OS_PAYMENT'), Configuration::get('PS_OS_WS_PAYMENT'))))
			Hook::exec('actionPaymentConfirmation', array('id_order' => (int)$order->id), null, false, true, false, $order->id_shop);

		// executes hook
		Hook::exec('actionOrderStatusUpdate', array('newOrderStatus' => $new_os, 'id_order' => (int)$order->id), null, false, true, false, $order->id_shop);

		if (Validate::isLoadedObject($order) && ($new_os instanceof OrderState))
		{
			// An email is sent the first time a virtual item is validated
			$virtual_products = $order->getVirtualProducts();
			if ($virtual_products && (!$old_os || !$old_os->logable) && $new_os && $new_os->logable)
			{
				$context = Context::getContext();
				$assign = array();
				foreach ($virtual_products as $key => $virtual_product)
				{
					$id_product_download = ProductDownload::getIdFromIdProduct($virtual_product['product_id']);
					$product_download = new ProductDownload($id_product_download);
					// If this virtual item has an associated file, we'll provide the link to download the file in the email
					if ($product_download->display_filename != '')
					{
						$assign[$key]['name'] = $product_download->display_filename;
						$dl_link = $product_download->getTextLink(false, $virtual_product['download_hash'])
							.'&id_order='.(int)$order->id
							.'&secure_key='.$order->secure_key;
						$assign[$key]['link'] = $dl_link;
						if (isset($virtual_product['download_deadline']) && $virtual_product['download_deadline'] != '0000-00-00 00:00:00')
							$assign[$key]['deadline'] = Tools::displayDate($virtual_product['download_deadline']);
						if ($product_download->nb_downloadable != 0)
							$assign[$key]['downloadable'] = (int)$product_download->nb_downloadable;
					}
				}

				$customer = new Customer((int)$order->id_customer);
				
				$links = '<ul>';
				foreach ($assign as $product)
				{
					$links .= '<li>';
					$links .= '<a href="'.$product['link'].'">'.Tools::htmlentitiesUTF8($product['name']).'</a>';
					if (isset($product['deadline']))
						$links .= '&nbsp;'.Tools::htmlentitiesUTF8(Tools::displayError('expires on', false)).'&nbsp;'.$product['deadline'];
					if (isset($product['downloadable']))
						$links .= '&nbsp;'.Tools::htmlentitiesUTF8(sprintf(Tools::displayError('downloadable %d time(s)', false), (int)$product['downloadable']));	
					$links .= '</li>';
				}
				$links .= '</ul>';
				$data = array(
					'{lastname}' => $customer->lastname,
					'{firstname}' => $customer->firstname,
					'{id_order}' => (int)$order->id,
					'{order_name}' => $order->getUniqReference(),
					'{nbProducts}' => count($virtual_products),
					'{virtualProducts}' => $links
				);
				// If there is at least one downloadable file
				if (!empty($assign))
					Mail::Send((int)$order->id_lang, 'download_product', Mail::l('The virtual product that you bought is available for download', $order->id_lang), $data, $customer->email, $customer->firstname.' '.$customer->lastname,
						null, null, null, null, _PS_MAIL_DIR_, false, (int)$order->id_shop);
			}

			// @since 1.5.0 : gets the stock manager
			$manager = null;
			if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
				$manager = StockManagerFactory::getManager();
				
			$errorOrCanceledStatuses = array(Configuration::get('PS_OS_ERROR'), Configuration::get('PS_OS_CANCELED'));
			
			// foreach products of the order
			if (Validate::isLoadedObject($old_os))			
				foreach ($order->getProductsDetail() as $product)
				{
					// if becoming logable => adds sale
					if ($new_os->logable && !$old_os->logable)
					{
						ProductSale::addProductSale($product['product_id'], $product['product_quantity']);
						// @since 1.5.0 - Stock Management
						if (!Pack::isPack($product['product_id']) &&
							in_array($old_os->id, $errorOrCanceledStatuses) &&
							!StockAvailable::dependsOnStock($product['id_product'], (int)$order->id_shop))
							StockAvailable::updateQuantity($product['product_id'], $product['product_attribute_id'], -(int)$product['product_quantity'], $order->id_shop);
					}
					// if becoming unlogable => removes sale
					elseif (!$new_os->logable && $old_os->logable)
					{
						ProductSale::removeProductSale($product['product_id'], $product['product_quantity']);
	
						// @since 1.5.0 - Stock Management
						if (!Pack::isPack($product['product_id']) &&
							in_array($new_os->id, $errorOrCanceledStatuses) &&
							!StockAvailable::dependsOnStock($product['id_product']))
							StockAvailable::updateQuantity($product['product_id'], $product['product_attribute_id'], (int)$product['product_quantity'], $order->id_shop);
					}
					// if waiting for payment => payment error/canceled
					elseif (!$new_os->logable && !$old_os->logable &&
							 in_array($new_os->id, $errorOrCanceledStatuses) &&
							 !in_array($old_os->id, $errorOrCanceledStatuses) &&
							 !StockAvailable::dependsOnStock($product['id_product']))
							 StockAvailable::updateQuantity($product['product_id'], $product['product_attribute_id'], (int)$product['product_quantity'], $order->id_shop);
					// @since 1.5.0 : if the order is being shipped and this products uses the advanced stock management :
					// decrements the physical stock using $id_warehouse
					if ($new_os->shipped == 1 && $old_os->shipped == 0 &&
						Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') &&
						Warehouse::exists($product['id_warehouse']) &&
						$manager != null &&
						((int)$product['advanced_stock_management'] == 1 || Pack::usesAdvancedStockManagement($product['product_id'])))
					{
						// gets the warehouse
						$warehouse = new Warehouse($product['id_warehouse']);
	
						// decrements the stock (if it's a pack, the StockManager does what is needed)
						$manager->removeProduct(
							$product['product_id'],
							$product['product_attribute_id'],
							$warehouse,
							$product['product_quantity'],
							Configuration::get('PS_STOCK_CUSTOMER_ORDER_REASON'),
							true,
							(int)$order->id
						);
					}
					// @since.1.5.0 : if the order was shipped, and is not anymore, we need to restock products
					elseif ($new_os->shipped == 0 && $old_os->shipped == 1 &&
							 Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') &&
							 Warehouse::exists($product['id_warehouse']) &&
							 $manager != null &&
							 ((int)$product['advanced_stock_management'] == 1 || Pack::usesAdvancedStockManagement($product['product_id'])))
					{
						// if the product is a pack, we restock every products in the pack using the last negative stock mvts
						if (Pack::isPack($product['product_id']))
						{
							$pack_products = Pack::getItems($product['product_id'], Configuration::get('PS_LANG_DEFAULT', null, null, $order->id_shop));
							foreach ($pack_products as $pack_product)
							{
								if ($pack_product->advanced_stock_management == 1)
								{
									$mvts = StockMvt::getNegativeStockMvts($order->id, $pack_product->id, 0, $pack_product->pack_quantity * $product['product_quantity']);
									foreach ($mvts as $mvt)
									{
										$manager->addProduct(
											$pack_product->id,
											0,
											new Warehouse($mvt['id_warehouse']),
											$mvt['physical_quantity'],
											null,
											$mvt['price_te'],
											true
										);
									}
									if (!StockAvailable::dependsOnStock($product['id_product']))
										StockAvailable::updateQuantity($pack_product->id, 0, (int)$pack_product->pack_quantity * $product['product_quantity'], $order->id_shop);
								}
							}
						}
						// else, it's not a pack, re-stock using the last negative stock mvts
						else
						{
							$mvts = StockMvt::getNegativeStockMvts($order->id, $product['product_id'], $product['product_attribute_id'], $product['product_quantity']);
							foreach ($mvts as $mvt)
							{
								$manager->addProduct(
									$product['product_id'],
									$product['product_attribute_id'],
									new Warehouse($mvt['id_warehouse']),
									$mvt['physical_quantity'],
									null,
									$mvt['price_te'],
									true
								);
							}
						}
					}
				}
		}

		$this->id_order_state = (int)$new_order_state;
		
		// changes invoice number of order ?
		if (!Validate::isLoadedObject($new_os) || !Validate::isLoadedObject($order))
			die(Tools::displayError('Invalid new order status'));

		// the order is valid if and only if the invoice is available and the order is not cancelled
		$order->current_state = $this->id_order_state;
		$order->valid = $new_os->logable;
		$order->update();

		if ($new_os->invoice && !$order->invoice_number)
			$order->setInvoice($use_existing_payment);
		elseif ($new_os->delivery && !$order->delivery_number)
			$order->setDeliverySlip();

		// set orders as paid
		if ($new_os->paid == 1)
		{
			$invoices = $order->getInvoicesCollection();
			if ($order->total_paid != 0)
				$payment_method = Module::getInstanceByName($order->module);

			foreach ($invoices as $invoice)
			{
				$rest_paid = $invoice->getRestPaid();
				if ($rest_paid > 0)
				{
					$payment = new OrderPayment();
					$payment->order_reference = $order->reference;
					$payment->id_currency = $order->id_currency;
					$payment->amount = $rest_paid;

					if ($order->total_paid != 0)
						$payment->payment_method = $payment_method->displayName;
					else 
						$payment->payment_method = null;
					
					// Update total_paid_real value for backward compatibility reasons
					if ($payment->id_currency == $order->id_currency)
						$order->total_paid_real += $payment->amount;
					else
						$order->total_paid_real += Tools::ps_round(Tools::convertPrice($payment->amount, $payment->id_currency, false), 2);
					$order->save();
						
					$payment->conversion_rate = 1;
					$payment->save();
					Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'order_invoice_payment` (`id_order_invoice`, `id_order_payment`, `id_order`)
					VALUES('.(int)$invoice->id.', '.(int)$payment->id.', '.(int)$order->id.')');
				}
			}
		}

		// updates delivery date even if it was already set by another state change
		if ($new_os->delivery)
			$order->setDelivery();

		// executes hook
		Hook::exec('actionOrderStatusPostUpdate', array('newOrderStatus' => $new_os,'id_order' => (int)$order->id,), null, false, true, false, $order->id_shop);

		ShopUrl::resetMainDomainCache();
	}

	/**
	 * Returns the last order status
	 * @param int $id_order
	 * @return OrderState|bool
	 * @deprecated 1.5.0.4
	 * @see Order->current_state
	 */
	public static function getLastOrderState($id_order)
	{
		Tools::displayAsDeprecated();
		$id_order_state = Db::getInstance()->getValue('
		SELECT `id_order_state`
		FROM `'._DB_PREFIX_.'order_history`
		WHERE `id_order` = '.(int)$id_order.'
		ORDER BY `date_add` DESC, `id_order_history` DESC');

		// returns false if there is no state
		if (!$id_order_state)
			return false;

		// else, returns an OrderState object
		return new OrderState($id_order_state, Configuration::get('PS_LANG_DEFAULT'));
	}

	/**
	 * @param bool $autodate Optional
	 * @param array $template_vars Optional
	 * @param Context $context Optional
	 * @return bool
	 */
	public function addWithemail($autodate = true, $template_vars = false, Context $context = null)
	{
		if (!$context)
			$context = Context::getContext();
		$order = new Order($this->id_order);
		
		if (!$this->add($autodate))
			return false;

		$result = Db::getInstance()->getRow('
			SELECT osl.`template`, c.`lastname`, c.`firstname`, osl.`name` AS osname, c.`email`, os.`module_name`, os.`id_order_state`
			FROM `'._DB_PREFIX_.'order_history` oh
				LEFT JOIN `'._DB_PREFIX_.'orders` o ON oh.`id_order` = o.`id_order`
				LEFT JOIN `'._DB_PREFIX_.'customer` c ON o.`id_customer` = c.`id_customer`
				LEFT JOIN `'._DB_PREFIX_.'order_state` os ON oh.`id_order_state` = os.`id_order_state`
				LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = o.`id_lang`)
			WHERE oh.`id_order_history` = '.(int)$this->id.' AND os.`send_email` = 1');
		if (isset($result['template']) && Validate::isEmail($result['email']))
		{
			ShopUrl::cacheMainDomainForShop($order->id_shop);
			
			$topic = $result['osname'];
			
			//
			//Insertamos en la base de datos
			$orden = $this->id_order;
			//echo $orden;
				
			if(($topic=="OXXO: Pago Confirmado") || ($topic=="SPEI: Confirmado") || ($topic=="REC: En Pago Recurrete") || ($topic=="TDC: Pagado") || ($topic=="TDC: Challenge Aceptado"))
			{
				//Obtengo datos del cliente
				$sql_customer = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'orders.id_customer, '._DB_PREFIX_.'orders.id_cart
					FROM '._DB_PREFIX_.'orders 
					INNER JOIN '._DB_PREFIX_.'customer ON ('._DB_PREFIX_.'orders.id_customer = '._DB_PREFIX_.'customer.id_customer)
					WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'"');
							
				$id_customer = (!empty($sql_customer) ? $sql_customer[0]['id_customer'] : '');							
				//Realizamos consulta para saber si la compra es por regalo
				$data_regalo = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'customer_regalo WHERE id_customer_regala = "'.$id_customer.'" AND bregalo = 1');
				
				$bregalo = (!empty($data_regalo) ? true : false);
				
				if($bregalo){
					$id_cliente_recibe = (!empty($data_regalo) ? $data_regalo[0]['id_customer_recibe'] : 'NULL');
					//Obengo el email del cliente
					$sql_email_recibe = Db::getInstance()->executeS('SELECT email FROM '._DB_PREFIX_.'customer WHERE id_customer = "'.$id_cliente_recibe.'"');
					$email_customer_recibe = (!empty($sql_email_recibe) ? $sql_email_recibe[0]['email'] : '');
					//Carro de la compra 
					$id_cart_recibe = (!empty($sql_customer) ? $sql_customer[0]['id_cart'] : '');	
					//
						//Obtengo la cantidad de productos adquiridos
						$result_quantity = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'cart_product.quantity FROM '._DB_PREFIX_.'orders 
							INNER JOIN '._DB_PREFIX_.'cart_product ON ('._DB_PREFIX_.'orders.id_cart = '._DB_PREFIX_.'cart_product.id_cart)
							WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'"');
							
						//Cuento los email para saber cuantos usuarios hay ya registrados
						$count_ps_customer = Db::getInstance()->executeS('SELECT count(email) AS num_user FROM '._DB_PREFIX_.'customer_roku WHERE id_customer = "'.$id_cliente_recibe.'"');
						$count_customer = (!empty($count_ps_customer) ? $count_ps_customer[0]['num_user'] : null);
							
						if(!empty($result_quantity))
						{
							$quantity = 0;
							foreach ($result_quantity as $value){
								//numero de veces a ejecutar el script del insert en la tabla de ps_customer_roku
								$quantity += $value['quantity']; 						
							}

							//El incremental va a depender de si ya hay usuarios o no registrados
							$incremental = (!empty($count_customer) ? $count_customer + 1 : 1);
									
							//Ciclo para realizar el insert las n veces de articulos que hayan comprado
							for ($i = 1; $i <= $quantity; $i++) {
								//Se arma el nombre de usuario en base a la clave(utv_), id del cliente(id_customer) y el autoincremental.
								$user_compuesto = 'utv_' . $id_cliente_recibe . '_' . $incremental++;						
								//Se obtiene la fecha para proximo pago
								$fecha = date('m/d/Y');
								$new_fecha = date("Y-m-d", strtotime("$fecha +30 days"));
								//Se arma el insert
								$sql = 'INSERT INTO '._DB_PREFIX_.'customer_roku 
									(email, id_customer, user, clave_roku, fecha_pago, tipo_pago, id_orden, fecha_banwire) 
										VALUES (
											"'.$email_customer_recibe.'", 
											"'.$id_cliente_recibe.'", 
											"'.$user_compuesto.'", 
											"utv", 
											"'.$new_fecha.'",
											"'.$topic.'",
											"'.$orden.'",
											"'.date('Y-m-d').'")';
								Db::getInstance()->execute($sql);
							}
						}	
						
						Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_regalo
							SET id_cart = "'.$id_cart_recibe.'"
						    WHERE id_customer_recibe = "'.$id_cliente_recibe.'"');
							
						$this->sendMail($id_cliente_recibe, $id_customer);
						
						//Obtengo la dirección de entrega
						$address_delivery = Db::getInstance()->executeS('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = "'.$id_cliente_recibe.'"');
						$id_address_delivery = (!empty($address_delivery) ? $address_delivery[0]['id_address'] : NULL);
						
						//Actualizo en el pedido la dirección de entrega
						Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders
							SET id_address_delivery = "'.$id_address_delivery.'"
						    WHERE id_order = "'.$orden.'"');
						
					//
					
				}
				else{
					//inicio codigo si no es regalo	
					//Primero realizo consulta para saber si son rokus o son cuota cliente existente
					$data_productos = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'cart_product.quantity FROM '._DB_PREFIX_.'orders 
						INNER JOIN '._DB_PREFIX_.'cart_product ON ('._DB_PREFIX_.'orders.id_cart = '._DB_PREFIX_.'cart_product.id_cart)
						WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'" AND '._DB_PREFIX_.'cart_product.id_product <> 4');
							
					$bregistrar = (!empty($data_productos) ? true : false);
					if($bregistrar)
					{
						//Obtengo datos del cliente
						$data_customer = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'orders.id_customer, '._DB_PREFIX_.'customer.email
							FROM '._DB_PREFIX_.'orders 
							INNER JOIN '._DB_PREFIX_.'customer ON ('._DB_PREFIX_.'orders.id_customer = '._DB_PREFIX_.'customer.id_customer)
							WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'"');
								
						$email_customer = (!empty($data_customer) ? $data_customer[0]['email'] : '');
						$id_customer = (!empty($data_customer) ? $data_customer[0]['id_customer'] : '');
							
						//Obtengo la cantidad de productos adquiridos
						$result_quantity = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'cart_product.quantity FROM '._DB_PREFIX_.'orders 
							INNER JOIN '._DB_PREFIX_.'cart_product ON ('._DB_PREFIX_.'orders.id_cart = '._DB_PREFIX_.'cart_product.id_cart)
							WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'"');
							
						//Cuento los email para saber cuantos usuarios hay ya registrados
						$count_ps_customer = Db::getInstance()->executeS('SELECT count(email) AS num_user FROM ps_customer_roku WHERE email = "'.$email_customer.'"');
						$count_customer = (!empty($count_ps_customer) ? $count_ps_customer[0]['num_user'] : null);
							
						if(!empty($result_quantity))
						{
							$quantity = 0;
							foreach ($result_quantity as $value){
								//numero de veces a ejecutar el script del insert en la tabla de ps_customer_roku
								$quantity += $value['quantity']; 						
							}

							//El incremental va a depender de si ya hay usuarios o no registrados
							$incremental = (!empty($count_customer) ? $count_customer + 1 : 1);
									
							//Ciclo para realizar el insert las n veces de articulos que hayan comprado
							for ($i = 1; $i <= $quantity; $i++) {
								//Se arma el nombre de usuario en base a la clave(utv_), id del cliente(id_customer) y el autoincremental.
								$user_compuesto = 'utv_' . $id_customer . '_' . $incremental++;						
								//Se obtiene la fecha para proximo pago
								$fecha = date('m/d/Y');
								$new_fecha = date("Y-m-d", strtotime("$fecha +30 days"));
								//Se arma el insert
								$sql = 'INSERT INTO '._DB_PREFIX_.'customer_roku 
									(email, id_customer, user, clave_roku, fecha_pago, tipo_pago, id_orden, fecha_banwire) 
										VALUES (
											"'.$email_customer.'", 
											"'.$id_customer.'", 
											"'.$user_compuesto.'", 
											"utv", 
											"'.$new_fecha.'",
											"'.$topic.'",
											"'.$orden.'",
											"'.date('Y-m-d').'")';
								Db::getInstance()->execute($sql);
							}
						}
					}else
					{
						//Realizo consulta para obtener los usuarios que se van a pagar
						$result_usuarios = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'shopping_cart.*, ps_customer_roku.fecha_pago
							FROM '._DB_PREFIX_.'orders
							INNER JOIN '._DB_PREFIX_.'shopping_cart ON ('._DB_PREFIX_.'shopping_cart.id_cart = '._DB_PREFIX_.'orders.id_cart)
							INNER JOIN '._DB_PREFIX_.'customer_roku ON ('._DB_PREFIX_.'customer_roku.user = '._DB_PREFIX_.'shopping_cart.usuario)
							WHERE '._DB_PREFIX_.'orders.id_order = "'.$orden.'"');
							
						//echo "<pre><small>". print_r($result_usuarios, true) ."</small></pre>"; exit;
						
						if(($topic=="SPEI: Confirmado") || ($topic=="REC: En Pago Recurrete")){
							if(!empty($result_usuarios)){
								foreach($result_usuarios as $value){
									$usuario = $value['usuario'];
									$cantidad = $value['cantidad'];
									$fecha_pago = $value['fecha_pago'];
									$days = $cantidad * 30;
									$new_fecha_pago = date("Y-m-d", strtotime("$fecha_pago + $days days"));
									//Con el usuario y la cantidad recogidas actualizo la fecha de pago
									Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_roku
										SET fecha_pago = "'.$new_fecha_pago.'", tipo_pago = "'.$topic.'", recurrente_aplicado = 1, brecurrente = 0
										WHERE user = "'.$usuario.'"');
								}
							}
						}else{
							if(!empty($result_usuarios)){
								foreach($result_usuarios as $value){
									$usuario = $value['usuario'];
									$cantidad = $value['cantidad'];
									$fecha_pago = $value['fecha_pago'];
									$days = $cantidad * 30;
									$new_fecha_pago = date("Y-m-d", strtotime("$fecha_pago + $days days"));
									//Con el usuario y la cantidad recogidas actualizo la fecha de pago
									Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_roku
										SET fecha_pago = "'.$new_fecha_pago.'", tipo_pago = "'.$topic.'"
										WHERE user = "'.$usuario.'"');
								}
							}						
						}
					}
					//fin codigo no es regalo					
				}
			
			}
			
			//			
			
			$data = array(
				'{lastname}' => $result['lastname'],
				'{firstname}' => $result['firstname'],
				'{email}' => $result['email'],
				'{id_order}' => (int)$this->id_order,
				'{total_products}' => $order->total_products,
				'{total_discounts}' => $order->total_discounts,
				'{total_wrapping}' => $order->total_wrapping,
				'{total_shipping}' => $order->total_shipping,
				'{total_tax_paid}' => $order->total_tax_paid,
				'{order_name}' => $order->getUniqReference()
			);
			if ($template_vars)
				$data = array_merge($data, $template_vars);

			if ($result['module_name'])
			{
				$module = Module::getInstanceByName($result['module_name']);
				if (Validate::isLoadedObject($module) && isset($module->extra_mail_vars) && is_array($module->extra_mail_vars))
					$data = array_merge($data, $module->extra_mail_vars);
			}
			//echo print_r($module,true);
			
			$data['{total_paid}'] = Tools::displayPrice((float)$order->total_paid, new Currency((int)$order->id_currency), false);
			$data['{order_name}'] = $order->getUniqReference();

			if (Validate::isLoadedObject($order))
			{
				// Join PDF invoice if order status is "payment accepted"
				if ((int)$result['id_order_state'] === 2 && (int)Configuration::get('PS_INVOICE') && $order->invoice_number)
				{
					$context = Context::getContext();
					$pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $context->smarty);
					$file_attachement['content'] = $pdf->render(false);
					$file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang, null, $order->id_shop).sprintf('%06d', $order->invoice_number).'.pdf';
					$file_attachement['mime'] = 'application/pdf';
				}
				else
					$file_attachement = null;
				
				
				$result2 = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = "'.(int)$this->id_order.'"');
				//echo "<pre><small>".print_r($result2,true)."</small></pre>"; exit;
				$products="";
				
				foreach($result2 as $key => $value){
					$products.='	
						<tr>
							<td style="border:1px solid #D6D4D4;" width="17%">
								<table class="table">
									<tr>
										<td width="10">&nbsp;</td>
										<td>
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												&nbsp;
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td style="border:1px solid #D6D4D4;" colspan="3">
								<table class="table">
									<tr>
										<td width="10">&nbsp;</td>
										<td>
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												<strong>'.$result2[$key]["product_name"].'</strong>
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							
							<td style="border:1px solid #D6D4D4;" width="17%">
								<table class="table">
									<tr>
										<td width="10">&nbsp;</td>
										<td align="right">
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												'.$result2[$key]["product_quantity"].'
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							
						</tr>
					';
				}
				
				$data['{products}'] = $products;
				
				$result_usuarios = Db::getInstance()->executeS('SELECT user, DATE_FORMAT(fecha_creacion, "%d/%m/%Y") as fecha_creacion, clave_roku 
					FROM '._DB_PREFIX_.'customer_roku WHERE id_orden = "'.$this->id_order.'"');
					
				if(empty($result_usuarios)){
					$result_usuarios = Db::getInstance()->executeS('SELECT user, DATE_FORMAT(fecha_creacion, "%d/%m/%Y") as fecha_creacion, clave_roku 
					FROM '._DB_PREFIX_.'customer_roku WHERE email = "'.$result['email'].'"');
				}
					
				$usuarios = "";
				foreach($result_usuarios as $usuario){				
					$usuarios.='	
						<tr>
							<td style="border:1px solid #D6D4D4;" width="17%">
								<table class="table">
									<tr>
										<td>
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												'.$usuario["user"].'
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td style="border:1px solid #D6D4D4;" colspan="3">
								<table class="table">
									<tr>
										<td>
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												'.$usuario["fecha_creacion"].'
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							
							<td style="border:1px solid #D6D4D4;" width="17%">
								<table class="table">
									<tr>
										<td align="right">
											<font size="2" face="Open-sans, sans-serif" color="#555454">
												'.$usuario["clave_roku"].'
											</font>
										</td>
										<td width="10">&nbsp;</td>
									</tr>
								</table>
							</td>
							
						</tr>
					';					
				}
				
				$data['{usuarios}'] = $usuarios;
				
				//Obtengo datos del cliente
				$carro_customer = Db::getInstance()->executeS('SELECT '._DB_PREFIX_.'orders.id_cart
					FROM '._DB_PREFIX_.'orders 
					INNER JOIN '._DB_PREFIX_.'customer ON ('._DB_PREFIX_.'orders.id_customer = '._DB_PREFIX_.'customer.id_customer)
					WHERE '._DB_PREFIX_.'orders.id_order = "'.$this->id_order.'"');			
					
				$id_carro_recibe = (!empty($carro_customer) ? $carro_customer[0]['id_cart'] : '');	
				
				$sql_bandera = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'customer_regalo WHERE id_cart = "'.$id_carro_recibe.'"');
				$bandera_bregalo = (!empty($sql_bandera) ? $sql_bandera[0]['bregalo'] : 'NULL');
				$msn_mail = (($bandera_bregalo == 1) ? 'Los usuarios que compro para regalo son:' : 'Los usuarios con los que cuenta son:');					
				
				$data['{msn}'] = $msn_mail;
				
				Mail::Send((int)$order->id_lang, $result['template'], $topic, $data, $result['email'], $result['firstname'].' '.$result['lastname'],
					null, null, $file_attachement, null, _PS_MAIL_DIR_, false, (int)$order->id_shop);
			}

			ShopUrl::resetMainDomainCache();
		}

		return true;
	}

	public function add($autodate = true, $null_values = false)
	{
		if (!parent::add($autodate))
			return false;

		$order = new Order((int)$this->id_order);
		// Update id_order_state attribute in Order
		$order->current_state = $this->id_order_state;
		$order->update();

		Hook::exec('actionOrderHistoryAddAfter', array('order_history' => $this), null, false, true, false, $order->id_shop);

		return true;
	}

	/**
	 * @return int
	 */
	public function isValidated()
	{
		return Db::getInstance()->getValue('
		SELECT COUNT(oh.`id_order_history`) AS nb
		FROM `'._DB_PREFIX_.'order_state` os
		LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (os.`id_order_state` = oh.`id_order_state`)
		WHERE oh.`id_order` = '.(int)$this->id_order.'
		AND os.`logable` = 1');
	}

    /**
     * Add method for webservice create resource Order History      
     * If sendemail=1 GET parameter is present sends email to customer otherwise does not
     * @return bool
     */
	public function addWs()
	{
	    $sendemail = (bool)Tools::getValue('sendemail', false);
	    $this->changeIdOrderState($this->id_order_state, $this->id_order);
	    
	    if ($sendemail)
	    {
	        //Mail::Send requires link object on context and is not set when getting here
	        $context = Context::getContext();
	        if ($context->link == null)
	        {
	            $protocol_link = (Tools::usingSecureMode() && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
	            $protocol_content = (Tools::usingSecureMode() && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
	            $context->link = new Link($protocol_link, $protocol_content);
	        }
	        return $this->addWithemail();            
	    }
		else
	        return $this->add();
	}
	
	public function sendMail($idcustomer_recibe, $idcustomer_regala){
		
		//require_once('C:/xampp/htdocs/roku/classes/PHPMailer/class.phpmailer.php');
		require_once('/home/canalroku/public_html/classes/PHPMailer/class.phpmailer.php');
		
		//Obtenemos los datos del cliente que recibe el regalo
		$data_customer_recibe = Db::getInstance()->executeS('SELECT gender.name, CONCAT(customer.firstname, " ", customer.lastname) AS nombreCompleto, customer.email, 
			CONCAT(LOWER(customer.firstname), "2015") as contrasena
			FROM '._DB_PREFIX_.'customer customer
			INNER JOIN '._DB_PREFIX_.'gender_lang gender ON (customer.id_gender = gender.id_gender) AND gender.id_lang = 1
			WHERE customer.id_customer = "'.$idcustomer_recibe.'"');
		
		if(!empty($data_customer_recibe)){
			$gender_recibe = $data_customer_recibe[0]['name'];
			$nombreCompleto_recibe = $data_customer_recibe[0]['nombreCompleto'];
			$email_recibe = $data_customer_recibe[0]['email'];
			$contrasena_recibe = $data_customer_recibe[0]['contrasena'];
		}
		
		$address_recibe = Db::getInstance()->executeS('SELECT CONCAT(address.address1, " ", address.address2) AS direccion, CONCAT(address.postcode, " ", address.city) AS direccion2, state.name 
			FROM '._DB_PREFIX_.'address address 
			INNER JOIN '._DB_PREFIX_.'state state ON (address.id_state = state.id_state)
			WHERE id_customer = "'.$idcustomer_recibe.'"');
			
		if(!empty($address_recibe)){
			$direccion1 = $address_recibe[0]['direccion'];
			$direccion2 = $address_recibe[0]['direccion2'];
			$state = $address_recibe[0]['name'];
		}
		
		//Inicio recuperación de datos para llenar el mail
		//Se obtienen los datos del cliente que realiza el pedido
		$data_customer = Db::getInstance()->executeS('SELECT firstname, lastname, email FROM '._DB_PREFIX_.'customer WHERE id_customer = "'.$idcustomer_regala.'"');
		if(!empty($data_customer)){
			$nombre_completo_regala = $data_customer[0]['firstname'] . " " . $data_customer[0]['lastname'];
			$email_regala = $data_customer[0]['email'];
		}

		//Incio armado del mail
		$mail = new PHPMailer;
		 
		/** Configurar SMTP **/
		$mail->isSMTP();                                    // Indicamos que use SMTP
		$mail->Host = 'smtp.gmail.com';  					// Indicamos los servidores SMTP
		$mail->SMTPAuth = true;                             // Habilitamos la autenticación SMTP
		$mail->Username = 'info@adimpactomexico.com';       // SMTP username
		$mail->Password = 'info2015';                       // SMTP password
		$mail->SMTPSecure = 'ssl';                          // Habilitar encriptación TLS o SSL
		$mail->Port = 465;                                  // TCP port
		 
		/** Configurar cabeceras del mensaje **/
		$mail->From = 'info@adimpactomexico.com';     // Correo del remitente
		$mail->FromName = 'Under TV';           	  // Nombre del remitente
		$mail->Subject = 'Regalo roku';      // Asunto
		 
		/** Incluir destinatarios. El nombre es opcional **/
		$mail->addAddress($email_recibe);		 
		
 
		/** Enviarlo en formato HTML **/
		$mail->isHTML(true);                                  
		$mail->CharSet = 'UTF-8';		 
		/** Configurar cuerpo del mensaje **/
		$mail->Body = '<table class="table table-mail" style="width: 100%; margin-top: 10px; -moz-box-shadow: 0 0 5px #afafaf; -webkit-box-shadow: 0 0 5px #afafaf; -o-box-shadow: 0 0 5px #afafaf; box-shadow: 0 0 5px #afafaf; filter: progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5);">
		<tbody>
		<tr>
		<td class="space" style="width: 20px; padding: 7px 0;">&nbsp;</td>
		<td style="padding: 7px 0;" align="center">
		<table class="table" style="width: 100%;" bgcolor="#ffffff">
		<tbody>
		<tr>
		<td class="logo" style="border-bottom: 4px solid #333333; padding: 7px 0;" align="center"></td>
		</tr>
		<tr>
		<td class="titleblock" style="padding: 7px 0;" align="center"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> <span class="title" style="font-weight: 500; font-size: 28px; text-transform: uppercase; line-height: 33px;">Enhorabuena!</span> </span></td>
		</tr>
		<tr>
		<td class="linkbelow" style="padding: 7px 0;"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> '. $gender_recibe .' '. $nombreCompleto_recibe .', bienvenido a Under TV usted cuenta con un regalo realizado por el cliente : '.$nombre_completo_regala.' ('.$email_regala.') </span></td>
		</tr>
		<tr>
		<td class="linkbelow" style="padding: 7px 0;"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> El cual sera enviado a la siguiente dirección que ha especificado el cliente.</span></td>
		</tr>
		<tr>
		<td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" colspan="3">
		<table class="table" style="width: 100%;">
		<tbody>
		<tr>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		<td style="padding: 7px 0;">
		<p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;" data-html-only="1">Dirección.</p>
		<span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> 
			'. $direccion1 .' <br />
			'. $direccion2 .' <br />
			'. $state .' 
		</span>
		</td>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>	
		<tr>
		<td class="linkbelow" style="padding: 7px 0;"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> Sus datos de acceso son los siguientes: </span></td>
		</tr>
		<tr>
		<td class="space_footer" style="padding: 0!important;">&nbsp;</td>
		</tr>
		<tr>
		<td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" colspan="3">
		<table class="table" style="width: 100%;">
		<tbody>
		<tr>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		<td style="padding: 7px 0;">
		<p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;" data-html-only="1">Detalles de la cuenta.</p>
		<span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> <span style="color: #777;"> <span style="color: #333;"> <strong>Correo electrónico	:</strong> </span> '.$email_recibe.' <br /><br /> <span style="color: #333;"><strong>Contraseña:</strong> </span>  '. $contrasena_recibe .'<br /><br /> </span> </span></td>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" colspan="3">
		<table class="table" style="width: 100%;">
		<tbody>
		<tr>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		<td style="padding: 7px 0;">
		<p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;" data-html-only="1">Mensaje:</p>
		<span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"><span style="color: #777;"> Para actualizar dicha dirección entrar a su cuenta Under Tv. Con el siguiente enlace <a style="color: #337ff1;" href="http://canalutv.mx/mi-cuenta"> canalutv.mx/mi-cuenta </a> en MIS DATOS PERSONALES -> MI DIRECCION </span> </span></td>
		<td style="padding: 7px 0;" width="10">&nbsp;</td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		<tr>
		<td class="space_footer" style="padding: 0!important;">&nbsp;</td>
		</tr>
		<tr>
		<td class="footer" style="border-top: 4px solid #333333; padding: 7px 0;">
		<p><a style="color: #337ff1;" href="canalutv.mx">Under TV</a></p>
		</td>
		</tr>
		</tbody>
		</table>
		</td>
		<td class="space" style="width: 20px; padding: 7px 0;">&nbsp;</td>
		</tr>
		</tbody>
		</table>
		<p> </p>';

		$mail->AltBody = '';
		 
		/** Para que use el lenguaje español **/
		$mail->setLanguage('es');
		$mail->send();
	}	
}
