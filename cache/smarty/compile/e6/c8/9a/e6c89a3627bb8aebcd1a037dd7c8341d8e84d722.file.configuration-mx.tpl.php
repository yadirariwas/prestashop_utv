<?php /* Smarty version Smarty-3.1.19, created on 2017-07-13 15:20:20
         compiled from "/home/canalutv/public_html/modules/banwire/views/templates/admin/configuration-mx.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19275518175967d6043037e0-93301795%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e6c89a3627bb8aebcd1a037dd7c8341d8e84d722' => 
    array (
      0 => '/home/canalutv/public_html/modules/banwire/views/templates/admin/configuration-mx.tpl',
      1 => 1448042679,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19275518175967d6043037e0-93301795',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banwire_ps_14' => 0,
    'banwire_js_files' => 0,
    'banwire_tracking' => 0,
    'module_dir' => 0,
    'banwire_merchant_country_is_usa' => 0,
    'banwire_validation' => 0,
    'validation' => 0,
    'banwire_error' => 0,
    'error' => 0,
    'banwire_warning' => 0,
    'warning' => 0,
    'banwire_form_link' => 0,
    'banwire_configuracion' => 0,
    'banwire_order_statuses' => 0,
    'key' => 0,
    'recurrencia' => 0,
    'banwire_rec_select' => 0,
    'banwire_rec_retry' => 0,
    'banwire_valores_prod_del_cd' => 0,
    'banwire_valores_ship_mthd_cd' => 0,
    'banwire_merchant_country_is_mx' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5967d6046233e3_01637993',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5967d6046233e3_01637993')) {function content_5967d6046233e3_01637993($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['banwire_ps_14']->value) {?>
<script type="text/javascript">
		
		$(document).ready(function() {
			var scripts = [<?php echo $_smarty_tpl->tpl_vars['banwire_js_files']->value;?>
];
			for(var i = 0; i < scripts.length; i++) {
				$.getScript(scripts[i], function() {banwire_init()});
			}
		});
		
</script>
<?php }?>
<div class="banwire-module-wrapper">
	<div class="banwire-module-header">
		<img src="<?php echo htmlentities($_smarty_tpl->tpl_vars['banwire_tracking']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="" style="display: none;" />
		<a rel="external" href="https://www.banwire.com/" target="_blank"><img class="banwire-logo" alt="" src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
/img/logo.png" /></a>
		<span class="banwire-module-intro"><?php echo smartyTranslate(array('s'=>'Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO, SPEI.','mod'=>'banwire'),$_smarty_tpl);?>
<br />
		<a class="banwire-module-create-btn L" rel="external" href="https://banwire.com/login/" target="_blank"><span><?php echo smartyTranslate(array('s'=>'Ir a Banwire','mod'=>'banwire'),$_smarty_tpl);?>
</span></a></span>
	</div>
	<div class="banwire-module-wrap">
		<div class="banwire-module-col2">
			<div class="banwire-module-col2inner">
				<h3><?php echo smartyTranslate(array('s'=>'Online & Mobile Payments.','mod'=>'banwire'),$_smarty_tpl);?>
</h3>
				<div style="line-height: 9px; width: 194px; float: left;">
				<img class="banwire-cc" alt="" src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
/img/tarjetas.png" style="float: left;" />
				</div>
				<div style="line-height: 9px; width: 255px; float: left;">
						<img class="banwire-cc" alt="" src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
/img/efectivo_oxxo.png" style="float: left;" />
				</div>
				<?php if ($_smarty_tpl->tpl_vars['banwire_merchant_country_is_usa']->value) {?>
					<div style="line-height: 9px; width: 194px; float: left;">
						<img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
/img/ebank.png" />
					</div>
				<?php }?>
			</div>
		</div>	
	</div>
	<?php if ($_smarty_tpl->tpl_vars['banwire_validation']->value) {?>
		<div class="conf">
			<?php  $_smarty_tpl->tpl_vars['validation'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['validation']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banwire_validation']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['validation']->key => $_smarty_tpl->tpl_vars['validation']->value) {
$_smarty_tpl->tpl_vars['validation']->_loop = true;
?>
				<?php echo htmlentities($_smarty_tpl->tpl_vars['validation']->value, ENT_QUOTES, 'UTF-8', true);?>
<br />
			<?php } ?>
		</div>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['banwire_error']->value) {?>
		<div class="error">
			<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banwire_error']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
				<?php echo htmlentities($_smarty_tpl->tpl_vars['error']->value, ENT_QUOTES, 'UTF-8', true);?>
<br />
			<?php } ?>
		</div>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['banwire_warning']->value) {?>
		<div class="info">
			<?php  $_smarty_tpl->tpl_vars['warning'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['warning']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banwire_warning']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['warning']->key => $_smarty_tpl->tpl_vars['warning']->value) {
$_smarty_tpl->tpl_vars['warning']->_loop = true;
?>
				<?php echo htmlentities($_smarty_tpl->tpl_vars['warning']->value, ENT_QUOTES, 'UTF-8', true);?>
<br />
			<?php } ?>
		</div>
	<?php }?>
	<form action="<?php echo htmlentities($_smarty_tpl->tpl_vars['banwire_form_link']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
		<fieldset>
			<legend><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
/img/banwire_shield.png" alt="" /><?php echo smartyTranslate(array('s'=>'Banwire API´s','mod'=>'banwire'),$_smarty_tpl);?>
</legend>
			<a href="https://banwire.com" class="banwire-module-btn right resetMargin" target="_blank"><?php echo smartyTranslate(array('s'=>'Conoce cada una de las API´s de Banwire','mod'=>'banwire'),$_smarty_tpl);?>
</a>
			<h4><?php echo smartyTranslate(array('s'=>'Activa/desactiva el API de Banwire necesaria','mod'=>'banwire'),$_smarty_tpl);?>
</h4>
			<div class="banwire-usa-threecol">
				<div class="banwire-usa-product first fixCol<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_TDC']) {?> banwire-usa-product-active<?php }?>">
					<h4><?php echo smartyTranslate(array('s'=>'Tarjeta de Crédito o Débito','mod'=>'banwire'),$_smarty_tpl);?>
</h4>
					<div class="box-content-payment" >
						<input type='checkbox' name='banwire_tdc_red' id='banwire_tdc_red' <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_TDC_RED']) {?> checked="checked"<?php }?> /> <label for="banwire_tdc_red"> <?php echo smartyTranslate(array('s'=>'Banwire Shield','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<input type='checkbox' name='banwire_tdc_pro' id='banwire_tdc_pro' <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_TDC_PRO']) {?> checked="checked"<?php }?> /> <label for="banwire_tdc_pro"> <?php echo smartyTranslate(array('s'=>'Banwire Pago PRO','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<label for="status_banwire_tdc"> <?php echo smartyTranslate(array('s'=>'Pago completado:','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<select name='status_banwire_tdc' id='status_banwire_tdc' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_TDC']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
						<?php } ?>
						</select>
						<label for="status_banwire_tdc_revision"> <?php echo smartyTranslate(array('s'=>'Challenge: En revisión','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<select name='status_banwire_tdc_revision' id='status_banwire_tdc_revision' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_TDC_REVISION']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
						<?php } ?>
						</select>
						<label for="status_banwire_tdc_denegada"> <?php echo smartyTranslate(array('s'=>'Challenge: Denegado','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<select name='status_banwire_tdc_denegada' id='status_banwire_tdc_denegada' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_TDC_DENEGADA']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
						<?php } ?>
						</select>
						<label for="status_banwire_tdc_aceptada"> <?php echo smartyTranslate(array('s'=>'Challenge: Aceptado','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
						<select name='status_banwire_tdc_aceptada' id='status_banwire_tdc_aceptada' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_TDC_ACEPTADA']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
						<?php } ?>
						</select>
					</div>
					<center><input type="checkbox" name="banwire_tdc" id="banwire_tdc" <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_TDC']) {?> checked="checked"<?php }?> /> <label for="banwire_tdc"> <?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'banwire'),$_smarty_tpl);?>
</label></center>
				</div>
				<div class="banwire-usa-product fixCol<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_OXXO']) {?> banwire-usa-product-active<?php }?>">
					<h4><?php echo smartyTranslate(array('s'=>'OXXO','mod'=>'banwire'),$_smarty_tpl);?>
</h4>
					<div class="box-content-payment" ><input type="checkbox" name="banwire_oxxo_sendpdf" id="banwire_oxxo_sendpdf" <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_OXXO_SENDPDF']) {?> checked="checked"<?php }?> /> <label for="banwire_oxxo_sendpdf"> <?php echo smartyTranslate(array('s'=>'Enviar PDF','mod'=>'banwire'),$_smarty_tpl);?>
</label><br>
						<label for="banwire_oxxo_vig"> <?php echo smartyTranslate(array('s'=>'Vigencia','mod'=>'banwire'),$_smarty_tpl);?>
</label><input type="text" name="banwire_oxxo_vig" id="banwire_oxxo_vig" size="5" value = "<?php echo $_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_OXXO_VIG'];?>
" /> <br>
						<label for="status_banwire_oxxo_espera"> <?php echo smartyTranslate(array('s'=>'OXXO: Espera de Pago','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_oxxo_espera' id='status_banwire_oxxo_espera' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_OXXO_ESPERA']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
						<label for="status_banwire_oxxo_confirmado"> <?php echo smartyTranslate(array('s'=>'Challenge: Aceptado','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_oxxo_confirmado' id='status_banwire_oxxo_confirmado' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_OXXO_CONFIRMADO']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
					</div>
					<center><input type="checkbox" name="banwire_oxxo" id="banwire_oxxo_vig" <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_OXXO']) {?> checked="checked"<?php }?> /> <label for="banwire_oxxo"> <?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'banwire'),$_smarty_tpl);?>
</label></center>
				</div>
				<div class="banwire-usa-product last fixCol<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_SPEI']) {?> banwire-usa-product-active<?php }?>">
					<h4><?php echo smartyTranslate(array('s'=>'SPEI','mod'=>'banwire'),$_smarty_tpl);?>
</h4>
					<div class="box-content-payment" ><?php echo smartyTranslate(array('s'=>'Activa pagos via tranferencia bancaria referenciada mediante SPEI-FAST (solo repblica Mexicana) ','mod'=>'banwire'),$_smarty_tpl);?>
</br>
						<label for="status_banwire_spei_espera"> <?php echo smartyTranslate(array('s'=>'SPEI: En Espera de Transferencia','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_spei_espera' id='status_banwire_spei_espera' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_SPEI_ESPERA']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
						<label for="status_banwire_spei_confirmado"> <?php echo smartyTranslate(array('s'=>'SPEI: Confirmado','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_spei_confirmado' id='status_banwire_spei_confirmado' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_SPEI_CONFIRMADO']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
					</div>
					<center><input type="checkbox" name="banwire_spei" id="banwire_spei" <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_SPEI']) {?> checked="checked"<?php }?> /> <label for="banwire_spei"> <?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'banwire'),$_smarty_tpl);?>
</label></center>
				</div>
			</div>
			<div class="banwire-usa-onecol">
				<div class="banwire-usa-product_eco fixCol<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC']) {?> banwire-usa-product-active<?php }?>">
					<h4><?php echo smartyTranslate(array('s'=>'Pagos Recurrentes (V4).','mod'=>'banwire'),$_smarty_tpl);?>
</h4>
					<div class="box-content-payment" >
						<label for="banwire_tdc_rec_select"> <?php echo smartyTranslate(array('s'=>'Recurrencia','mod'=>'banwire'),$_smarty_tpl);?>
</label>
						<select name='banwire_rec_select' id='banwire_rec_select' >
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_rec_select']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC_SELECT']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
						<?php } ?>
						</select></br>
						<label for="status_banwire_rec"> <?php echo smartyTranslate(array('s'=>'En Pago Recurrente','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_rec' id='status_banwire_rec' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_REC']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
						<label for="status_banwire_rec_fallo"> <?php echo smartyTranslate(array('s'=>'Fallo Pago Recurrente','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<select name='status_banwire_rec_fallo' id='status_banwire_rec_fallo' style="width:200px">
							<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_REC_FALLO']==$_smarty_tpl->tpl_vars['key']->value) {?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php } else { ?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
								<?php }?>
							<?php } ?>
							</select>
							
						
						<div id='datos_rec'>
							<label for="status_banwire_rec_scosto"> <?php echo smartyTranslate(array('s'=>'Recurrente Inicio sin costo','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
							<sup>*Solo si activa Inicio sin costo</sup><br />
								<select name='status_banwire_rec_scosto' id='status_banwire_rec_scosto' style="width:200px">
								<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
									<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['STATUS_BANWIRE_REC_SCOSTO']==$_smarty_tpl->tpl_vars['key']->value) {?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php } else { ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php }?>
								<?php } ?>
								</select>	
							<label for="banwire_rec_retryn"> <?php echo smartyTranslate(array('s'=>'Activar número de reintentos:','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
								<select name='banwire_rec_retryn' id='banwire_rec_retryn' style="width:100px" >
								<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_rec_retry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
									<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC_RETRYN']==$_smarty_tpl->tpl_vars['key']->value) {?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php } else { ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php }?>
								<?php } ?>
								</select></br>
							<label for="banwire_rec_retryd"> <?php echo smartyTranslate(array('s'=>'Activar días de reintentos:','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
								<select name='banwire_rec_retryd' id='banwire_rec_retryd' style="width:100px" >
								<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_rec_retry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
									<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC_RETRYD']==$_smarty_tpl->tpl_vars['key']->value) {?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php } else { ?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php }?>
								<?php } ?>
								</select></br>
							<label for="banwire_rec_trail"> <?php echo smartyTranslate(array('s'=>'Iniciar Recurrencia un mes despues:','mod'=>'banwire'),$_smarty_tpl);?>
</label></br>
								<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC_TRAIL']==1) {?>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_traily' value="1" checked='checked' /> <label for="banwire_rec_traily"> <?php echo smartyTranslate(array('s'=>'Si','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_trailn' value="null" /> <label for="banwire_rec_trailn"> <?php echo smartyTranslate(array('s'=>'No','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<?php } else { ?>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_traily' value="1" /> <label for="banwire_rec_traily"> <?php echo smartyTranslate(array('s'=>'Si','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_trailn' value="null" checked='checked' /> <label for="banwire_rec_trailn"> <?php echo smartyTranslate(array('s'=>'No','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<?php }?>
						</div>
					</div>
					<center><input type="checkbox" id="banwire_rec" name="banwire_rec" <?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_REC']) {?> checked="checked"<?php }?> /> <label for="banwire_rec"> <?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'banwire'),$_smarty_tpl);?>
</label></center>
				</div>
			</div>
		</fieldset>
	<br />
		<fieldset>
			<legend><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/settings.gif" alt="" /><span><?php echo smartyTranslate(array('s'=>'Configuración general de API Banwire','mod'=>'banwire'),$_smarty_tpl);?>
</span></legend>
			<div id="banwire-usa-basic-settings-table">
				<label for="banwire_sandbox_on"><?php echo smartyTranslate(array('s'=>'Desarrollo / Producción','mod'=>'banwire'),$_smarty_tpl);?>
</label>
				<div class="margin-form PT4">
					<input type="radio" name="banwire_sandbox" id="banwire_sandbox_on" value="0"<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_SANDBOX']==0) {?> checked="checked"<?php }?> /> <label for="banwire_sandbox_on" class="resetLabel"><?php echo smartyTranslate(array('s'=>'Producción','mod'=>'banwire'),$_smarty_tpl);?>
</label>
					<input type="radio" name="banwire_sandbox" id="banwire_sandbox_off" value="1"<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_SANDBOX']==1) {?> checked="checked"<?php }?> /> <label for="banwire_sandbox_off" class="resetLabel"><?php echo smartyTranslate(array('s'=>'Desarrollo (Sandbox)','mod'=>'banwire'),$_smarty_tpl);?>
</label>
					<p><?php echo smartyTranslate(array('s'=>'En modo de Desarrollo no sera visible la transacción en el administrador de Banwire:','mod'=>'banwire'),$_smarty_tpl);?>
<br /></p>
				</div>
				<label for="banwire_username"><?php echo smartyTranslate(array('s'=>'Usuario en Banwire:','mod'=>'banwire'),$_smarty_tpl);?>
</label>
				<div class="margin-form">
					<input type="text" name="banwire_username" class="input-text" value="<?php echo $_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_USERNAME'];?>
" /> <sup>*</sup>
				</div>
				<label for="banwire_concept"><?php echo smartyTranslate(array('s'=>'Concepto:','mod'=>'banwire'),$_smarty_tpl);?>
</label></td>
				<div class="margin-form">
					<input type="text" name="banwire_concept" class="input-text" value="<?php echo $_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_CONCEPT'];?>
" /> <sup>*</sup>
				</div>
				
				<label for="banwire_prod_del_cd"><?php echo smartyTranslate(array('s'=>'Tipo de Producto que comercializa:','mod'=>'banwire'),$_smarty_tpl);?>
</label>
				</br<sup><?php echo smartyTranslate(array('s'=>'* Este campo es obligatorio si activa Banwire-Shield','mod'=>'banwire'),$_smarty_tpl);?>
</sup>
				<div class="margin-form">
					<select name='banwire_prod_del_cd' id='banwire_prod_del_cd' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_valores_prod_del_cd']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
							<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_PROD_DEL_CD']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
							<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
				<label for="banwire_ship_mthd_cd"><?php echo smartyTranslate(array('s'=>'Metodo de Envio (BW-Shield):','mod'=>'banwire'),$_smarty_tpl);?>
</label>
				</br<sup><?php echo smartyTranslate(array('s'=>'* Este campo es obligatorio si activa Banwire-Shield','mod'=>'banwire'),$_smarty_tpl);?>
</sup>
				<div class="margin-form">
					<select name='banwire_ship_mthd_cd' id='banwire_ship_mthd_cd' style="width:200px">
						<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banwire_valores_ship_mthd_cd']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
							<?php if ($_smarty_tpl->tpl_vars['banwire_configuracion']->value['BANWIRE_SHIP_MTHD_CD']==$_smarty_tpl->tpl_vars['key']->value) {?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" selected='selected'><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
							<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
							<?php }?>
						<?php } ?>
					</select>
				</div>				
			</div>
			
			<div class="clear centerText">
				<input type="submit" name="SubmitProducts" class="button MB15" value="<?php echo smartyTranslate(array('s'=>'Modificar Configuración','mod'=>'banwire'),$_smarty_tpl);?>
" />
			</div>
			<span class="small"><sup style="color: red;">*</sup> <?php echo smartyTranslate(array('s'=>'Campos requeridos','mod'=>'banwire'),$_smarty_tpl);?>
</span>
		</fieldset>
	</form>
</div>
<?php if ($_smarty_tpl->tpl_vars['banwire_merchant_country_is_mx']->value) {?>
	<script type="text/javascript">
		
		$(document).ready(function() {
			$('#banwire_rec_select').bind('change', function(){
				
				if($(this).val() == 1 )
					$("#datos_rec").css('display', 'none');
				else
					$("#datos_rec").css('display', 'block');
			})
			$('#content table.table tbody tr th span').html('banwiremx');
			
			
		});
		
	</script>
<?php }?><?php }} ?>
