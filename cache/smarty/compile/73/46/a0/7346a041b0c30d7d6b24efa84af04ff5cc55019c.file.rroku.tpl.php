<?php /* Smarty version Smarty-3.1.19, created on 2017-06-19 14:44:56
         compiled from "/home/canalutv/public_html/themes/default-bootstrap/rroku.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1168100856594829b8663d59-71183000%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7346a041b0c30d7d6b24efa84af04ff5cc55019c' => 
    array (
      0 => '/home/canalutv/public_html/themes/default-bootstrap/rroku.tpl',
      1 => 1448047943,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1168100856594829b8663d59-71183000',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'email_create' => 0,
    'back' => 0,
    'HOOK_CREATE_ACCOUNT_TOP' => 0,
    'genders' => 0,
    'gender' => 0,
    'HOOK_CREATE_ACCOUNT_FORM' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_594829b873dfe5_04646316',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_594829b873dfe5_04646316')) {function content_594829b873dfe5_04646316($_smarty_tpl) {?>

<h1 class="page-heading"><?php if (!isset($_smarty_tpl->tpl_vars['email_create']->value)) {?><?php echo smartyTranslate(array('s'=>'Quiero regalar roku'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Create an account'),$_smarty_tpl);?>
<?php }?></h1>
<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&preg_match("/^http/",$_smarty_tpl->tpl_vars['back']->value)) {?><?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('login', null, 0);?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(false, null, 0);?>
<?php $_smarty_tpl->tpl_vars["postCodeExist"] = new Smarty_variable(false, null, 0);?>
<?php $_smarty_tpl->tpl_vars["dniExist"] = new Smarty_variable(false, null, 0);?>

<div id='register_persona'>
<form action="" method="post" id="account-creation_form" class="std box">
		<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_TOP']->value;?>

		<div class="account_creation">
			<h3 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Datos de la persona a la que va a regalar'),$_smarty_tpl);?>
</h3>
			<div class="clearfix">
				<label><?php echo smartyTranslate(array('s'=>'Tratamiento'),$_smarty_tpl);?>
</label>
				<br />
				<?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['genders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value) {
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
					<div class="radio-inline">
						<label for="id_gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" class="top">
							<input type="radio" name="id_gender" id="id_gender" value="<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id) {?>checked="checked"<?php }?> />
						<?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>

						</label>
					</div>
				<?php } ?>
			</div>
			<div id='nombre' class="required form-group">
				<label for="customer_firstname"><?php echo smartyTranslate(array('s'=>'Nombre'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<input placeholder='Nombre' onkeyup="$('#firstname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" id="customer_firstname" name="customer_firstname" value="<?php if (isset($_POST['customer_firstname'])) {?><?php echo $_POST['customer_firstname'];?>
<?php }?>" />
			</div>
			<div id='apellido' class="required form-group">
				<label for="customer_lastname"><?php echo smartyTranslate(array('s'=>'Apellido'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<input placeholder='Apellido' onkeyup="$('#lastname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" id="customer_lastname" name="customer_lastname" value="<?php if (isset($_POST['customer_lastname'])) {?><?php echo $_POST['customer_lastname'];?>
<?php }?>" />
			</div>
			<div class="required form-group">
				<label for="phone_invoice"><?php echo smartyTranslate(array('s'=>'Un teléono de contacto'),$_smarty_tpl);?>
</label>
				<input placeholder='teléfono' type="text" class="form-control" name="phone_invoice" id="phone_invoice" />
			</div>						
			<div id='correo' class="required form-group">
				<label for="email"><?php echo smartyTranslate(array('s'=>'Correo electrónico'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<input placeholder='correo electrónico' type="text" class="is_required validate form-control" data-validate="isEmail" id="email" name="email" value="<?php if (isset($_POST['email'])) {?><?php echo $_POST['email'];?>
<?php }?>" />
			</div>
			<div id='direccion' class="required form-group">
				<label for="address1">
					Dirección
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Dirección' id="address1" class="is_required validate form-control" type="text" value="" name="address1" data-validate="isAddress">
				<span class="form_info">Poner la dirección donde quiere que se haga la entrega.</span>
			</div>			
			<div id='colonia' class="required form-group">
				<label for="address2">
					Colonia
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Colonia' id="address2" class="is_required validate form-control" type="text" value="" name="address2" data-validate="isAddress">
			</div>			
			<div id='cp' class="required form-group unvisible" style="display: block;">
				<label for="codigo_postal">
					Código postal
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Código postal' id="codigo_postal" class="is_required validate form-control uniform-input text" type="text" value="" name="codigo_postal" data-validate="isPostCode">
			</div>			
			<div id='ciudad' class="required form-group">
				<label for="city">
					Ciudad
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Ciudad' id="city" class="is_required validate form-control" type="text" maxlength="64" value="" name="city" data-validate="isCityName">
			</div>		
		
			<!--<div class="required id_state form-group">
				<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<select name="id_state" id="id_state" class="form-control">
					<option value="">-</option>
				</select>
			</div>-->
			
			<div class="required form-group">
				<label for="id_estado"><?php echo smartyTranslate(array('s'=>'Estado'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<select id="id_estado" class="form-control" name="id_estado">
					<option value="">-</option>
					<option value="54">Aguascalientes</option>
					<option value="55">Baja California</option>
					<option value="56">Baja California Sur</option>
					<option value="57">Campeche</option>
					<option value="58">Chiapas</option>
					<option value="59">Chihuahua</option>
					<option value="60">Coahuila</option>
					<option value="61">Colima</option>
					<option value="62">Distrito Federal</option>
					<option value="63">Durango</option>
					<option value="68">Estado de México</option>
					<option value="64">Guanajuato</option>
					<option value="65">Guerrero</option>
					<option value="66">Hidalgo</option>
					<option value="67">Jalisco</option>
					<option value="69">Michoacán</option>
					<option value="70">Morelos</option>
					<option value="71">Nayarit</option>
					<option value="72">Nuevo León</option>
					<option value="73">Oaxaca</option>
					<option value="74">Puebla</option>
					<option value="75">Querétaro</option>
					<option value="76">Quintana Roo</option>
					<option value="77">San Luis Potosí</option>
					<option value="78">Sinaloa</option>
					<option value="79">Sonora</option>
					<option value="80">Tabasco</option>
					<option value="81">Tamaulipas</option>
					<option value="82">Tlaxcala</option>
					<option value="83">Veracruz</option>
					<option value="84">Yucatán</option>
					<option value="85">Zacatecas</option>
				</select>				
			</div>

			<div id='validate_estado' class="alert alert-danger-two" style='display:none'>
				<ol>
					<li>Debe seleccionar un estado.</li>
				</ol>
			</div>
			
			<div class="required form-group">
				<label for="id_pais"><?php echo smartyTranslate(array('s'=>'País'),$_smarty_tpl);?>
 <sup><font color="red">*</font></sup></label>
				<select id="id_pais" class="form-control" name="id_pais">
				  <option selected="selected" value="145">México</option>
				</select>				
			</div>			

		</div>


		<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value;?>

		<div class="submit clearfix">
			<input type="hidden" name="email_create" value="1" />
			<input type="hidden" name="is_new_customer" value="1" />
			<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true);?>
" /><?php }?>
						<a class="btn btn-default button button-medium guardar">
							<span id='guardar_regalo'>
								<?php echo smartyTranslate(array('s'=>'Regalar Roku'),$_smarty_tpl);?>

							</span>
						</a>			
			<p class="pull-right required"><span><sup>*</sup><?php echo smartyTranslate(array('s'=>'Campo requerido'),$_smarty_tpl);?>
</span></p>
		</div>
	</form>
</div>
<div id='iddivscript'></div>



<script type="text/javascript" charset="UTF-8">

function validateForm(){
	var breturn = true;
	
	if($('#customer_firstname').val() == ''){
	  $('#nombre').addClass('form-error');
	  breturn = false;
	}	

	if($('#customer_lastname').val() == ''){
	  $('#apellido').addClass('form-error');
	  breturn = false;
	}	

	if($('#email').val() == ''){
	  $('#correo').addClass('form-error');
	  breturn = false;
	}	

	if($('#address1').val() == ''){
	  $('#direccion').addClass('form-error');
	  breturn = false;
	}	

	if($('#codigo_postal').val() == ''){
	  $('#cp').addClass('form-error');
	  breturn = false;
	}	

	if($('#address2').val() == ''){
	  $('#colonia').addClass('form-error');
	  breturn = false;
	}

	if($('#city').val() == ''){
	  $('#ciudad').addClass('form-error');
	  breturn = false;
	}	

	if($('#id_estado').val() == ''){
	  $('#validate_estado').css('display', 'block');
	  breturn = false;
	}		
	
	return breturn;
}

$(function () {
	$("#guardar_regalo").unbind('click').bind("click", function (event) {	 
		validateForm();
		if (!validateForm()) {
			alert('No se puede guardar el regalo, se encontraron los siguientes errores:');
			return false;
		}		
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_user_regalo.php',
		  data: {id_gender : $("#account-creation_form input[type='radio']:checked").val(), nombre : $("#customer_firstname").val(), apellido : $("#customer_lastname").val(), 
			email : $("#email").val(), address1 : $('#address1').val(), codigo_postal : $('#codigo_postal').val(), address2 : $('#address2').val(), city : $('#city').val(), 
			id_estado : $('#id_estado').val(), id_pais : $('#id_pais').val(), phone_invoice : $('#phone_invoice').val()}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data); 
			location.href = 'http://canalutv.mx/14-tienda';
		},
		});	  
	});	
 		
	
});
</script>
<?php }} ?>
