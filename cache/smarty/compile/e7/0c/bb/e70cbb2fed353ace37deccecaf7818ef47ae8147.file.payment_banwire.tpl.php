<?php /* Smarty version Smarty-3.1.19, created on 2017-06-17 14:11:08
         compiled from "/home/canalutv/public_html/modules/banwire/views/templates/front/payment_banwire.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29273042159457ecca35c04-15211595%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e70cbb2fed353ace37deccecaf7818ef47ae8147' => 
    array (
      0 => '/home/canalutv/public_html/modules/banwire/views/templates/front/payment_banwire.tpl',
      1 => 1476126042,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29273042159457ecca35c04-15211595',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this_path' => 0,
    'cookie' => 0,
    'cart' => 0,
    'link' => 0,
    'esregalo' => 0,
    'navigationPipe' => 0,
    'cartProducts' => 0,
    'nbProducts' => 0,
    'banwire_tdc' => 0,
    'idProducto' => 0,
    'banwire_rec' => 0,
    'banwire_oxxo' => 0,
    'banwire_spei' => 0,
    'module_dir' => 0,
    'months' => 0,
    'key' => 0,
    'recurrencia' => 0,
    'years' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59457eccbeb420_91525262',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59457eccbeb420_91525262')) {function content_59457eccbeb420_91525262($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/canalutv/public_html/tools/smarty/plugins/modifier.date_format.php';
?><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['this_path']->value;?>
/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['this_path']->value;?>
/css/layout.css" type="text/css" media="all">
<?php $_smarty_tpl->tpl_vars['esregalo'] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getisRegalo($_smarty_tpl->tpl_vars['cookie']->value->id_customer), null, 0);?>
<script>
			$('#fancybox_inline').fancybox({
			    'speedOut': 150,
			    'modal': {
				'enableEscapeButton': true
			    },
			    'centerOnScroll': true,
			    'overlayColor': '#40E0FF',
			    'overlayOpacity': .5,
			    'transitionIn': 'none'
			});
			
			var tipo = $("#banwire_card_type");
			tipo.live('change', function(){
			/*
				if(tipo.val() == 'amex')
					$("#banwire_amex").css('display', 'block');
				else
					$("#banwire_amex").css('display', 'none');
			*/
			});
			$("#button_payment").live('click', function(){
				$.ajax({
					url:"<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('banwire','ajax',array(),true);?>
",
					data: $("#form_banwire").serialize(),
					type: 'post',
					dataType: 'json',
					beforeSend:function(){
						$("#fancybox_inline").trigger('click');
                        $("#button_payment").css('disabled', true);
					},
					success: function(data){
						if (data.errors){
							html ='';
							$.each(data.errors, function(a, b){
								html += '<span class="renglon" > ' + b + '</span>';
							});
							$("#errors").html(html).css("display", "block");
                                                        $.fancybox.close();
                                                        $("#button_payment").css('disabled', false);        
						}
						if (data.redirect) {
							$(location).attr('href', data.redirect);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						      $.fancybox.close();
                                                      $("#button_payment").css('disabled', false);
					}
				});
			});
			$("#payment_banwire_tdc").css('display', 'block');
			$("#payment_banwire_rec").css('display', 'none');
			$("#payment_banwire_oxxo").css('display', 'none');
			$("#payment_banwire_spei").css('display', 'none');	

			//$("#banwire_rec").css('display', 'none');        /*               
        function selectRecYN(value) {			$("#banwire_rex").css('display', 'none');			if (value == 1 )				$("#banwire_rec").css('display', 'block');
		}
		*/
                function newPaymentMethod(api){
                    if(api == 'oxxo'){
                        $("#payment_banwire_oxxo").css('display', 'block');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('oxxo');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").addClass("active");
                        $("#tab_spei").removeClass("active");
                    }
                    if(api == 'tdc'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'block');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('tdc');
                        $("#tab_cc").addClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").removeClass("active");
                    }
					
					if(api == 'rec'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'block');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('rec');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").addClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").removeClass("active");
                    }
                    if(api == 'spei'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'block');
                        $("#banwire_api").val('spei');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").addClass("active");
                    }
                }
		// basic configuration
		var io_install_flash = false;
		// do not install Flash
		var io_install_stm = false;
		// do not require install of Active X
		var io_exclude_stm = 12;
		// do not run Active X under IE 8 platforms
		var io_enable_rip = true;
		// collect Real IP information
		var io_bb_callback = function (bb, isComplete){ // populate hidden form fields in both forms
		    var login_field = document.getElementById("EBT_DEVICEPRINT");
		    if (login_field)
				login_field.value = bb;
		};
		
		$(function() {
			var is_rec = "<?php echo $_smarty_tpl->tpl_vars['cookie']->value->isrecurrente;?>
";
			if ((is_rec.length > 0)){
				$("#api_rec").trigger('click');
				$("#tab_cc").css("display", 'none');
				$("#tab_oxxo").css("display", 'none');
				$("#tab_spei").css("display", 'none');			    
			}
			
			if("<?php echo $_smarty_tpl->tpl_vars['esregalo']->value;?>
"){
				$("#tab_oxxo").css("display", 'none');
				$("#tab_spei").css("display", 'none');				
			}
		});
        </script>
	<script language="javascript" src=https://mpsnare.iesnare.com/snare.js></script>	
	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true,null,"step=3"), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Go back to the Checkout','mod'=>'banwire'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Checkout','mod'=>'banwire'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'Banwire payment','mod'=>'banwire'),$_smarty_tpl);?>

	<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('payment', null, 0);?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php $_smarty_tpl->tpl_vars['cartProducts'] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getProducts(), null, 0);?>
	<?php $_smarty_tpl->tpl_vars['idProducto'] = new Smarty_variable($_smarty_tpl->tpl_vars['cartProducts']->value[0]['id_product'], null, 0);?>
	<?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable($_smarty_tpl->tpl_vars['cookie']->value->price_total, null, 0);?>
	
	<?php if ($_smarty_tpl->tpl_vars['nbProducts']->value<=0) {?>
		<p class="warning"><?php echo smartyTranslate(array('s'=>'Your shopping cart is empty.','mod'=>'banwire'),$_smarty_tpl);?>
</p>
	<?php } else { ?>
		<div id='errors' style="display: none"></div>
		<form id="form_banwire">
            <input type="hidden" value="tdc" name='banwire_api' id = 'banwire_api' />			<div class='header-title-payments'>
				<ul>
					<?php if ($_smarty_tpl->tpl_vars['banwire_tdc']->value) {?>							<li id="tab_cc" class="active"><a tab = 'payment_banwire_tdc' id = 'paymnet_banwire_tdc_btn' > <input type="radio" name="banwire_api" id='api_tdc' value="tdc" onclick = "newPaymentMethod('tdc')" checked = checked/><label for="api_tdc"><?php echo smartyTranslate(array('s'=>'PAGO TARJETA','mod'=>'banwire'),$_smarty_tpl);?>
</label></a></li> 					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['idProducto']->value==4||$_smarty_tpl->tpl_vars['idProducto']->value==9) {?>
						<?php if ($_smarty_tpl->tpl_vars['banwire_rec']->value) {?>	
							<li id="tab_rec"><a tab = 'payment_banwire_rec' id = 'paymnet_banwire_rec_btn' > <input type="radio" name="banwire_api" id='api_rec' value="rec" onclick = "newPaymentMethod('rec')" /><label for="api_rec"><?php echo smartyTranslate(array('s'=>'DOMICILIAR PAGO','mod'=>'banwire'),$_smarty_tpl);?>
</label></a></li> 
						<?php }?>
					<?php }?>					<?php if ($_smarty_tpl->tpl_vars['banwire_oxxo']->value) {?>						<li id="tab_oxxo"> <a tab = 'payment_banwire_oxxo' id ='payment_banwire_oxxo_btn' ><input type="radio" name="banwire_api" id='api_oxxo' value='oxxo' onclick = "newPaymentMethod('oxxo')" /><label for="api_oxxo"><?php echo smartyTranslate(array('s'=>'PAGO OXXO','m'=>'banwire'),$_smarty_tpl);?>
</label></a></li> 					<?php }?>					<?php if ($_smarty_tpl->tpl_vars['banwire_spei']->value) {?>							<li id="tab_spei"><a tab = 'payment_banwire_spei' id = 'paymnet_banwire_spei_btn' ><input type="radio" name="banwire_api" id='api_spei' value='spei' onclick = "newPaymentMethod('spei')" /><label for="api_spei"><?php echo smartyTranslate(array('s'=>'PAGO SPEI','mod'=>'banwire'),$_smarty_tpl);?>
</label></a></li> 
					<?php }?>
				</ul>
            </div>
            <div id='content-payments'>
				<input type="hidden" id='EBT_DEVICEPRINT' name='EBT_DEVICEPRINT' />				<?php if ($_smarty_tpl->tpl_vars['banwire_tdc']->value) {?>						<div class = 'content-description-payment' id = 'payment_banwire_tdc'>						<fieldset class='section_banwire_form'>							<legend><h3><?php echo smartyTranslate(array('s'=>'Pago Tarjeta','mod'=>'banwire'),$_smarty_tpl);?>
</h3></legend>							<div class="logos-tarjeta"><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/logos_pagos_tarjetas.jpg" class="wb"/></div>							<div class="banwire_field">								<label><?php echo smartyTranslate(array('s'=>'Nombre en Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>								<input type="text" name='banwire[card_name]' />
								</br><span class="form_info"><?php echo smartyTranslate(array('s'=>'Poner el nombre tal cual, como sale en su tarjeta.'),$_smarty_tpl);?>
</span><br><br>							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Número de Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<input placeholder='1234123412341234' type="text"  name='banwire[card_num]' />
								</br><span class="form_info"><?php echo smartyTranslate(array('s'=>'Poner los dìgitos tal cual y como vienen en su tarjeta.'),$_smarty_tpl);?>
</span><br><br>
							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Tipo de Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<select  name='banwire[card_type]' id = 'banwire_card_type' onchange="if(this.value=='amex') document.getElementById('banwire_amex').style.display='block'; else document.getElementById('banwire_amex').style.display='none';">
									<option value="visa">VISA</option>									<option value="mastercard">MasterCard</option>									<option value="amex">AmericanExpress</option>								</select>
								<br><br>
							</div>
							<div class="banwire_field">								<label><?php echo smartyTranslate(array('s'=>'Fecha de Vigencia: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<select name='banwire_vig_month' id='banwire_vig_month' >									<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>									<?php } ?>								</select>
								<select name='banwire_vig_year' id='banwire_vig_year' >
									<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
?>										<option value="<?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>									<?php } ?>
								</select>
								<br><br>
							</div>
							<!--
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Código CCV: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<input type="password" maxlength="4" size="5" name='banwire[card_ccv]' />
							</div>
							-->
                            <div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Código CCV: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
                                <input type="password" maxlength="4" size="5" name='banwire[card_ccv]' />
								<a class="iframe2" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('15');?>
?content_only=1">
									<b>¿Qué es CCV?</b>
								</a>
								<script type="text/javascript">
									$(document).ready(function(){
										$("a.iframe2").fancybox({
										'autoScale'  : false,
										'transitionIn'  : 'none',
										'transitionOut'  : 'none',
										'width'  : 420,
										'height'  : 1200,
										'type'  : 'iframe'
										});
									});
								</script>
                            </div>							
							
							<div id="banwire_amex" style="display: none">
								<span class="help"><?php echo smartyTranslate(array('s'=>'* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.','mod'=>'banwire'),$_smarty_tpl);?>
</span>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Código Postal: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type="text"  name='banwire[postcode]' />
								</div>
								
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Dirección: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type="text" name='banwire[direction]' />
								</div>
								
							</div>
						</fieldset>
					</div>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['idProducto']->value==4||$_smarty_tpl->tpl_vars['idProducto']->value==9) {?>
				<?php if ($_smarty_tpl->tpl_vars['banwire_rec']->value) {?>
					<div class = 'content-description-payment' id = 'payment_banwire_rec' style="display: none">
						<fieldset class='section_banwire_form'>
							<legend><h3><?php echo smartyTranslate(array('s'=>'Domiciliar Pago TDC Banwire','mod'=>'banwire'),$_smarty_tpl);?>
</h3></legend>
							<div class="logos-tarjeta"><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/logos_pagos_tarjetas.jpg"/></div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Nombre en Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<input type="text" name='banwire_rec[card_name]' />
							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Número de Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<input type="text"  name='banwire_rec[card_num]' />
							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Tipo de Tarjeta: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<select  name='banwire_rec[card_type]' id = 'banwire_card_type' onchange="if(this.value=='amex') document.getElementById('banwire_rec_amex').style.display='block'; else document.getElementById('banwire_rec_amex').style.display='none';">
									<option value="visa">VISA</option>
									<option value="mastercard">MasterCard</option>
									<option value="amex">AmericanExpress</option>
								</select>
							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Fecha de Vigencia: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<select name='banwire_vig_month_rec' id='banwire_vig_month' >
									<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['recurrencia']->key;
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php } ?>
								</select>
								<select name='banwire_vig_year_rec' id='banwire_vig_year' >
									<?php  $_smarty_tpl->tpl_vars['recurrencia'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['recurrencia']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['recurrencia']->key => $_smarty_tpl->tpl_vars['recurrencia']->value) {
$_smarty_tpl->tpl_vars['recurrencia']->_loop = true;
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['recurrencia']->value;?>
</option>
									<?php } ?>
								</select>
							</div>
							<div class="banwire_field">
								<label><?php echo smartyTranslate(array('s'=>'Código CCV: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
								<input type="password" maxlength="4" size="5" name='banwire_rec[card_ccv]' />
							</div>
							
							<div id='banwire_rec'>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Monto a domiciliar: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<label>$ <?php echo number_format($_smarty_tpl->tpl_vars['total']->value,2,".",",");?>
</label>
								</div>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Recurrencia: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<label>30 Días</label>
								</div>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Inicio Recurrencia: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<label><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['cart']->value->angel_f,"%d/%m/%Y");?>
</label>
								</div>
								<div class="banwire_field">
									<input type="hidden" maxlength="4" size="5" name='banwire_rec[recurrencias]' value='IND' />
									<input type="hidden" maxlength="1" size="1" name='select_rec_yes_rec' value='1' />
									
								</div>
							</div>
							
							<div id="banwire_rec_amex" style="display: none">
								<span class="help"><?php echo smartyTranslate(array('s'=>'* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.','mod'=>'banwire'),$_smarty_tpl);?>
</span>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Código Postal: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type="text"  name='banwire_rec[postcode]' />
								</div>
								<div class="banwire_field">
									<label><?php echo smartyTranslate(array('s'=>'Dirección: ','mod'=>'banwire'),$_smarty_tpl);?>
</label>
									<input type="text" name='banwire_rec[direction]' />
								</div>
							</div>
						</fieldset>
					</div>
				<?php }?>				<?php }?>
			    <?php if ($_smarty_tpl->tpl_vars['banwire_oxxo']->value) {?>	
					<div class='content-description-payment' id= 'payment_banwire_oxxo' style="display: none">
						<div class="logos-oxxo"><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/logos_pagos_oxxo.jpg"/></div>
						<p><b><?php echo smartyTranslate(array('s'=>'Al escoger pagar con OXXO se generará y enviara un cupón de pago al correo de tu cuenta.','mod'=>'banwire'),$_smarty_tpl);?>
</b></p><br>
						<p><?php echo smartyTranslate(array('s'=>'Este cupón es válido únicamente para realizar el pago correspondiente y dentro de la fecha de vigencia establecida. La acreditación del mismo es a las 24 hrs. de realizado, hasta entonces el pago podrá ser corroborado por el vendedor.','mod'=>'banwire'),$_smarty_tpl);?>
</p><br>
						<p><?php echo smartyTranslate(array('s'=>'El pago se verá reflejado en tienda dentro de las siguientes 72 horas después de haberlo realizado.','mod'=>'banwire'),$_smarty_tpl);?>
</p><br>
						<p><b><br><?php echo smartyTranslate(array('s'=>'Imprime el cupón de manera clara y legible, usa de preferencia impresora láser, consérvalo en buen estado sin tachar ó doblar la parte del código de barras. En caso de no ser legible, la tienda puede rechazar el pago correspondiente.','mod'=>'banwire'),$_smarty_tpl);?>
</b></p>
					</div>
			    <?php }?>				<?php if ($_smarty_tpl->tpl_vars['banwire_spei']->value) {?>					<div class="content-description-payment" id = 'payment_banwire_spei' style="display: none">
						<div class="logos-spei"><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/logos_pagos_spei.jpg"/></div>						<p><?php echo smartyTranslate(array('s'=>'Al escoger pagar transferencia se generará una cuenta CLABE (La CLABE cuenta con 18 dígitos) con la cual podrás realizar el pago desde tu portal de banca electrónica o acudir a tu banco y realizar un SPEI en ventanilla.','mod'=>'banwire'),$_smarty_tpl);?>
</p><br>
						<p><b><?php echo smartyTranslate(array('s'=>'• Copia la cuenta CLABE (La CLABE cuenta con 18 dígitos) que te es asignada.','mod'=>'banwire'),$_smarty_tpl);?>
</b></p>
						<p>
							<b><?php echo smartyTranslate(array('s'=>'• Ingresa a tu banca en línea y realiza una transferencia a esta cuenta CLABE.','mod'=>'banwire'),$_smarty_tpl);?>
 
							<!--<sup>-->
								<?php echo smartyTranslate(array('s'=>'[Si es la primera vez que realizas un pago a través de SPEI FAST tendrás que dar de alta esta CLABE para poder realizar la transferencia].','mod'=>'banwire'),$_smarty_tpl);?>
 </b>
							<!--</sup>-->
						</p>
						<p><b><?php echo smartyTranslate(array('s'=>'• No olvides indicar NOMBRE Y APELLIDOS del USUARIO con el que está registrada la cuenta.','mod'=>'banwire'),$_smarty_tpl);?>
 </b></p>
						<p><b><?php echo smartyTranslate(array('s'=>'• Listo, tu pago ha sido completado, el vendedor recibirá de inmediato la notificación de tu pago.','mod'=>'banwire'),$_smarty_tpl);?>
</b></p>					</div>				<?php }?>
            </div>	
			<p class="cart_navigation" id="cart_navigation">
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true,null,"step=3"), ENT_QUOTES, 'ISO-8859-1', true);?>
" class="button_large"><?php echo smartyTranslate(array('s'=>'Volver','mod'=>'banwire'),$_smarty_tpl);?>
</a>
				<label id="button_payment"  class="exclusive_large" ><?php echo smartyTranslate(array('s'=>'Confirmar Pago','mod'=>'banwire'),$_smarty_tpl);?>
</label>
			</p>
		</form>
	<?php }?>
	<div style="display:none">
		<a id="fancybox_inline" href="#data" style="display:none"></a>
		<div id="data">
			<img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
img/BanWire-Payment-Process.gif" alt="BanWire Security">                
		</div>
	</div><?php }} ?>
