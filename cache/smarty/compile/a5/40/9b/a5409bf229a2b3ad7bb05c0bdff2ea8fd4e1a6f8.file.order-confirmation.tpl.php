<?php /* Smarty version Smarty-3.1.19, created on 2017-06-17 22:12:08
         compiled from "/home/canalutv/public_html/modules/paypalmx/views/templates/hook/order-confirmation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17450124775945ef88582170-59611239%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5409bf229a2b3ad7bb05c0bdff2ea8fd4e1a6f8' => 
    array (
      0 => '/home/canalutv/public_html/modules/paypalmx/views/templates/hook/order-confirmation.tpl',
      1 => 1461232308,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17450124775945ef88582170-59611239',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'paypal_mx_order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5945ef886029c3_80843277',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5945ef886029c3_80843277')) {function content_5945ef886029c3_80843277($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['paypal_mx_order']->value['valid']==1) {?>
<div class="conf confirmation">
	<?php echo smartyTranslate(array('s'=>'Congratulations! Your payment is pending verification, and your order has been saved under','mod'=>'paypalmx'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['paypal_mx_order']->value['reference'])) {?> <?php echo smartyTranslate(array('s'=>'the reference','mod'=>'paypalmx'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paypal_mx_order']->value['reference'], ENT_QUOTES, 'UTF-8', true);?>
</b><?php } else { ?> <?php echo smartyTranslate(array('s'=>'the ID','mod'=>'paypalmx'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paypal_mx_order']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
</b><?php }?>.
</div>
<?php } else { ?>
<div class="error">
	<?php echo smartyTranslate(array('s'=>'Unfortunately, an error occurred during the transaction.','mod'=>'paypalmx'),$_smarty_tpl);?>
<br /><br />
	<?php echo smartyTranslate(array('s'=>'Please double-check your credit card details and try again. If you need further assistance, feel free to contact us anytime.','mod'=>'paypalmx'),$_smarty_tpl);?>
<br /><br />
<?php if (isset($_smarty_tpl->tpl_vars['paypal_mx_order']->value['reference'])) {?>
	(<?php echo smartyTranslate(array('s'=>'Your Order\'s Reference:','mod'=>'paypalmx'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paypal_mx_order']->value['reference'], ENT_QUOTES, 'UTF-8', true);?>
</b>)
<?php } else { ?>
	(<?php echo smartyTranslate(array('s'=>'Your Order\'s ID:','mod'=>'paypalmx'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['paypal_mx_order']->value['id'], ENT_QUOTES, 'UTF-8', true);?>
</b>)
<?php }?>
</div>
<?php }?><?php }} ?>
