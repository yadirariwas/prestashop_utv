<?php /* Smarty version Smarty-3.1.19, created on 2017-06-17 20:31:39
         compiled from "/home/canalutv/public_html/themes/default-bootstrap/accounts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9409026255945d7fb5b1124-76422261%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a1d3a8a325a018d3e2a6fd83f44b14ed982035f0' => 
    array (
      0 => '/home/canalutv/public_html/themes/default-bootstrap/accounts.tpl',
      1 => 1448047699,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9409026255945d7fb5b1124-76422261',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'accounts' => 0,
    'PS_STOCK_MANAGEMENT' => 0,
    'account' => 0,
    'base_dir' => 0,
    'idProduct' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5945d7fb6b62e4_00662511',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5945d7fb6b62e4_00662511')) {function content_5945d7fb6b62e4_00662511($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/canalutv/public_html/tools/smarty/plugins/modifier.date_format.php';
?>
<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><span class="navigation_page"><?php echo smartyTranslate(array('s'=>'My vouchers'),$_smarty_tpl);?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<h1 class="page-heading">
	<?php echo smartyTranslate(array('s'=>'My accounts'),$_smarty_tpl);?>

</h1>

<?php if (isset($_smarty_tpl->tpl_vars['accounts']->value)&&!empty($_smarty_tpl->tpl_vars['accounts']->value)) {?>
	<div id="order-detail-content" class="table_block table-responsive">
		<table id="cart_summary" class="table table-bordered <?php if ($_smarty_tpl->tpl_vars['PS_STOCK_MANAGEMENT']->value) {?>stock-management-on<?php } else { ?>stock-management-off<?php }?>">
			<thead>
				<tr>
					<th class="cart_product first_item"><?php echo smartyTranslate(array('s'=>'User'),$_smarty_tpl);?>
</th>
					<th class="cart_product item"><?php echo smartyTranslate(array('s'=>'Alias'),$_smarty_tpl);?>
</th>
					<th class="cart_description item"><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</th>
					<th class="cart_unit item"><?php echo smartyTranslate(array('s'=>'Date Add'),$_smarty_tpl);?>
</th>
					<th class="cart_quantity item"><?php echo smartyTranslate(array('s'=>'Payment Date'),$_smarty_tpl);?>
</th>
					<th class="cart_total item"><?php echo smartyTranslate(array('s'=>'Price'),$_smarty_tpl);?>
</th>
					<th class="cart_total item"><?php echo smartyTranslate(array('s'=>'Payment'),$_smarty_tpl);?>
</th>
					<th class="item" style='width:15%'><?php echo smartyTranslate(array('s'=>'Recurrente'),$_smarty_tpl);?>
</th>
				</tr>
			</thead>
			<tbody>
			<!--<pre><small><?php echo print_r($_smarty_tpl->tpl_vars['accounts']->value);?>
</small></pre>-->
				<?php  $_smarty_tpl->tpl_vars['account'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['account']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['accounts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['account']->key => $_smarty_tpl->tpl_vars['account']->value) {
$_smarty_tpl->tpl_vars['account']->_loop = true;
?>
					<tr>
						<td class="cart_quantity">
							<?php echo $_smarty_tpl->tpl_vars['account']->value['user'];?>
							
						</td>
						<td class="cart_quantity">
							<input id="user-<?php echo $_smarty_tpl->tpl_vars['account']->value['user'];?>
" class="form-control validate" type="text" value="<?php echo $_smarty_tpl->tpl_vars['account']->value['alias'];?>
" name="user" placeholder='alias'>
						</td>
						<td class="cart_quantity">
							<?php echo $_smarty_tpl->tpl_vars['account']->value['clave_roku'];?>

						</td>					
						<td class="cart_quantity">
							<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['account']->value['fecha_creacion'],"%d-%m-%Y");?>

						</td>
						<td class="cart_quantity">
							<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['account']->value['fecha_pago'],"%d-%m-%Y");?>

						</td>					
						<td class="cart_quantity">							
							<?php if ($_smarty_tpl->tpl_vars['account']->value['grupo']==3) {?>
								485
							<?php } elseif ($_smarty_tpl->tpl_vars['account']->value['grupo']==7) {?>
								450
							<?php } elseif ($_smarty_tpl->tpl_vars['account']->value['grupo']==6) {?>
								300							
							<?php } elseif ($_smarty_tpl->tpl_vars['account']->value['grupo']==5) {?>
								350	
							<?php } elseif ($_smarty_tpl->tpl_vars['account']->value['grupo']==4) {?>
								400															
							<?php }?>
						</td>	
						<td class="cart_quantity">
							<?php echo $_smarty_tpl->tpl_vars['account']->value['tipo_pago'];?>

						</td>
						<td>
						<?php if (!$_smarty_tpl->tpl_vars['account']->value['recurrente_aplicado']) {?>
							<div class='checker_r'>
								<table border = 0>
									<tr>
										<td>
											<input type="checkbox" class='validate_rec' id="rec-<?php echo $_smarty_tpl->tpl_vars['account']->value['user'];?>
-<?php echo $_smarty_tpl->tpl_vars['account']->value['id_customer'];?>
" value="Cheese"/>
										</td>
										<td>
											<span id='aplicar'>Aplicar</span>
										</td>
									</tr>
								</table>
								
							</div>
						<?php } else { ?>
							Aplicado
						<?php }?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td>
					</td>
					<td>
						<a class="btn btn-default button button-small guardar">
							<span id='guardar_alias'>
								<?php echo smartyTranslate(array('s'=>'Guardar'),$_smarty_tpl);?>

							</span>
						</a>					
					<td colspan='5'>
					</td>
					<td>
						<a class="btn btn-default button button-small guardar">
							<span id='guardar_rec'>
								<?php echo smartyTranslate(array('s'=>'Recurrente'),$_smarty_tpl);?>

							</span>
						</a>
					</td>
				</tr>
			</tfoot>
		</table>
	</div> 	
<?php } else { ?>
	<p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'You do not have accounts.'),$_smarty_tpl);?>
</p>
<?php }?>

<ul class="footer_links clearfix">
	<li>
		<a class="btn btn-default button button-small" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
			<span>
				<i class="icon-chevron-left"></i> <?php echo smartyTranslate(array('s'=>'Back to your account'),$_smarty_tpl);?>

			</span>
		</a>
	</li>
	<li>
		<a class="btn btn-default button button-small" href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
">
			<span>
				<i class="icon-chevron-left"></i> <?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>

			</span>
		</a>
	</li>	
	<!--<li>
		<a class="btn btn-default button button-small guardar">
			<span id='guardar_alias'>
				<?php echo smartyTranslate(array('s'=>'Guardar'),$_smarty_tpl);?>

			</span>
		</a>
	</li>	
	<li>
		<a class="btn btn-default button button-small guardar">
			<span id='guardar_rec'>
				<?php echo smartyTranslate(array('s'=>'Recurrente'),$_smarty_tpl);?>

			</span>
		</a>
		<!--<?php $_smarty_tpl->tpl_vars['idProduct'] = new Smarty_variable('4', null, 0);?>
		<a class="button ajax_add_to_cart_button btn btn-default" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',false,null,"add=1&amp;id_product=".((string)$_smarty_tpl->tpl_vars['idProduct']->value)."&amp;qty=4&amp;token=".((string)$_smarty_tpl->tpl_vars['static_token']->value),false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
" data-id-product="<?php echo $_smarty_tpl->tpl_vars['idProduct']->value;?>
">
			<span><?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
</span>
		</a>	
	</li>-->
</ul>
<div id='iddivscript' style='display:none'></div>


<script type="text/javascript" charset="UTF-8">
$(function () {
	$("#guardar_alias").unbind('click').bind("click", function (event) {
		var usuarios = new Object();
		var cont = 1;
		$("#cart_summary input.validate").each(function (index, item) {
			var input = $(item);
			var id = $(item).attr('id');
			ids = id.split('-');
			usuario = ids[1];
			var value = $("#user-" + usuario).val();
			usuarios[cont++] = {user: usuario, alias: value};
		});	  
	  //console.log(usuarios);
	  
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_alias_user.php',
		  data: {customer_roku : usuarios}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data);
			alert('Se han registrado correctamente los alias para los usuarios.');
		},
		});	  
	});	

	$("#guardar_rec").unbind('click').bind("click", function (event) {
		var usuarios = new Object();
		var cont = 1;
		$("#cart_summary input.validate_rec").each(function (index, item) {
			var input = $(item);
			var id = $(item).attr('id');
			ids = id.split('-');
			id_usuario = ids[1];
			id_customer = ids[2];
			if($("#" + id).is(":checked")){
				//console.log('ok');
				usuarios[cont++] = {user: id_usuario, customer: id_customer};
			}
		});	 
		var new_cont = cont - 1;
		console.log(new_cont);
	  
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_recurrente.php',
		  data: {customer_roku : usuarios}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data);
			//alert('Se esta porocesando la información.');
			addCartCustom(new_cont)
			
		},
		});	  
	});		
	
	function addCartCustom(quantity){
		var idCombination = $('#idCombination').val();
		$.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: baseUri + '?rand=' + new Date().getTime(),
			async: true,
			cache: false,
			dataType : "json",
			data: 'controller=cart&add=1&ajax=true&qty=' + quantity + '&id_product=4' + '&token=' + static_token + ( (parseInt(idCombination) && idCombination != null) ? '&ipa=' + parseInt(idCombination): ''),
			success: function(jsonData,textStatus,jqXHR)
			{
				location.href = 'http://canalutv.mx/carrito';
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				var error = "Impossible to add the product to the cart.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
				if (!!$.prototype.fancybox)
				    $.fancybox.open([
				    {
				        type: 'inline',
				        autoScale: true,
				        minHeight: 30,
				        content: '<p class="fancybox-error">' + error + '</p>'
				    }],
					{
				        padding: 0
				    });
				else
				    alert(error);
				//reactive the button when adding has finished
				if (addedFromProductPage)
					$('#add_to_cart button').removeProp('disabled').removeClass('disabled');
				else
					$(callerElement).removeProp('disabled');
			}
		});		
	}
});
</script>
<?php }} ?>
