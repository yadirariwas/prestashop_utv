<?php /* Smarty version Smarty-3.1.19, created on 2017-06-17 14:10:45
         compiled from "/home/canalutv/public_html/modules/paypalmx/views/templates/hook/standard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175022908259457eb54f12e7-58829774%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd934f56e48768d1b106bca37c39a975850dff34c' => 
    array (
      0 => '/home/canalutv/public_html/modules/paypalmx/views/templates/hook/standard.tpl',
      1 => 1473292919,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175022908259457eb54f12e7-58829774',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'paypal_mx_action' => 0,
    'paypal_mx_business_account' => 0,
    'currency' => 0,
    'cart' => 0,
    'paypal_mx_billing_address' => 0,
    'paypal_mx_customer' => 0,
    'paypal_mx_total_discounts' => 0,
    'paypal_mx_product' => 0,
    'paypal_mx_total_shipping' => 0,
    'show_taxes' => 0,
    'paypal_mx_total_tax' => 0,
    'paypal_mx_notify_url' => 0,
    'paypal_mx_return_url' => 0,
    'paypal_mx_cancel_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59457eb5673fe0_73541327',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59457eb5673fe0_73541327')) {function content_59457eb5673fe0_73541327($_smarty_tpl) {?>
<form action="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_action']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
	<p class="payment_module">
		<input type="hidden" name="cmd" value="_cart" />
		<input type="hidden" name="upload" value="1" />
		<input type="hidden" name="charset" value="utf8" />
		<input type="hidden" name="business" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_business_account']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="currency_code" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['currency']->value->iso_code, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="custom" value="<?php echo intval($_smarty_tpl->tpl_vars['cart']->value->id);?>
;<?php if (isset($_smarty_tpl->tpl_vars['cart']->value->id_shop)) {?><?php echo intval($_smarty_tpl->tpl_vars['cart']->value->id_shop);?>
<?php } else { ?>0<?php }?>" />
		<input type="hidden" name="amount" value="<?php echo floatval($_smarty_tpl->tpl_vars['cart']->value->getOrderTotal(true));?>
" />
		<input type="hidden" name="first_name" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->firstname, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="last_name" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->lastname, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="address1" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->address1, ENT_QUOTES, 'UTF-8', true);?>
" />
		<?php if ($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->address2) {?><input type="hidden" name="address2" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->address2, ENT_QUOTES, 'UTF-8', true);?>
" /><?php }?>
		<input type="hidden" name="city" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->city, ENT_QUOTES, 'UTF-8', true);?>
" />
		<?php if (($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->id_state!=0)) {?>
			<input type="hidden" name="state" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->state->iso_code, ENT_QUOTES, 'UTF-8', true);?>
" />
		<?php }?>
		<input type="hidden" name="zip" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->postcode, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="email" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_customer']->value->email, ENT_QUOTES, 'UTF-8', true);?>
" />
		<?php if ((isset($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone_mobile)&&!empty($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone_mobile))||(isset($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone)&&!empty($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone))) {?>
		<input type="hidden" name="night_phone_b" value="<?php if (isset($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone_mobile)&&!empty($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone_mobile)) {?><?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone_mobile, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php if (isset($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone)&&!empty($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone)) {?><?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_billing_address']->value->phone, ENT_QUOTES, 'UTF-8', true);?>
<?php }?><?php }?>" />
		<?php }?>
		<input type="hidden" name="address_override" value="1" />
		
		<?php $_smarty_tpl->tpl_vars["paypal_mx_total_discounts"] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getOrderTotal(true,Cart::ONLY_DISCOUNTS), null, 0);?>
		<?php if ($_smarty_tpl->tpl_vars['paypal_mx_total_discounts']->value==0) {?>
			<?php  $_smarty_tpl->tpl_vars['paypal_mx_product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['paypal_mx_product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value->getProducts(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["paypal_mx_products"]['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['paypal_mx_product']->key => $_smarty_tpl->tpl_vars['paypal_mx_product']->value) {
$_smarty_tpl->tpl_vars['paypal_mx_product']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["paypal_mx_products"]['index']++;
?>
				<input type="hidden" name="item_name_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(1, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" />
				<input type="hidden" name="amount_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(1, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo floatval(Tools::ps_round($_smarty_tpl->tpl_vars['paypal_mx_product']->value['price'],2));?>
" />
				<input type="hidden" name="quantity_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(1, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo intval($_smarty_tpl->tpl_vars['paypal_mx_product']->value['quantity']);?>
" />
			<?php } ?>
			<?php $_smarty_tpl->tpl_vars["paypal_mx_total_shipping"] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getOrderTotal(true,Cart::ONLY_SHIPPING), null, 0);?>
			<?php if ($_smarty_tpl->tpl_vars['paypal_mx_total_shipping']->value) {?>
				<input type="hidden" name="item_name_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(2, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo smartyTranslate(array('s'=>'Shipping','mod'=>'paypalmx'),$_smarty_tpl);?>
" />
				<input type="hidden" name="amount_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(2, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo floatval($_smarty_tpl->tpl_vars['paypal_mx_total_shipping']->value);?>
" />
				<input type="hidden" name="quantity_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['paypal_mx_products']['index']+htmlentities(2, ENT_QUOTES, 'UTF-8', true);?>
" value="1">
			<?php }?>
		<?php } else { ?>	
			<input type="hidden" name="item_name_1" value="<?php echo smartyTranslate(array('s'=>"Your order",'mod'=>"paypalmx"),$_smarty_tpl);?>
" />
			<input type="hidden" name="amount_1" value="<?php echo floatval($_smarty_tpl->tpl_vars['cart']->value->getOrderTotal(!$_smarty_tpl->tpl_vars['show_taxes']->value));?>
" />
		<?php }?>
		
		<input type="hidden" name="tax_cart" value="<?php echo floatval($_smarty_tpl->tpl_vars['paypal_mx_total_tax']->value);?>
" />
		
		<input type="hidden" name="notify_url" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_notify_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="return" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_return_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="cancel_return" value="<?php echo htmlentities($_smarty_tpl->tpl_vars['paypal_mx_cancel_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
		<input type="hidden" name="no_shipping" value="1" />
		<input type="hidden" name="bn" value="PrestashopUS_Cart" />
		<input id="paypal-standard-btn" type="image" name="submit" src="http://canalutv.mx/modules/paypalmx/img/paypal_cards.png" alt="" /> <?php echo smartyTranslate(array('s'=>'Pago con PayPal','mod'=>'paypalmx'),$_smarty_tpl);?>

	</p>
</form>

<p class="cart_navigation clearfix"><b>O si desea domiciliar con paypal haga click en el siguiente botón</b></p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_xclick-subscriptions">
<input type="hidden" name="business" value="info@canalutv.mx">
<input type="hidden" name="lc" value="MX">
<input type="hidden" name="item_name" value="Suscripcion Mensual al Canal Under TV">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="src" value="1">
<input type="hidden" name="a3" value="<?php echo floatval($_smarty_tpl->tpl_vars['cart']->value->getOrderTotal());?>
">
<input type="hidden" name="p3" value="1">
<input type="hidden" name="t3" value="M">
<input type="hidden" name="currency_code" value="MXN">
<input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHostedGuest">
<input type="image" src="https://c1.staticflickr.com/9/8222/29421354801_79ab5b04e4_o.png" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">  
<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
</form>


<hr><?php }} ?>
