<?php /* Smarty version Smarty-3.1.19, created on 2017-06-18 16:59:46
         compiled from "/home/canalutv/public_html/modules/banwire/views/templates/hook/payment_return_banwire.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12131390405946f7d2c390b9-11364386%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd97d1193b503ca4c13f305dc335fe257dd2983de' => 
    array (
      0 => '/home/canalutv/public_html/modules/banwire/views/templates/hook/payment_return_banwire.tpl',
      1 => 1466183079,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12131390405946f7d2c390b9-11364386',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this_path' => 0,
    'data' => 0,
    'cart' => 0,
    'cartProduct5' => 0,
    'total' => 0,
    'imprimir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5946f7d2cca163_73879380',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5946f7d2cca163_73879380')) {function content_5946f7d2cca163_73879380($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/canalutv/public_html/tools/smarty/plugins/modifier.replace.php';
?>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['this_path']->value;?>
modules/banwire/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['this_path']->value;?>
modules/banwire/css/layout.css" type="text/css" media="all">
                
<div id="content_confiramtion">
<!--<pre><small><?php echo print_r($_smarty_tpl->tpl_vars['data']->value);?>
</pre></small>-->
	<?php $_smarty_tpl->tpl_vars['cartProduct5'] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getProduct5($_smarty_tpl->tpl_vars['data']->value['Orden']), null, 0);?>
	<?php $_smarty_tpl->tpl_vars['cartProduct4'] = new Smarty_variable($_smarty_tpl->tpl_vars['cart']->value->getProduct4($_smarty_tpl->tpl_vars['data']->value['Orden']), null, 0);?>
	
    <div id='msg_confirmation'>
		<span><?php echo smartyTranslate(array('s'=>'¡GRACIAS POR SU COMPRA!','mod'=>'banwire'),$_smarty_tpl);?>
</span><br>
		<?php echo smartyTranslate(array('s'=>'El proceso de pago a través de BanWire ha sido completado, a continuación los detalles de su pago:','mod'=>'banwire'),$_smarty_tpl);?>

		<br><br>
		<?php if ($_smarty_tpl->tpl_vars['cartProduct5']->value) {?>
			<!--<span>Para activación o reactivación del servicio favor de comunicarse al teléfono  55 61 11 19 35 o al correo undertvmexico@yahoo.com</span>-->
			<span>En caso de que no tenga descargado su canal Under Tv en su roku, por favor envíe un whatsapp al +5215561111935</span><br>
			<span>HORARIO: LUNES A DOMINGO de 10am - 7pm</span><br>
			<span>FUERA DEL HORARIO NO SE CONTESTAN MENSAJES, NI SE HACEN DESCARGAS HASTA EL SIGUIENTE DIA EN EL HORARIO INDICADO.</span>
		<?php }?>
	</div>
    <div class='data_confirmation'>
	
	<?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['data']->value['TotalBruto'],"$",'')," ",''),".",''),",",''), null, 0);?>
	<img src="http://v2.afilio.com.br/sale.php?pid=1792&order_id=<?php echo $_smarty_tpl->tpl_vars['data']->value['Orden'];?>
&order_price=<?php echo $_smarty_tpl->tpl_vars['total']->value;?>
" border="0" width="1" height="1" />
	
	<center>
	<h2 style="font-size: 24px;">Formato de pago en tiendas OXXO</h2>
	<br/><p>Para realizar tu pago, imprime y presenta este comprobante en cualquier tienda OXXO de México</p><br/><br/>
	</center>

	<div style="margin: 0 0 0 50px;">
		<div class="row_data"><label><b>Orden: </b></label> <?php echo $_smarty_tpl->tpl_vars['data']->value['Orden'];?>
</div>
		<div class="row_data"><label><b>Total: </b></label> <?php echo $_smarty_tpl->tpl_vars['data']->value['Total'];?>
</div>
		<div class="row_data"><label><b>Código de barras: </b></label> <?php echo $_smarty_tpl->tpl_vars['data']->value['C_Barras'];?>
</div>
		<div class="row_data"><label><b>Vigencia: </b></label> <?php echo $_smarty_tpl->tpl_vars['data']->value['Vigencia'];?>
</div>
	</div>
	
	<div class="oxxo_codebar" style="text-align:center;width:370px;margin:50px auto 0;">
	    <div class="row_data"><label>&nbsp;</label> <img src="data:image/jpg;base64,<?php echo $_smarty_tpl->tpl_vars['data']->value['barcode_img'];?>
"></div>
	</div>
	<div class="oxxo_footer" style="text-align:center;width:800px;margin:50px auto 0;font-size: 16px;">
	<br/><br/><p style="text-align:left;">Este cupón es válido úncamente para realizar el pago correspondiente y dentro
	de la fecha de vigencia establecida. La acreditación del mismo es a las 24 hrs.
	de realizado, hasta entonces el pago podrá ser corroborado por el vendedor.
	Cualquier aclaración sobre la compra, favor de comunicarse con el vendedor.
	BanWire no se hace responsible por cualquier reclamo ó aclaración, es
	responsabilidad del vendedor resolver cualquier situación relacionada con la
	compra del producto o servicio adquirido.</p>

	<br/><br/><p style="text-align:left;">*Imprime el cupón de manera clara y legible, usa de preferencia impresora
	láser, Consérvalo en buen estado sin tachar ó doblar la parte del código de
	barras. En caso de no ser legible, la tienda puede rechazar el pago
	correspondiente.</p>

	</div>

    </div>
	
</div>

<?php if (isset($_smarty_tpl->tpl_vars['imprimir']->value)&&$_smarty_tpl->tpl_vars['imprimir']->value) {?>
<br/><p><a class="print_confirmation button-exclusive btn btn-default" href="#" title="Imprimir"><span>Imprimir</span></a></p><br/>
<script>
	function print_confirmation(){
		var tmp = $("#content_confiramtion").clone();	
		$("#msg_confirmation",tmp).remove();
		tmp.printArea();
		return false;
	}
	$(".print_confirmation").click(print_confirmation);
</script>
<?php }?>
<?php }} ?>
