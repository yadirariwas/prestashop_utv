{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
	{/foreach}
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{literal}		
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1651729265082937');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1651729265082937&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<!-- Google analytics -->
			<script>
				 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-70775493-1', 'auto');
				ga('send', 'pageview');
	
			</script>
		<!-- Fin analytics -->
		{/literal}		
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span></p>
			</div>
		{/if}
		<div id="page">
		
			<div class="header-container">
				<header id="header">
					<div class="banner">
						<div class="container">
							<div class="row">
								{hook h="displayBanner"}
							</div>
						</div>
					</div>
					<div>
						<div class="container">
							<div class="row">
								<div id="top_column" class="center_column col-xs-4 col-sm-4 navleft" style="text-align: center;">
									<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}" height="95 px"/>
								</div>
								<div id="top_column" class="right_column col-xs-8 col-sm-8">
									<div class="nav">
										<div class="container">
											<div class="row">
												<nav>
													{hook h="displayNav2"}
													{hook h="displayNav"}
												</nav>
											</div>
										</div>
									</div>
									<div>
										<div class="container">
											<div class="row">
												{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					{*
					<div class="nav">
						<div class="container">
							<div class="row">
								<nav>
									<div id="top_column" class="center_column col-xs-6 col-sm-6 navleft"></div>
									<div id="top_column" class="right_column col-xs-6 col-sm-6">{hook h="displayNav"}</div>
								</nav>
							</div>
						</div>
					</div>
					<div>
						<div class="container">
							<div class="row">
								<div id="header_logo">
									<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}" width="100 px" height="95 px"/>
									</a>
								</div>
								
								<div id="top_column" class="center_column col-xs-6 col-sm-6">
									<section id="social_block">
										<ul>
											<li class="facebook fb">
												<a target="_blank" href="https://www.facebook.com/pages/UNDER-TV/813070072147311">													
												</a>
											</li>
											<li class="twitter tt">
												<a target="_blank" href="https://twitter.com/undertv3">
												</a>
											</li>
										</ul>
									  
									</section>								
								</div>
								<!--<div id="top_column" class="center_column col-xs-2 col-sm-2 pfacebook">
									<ul>

									</ul>
								</div>	-->						
								
								{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
								
							</div>
						</div>
					</div>
					*}
				</header>
			</div>
			<div class="columns-container">
				<div id="columns" class="container">
					
					{if $page_name !='index' && $page_name !='pagenotfound'}
						
					{/if}
					
					<div class="row">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-12">
						<!-- <div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}"> -->
						<div id='iddivscript' style='display:none'></div>
	{/if}
		<!--{$page_name}
		<pre><small>{$cookie|@print_r}</small></pre>-->
		<!--{$cookie->isrecurrente}-->
	{if $page_name != 'order'}
		<script>
			resetRecurrente('{$cookie->id_customer}');
		</script>
	{/if}
	{if $page_name != 'category'}
		{if $page_name == 'order' or $page_name == 'module-banwire-payment'}
			{if $cookie->isregalo}
				<script>
					ResetRegalo('{$cookie->id_customer}', '{$page_name}', 'true');
				</script>
			{/if}
		{else}
		<script>
			ResetRegalo('{$cookie->id_customer}', '{$page_name}', 'false');
		</script>		
		{/if}
	{/if}
		<!--{$page_name}<br>
		es:: {$cookie->isregalo} <br>	-->
