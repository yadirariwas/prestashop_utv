{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Eduardo Neria <eneria@outlook.com>
*  @copyright  2015 Adimpacto
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">{l s='My account'}</a><span class="navigation-pipe">{$navigationPipe}</span><span class="navigation_page">{l s='My vouchers'}</span>{/capture}

<h1 class="page-heading">
	{l s='My accounts'}
</h1>

{if isset($accounts) && !empty($accounts)}
	<div id="order-detail-content" class="table_block table-responsive">
		<table id="cart_summary" class="table table-bordered {if $PS_STOCK_MANAGEMENT}stock-management-on{else}stock-management-off{/if}">
			<thead>
				<tr>
					<th class="cart_product first_item">{l s='User'}</th>
					<th class="cart_product item">{l s='Alias'}</th>
					<th class="cart_description item">{l s='Password'}</th>
					<th class="cart_unit item">{l s='Date Add'}</th>
					<th class="cart_quantity item">{l s='Payment Date'}</th>
					<th class="cart_total item">{l s='Price'}</th>
					<th class="cart_total item">{l s='Payment'}</th>
					<th class="item" style='width:15%'>{l s='Recurrente'}</th>
				</tr>
			</thead>
			<tbody>
			<!--<pre><small>{$accounts|@print_r}</small></pre>-->
				{foreach $accounts as $account}
					<tr>
						<td class="cart_quantity">
							{$account['user']}							
						</td>
						<td class="cart_quantity">
							<input id="user-{$account['user']}" class="form-control validate" type="text" value="{$account['alias']}" name="user" placeholder='alias'>
						</td>
						<td class="cart_quantity">
							{$account['clave_roku']}
						</td>					
						<td class="cart_quantity">
							{$account['fecha_creacion']|date_format:"%d-%m-%Y"}
						</td>
						<td class="cart_quantity">
							{$account['fecha_pago']|date_format:"%d-%m-%Y"}
						</td>					
						<td class="cart_quantity">							
							{if $account['grupo'] == 3}
								485
							{elseif $account['grupo'] == 7}
								450
							{elseif $account['grupo'] == 6}
								300							
							{elseif $account['grupo'] == 5}
								350	
							{elseif $account['grupo'] == 4}
								400															
							{/if}
						</td>	
						<td class="cart_quantity">
							{$account['tipo_pago']}
						</td>
						<td>
						{if !$account['recurrente_aplicado']}
							<div class='checker_r'>
								<table border = 0>
									<tr>
										<td>
											<input type="checkbox" class='validate_rec' id="rec-{$account['user']}-{$account['id_customer']}" value="Cheese"/>
										</td>
										<td>
											<span id='aplicar'>Aplicar</span>
										</td>
									</tr>
								</table>
								
							</div>
						{else}
							Aplicado
						{/if}
						</td>
					</tr>
				{/foreach}
			</tbody>
			<tfoot>
				<tr>
					<td>
					</td>
					<td>
						<a class="btn btn-default button button-small guardar">
							<span id='guardar_alias'>
								{l s='Guardar'}
							</span>
						</a>					
					<td colspan='5'>
					</td>
					<td>
						<a class="btn btn-default button button-small guardar">
							<span id='guardar_rec'>
								{l s='Recurrente'}
							</span>
						</a>
					</td>
				</tr>
			</tfoot>
		</table>
	</div> 	
{else}
	<p class="alert alert-warning">{l s='You do not have accounts.'}</p>
{/if}

<ul class="footer_links clearfix">
	<li>
		<a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
			<span>
				<i class="icon-chevron-left"></i> {l s='Back to your account'}
			</span>
		</a>
	</li>
	<li>
		<a class="btn btn-default button button-small" href="{$base_dir}">
			<span>
				<i class="icon-chevron-left"></i> {l s='Home'}
			</span>
		</a>
	</li>	
	<!--<li>
		<a class="btn btn-default button button-small guardar">
			<span id='guardar_alias'>
				{l s='Guardar'}
			</span>
		</a>
	</li>	
	<li>
		<a class="btn btn-default button button-small guardar">
			<span id='guardar_rec'>
				{l s='Recurrente'}
			</span>
		</a>
		<!--{assign var='idProduct' value='4'}
		<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$idProduct}&amp;qty=4&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$idProduct}">
			<span>{l s='Add to cart'}</span>
		</a>	
	</li>-->
</ul>
<div id='iddivscript' style='display:none'></div>

{literal}
<script type="text/javascript" charset="UTF-8">
$(function () {
	$("#guardar_alias").unbind('click').bind("click", function (event) {
		var usuarios = new Object();
		var cont = 1;
		$("#cart_summary input.validate").each(function (index, item) {
			var input = $(item);
			var id = $(item).attr('id');
			ids = id.split('-');
			usuario = ids[1];
			var value = $("#user-" + usuario).val();
			usuarios[cont++] = {user: usuario, alias: value};
		});	  
	  //console.log(usuarios);
	  
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_alias_user.php',
		  data: {customer_roku : usuarios}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data);
			alert('Se han registrado correctamente los alias para los usuarios.');
		},
		});	  
	});	

	$("#guardar_rec").unbind('click').bind("click", function (event) {
		var usuarios = new Object();
		var cont = 1;
		$("#cart_summary input.validate_rec").each(function (index, item) {
			var input = $(item);
			var id = $(item).attr('id');
			ids = id.split('-');
			id_usuario = ids[1];
			id_customer = ids[2];
			if($("#" + id).is(":checked")){
				//console.log('ok');
				usuarios[cont++] = {user: id_usuario, customer: id_customer};
			}
		});	 
		var new_cont = cont - 1;
		console.log(new_cont);
	  
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_recurrente.php',
		  data: {customer_roku : usuarios}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data);
			//alert('Se esta porocesando la información.');
			addCartCustom(new_cont)
			
		},
		});	  
	});		
	
	function addCartCustom(quantity){
		var idCombination = $('#idCombination').val();
		$.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: baseUri + '?rand=' + new Date().getTime(),
			async: true,
			cache: false,
			dataType : "json",
			data: 'controller=cart&add=1&ajax=true&qty=' + quantity + '&id_product=4' + '&token=' + static_token + ( (parseInt(idCombination) && idCombination != null) ? '&ipa=' + parseInt(idCombination): ''),
			success: function(jsonData,textStatus,jqXHR)
			{
				location.href = 'http://canalutv.mx/carrito';
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				var error = "Impossible to add the product to the cart.<br/>textStatus: '" + textStatus + "'<br/>errorThrown: '" + errorThrown + "'<br/>responseText:<br/>" + XMLHttpRequest.responseText;
				if (!!$.prototype.fancybox)
				    $.fancybox.open([
				    {
				        type: 'inline',
				        autoScale: true,
				        minHeight: 30,
				        content: '<p class="fancybox-error">' + error + '</p>'
				    }],
					{
				        padding: 0
				    });
				else
				    alert(error);
				//reactive the button when adding has finished
				if (addedFromProductPage)
					$('#add_to_cart button').removeProp('disabled').removeClass('disabled');
				else
					$(callerElement).removeProp('disabled');
			}
		});		
	}
});
</script>
{/literal}