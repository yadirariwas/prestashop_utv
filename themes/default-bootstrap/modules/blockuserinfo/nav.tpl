<!-- Block user information module NAV  -->
<div class="header_user_info">
	{if $is_logged}
		{assign var='cust_text' value=$cookie->customer_firstname|cat:' '|cat:$cookie->customer_lastname} 
		<a class="logout" href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			{l s='Sign out' mod='blockuserinfo'}
		</a>
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="logout" rel="nofollow"><span>{$cust_text|truncate:15:"":true}</span></a>
	{else}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			{l s='Sign in' mod='blockuserinfo'}
		</a>
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			Reg&iacute;strate
		</a>
	{/if}
</div>

<div class="header_user_info_mobile">
	{if $is_logged}
		
		<a class="logout" style="border: 1px solid #98c21d;" href="{$link->getPageLink('index', true, NULL, 'mylogout')|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			<i class="icon-user"></i>
		</a>
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="logout" rel="nofollow">
			<span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span>
		</a>
	{else}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<i class="icon-user"></i>
		</a>
	{/if}
	
</div>

<!-- /Block usmodule NAV -->