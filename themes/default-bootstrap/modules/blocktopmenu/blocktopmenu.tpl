{if $MENU != ''}
	<!-- Menu -->
<div class="row">
	
	<div class="col-xs-10 col-sm-10">
		<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
			<div class="cat-title"><i class="icon-reorder"></i>{* l s="Categories" mod="blocktopmenu" *}</div>
			<ul class="sf-menu clearfix menu-content">
				{$MENU}
				{if $MENU_SEARCH}
					<li class="sf-search noBack" style="float:right">
						<form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
							<p>
								<input type="hidden" name="controller" value="search" />
								<input type="hidden" value="position" name="orderby"/>
								<input type="hidden" value="desc" name="orderway"/>
								<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
							</p>
						</form>
					</li>
				{/if}
			</ul>
		</div>
	</div>
	<div id="top_column" class="center_column col-xs-2 col-sm-2">
		<section id="social_block" class="top_socialm" style="float: center;">
		{*
			<table>
				<tr>
					<td>
						<ul>
							<li class="facebook fb">
								<a target="_blank" href="https://www.facebook.com/pages/UNDER-TV/813070072147311"></a>
							</li>
						</ul>
					</td>
					<td>
						<ul>
							<li class="twitter tt">
								<a target="_blank" href="https://twitter.com/undertv3"></a>
							</li>
						</ul>
					</td>
				</tr>
			</table>
		  *}
		</section>			
	</div>		
</div>
	<!--/ Menu -->
{/if}
