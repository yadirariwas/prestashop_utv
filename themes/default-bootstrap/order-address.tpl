{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$opc}
	{assign var='current_step' value='address'}
	{capture name=path}{l s='Addresses'}{/capture}
	{assign var="back_order_page" value="order.php"}
	<h1 class="page-heading">{l s='Addresses'}</h1>
	{include file="$tpl_dir./order-steps.tpl"}
	{include file="$tpl_dir./errors.tpl"}
		<form action="{$link->getPageLink($back_order_page, true)|escape:'html':'UTF-8'}" method="post">
{else}
	{assign var="back_order_page" value="order-opc.php"}
	<h1 class="page-heading step-num"><span>1</span> {l s='Addresses'}</h1>
	<div id="opc_account" class="opc-main-block">
		<div id="opc_account-overlay" class="opc-overlay" style="display: none;"></div>
{/if}

	
<div class="addresses clearfix">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="address_delivery select form-group selector1">
				<label for="id_address_delivery">{if $cart->isVirtualCart()}{l s='Choose a billing address:'}{else}{l s='Choose a delivery address:'}{/if}</label>
				<select name="id_address_delivery" id="id_address_delivery" class="address_select form-control">
					{foreach from=$addresses key=k item=address}
						<option value="{$address.id_address|intval}"{if $address.id_address == $cart->id_address_delivery} selected="selected"{/if}>
							{$address.alias|escape:'html':'UTF-8'}
						</option>
					{/foreach}
				</select>
				<span class="waitimage"></span>
				</br></br></br><span class="form_info">{l s='Solo actualizar si es diferente a su dirección de entrega.'}</span>
			</div>
			<p class="checkbox addressesAreEquals"{if $cart->isVirtualCart()} style="display:none;"{/if}>
				<input type="checkbox" name="same" id="addressesAreEquals" value="1"{if $cart->id_address_invoice == $cart->id_address_delivery || $addresses|@count == 1} checked="checked"{/if} />
				<label for="addressesAreEquals">{l s='Use the delivery address as the billing address.'}</label>
			</p>
		</div>
		<!--holas<br><br>
		<pre><small>{$addresses|@print_r}</small></pre>-->
		<div class="col-xs-12 col-sm-6">
			<div id="address_invoice_form" class="select form-group selector1"{if $cart->id_address_invoice == $cart->id_address_delivery} style="display: none;"{/if}>
				{if $addresses|@count > 1}
					<label for="id_address_invoice" class="strong">{l s='Choose a billing address:'}</label>
					<select name="id_address_invoice" id="id_address_invoice" class="address_select form-control">
					{section loop=$addresses step=-1 name=address}
						<option value="{$addresses[address].id_address|intval}"{if $addresses[address].id_address == $cart->id_address_invoice && $cart->id_address_delivery != $cart->id_address_invoice} selected="selected"{/if}>
							{$addresses[address].alias|escape:'html':'UTF-8'}
						</option>
					{/section}
					</select><span class="waitimage"></span>
				{else}
					<a href="{$link->getPageLink('address', true, NULL, "back={$back_order_page}?step=1&select_address=1{if $back}&mod={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Add'}" class="button button-small btn btn-default">
						<span>
							{l s='Add a new address'}
							<i class="icon-chevron-right right"></i>
						</span>
					</a>
				{/if}
			</div>
		</div>
	</div> 
	<!-- end row -->
	{assign var='esregalo' value=$cart->getisRegalo($cookie->id_customer)}	
	{if $esregalo}
		{assign var='dataCustomer' value=$cart->getDataCustomerRegalo($cookie->id_customer)}
		<div class="col-xs-12 col-sm-6">
			<ul id="delivery_address" class="address item box">
				<li><h3 class="page-subheading">{l s='Dirección de entrega'}&nbsp;<span class="address_alias">(Recibe regalo)</span></h3></li>
				<li class="address_name">{$dataCustomer[0]['nombrecompleto']}</li>
				<li class="address_address1">{$dataCustomer[0]['direccion']}</li>
				<li class="address_address2">{$dataCustomer[0]['direccion2']}</li>
				<li class="address_city">{$dataCustomer[0]['name']}</li>
				<li class="address_country">México</li>
				<li class="address_country">{$dataCustomer[0]['phone_mobile']}</li>
			</ul>
		</div>	
		
		<div class="col-xs-12 col-sm-6">
			<ul id="delivery_address" class="address item box">
				<li><h3 class="page-subheading">{l s='Dirección de facturación'}&nbsp;<span class="address_alias"></span></h3></li>
				<li class="address_name">{$addresses[0]['firstname']} {$addresses[0]['lastname']}</li>
				<li class="address_address1">{$addresses[0]['address1']}</li>
				<li class="address_address2">{$addresses[0]['address2']}</li>
				<li class="">{$addresses[0]['postcode']} {$addresses[0]['city']}</li>
				<li class="">{$addresses[0]['state']|escape:'html':'UTF-8'}</li>
				<li class="">{$addresses[0]['country']|escape:'html':'UTF-8'}</li>
				<li class="">{$addresses[0]['phone_mobile']|escape:'html':'UTF-8'}</li>
			</ul>
		</div>			
	{else}			
		<div class="row">
			<div class="col-xs-12 col-sm-6"{if $cart->isVirtualCart()} style="display:none;"{/if}>
				<ul class="address item box sdff" id="address_delivery">
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6">
				<ul class="address alternate_item{if $cart->isVirtualCart()} full_width{/if} box" id="address_invoice">
				</ul>
			</div>
		</div>
	{/if}
	<!-- end row -->
	<p class="address_add submit">
		<a href="{$link->getPageLink('address', true, NULL, "back={$back_order_page}?step=1{if $back}&mod={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Add'}" class="button button-small btn btn-default">
			<span>{l s='Add a new address'}<i class="icon-chevron-right right"></i></span>
		</a>
	</p>
	{assign var='cartProducts' value=$cart->getProducts()}
	{assign var='idProducto' value=$cartProducts[0]['id_product']}
	{assign var='compras' value=$cookie->rokus}
	<!--{$compras}-->
	{assign var='descripcion_compra' value="/"|explode:$compras} 
	{assign var='texto' value=""}

	{foreach $descripcion_compra as $cmp}
		{assign var='compra_usuario' value=$cmp}
		{assign var='user' value=","|explode:$compra_usuario}
			<!--{$user|print_r}-->
		{if $user[2] > 0}

			<!--<label>{$user[1]} meses para el usuario: {$user[1]}</label><br/>-->
			{if $texto == ''}
				{assign var='texto' value=$texto|cat:$user[2]|cat:" meses para el usuario: "|cat:$user[1]}
			{else}
				{assign var='texto' value=$texto|cat:", "|cat:$user[2]|cat:" meses para el usuario: "|cat:$user[1]}
			{/if}
		{/if}
	{/foreach}
			{if $texto != ''}
				{assign var='texto' value=$texto|cat:"."}
			{/if}	
	{if $idProducto == 4}
		{if !$opc}
			<div id="ordermsg" class="form-group">
				<label>{l s='Los usuarios que esta pagando son:'}</label>
				<!--{if isset($oldMessage)}{$oldMessage}{/if}-->
				<textarea class="form-control" cols="60" rows="6" name="message" readonly>{if isset($texto)}{$texto}{/if}</textarea>		
			</div>
		{/if}	
	{else}
		{if !$opc}
			<div id="ordermsg" class="form-group">
				<label>{l s='If you would like to add a comment about your order, please write it in the field below.'}</label>
				<!--{if isset($oldMessage)}{$oldMessage}{/if}-->
				<textarea class="form-control" cols="60" rows="6" name="message"></textarea>
			</div>
		{/if}
	{/if}
</div> <!-- end addresses -->
{if !$opc}
			<p class="cart_navigation clearfix">
				<input type="hidden" class="hidden" name="step" value="2" />
				<input type="hidden" name="back" value="{$back}" />
				<a href="{$link->getPageLink($back_order_page, true, NULL, "step=0{if $back}&back={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Previous'}" class="button-exclusive btn btn-default">
					<i class="icon-chevron-left"></i>
					{l s='Continue Shopping'}
				</a>
				<button type="submit" name="processAddress" class="button btn btn-default button-medium">
					<span>{l s='Proceed to checkout'}<i class="icon-chevron-right right"></i></span>
				</button>
			</p>
		</form>
{else}
	</div> <!--  end opc_account -->
{/if}
{strip}
{if !$opc}
	{addJsDef orderProcess='order'}
	{addJsDef currencySign=$currencySign|html_entity_decode:2:"UTF-8"}
	{addJsDef currencyRate=$currencyRate|floatval}
	{addJsDef currencyFormat=$currencyFormat|intval}
	{addJsDef currencyBlank=$currencyBlank|intval}
	{addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
	{addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
	{addJsDefL name=CloseTxt}{l s='Submit' js=1}{/addJsDefL}
{/if}
{capture}{if $back}&mod={$back|urlencode}{/if}{/capture}
{capture name=addressUrl}{$link->getPageLink('address', true, NULL, 'back='|cat:$back_order_page|cat:'?step=1'|cat:$smarty.capture.default)|escape:'quotes':'UTF-8'}{/capture}
{addJsDef addressUrl=$smarty.capture.addressUrl}
{capture}{'&multi-shipping=1'|urlencode}{/capture}
{addJsDef addressMultishippingUrl=$smarty.capture.addressUrl|cat:$smarty.capture.default}
{capture name=addressUrlAdd}{$smarty.capture.addressUrl|cat:'&id_address='}{/capture}
{addJsDef addressUrlAdd=$smarty.capture.addressUrlAdd}
{addJsDef formatedAddressFieldsValuesList=$formatedAddressFieldsValuesList}
{addJsDef opc=$opc|boolval}
{capture}<h3 class="page-subheading">{l s='Your billing address' js=1}</h3>{/capture}
{addJsDefL name=titleInvoice}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<h3 class="page-subheading">{l s='Your delivery address' js=1}</h3>{/capture}
{addJsDefL name=titleDelivery}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<a class="button button-small btn btn-default" href="{$smarty.capture.addressUrlAdd}" title="{l s='Update' js=1}"><span>{l s='Update' js=1}<i class="icon-chevron-right right"></i></span></a>{/capture}
{addJsDefL name=liUpdate}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{/strip}