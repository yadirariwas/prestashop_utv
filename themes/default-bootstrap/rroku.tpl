{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<h1 class="page-heading">{if !isset($email_create)}{l s='Quiero regalar roku'}{else}{l s='Create an account'}{/if}</h1>
{if isset($back) && preg_match("/^http/", $back)}{assign var='current_step' value='login'}{include file="$tpl_dir./order-steps.tpl"}{/if}
{include file="$tpl_dir./errors.tpl"}
{assign var='stateExist' value=false}
{assign var="postCodeExist" value=false}
{assign var="dniExist" value=false}

<div id='register_persona'>
<form action="" method="post" id="account-creation_form" class="std box">
		{$HOOK_CREATE_ACCOUNT_TOP}
		<div class="account_creation">
			<h3 class="page-subheading">{l s='Datos de la persona a la que va a regalar'}</h3>
			<div class="clearfix">
				<label>{l s='Tratamiento'}</label>
				<br />
				{foreach from=$genders key=k item=gender}
					<div class="radio-inline">
						<label for="id_gender{$gender->id}" class="top">
							<input type="radio" name="id_gender" id="id_gender" value="{$gender->id}" {if isset($smarty.post.id_gender) && $smarty.post.id_gender == $gender->id}checked="checked"{/if} />
						{$gender->name}
						</label>
					</div>
				{/foreach}
			</div>
			<div id='nombre' class="required form-group">
				<label for="customer_firstname">{l s='Nombre'} <sup><font color="red">*</font></sup></label>
				<input placeholder='Nombre' onkeyup="$('#firstname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" id="customer_firstname" name="customer_firstname" value="{if isset($smarty.post.customer_firstname)}{$smarty.post.customer_firstname}{/if}" />
			</div>
			<div id='apellido' class="required form-group">
				<label for="customer_lastname">{l s='Apellido'} <sup><font color="red">*</font></sup></label>
				<input placeholder='Apellido' onkeyup="$('#lastname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" id="customer_lastname" name="customer_lastname" value="{if isset($smarty.post.customer_lastname)}{$smarty.post.customer_lastname}{/if}" />
			</div>
			<div class="required form-group">
				<label for="phone_invoice">{l s='Un teléono de contacto'}</label>
				<input placeholder='teléfono' type="text" class="form-control" name="phone_invoice" id="phone_invoice" />
			</div>						
			<div id='correo' class="required form-group">
				<label for="email">{l s='Correo electrónico'} <sup><font color="red">*</font></sup></label>
				<input placeholder='correo electrónico' type="text" class="is_required validate form-control" data-validate="isEmail" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" />
			</div>
			<div id='direccion' class="required form-group">
				<label for="address1">
					Dirección
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Dirección' id="address1" class="is_required validate form-control" type="text" value="" name="address1" data-validate="isAddress">
				<span class="form_info">Poner la dirección donde quiere que se haga la entrega.</span>
			</div>			
			<div id='colonia' class="required form-group">
				<label for="address2">
					Colonia
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Colonia' id="address2" class="is_required validate form-control" type="text" value="" name="address2" data-validate="isAddress">
			</div>			
			<div id='cp' class="required form-group unvisible" style="display: block;">
				<label for="codigo_postal">
					Código postal
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Código postal' id="codigo_postal" class="is_required validate form-control uniform-input text" type="text" value="" name="codigo_postal" data-validate="isPostCode">
			</div>			
			<div id='ciudad' class="required form-group">
				<label for="city">
					Ciudad
					<sup><font color="red">*</font></sup>
				</label>
				<input placeholder='Ciudad' id="city" class="is_required validate form-control" type="text" maxlength="64" value="" name="city" data-validate="isCityName">
			</div>		
		
			<!--<div class="required id_state form-group">
				<label for="id_state">{l s='State'} <sup><font color="red">*</font></sup></label>
				<select name="id_state" id="id_state" class="form-control">
					<option value="">-</option>
				</select>
			</div>-->
			
			<div class="required form-group">
				<label for="id_estado">{l s='Estado'} <sup><font color="red">*</font></sup></label>
				<select id="id_estado" class="form-control" name="id_estado">
					<option value="">-</option>
					<option value="54">Aguascalientes</option>
					<option value="55">Baja California</option>
					<option value="56">Baja California Sur</option>
					<option value="57">Campeche</option>
					<option value="58">Chiapas</option>
					<option value="59">Chihuahua</option>
					<option value="60">Coahuila</option>
					<option value="61">Colima</option>
					<option value="62">Distrito Federal</option>
					<option value="63">Durango</option>
					<option value="68">Estado de México</option>
					<option value="64">Guanajuato</option>
					<option value="65">Guerrero</option>
					<option value="66">Hidalgo</option>
					<option value="67">Jalisco</option>
					<option value="69">Michoacán</option>
					<option value="70">Morelos</option>
					<option value="71">Nayarit</option>
					<option value="72">Nuevo León</option>
					<option value="73">Oaxaca</option>
					<option value="74">Puebla</option>
					<option value="75">Querétaro</option>
					<option value="76">Quintana Roo</option>
					<option value="77">San Luis Potosí</option>
					<option value="78">Sinaloa</option>
					<option value="79">Sonora</option>
					<option value="80">Tabasco</option>
					<option value="81">Tamaulipas</option>
					<option value="82">Tlaxcala</option>
					<option value="83">Veracruz</option>
					<option value="84">Yucatán</option>
					<option value="85">Zacatecas</option>
				</select>				
			</div>

			<div id='validate_estado' class="alert alert-danger-two" style='display:none'>
				<ol>
					<li>Debe seleccionar un estado.</li>
				</ol>
			</div>
			
			<div class="required form-group">
				<label for="id_pais">{l s='País'} <sup><font color="red">*</font></sup></label>
				<select id="id_pais" class="form-control" name="id_pais">
				  <option selected="selected" value="145">México</option>
				</select>				
			</div>			

		</div>


		{$HOOK_CREATE_ACCOUNT_FORM}
		<div class="submit clearfix">
			<input type="hidden" name="email_create" value="1" />
			<input type="hidden" name="is_new_customer" value="1" />
			{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
						<a class="btn btn-default button button-medium guardar">
							<span id='guardar_regalo'>
								{l s='Regalar Roku'}
							</span>
						</a>			
			<p class="pull-right required"><span><sup>*</sup>{l s='Campo requerido'}</span></p>
		</div>
	</form>
</div>
<div id='iddivscript'></div>


{literal}
<script type="text/javascript" charset="UTF-8">

function validateForm(){
	var breturn = true;
	
	if($('#customer_firstname').val() == ''){
	  $('#nombre').addClass('form-error');
	  breturn = false;
	}	

	if($('#customer_lastname').val() == ''){
	  $('#apellido').addClass('form-error');
	  breturn = false;
	}	

	if($('#email').val() == ''){
	  $('#correo').addClass('form-error');
	  breturn = false;
	}	

	if($('#address1').val() == ''){
	  $('#direccion').addClass('form-error');
	  breturn = false;
	}	

	if($('#codigo_postal').val() == ''){
	  $('#cp').addClass('form-error');
	  breturn = false;
	}	

	if($('#address2').val() == ''){
	  $('#colonia').addClass('form-error');
	  breturn = false;
	}

	if($('#city').val() == ''){
	  $('#ciudad').addClass('form-error');
	  breturn = false;
	}	

	if($('#id_estado').val() == ''){
	  $('#validate_estado').css('display', 'block');
	  breturn = false;
	}		
	
	return breturn;
}

$(function () {
	$("#guardar_regalo").unbind('click').bind("click", function (event) {	 
		validateForm();
		if (!validateForm()) {
			alert('No se puede guardar el regalo, se encontraron los siguientes errores:');
			return false;
		}		
		$.ajax({
		  type: 'POST',
		  url: baseDir + 'modules/gestionuser/save_user_regalo.php',
		  data: {id_gender : $("#account-creation_form input[type='radio']:checked").val(), nombre : $("#customer_firstname").val(), apellido : $("#customer_lastname").val(), 
			email : $("#email").val(), address1 : $('#address1').val(), codigo_postal : $('#codigo_postal').val(), address2 : $('#address2').val(), city : $('#city').val(), 
			id_estado : $('#id_estado').val(), id_pais : $('#id_pais').val(), phone_invoice : $('#phone_invoice').val()}, 
		  dataType: 'html',
		  success: function (data, textStatus) {
			$("#iddivscript").html(data); 
			location.href = 'http://canalutv.mx/14-tienda';
		},
		});	  
	});	
 		
	
});
</script>
{/literal}