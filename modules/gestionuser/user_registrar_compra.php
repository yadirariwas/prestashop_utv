<?php

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/../../classes/Cookie.php');

$data = $_POST['usuarios'];
$id_cart = $_POST['carro'];
$id_customer = $_POST['customer'];
$price = $_POST['price'];

$users = explode("/", $data);

Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'shopping_cart WHERE id_cart = "'.$id_cart.'" AND id_customer = "'.$id_customer.'"');

foreach($users as $user){
	$compra_usuario = explode(",", $user);
	if($compra_usuario[2] > 0){
		$sql = 'INSERT INTO '._DB_PREFIX_.'shopping_cart 
			(id_cart, usuario, cantidad, id_customer) 
			VALUES (
			"'.$id_cart.'", 
			"'.$compra_usuario[1].'", 
			"'.$compra_usuario[2].'",
			"'.$id_customer.'")';
		Db::getInstance()->execute($sql);		
	}
}

$context = Context::getContext();
$context->cookie->__set('price_total',$price);
$context->cookie->write();