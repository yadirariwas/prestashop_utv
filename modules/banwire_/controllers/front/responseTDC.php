<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

class BanwireResponseTDCModuleFrontController extends ModuleFrontController
{
	
	public function initContent()
	{
		if(isset ($_POST['ORD_ID']) )  {
			$this->saveLB($_POST);
			$order = new Order((int)Order::getOrderByCartId( $_POST['ORD_ID'] ));
			
			if($order->total_paid != null && isset($_POST['AUTH_CODE']) ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_TDC_ACEPTADA'), $order, true);
				$new_history->addWithemail(true);
			} else {
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_TDC_DENEGADA'), $order, true);
				$new_history->addWithemail(true);	
			} 
		}else exit('No post');
		exit();
	}
	private function saveLB($post) {
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 
							id_customer = '0', 
							id_cart = '" . $post['ORD_ID'] . "', 
							response = '" . serialize($post) . "', 
							send = 'response TDC-Challenge', 
							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 
							date_add = NOW();"
						);	
	}
	
}
