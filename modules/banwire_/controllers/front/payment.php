<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */
class BanwirePaymentModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $display_column_left = false;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		/*
		$angel3["Murrieta.emg@gmail.com"]="2015-12-03";
		$angel3["affernan@radioformula.com.mx"]="2015-11-04";
		$angel3["affernan@radioformula.com.mx"]="2015-11-04";
		$angel3["mauro_plantabaja@hotmail.com"]="2015-11-04";
		$angel3["ricardo.kerber@gmail.com"]="2015-07-06";
		$angel3["rafgonz43@yahoo.com"]="2015-12-06";
		$angel3["gflores@me.com"]="2015-07-07";
		$angel3["bladic@hotmail.com"]="2015-07-07";
		$angel3["maragarza@gmail.com"]="2015-07-07";
		$angel3["sofia_mtz5@hotmail.com"]="2015-07-07";
		$angel3["joselbq@yahoo.com"]="2015-08-07";
		$angel3["laila.garciag@hotmail.com"]="2015-08-07";
		$angel3["mzubiria@mac.com"]="2015-08-07";
		$angel3["federicosf@gmail.com"]="2015-09-07";
		$angel3["fredo.abrantes@gmail.com"]="2015-09-07";
		$angel3["jsauquet@gmail.com"]="2015-09-07";
		$angel3["marthagalas@gmail.com"]="2015-09-07";
		$angel3["manuel.castellanosc@gmail.com"]="2015-09-07";
		$angel3["Ro_avendano@hotmail.com"]="2015-10-07";
		$angel3["guillermo.lts@gmail.com"]="2015-10-07";
		$angel3["jorgeeduardo15@hotmail.com"]="2015-10-07";
		$angel3["lgonzalez@inpamex.com"]="2015-10-07";
		$angel3["visionk6@gmail.com"]="2015-10-07";
		$angel3["barbaraizzo@me.com"]="2015-11-07";
		$angel3["juan.f.mediavilla@gmail.com"]="2015-11-07";
		$angel3["nataliagarza@gmail.com"]="2015-11-07";
		$angel3["pindterbertha@gmail.com"]="2015-12-07";
		$angel3["inigomat@yahoo.com.mx"]="2015-12-07";
		$angel3["joseroman.garzon@gmail.com"]="2015-12-07";
		$angel3["alfonso_mtzr@hotmail.com"]="2015-12-07";
		$angel3["ricardodigo@hotmail.com"]="2015-12-07";
		$angel3["gerardjaguar@yahoo.com.mx"]="2015-07-13";
		$angel3["patricioalcocer@yahoo.com"]="2015-07-13";
		$angel3["kps@chelsearf.com"]="2015-07-13";
		$angel3["nizarclemente@gmail.com"]="2015-07-13";
		$angel3["kps@chelsearf.com"]="2015-07-14";
		$angel3["macahaca@gmail.com"]="2015-07-14";
		$angel3["gerenciawtc@teikit.mx"]="2015-07-14";
		$angel3["agaskdro@yahoo.com.mx"]="2015-07-14";
		$angel3["karlacapote@hotmail.com"]="2015-07-15";
		$angel3["lidibc@hotmail.com"]="2015-07-15";
		$angel3["mnhayito@hotmail.com"]="2015-07-15";
		$angel3["icastro@adimpacto.com"]="2015-07-15";
		$angel3["alejandroprats90@gmail.com"]="2015-07-15";
		$angel3["dondelees@gmail.com"]="2015-07-15";
		$angel3["adelarosa0728@yahoo.com"]="2015-07-16";
		$angel3["edivelazquez@hotmail.com"]="2015-07-16";
		$angel3["mankev@me.com"]="2015-07-16";
		$angel3["gildamgarciag@gmail.com"]="2015-07-16";
		$angel3["gretta27@gmail.com"]="2015-07-16";
		$angel3["juan_antoniohernandez@hotmail.com"]="2015-07-16";
		$angel3["centenomaria@gmail.com"]="2015-07-16";
		$angel3["melisaromo@gmail.com"]="2015-07-16";
		$angel3["Ugow@gaam.com.mx"]="2015-07-16";
		$angel3["bibist@hotmail.com"]="2015-07-17";
		$angel3["iabiega@yahoo.com"]="2015-07-17";
		$angel3["normacantu@me.com"]="2015-07-17";
		$angel3["vela_f@hotmail.com"]="2015-07-17";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["japedreroh@gmail.com"]="2015-07-18";
		$angel3["mda@chelsearf.com"]="2015-07-18";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["asdz58@hotmail.com"]="2015-07-19";
		$angel3["sobando@yahoo.com"]="2015-07-19";
		$angel3["fraga.emilio@gmail.com"]="2015-07-19";
		$angel3["fdeovando@hotmail.com"]="2015-07-19";
		$angel3["javfolch@mac.com"]="2015-07-19";
		$angel3["mestradag18@gmail.com"]="2015-07-19";
		$angel3["igavidal@gmail.com"]="2015-07-19";
		$angel3["schufani@chufani.com"]="2015-07-19";
		$angel3["garr231@interhabita.com.mx"]="2015-07-20";
		$angel3["jibarrola@lico.mx"]="2015-07-20";
		$angel3["monicagr22@hotmail.com"]="2015-07-20";
		$angel3["virues0@gmail.com"]="2015-07-21";
		$angel3["jorgegcastaneda@gmail.com"]="2015-07-21";
		$angel3["mallitaxoxo93@gmail.com"]="2015-07-21";
		$angel3["Credelag@gmail.com"]="2015-07-22";
		$angel3["david.perezsalinas@gmail.com"]="2015-07-22";
		$angel3["gcch@eog.mx"]="2015-07-22";
		$angel3["garciagus2003@yahoo.com"]="2015-07-22";
		$angel3["humbertocavazos@yahoo.com"]="2015-07-22";
		$angel3["humberto@cavazosflores.com"]="2015-07-22";
		$angel3["javier.amador@me.com"]="2015-07-23";
		$angel3["cellobene@hotmail.com"]="2015-07-23";
		$angel3["cecmc@me.com"]="2015-07-23";
		$angel3["federicobelden@gmail.com"]="2015-07-23";
		$angel3["lugaudi@gmail.com"]="2015-07-23";
		$angel3["drbetochas@gmail.com"]="2015-07-23";
		$angel3["mauro_308@hotmail.com"]="2015-07-24";
		$angel3["diego.fernandez@matesa.net"]="2015-07-24";
		$angel3["danperezsalazar@gmail.com"]="2015-07-24";
		$angel3["hazel_sandoval@hotmail.com"]="2015-07-24";
		$angel3["Javier@amrlabogados.mx"]="2015-07-24";
		$angel3["ropodo@hotmail.com"]="2015-07-24";
		$angel3["taniadiaz777@hotmail.com"]="2015-07-25";
		$angel3["queridomario@gmail.com"]="2015-07-25";
		$angel3["chabatito@yahoo.com.mx"]="2015-07-25";
		$angel3["pablosozzi@hotmail.com"]="2015-07-25";
		$angel3["amhilairefraga@hotmail.com"]="2015-07-25";
		$angel3["mariainesprats@gmail.com"]="2015-07-26";
		$angel3["giocerritelli@me.com"]="2015-07-26";
		$angel3["matute@hotmail.com"]="2015-07-26";
		$angel3["plavalleg@hotmail.com"]="2015-07-26";
		$angel3["susana-fernandez@matesa.net"]="2015-07-26";
		$angel3["sonia_bdm@hotmail.com"]="2015-07-26";
		$angel3["a_martinez_c@yahoo.com"]="2015-07-27";
		$angel3["federicobeldensada@gmail.com"]="2015-07-27";
		$angel3["jorge@recreoentretenimiento.com"]="2015-07-27";
		$angel3["Mab3067@gmail.com"]="2015-07-27";
		$angel3["nirvana_heaven@hotmail.com"]="2015-07-28";
		$angel3["almuval86@yahoo.com"]="2015-07-28";
		$angel3["Fjkm33@gmail.com"]="2015-07-28";
		$angel3["gerardomorancastellot@gmail.com"]="2015-07-28";
		$angel3["Leogarcia7@gmail.com"]="2015-07-28";
		$angel3["liliabh29@hotmail.com"]="2015-07-28";
		$angel3["mtt99@hotmail.com"]="2015-07-28";
		$angel3["susre@hotmail.com"]="2015-07-28";
		$angel3["mlarregui@hotmail.com"]="2015-07-28";
		$angel3["direccion@luxorytransfers.com"]="2015-07-29";
		$angel3["monicagzzfigueroa@gmail.com"]="2015-07-29";
		$angel3["pablozuelos@gmail.com"]="2015-07-29";
		$angel3["angaha-1001@hotmail.com"]="2015-07-29";
		$angel3["rlopez66@hotmail.com"]="2015-07-29";
		$angel3["caneggle87@gmail.com"]="2015-07-29";
		$angel3["virues0@gmail.com"]="2015-07-31";
		$angel3["lopez.jorgej@gmail.com"]="2015-07-31";
		$angel3["ARQ.KARLARMZ@GMAIL.COM"]="2015-07-31";
		$angel3["lavazquez001@hotmail.com"]="2015-07-31";
		$angel3["pitaglezs@yahoo.com.mx"]="2015-07-08";
		$angel3["mariolozano21@hotmail.com"]="2015-10-08";
		$angel3["mzubiriam@hotmail.com"]="2015-11-08";
		$angel3["antonio.herrero@deltagaming.com.mx"]="2015-08-13";
		$angel3["yessics@hotmail.com"]="2015-08-14";
		$angel3["fernanda.delafuente@gmail.com"]="2015-08-16";
		$angel3["nunziarojodelavega@gmail.com"]="2015-08-16";
		$angel3["scullenp@gmail.com"]="2015-08-18";
		$angel3["valeriant@gmail.com"]="2015-08-20";
		$angel3["jorgecarlos@inteligencialaboral.com"]="2015-08-22";
		$angel3["mariagaha@me.com"]="2015-08-23";
		$angel3["monicasanz@t-visa.com.mx"]="2015-08-26";
		$angel3["rokudf@gmail.com"]="2015-08-27";
		$angel3["fidelraul10191963@gmail.com"]="2015-08-27";
		$angel3["genaf@me.com"]="2015-08-27";
		$angel3["lgmartinez@almex.com.mx"]="2015-08-29";
		$angel3["Dandelrio@hotmail.com"]="2015-08-31";
		$angel3["ltostado@hotmail.com"]="2015-07-09";
		$angel3["robertofmiles@gmail.com"]="2015-09-09";
		$angel3["jsauquet@gmail.com"]="2015-11-09";
		$angel3["osalinas@tsigns.com.mx"]="2015-12-09";
		$angel3["osalinas@tsigns.com.mx"]="2015-09-13";
		$angel3["rherrerias@rhcei.mx"]="2015-09-13";
		$angel3["rherrerias@rhcei.mx"]="2015-09-14";
		$angel3["eclavijo@integrapostventa.com"]="2015-09-17";
		$angel3["anita_sada@hotmail.com"]="2015-09-18";
		$angel3["teransfam@gmail.com"]="2015-09-18";
		$angel3["salomonmansur@gmail.com"]="2015-09-23";
		$angel3["alberto@grupofrisa.com"]="2015-09-24";
		$angel3["alejandro@amrlabogados.mx"]="2015-09-27";
		$angel3["jrubenhernandez@hotmail.com"]="2015-10-17";
		$angel3["lcasamayor@gmail.com"]="2015-10-20";
		$angel3["jflozano@rimmsa.com"]="2015-11-15";
		$angel3["misaza55@yahoo.com.mx"]="2015-11-27";
		$angel3["mankev@me.com"]="2015-10-12";
		$angel3["morancastellot@yahoo.com.mx"]="2015-12-17";
		$angel3["e.o.vieira@gmail.com"]="2015-12-24";
		$angel3["jalazvar@yahoo.com.mx"]="2015-12-26";
		$angel3["rokudf@gmail.com"]="2015-12-31";
		$angel3["joaoalexandrevasconcelos@gmail.com"]="2016-02-01";
		$angel3["alesscavazos@gmail.com"]="2016-01-22";
		$angel3["jorgecarlos@cavazos.com"]="2016-01-22";
		$angel3["monica67cavchen@live.com.mx"]="2016-01-22";
		$angel3["lau.cardona@hotmail.com"]="2016-04-03";
		$angel3["martinfuentes@gmail.com"]="2016-01-05";
		$angel3["hpaliza@gmail.com"]="2016-10-05";	
		*/
		
		$angel3["aaya@sunedison.com"]="2015-08-06";
		$angel3["c4martell@gmail.com"]="2015-08-05";
		$angel3["pindterbertha@gmail.com"]="2015-07-12";
		$angel3["adri1calderon@gmail.com"]="2015-08-01";
		$angel3["asdz58@hotmail.com"]="2015-08-19";
		$angel3["mlopez@cpiproductos.com.mx"]="2015-08-05";
		$angel3["gerardjaguar@yahoo.com.mx"]="2015-07-13";
		$angel3["alberto@grupofrisa.com"]="2015-09-24";
		$angel3["patricioalcocer@yahoo.com"]="2015-07-13";
		$angel3["alejandro@delgado.com"]="2015-10-01";
		$angel3["alejandro.fernandez@matesa.net"]="2015-04-24";
		$angel3["nirvana_heaven@hotmail.com"]="2015-07-28";
		$angel3["alejandroespinosa@correoinfinitum.com"]="2015-08-04";
		$angel3["alesscavazos@gmail.com"]="2015-01-22";
		$angel3["rokudf@gmail.com"]="2015-08-27";
		$angel3["a_martinez_c@yahoo.com"]="2015-07-27";
		$angel3["marinalejandro@mac.com"]="2015-08-04";
		$angel3["almuval86@yahoo.com"]="2015-07-28";
		$angel3["prismav@yahoo.com"]="2015-03-23";
		$angel3["asg.1973@gmail.com"]="2015-09-06";
		$angel3["anac.almaguer@hotmail.com"]="2015-11-21";
		$angel3["javier.amador@me.com"]="2015-07-23";
		$angel3["Ro_avendano@hotmail.com"]="2015-08-10";
		$angel3["alejandro@amrlabogados.mx"]="2015-09-27";
		$angel3["anita_sada@hotmail.com"]="2015-09-18";
		$angel3["antonio.herrero@deltagaming.com.mx"]="2015-08-01";
		$angel3["antonio.herrero@deltagaming.com.mx"]="2015-08-13";
		$angel3["mauro_308@hotmail.com"]="2015-07-24";
		$angel3["lo8a61@gmail.com"]="2015-07-06";
		$angel3["adrianaparedeso@hotmail.com"]="2015-08-02";
		$angel3["aperez1105@gmail.com"]="2015-10-08";
		$angel3["adelarosa0728@yahoo.com"]="2015-07-16";
		$angel3["rt@wallakholding.com"]="2015-04-13";
		$angel3["balta@cavazos.com"]="2015-07-06";
		$angel3["blanca_blanco2007@hotmail.com"]="2015-11-26";
		$angel3["kps@chelsearf.com"]="2015-07-14";
		$angel3["lbenjami@hotmail.com"]="2015-04-29";
		$angel3["lbenjami@hotmail.com"]="2015-04-29";
		$angel3["cellobene@hotmail.com"]="2015-07-23";
		$angel3["sarkislara@gmail.com"]="2015-03-18";
		$angel3["barbaraizzo@me.com"]="2015-07-11";
		$angel3["bruno_reynoso@hotmail.com"]="2015-10-18";
		$angel3["liga_te@yahoo.com.mx"]="2015-06-16";
		$angel3["misaza55@yahoo.com.mx"]="2015-11-27";
		$angel3["nizarclemente@gmail.com"]="2015-08-02";
		$angel3["mariainesprats@gmail.com"]="2015-07-26";
		$angel3["martcado@hotmail.com"]="2015-09-03";
		$angel3["karlacapote@hotmail.com"]="2015-08-15";
		$angel3["carce2000@yahoo.com"]="2015-11-13";
		$angel3["cfreigva@televisa.com.mx"]="2015-07-03";
		$angel3["bienmanejado@hotmail.com"]="2015-10-06";
		$angel3["Credelag@gmail.com"]="2015-07-22";
		$angel3["sobando@yahoo.com"]="2015-08-19";
		$angel3["bibist@hotmail.com"]="2015-07-17";
		$angel3["taniadiaz777@hotmail.com"]="2015-07-25";
		$angel3["Ccouttolenc@me.com"]="2015-08-04";
		$angel3["cecmc@me.com"]="2015-07-23";
		$angel3["queridomario@gmail.com"]="2015-08-25";
		$angel3["mankev@me.com"]="2015-06-25";
		$angel3["chabatito@yahoo.com.mx"]="2015-07-25";
		$angel3["nizarclemente@gmail.com"]="2015-08-02";
		$angel3["dmz.dany@gmail.com"]="2015-04-16";
		$angel3["Danespino@gmail.com"]="2015-06-06";
		$angel3["cornago@gmail.com"]="2015-08-01";
		$angel3["david.solis@condenast.com.mx"]="2015-08-06";
		$angel3["Dandelrio@hotmail.com"]="2015-08-31";
		$angel3["diego.fernandez@matesa.net"]="2015-07-24";
		$angel3["teransfam@gmail.com"]="2015-09-18";
		$angel3["danperezsalazar@gmail.com"]="2015-07-24";
		$angel3["iarmida@gmail.com"]="2015-08-02";
		$angel3["jokhuysen@gmail.com"]="2015-06-26";
		$angel3["doporto@doporto.mx"]="2015-07-04";
		$angel3["guillermo.lts@gmail.com"]="2015-08-10";
		$angel3["david.perezsalinas@gmail.com"]="2015-07-22";
		$angel3["direccion@luxorytransfers.com"]="2015-07-29";
		$angel3["eclavijo@integrapostventa.com"]="2015-09-17";
		$angel3["edivelazquez@hotmail.com"]="2015-07-16";
		$angel3["virues0@gmail.com"]="2015-07-21";
		$angel3["virues0@gmail.com"]="2015-07-31";
		$angel3["Murrieta.emg@gmail.com"]="2015-03-12";
		$angel3["fraga.emilio@gmail.com"]="2015-07-19";
		$angel3["Esberatala@me.com"]="2015-08-05";
		$angel3["mankev@me.com"]="2015-07-16";
		$angel3["e.o.vieira@gmail.com"]="2015-12-24";
		$angel3["federicobelden@gmail.com"]="2015-07-23";
		$angel3["fabiolahg96@gmail.com"]="2015-08-06";
		$angel3["federicobeldensada@gmail.com"]="2015-07-27";
		$angel3["federicosf@gmail.com"]="2015-07-09";
		$angel3["fdeovando@hotmail.com"]="2015-07-19";
		$angel3["fidelraul10191963@gmail.com"]="2015-08-27";
		$angel3["Fjkm33@gmail.com"]="2015-07-28";
		$angel3["florerubio90@gmail.com"]="2015-10-31";
		$angel3["carlos.noyola@gmail.com"]="2015-07-25";
		$angel3["cupcakes.to.go@hotmail.com"]="2015-10-06";
		$angel3["juan.f.mediavilla@gmail.com"]="2015-09-11";
		$angel3["fredo.abrantes@gmail.com"]="2015-07-09";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["gbj007@gmail.com"]="2015-08-03";
		$angel3["gcch@eog.mx"]="2015-07-22";
		$angel3["gemmaburch1@mac.com"]="2015-08-05";
		$angel3["morancastellot@yahoo.com.mx"]="2015-12-17";
		$angel3["gildamgarciag@gmail.com"]="2015-07-16";
		$angel3["gilberto61c@gmail.com"]="2015-08-01";
		$angel3["giocerritelli@me.com"]="2015-07-26";
		$angel3["gerardomorancastellot@gmail.com"]="2015-07-28";
		$angel3["ricardo.kerber@gmail.com"]="2015-06-07";
		$angel3["moga_16@hotmail.com"]="2015-06-24";
		$angel3["gretta27@gmail.com"]="2015-07-16";
		$angel3["gflores@me.com"]="2015-07-07";
		$angel3["garr231@interhabita.com.mx"]="2015-07-20";
		$angel3["garciagus2003@yahoo.com"]="2015-07-22";
		$angel3["pitaglezs@yahoo.com.mx"]="2015-08-07";
		$angel3["humbertocavazos@yahoo.com"]="2015-07-22";
		$angel3["humberto@cavazosflores.com"]="2015-07-22";
		$angel3["helle_jeppsson@hotmail.com"]="2015-08-01";
		$angel3["hpaliza@gmail.com"]="2015-05-10";
		$angel3["hazel_sandoval@hotmail.com"]="2015-07-24";
		$angel3["iabiega@yahoo.com"]="2015-07-17";
		$angel3["inigomat@yahoo.com.mx"]="2015-08-06";
		$angel3["inigomat@yahoo.com.mx"]="2015-08-12";
		$angel3["jcabrera@jaca.com.mx"]="2015-09-06";
		$angel3["jalazvar@yahoo.com.mx"]="2015-12-26";
		$angel3["igrobles@hotmail.com"]="2015-07-05";
		$angel3["Javier@amrlabogados.mx"]="2015-07-24";
		$angel3["mankev@me.com"]="2015-06-25";
		$angel3["mankev@me.com"]="2015-08-04";
		$angel3["jorgecarlos@inteligencialaboral.com"]="2015-08-22";
		$angel3["jorgegcastaneda@gmail.com"]="2015-07-21";
		$angel3["fernanda.delafuente@gmail.com"]="2015-08-26";
		$angel3["asuntoarreglado@cavazos.com"]="2015-04-03";
		$angel3["fernanda.delafuente@gmail.com"]="2015-08-03";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["javfolch@mac.com"]="2015-07-19";
		$angel3["juan_antoniohernandez@hotmail.com"]="2015-07-16";
		$angel3["jibarrola@lico.mx"]="2015-07-20";
		$angel3["lopez.jorgej@gmail.com"]="2015-07-31";
		$angel3["genaf@me.com"]="2015-08-27";
		$angel3["jsauquet@gmail.com"]="2015-10-09";
		$angel3["rokudf@gmail.com"]="2015-12-31";
		$angel3["joseroman.garzon@gmail.com"]="2015-07-12";
		$angel3["joselbq@yahoo.com"]="2015-07-08";
		$angel3["jorgeeduardo15@hotmail.com"]="2015-07-10";
		$angel3["jorge@recreoentretenimiento.com"]="2015-07-27";
		$angel3["jreyes63@mac.com"]="2015-09-02";
		$angel3["jsauquet@gmail.com"]="2015-09-11";
		$angel3["japedreroh@gmail.com"]="2015-07-18";
		$angel3["jrmoreno@morenolaw.com"]="2015-05-22";
		$angel3["jcarlosgonzalez2000@hotmail.com"]="2015-07-13";
		$angel3["jamessprowls@gmail.com"]="2015-07-01";
		$angel3["mestradag18@gmail.com"]="2015-08-19";
		$angel3["ARQ.KARLARMZ@GMAIL.COM"]="2015-07-31";
		$angel3["kps@chelsearf.com"]="2015-07-13";
		$angel3["igavidal@gmail.com"]="2015-07-19";
		$angel3["alfonso_mtzr@hotmail.com"]="2015-07-12";
		$angel3["Leogarcia7@gmail.com"]="2015-08-28";
		$angel3["leyrafael@gmail.com"]="2015-07-02";
		$angel3["lidibc@hotmail.com"]="2015-07-15";
		$angel3["bladic@hotmail.com"]="2015-07-07";
		$angel3["hbladinieres@hotmail.com"]="2015-04-25";
		$angel3["liliabh29@hotmail.com"]="2015-07-28";
		$angel3["laila.garciag@hotmail.com"]="2015-08-08";
		$angel3["lmv@monsalvo.com.mx"]="2015-03-13";
		$angel3["lazcarraga88@aol.com"]="2015-04-01";
		$angel3["lazcarraga88@aol.com"]="2015-04-01";
		$angel3["lazcarraga88@aol.com"]="2015-04-01";
		$angel3["lugaudi@gmail.com"]="2015-07-23";
		$angel3["lcasamayor@gmail.com"]="2015-10-20";
		$angel3["lgonzalez@inpamex.com"]="2015-07-10";
		$angel3["proinsumos01@gmail.com"]="2015-11-15";
		$angel3["macahaca@gmail.com"]="2015-07-14";
		$angel3["centenomaria@gmail.com"]="2015-07-16";
		$angel3["melisaromo@gmail.com"]="2015-08-16";
		$angel3["mallitaxoxo93@gmail.com"]="2015-07-21";
		$angel3["lgmartinez@almex.com.mx"]="2015-08-29";
		$angel3["Mab3067@gmail.com"]="2015-07-27";
		$angel3["maragarza@gmail.com"]="2015-08-07";
		$angel3["mariagaha@me.com"]="2015-08-23";
		$angel3["mariolozano21@hotmail.com"]="2015-08-10";
		$angel3["mariza.mejiag@gmail.com"]="2015-01-03";
		$angel3["lavazquez001@hotmail.com"]="2015-07-31";
		$angel3["marthagalas@gmail.com"]="2015-08-09";
		$angel3["matute@hotmail.com"]="2015-07-26";
		$angel3["mautvtv@hotmail.com"]="2015-06-23";
		$angel3["maytegfigueroa@gmail.com"]="2015-08-03";
		$angel3["manuel.castellanosc@gmail.com"]="2015-07-09";
		$angel3["mda@chelsearf.com"]="2015-07-18";
		$angel3["paolatrevino10@gmail.com"]="2015-03-03";
		$angel3["mgarzah@hotmail.com"]="2015-08-06";
		$angel3["monicagzzfigueroa@gmail.com"]="2015-07-29";
		$angel3["gerenciawtc@teikit.mx"]="2015-07-14";
		$angel3["mnhayito@hotmail.com"]="2015-07-15";
		$angel3["monicagr22@hotmail.com"]="2015-07-20";
		$angel3["maytegfigueroa@gmail.com"]="2015-08-04";
		$angel3["monicasanz@t-visa.com.mx"]="2015-08-26";
		$angel3["mtt99@hotmail.com"]="2015-07-28";
		$angel3["mzubiria@mac.com"]="2015-07-08";
		$angel3["icastro@adimpacto.com"]="2015-07-15";
		$angel3["nataliagarza@gmail.com"]="2015-07-11";
		$angel3["normacantu@me.com"]="2015-07-17";
		$angel3["mankev@me.com"]="2015-04-23";
		$angel3["nizarclemente@gmail.com"]="2015-07-13";
		$angel3["nunziarojodelavega@gmail.com"]="2015-08-16";
		$angel3["osalinas@tsigns.com.mx"]="2015-09-12";
		$angel3["osalinas@tsigns.com.mx"]="2015-09-13";
		$angel3["plavalleg@hotmail.com"]="2015-05-28";
		$angel3["plavalleg@hotmail.com"]="2015-08-06";
		$angel3["jflozano@rimmsa.com"]="2015-11-15";
		$angel3["patyf2@mac.com"]="2015-08-02";
		$angel3["jdavila10@gmail.com"]="2015-07-18";
		$angel3["pedro_alvear@hotmail.com"]="2015-04-17";
		$angel3["pablozuelos@gmail.com"]="2015-07-29";
		$angel3["politrevino@hotmail.com"]="2015-04-01";
		$angel3["ropodo@hotmail.com"]="2015-07-24";
		$angel3["Garciabazan@hotmail.com"]="2015-03-24";
		$angel3["alejandroprats90@gmail.com"]="2015-07-15";
		$angel3["angaha-1001@hotmail.com"]="2015-07-29";
		$angel3["rafgonz43@yahoo.com"]="2015-06-12";
		$angel3["ricardodigo@hotmail.com"]="2015-07-12";
		$angel3["robertofmiles@gmail.com"]="2015-09-09";
		$angel3["rherrerias@rhcei.mx"]="2015-09-13";
		$angel3["rherrerias@rhcei.mx"]="2015-09-14";
		$angel3["rlopez66@hotmail.com"]="2015-07-29";
		$angel3["drbetochas@gmail.com"]="2015-07-23";
		$angel3["Roberto.sanchez@consyrsa.com"]="2015-08-04";
		$angel3["torresl.german@gmail.com"]="2015-04-28";
		$angel3["rtogores@ludicus.com"]="2015-11-05";
		$angel3["jrubenhernandez@hotmail.com"]="2015-10-17";
		$angel3["claude.mongala@gmail.com"]="2015-06-17";
		$angel3["rmatenorio@outlook.com"]="2015-08-16";
		$angel3["mrvieira@yahoo.com"]="2015-08-16";
		$angel3["undertvsa@gmail.com"]="2015-04-09";
		$angel3["rmatenorio@gmail.com"]="2015-06-16";
		$angel3["bernard.ruberg@gmail.com"]="2015-04-02";
		$angel3["Greg.lang@touchhome.co.za"]="2015-04-02";
		$angel3["claude@seragliojewellers.com"]="2015-05-04";
		$angel3["salomonmansur@gmail.com"]="2015-09-23";
		$angel3["mankev@me.com"]="2015-08-03";
		$angel3["schufani@chufani.com"]="2015-07-19";
		$angel3["sergiochedraui@gmail.com"]="2015-08-06";
		$angel3["visionk6@gmail.com"]="2015-07-10";
		$angel3["visionk6@gmail.com"]="2015-08-01";
		$angel3["caneggle87@gmail.com"]="2015-07-29";
		$angel3["diego.fernandez@matesa.net"]="2015-07-26";
		$angel3["stephaniemdl@hotmail.com"]="2015-04-17";
		$angel3["sofia_mtz5@hotmail.com"]="2015-07-07";
		$angel3["sonia_bdm@hotmail.com"]="2015-07-26";
		$angel3["pablosozzi@hotmail.com"]="2015-07-25";
		$angel3["Stephaniemdl@hotmail.com"]="2015-06-29";
		$angel3["susre@hotmail.com"]="2015-07-28";
		$angel3["dondelees@gmail.com"]="2015-07-15";
		$angel3["mankev@me.com"]="2015-12-10";
		$angel3["scullenp@gmail.com"]="2015-08-18";
		$angel3["ltostado@hotmail.com"]="2015-09-07";
		$angel3["Ugow@gaam.com.mx"]="2015-07-16";
		$angel3["affernan@radioformula.com.mx"]="2015-04-11";
		$angel3["affernan@radioformula.com.mx"]="2015-04-11";
		$angel3["mauro_plantabaja@hotmail.com"]="2015-04-11";
		$angel3["mlarregui@hotmail.com"]="2015-07-28";
		$angel3["valeriant@gmail.com"]="2015-08-20";
		$angel3["vela_f@hotmail.com"]="2015-07-17";
		$angel3["rokudf@gmail.com"]="2015-03-26";
		$angel3["drvalpuesta@gmail.com"]="2015-07-05";
		$angel3["dvillarreal@aceromex.com"]="2015-09-06";
		$angel3["e.trejo@me.com"]="2015-02-27";
		$angel3["josed_rgc@hotmail.com"]="2015-01-24";
		$angel3["agaskdro@yahoo.com.mx"]="2015-07-14";
		$angel3["amhilairefraga@hotmail.com"]="2015-07-25";
		$angel3["yessics@hotmail.com"]="2015-08-14";
		$angel3["marioab75@yahoo.com.mx"]="2015-07-01";
		$angel3["mzubiriam@hotmail.com"]="2015-09-11";

		$angel3["angel.servin@gmail.com"]="2015-08-01";
		
		foreach($angel3 as $key => $value){
			$angel3[strtolower($key)]=$value;
		}
		
        parent::initContent();
        if(	version_compare(_PS_VERSION_, '1.5', '<')){
			$css_files = array($this->_path.'css/banwire.css');

			foreach($css_files as $cssfile){
				echo 	'<link type="text/css" rel="stylesheet" href="'.$cssfile.'" />';
			}
		}
		
		$cart = $this->context->cart;
                    
		if (!$this->module->checkCurrency($cart))
			Tools::redirect('index.php?controller=order');
                
                $cards = array('visa' => 'VISA', 'mastercar' => 'MasterCard', 'amex' => 'AmericanExpress');
                $months = array(1 => '01 - '.$this->module->l('Enero'), 2 => '02 - '.$this->module->l('Febrero'), 3 => '03 - '.$this->module->l('Marzo'), 4 => '04 - '.$this->module->l('Abril'), 5 => '05 - '.$this->module->l('Mayo'), 6 => '06 - '.$this->module->l('Junio'), 7 => '07 - '.$this->module->l('Julio'), 8 => '08 - '.$this->module->l('Agosto'), 9 => '09 - '.$this->module->l('Septiembre'), 10 => '10 - '.$this->module->l('Octubre'), 11 => '11 - '.$this->module->l('Noviembre'), 12 => '12 - '.$this->module->l('Diciembre'));
                $years = array();
                for($i = date('Y'); $i <= date('Y') + 10; $i++)
                    $years[] = $i;
                
                $recurrencias = array(1 => $this->module->l('Semanal'), 2 => $this->module->l('Mensual'), 3 => $this->module->l('Trimestral'), 4 => $this->module->l('Cuatrimestral'), 6 => $this->module->l('Semestral'), 7 => $this->module->l('Anual'));
		
                $this->context->smarty->assign(array(
                        'banwire_sandbox'   => Configuration::get('BANWIRE_SANDBOX'),
                        'banwire_tdc'       => Configuration::get('BANWIRE_TDC'),
                        'banwire_tdc_red'   => Configuration::get('BANWIRE_TDC_RED'),
                        'banwire_tdc_pro'   => Configuration::get('BANWIRE_TDC_PRO'),
                        'banwire_oxxo'      => Configuration::get('BANWIRE_OXXO'),
                        'banwire_spei'      => Configuration::get('BANWIRE_SPEI'),
                        'banwire_rec'       => Configuration::get('BANWIRE_REC'),
                        'banwire_rec_sel'   => $recurrencias[Configuration::get('BANWIRE_REC_SELECT')],
                        'cards'             => $cards,
                        'years'             => $years,
                        'months'            => $months,
                        'module_dir'        => $this->module->getPathUri(),
			'nbProducts'        => $cart->nbProducts(),
			'cust_currency'     => $cart->id_currency,
			'currencies'        => $this->module->getCurrency((int)$cart->id_currency),
			'total'             => $cart->getOrderTotal(true, Cart::BOTH),
			'this_path'         => $this->module->getPathUri(),
			'this_path_bw'      => $this->module->getPathUri(),
			'this_path_ssl'     => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/'
		));
		
		//
		/*
		foreach($cart as $a => $b) {
			print "$a: $b<br>";
		}
		*/
		
		//var_dump(get_object_vars($cart));
		//$cart2=get_object_vars($cart);
		//$cart2=print_r($cart,true);
		//echo "<pre><small>".print_r($cart,true)."</small></pre>"; exit;
		//$angel = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('select * from `'._DB_PREFIX_.'orders` o, `'._DB_PREFIX_.'order_detail` d where o.id_customer="'.$cart["id_customer"].'" and o.id_order=d.id_order and o.id_order="'.$cart["id_carrier"].'")');
		if($cart->id_carrier!=0)
			$angel = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('select * from '._DB_PREFIX_.'cart c, '._DB_PREFIX_.'cart_product cp where c.id_carrier="'.$cart->id_carrier.'" and c.id_cart=cp.id_cart and c.id_customer="'.$cart->id_customer.'" order by c.id_cart desc limit 0,1');
		else
			$angel = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('select * from '._DB_PREFIX_.'cart c, '._DB_PREFIX_.'cart_product cp where c.secure_key="'.$cart->secure_key.'" and c.id_cart=cp.id_cart and c.id_customer="'.$cart->id_customer.'" order by c.id_cart desc limit 0,1');
		//echo 'select * from '._DB_PREFIX_.'cart c, '._DB_PREFIX_.'cart_product cp where c.secure_key="'.$cart->secure_key.'" and c.id_cart=cp.id_cart and c.id_customer="'.$cart->id_customer.'" order by c.id_cart desc limit 1,1';
		//echo "<pre><small>".print_r($angel,true)."</small></pre>"; exit;
		
		$angel_2f = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('select * from '._DB_PREFIX_.'customer where id_customer="'.$cart->id_customer.'"');
		//echo "<pre><small>".print_r($angel_2f,true)."</small></pre>"; 
		
		$angel2="No";
		for($i=0; $i<count($angel); $i++){
			if($angel[$i]["id_product"]==4){
				$angel2="Si";
			}
		}
		
		$angel_f=date("Y-m-d");
		
		if (isset($angel_2f[0]["email"])) {
			if (isset($angel3[strtolower($angel_2f[0]["email"])])) {
				if(isset($angel3[strtolower($angel_2f[0]["email"])])){
					$angel_f=$angel3[strtolower($angel_2f[0]["email"])];
				}
			}
		}
		
		$cart->angel=$angel2;
		$cart->angel_f=$angel_f;
		//echo "<pre><small>".print_r($cart,true)."</small></pre>"; exit;
		
		$this->setTemplate('payment_banwire.tpl');
	}
}
