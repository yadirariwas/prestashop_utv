<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

class BanwireResponseRECModuleFrontController extends ModuleFrontController
{
	
	public function initContent()
	{
		if(isset ($_POST['RESPONSE']) )  {
			$this->saveLB($_POST);
			$order = new Order((int)Order::getOrderByCartId($_POST['ORD_ID']));
			
			if($order->total_paid != null && $order->total_paid == $_POST['ORD_AMT'] && isset($_POST['CODE_AUTH']) && $_POST['RESPONSE'] == 'ok' ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_REC'), $order, true);
				$new_history->addWithemail(true);
			}
			if($order->total_paid != null && $order->total_paid == $_POST['ORD_AMT'] && isset($_POST['CODE_AUTH']) && $_POST['RESPONSE'] == 'ko' ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_REC_FALLO'), $order, true);
				$new_history->addWithemail(true);
			}
		}else exit('No post');
		exit();
	}
	
	private function saveLB($post) {
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 
							id_customer = '0', 
							id_cart = '" . $post['ORD_ID'] . "', 
							response = '" . serialize($post) . "', 
							send = 'response Recurrente', 
							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 
							date_add = NOW();"
						);	
	}
	
}
