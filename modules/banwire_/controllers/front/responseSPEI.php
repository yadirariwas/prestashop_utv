<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

class BanwireResponseSPEIModuleFrontController extends ModuleFrontController
{
	
	public function initContent()
	{
		$json = file_get_contents('php://input');
		
		$json = json_decode($json);
		
		if(isset ( $json->ord_id ) )  {
			$this->saveLB($json);
			$order = new Order((int)Order::getOrderByCartId($json->ord_id));
			
			//if($order->total_paid != null && $order->total_paid == $json->amount ){
			if($order->total_paid != null ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_SPEI_CONFIRMADO'), $order, true);
				$new_history->addWithemail(true);
			} else exit('No Mach');
		}else exit('No json');
		exit();
	}
	
	private function saveLB($json) {
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 
							id_customer = '0', 
							id_cart = '" . $json->ord_id . "', 
							response = '" . serialize($json) . "', 
							send = 'response SPEI', 
							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 
							date_add = NOW();"
						);	
	}
	
	
}
