{*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}/css/layout.css" type="text/css" media="all">
                

	<a href="{$banwire_action|escape:'htmlall':'UTF-8'}" title="{l s='Elige tu método de pago Banwire' mod='banwire'}" class="btn btn-default button button-medium">
		{*<img src="https://www.banwire.com/home/img/banwire_logo.png" alt="{l s='Elige tu método de pago Banwire' mod='banwire'}" width="130" height="58"/>*}
		{*l s='Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO, SPEI.' mod='banwire'*}
		<span>Pagar &nbsp;<i class="icon-chevron-right right"></i></span>
	</a>


						