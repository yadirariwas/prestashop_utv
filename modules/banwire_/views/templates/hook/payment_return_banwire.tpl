{*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}modules/banwire/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}modules/banwire/css/layout.css" type="text/css" media="all">
                
<div id="content_confiramtion">
    <div id='msg_confirmation'><span>{l s='¡GRACIAS POR SU COMPRA!' mod='banwire'}</span><br>{l s='El proceso de pago a través de BanWire ha sido completado, a continuación los detalles de su pago:' mod='banwire'}</div>
    <div class='data_confirmation'>

	<center>
	<h2 style="font-size: 24px;">Formato de pago en tiendas OXXO</h2>
	<br/><p>Para realizar tu pago, imprime y presenta este comprobante en cualquier tienda OXXO de México</p><br/><br/>
	</center>

	<div style="margin: 0 0 0 50px;">
		<div class="row_data"><label><b>Orden: </b></label> {$data.Orden}</div>
		<div class="row_data"><label><b>Total: </b></label> {$data.Total}</div>
		<div class="row_data"><label><b>Código de barras: </b></label> {$data.C_Barras}</div>
		<div class="row_data"><label><b>Vigencia: </b></label> {$data.Vigencia}</div>
	</div>
	
	<div class="oxxo_codebar" style="text-align:center;width:370px;margin:50px auto 0;">
	    <div class="row_data"><label>&nbsp;</label> <img src="data:image/jpg;base64,{$data.barcode_img}"></div>
	</div>
	<div class="oxxo_footer" style="text-align:center;width:800px;margin:50px auto 0;font-size: 16px;">
	<br/><br/><p style="text-align:left;">Este cupón es válido úncamente para realizar el pago correspondiente y dentro
	de la fecha de vigencia establecida. La acreditación del mismo es a las 24 hrs.
	de realizado, hasta entonces el pago podrá ser corroborado por el vendedor.
	Cualquier aclaración sobre la compra, favor de comunicarse con el vendedor.
	BanWire no se hace responsible por cualquier reclamo ó aclaración, es
	responsabilidad del vendedor resolver cualquier situación relacionada con la
	compra del producto o servicio adquirido.</p>

	<br/><br/><p style="text-align:left;">*Imprime el cupón de manera clara y legible, usa de preferencia impresora
	láser, Consérvalo en buen estado sin tachar ó doblar la parte del código de
	barras. En caso de no ser legible, la tienda puede rechazar el pago
	correspondiente.</p>

	</div>

    </div>
</div>

{if isset($imprimir) && $imprimir }
<br/><p><a class="print_confirmation button-exclusive btn btn-default" href="#" title="Imprimir"><span>Imprimir</span></a></p><br/>
<script>
	function print_confirmation(){
		var tmp = $("#content_confiramtion").clone();	
		$("#msg_confirmation",tmp).remove();
		tmp.printArea();
		return false;
	}
	$(".print_confirmation").click(print_confirmation);
</script>
{/if}
