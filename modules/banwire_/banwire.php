<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */
if (!defined('_PS_VERSION_'))
	exit;

class Banwire extends PaymentModule
{

	private $_error = array();
	private $_validation = array();
	private $_shop_country = array();
	
	public $display_column_left = false;

	public function __construct()
	{
		
		$this->name 		= 'banwire';
		$this->version 		= '1.0';
		$this->author 		= 'Banwire';
		$this->className 	= 'banwire';
		$this->tab 		= 'payments_gateways';

		parent::__construct();
		
		$this->_shop_country = new Country((int)Configuration::get('PS_SHOP_COUNTRY_ID'));
		$this->displayName = $this->l((Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX') ? 'Banwire' : '');
		$this->description = $this->l((Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX') ? 'Pago seguro con tarjeta de crédito/débito: Visa/MasterCard/Amex, Oxxo, SPEI' : '');
		$this->confirmUninstall = $this->l('¿Seguro que desea eliminar este modulo?');

		/* Backward compatibility */
		require(_PS_MODULE_DIR_.'banwire/backward_compatibility/backward.php');
		$this->context->smarty->assign('base_dir', __PS_BASE_URI__);
	}

	public function install()
	{
		/* The cURL PHP extension must be enabled to use this module */
		if (!function_exists('curl_version'))
		{
			$this->_errors[] = $this->l('Lo Sentimos, este modulo requiere la extensión cURL PHP (http://www.php.net/curl), no se encuentra activo en su servidor. Por favor comuniquese con su proveedor de hosting.');
			return false;
		}

		/* Desarrollo / Producción */
		Configuration::updateValue('BANWIRE_SANDBOX', true);
		
		/* Configuración general de BanWire */
		Configuration::updateValue('BANWIRE_USERNAME', 'userBanwire');
		Configuration::updateValue('BANWIRE_CONCEPT', 'Pago en Línea');
		Configuration::updateValue('BANWIRE_OXXO_VIG', 3);
		Configuration::updateValue('BANWIRE_OXXO_SENDPDF', true);
		/*STATUS DE PEDIDO EN 0*/
		Configuration::updateValue('STATUS_BANWIRE_TDC', 2);
		Configuration::updateValue('STATUS_BANWIRE_TDC_REVISION', 8);
		Configuration::updateValue('STATUS_BANWIRE_TDC_DENEGADA', 8);
		Configuration::updateValue('STATUS_BANWIRE_TDC_ACEPTADA', 2);
		Configuration::updateValue('STATUS_BANWIRE_OXXO_ESPERA', 8);
		Configuration::updateValue('STATUS_BANWIRE_OXXO_CONFIRMADO', 2);
		Configuration::updateValue('STATUS_BANWIRE_SPEI_ESPERA', 8);
		Configuration::updateValue('STATUS_BANWIRE_SPEI_CONFIRMADO', 2);
		Configuration::updateValue('STATUS_BANWIRE_REC', 2);
		Configuration::updateValue('STATUS_BANWIRE_REC_FALLO', 8);
		Configuration::updateValue('STATUS_BANWIRE_REC_SCOSTO', 8);
		/* API´s Disponibles */
		Configuration::updateValue('BANWIRE_TDC', true);
		Configuration::updateValue('BANWIRE_TDC_PRO', true);
		Configuration::updateValue('BANWIRE_TDC_RED', false);
		Configuration::updateValue('BANWIRE_OXXO', true);
		Configuration::updateValue('BANWIRE_REC', false);
		Configuration::updateValue('BANWIRE_SPEI', false);
		Configuration::updateValue('BANWIRE_REC_SELECT', 2);
		/*Valores solo para RED*/
		Configuration::updateValue('BANWIRE_PROD_DEL_CD', 2);
		Configuration::updateValue('BANWIRE_SHIP_MTHD_CD', 'C');
		Configuration::updateValue('BANWIRE_REC_RETRYN', null); 
		Configuration::updateValue('BANWIRE_REC_RETRYD', null); 
		Configuration::updateValue('BANWIRE_REC_TRAIL', null);
		
		/* Configuración de datos para API's*/
		if(	version_compare(_PS_VERSION_, '1.5', '<')){
			Configuration::updateValue('PS_SHOP_COUNTRY','MX');
			Configuration::updateValue('BANWIRE_SHOP_COUNTRY','MX');
		}else{
			Configuration::updateValue('PS_SHOP_COUNTRY_ID',145);
			Configuration::updateValue('BANWIRE_SHOP_COUNTRY','MX');
		}

		return parent::install() && $this->registerHook('payment') && $this->_installDb() && $this->_installStatuses() && $this->registerHook('paymentReturn');
	}
	

	private function _installDb()
	{
		return Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'banwire_log` (
			`id_banwire_log` int(11) NOT NULL AUTO_INCREMENT,
			`id_customer` int(11) unsigned NOT NULL,
			`id_cart` int(11) unsigned NOT NULL,
			`response` text NOT NULL,
			`send` text NOT NULL,
			`mode` varchar(10) NOT NULL,
			`date_add` datetime NOT NULL,
		PRIMARY KEY (`id_banwire_log`));');
	}
	
	private function _installStatuses(){
		$languages = Language::getLanguages(false);
		
		/*OXXO*/
		Db::getInstance()->Execute( "INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#f76900';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='OXXO: Espera de Pago';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='OXXO: Pago Confirmado';" );
		}
		/*SPEI*/
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#f76900';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='SPEI: Espera de Transferencia';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='SPEI: Confirmado';" );
		}
		
		/*TDC*/
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Pagado';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#f76900';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Challenge en Revisión';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Challenge Denegado';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#bf0500';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Challenge Aceptado';" );
		}
		
		/*REC*/
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#59c300'; ");
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='REC: En Pago Recurrete';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#f76900'; ");
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='REC: Fallo Pago Recurrente';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'banwire', color = '#bf0500'; ");
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='REC: Inicio sin Costo';" );
		}
		return true;
	}
	
	public function uninstall()
	{
		$keys_to_uninstall = array('BANWIRE_TDC', 'BANWIRE_TDC_RED','BANWIRE_TDC_PRO',
					   'BANWIRE_OXXO', 'BANWIRE_OXXO_VIG',
					   'BANWIRE_SPEI', 'BANWIRE_REC', 'BANWIRE_REC_SELECT',
					   'BANWIRE_SANDBOX', 'BANWIRE_USERNAME', 'BANWIRE_CONCEPT',  
					   'BANWIRE_OXXO_SENDPDF',
					   'STATUS_BANWIRE_TDC', 'STATUS_BANWIRE_TDC_REVISION',
					   'STATUS_BANWIRE_TDC_DENEGADA', 'STATUS_BANWIRE_TDC_ACEPTADA', 'STATUS_BANWIRE_OXXO_ESPERA',
					   'STATUS_BANWIRE_OXXO_CONFIRMADO', 'STATUS_BANWIRE_SPEI_ESPERA',
					   'STATUS_BANWIRE_SPEI_CONFIRMADO','STATUS_BANWIRE_REC', 'STATUS_BANWIRE_REC_FALLO',
					   'BANWIRE_PROD_DEL_CD', 'BANWIRE_SHIP_MTHD_CD', 'BANWIRE_SHOP_COUNTRY',
					   'BANWIRE_REC_RETRYN', 'BANWIRE_REC_RETRYD', 'BANWIRE_REC_TRAIL'
					);
		$result = true;
		
		$estatus_banwire = Db::getInstance()->executeS("SELECT * FROM " . _DB_PREFIX_ . "order_state WHERE module_name LIKE 'banwire' ");
		foreach($estatus_banwire as $estatus )
			if(Db::getInstance()->Execute("DELETE FROM " . _DB_PREFIX_ . "order_state_lang WHERE id_order_state = " . $estatus['id_order_state']))
				Db::getInstance()->Execute("DELETE FROM " . _DB_PREFIX_ . "order_state WHERE id_order_state = " . $estatus['id_order_state']);
				
		foreach ($keys_to_uninstall as $key_to_uninstall)
			$result &= Configuration::deleteByName($key_to_uninstall);
		/* descomentar la siguiente linea para eliminar los detalles del log de Banwire*/
		$result &= Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'banwire_log`'); 
		
		return $result && parent::uninstall();
	}

	public function getContent()
	{
		
		//echo "ESTO ES EN ADMIN INDEX PAGO ANGEL"; exit;
		/* Loading CSS and JS files */
		if(isset($this->context->controller)){
			$this->context->controller->addCSS(array($this->_path.'css/banwire.css'));
			$this->context->controller->addJS(array(_PS_JS_DIR_.'jquery/jquery-ui-1.8.10.custom.min.js', $this->_path.'js/colorpicker.js', $this->_path.'js/jquery.lightbox_me.js', $this->_path.'js/banwire.js'));
		}

		/* Update the Configuration option values depending on which form has been submitted */
		if ((Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX') && Tools::isSubmit('SubmitBasicSettings'))
		{
			$this->_saveSettingsProducts();
			unset($this->_validation[count($this->_validation) - 1]);
		}
		elseif (Tools::isSubmit('SubmitProducts'))
			$this->_saveSettingsProducts();

		$order_statuses = array();	
		$statuses = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang = 1 ');
		
		foreach($statuses as $status)
			$order_statuses[$status['id_order_state']] = $status['name'];
			
		$this->context->smarty->assign(array(
			'banwire_tracking' 			=> 'http://www.prestashop.com/modules/banwire.png?url_site='.Tools::safeOutput($_SERVER['SERVER_NAME']).'&id_lang='.(int)$this->context->cookie->id_lang,
			'banwire_form_link' 			=> './index.php?tab=AdminModules&configure=banwire&token='.Tools::getAdminTokenLite('AdminModules').'&tab_module='.$this->tab.'&module_name=banwire',
			'banwire_ssl' 				=> Configuration::get('PS_SSL_ENABLED'),
			'banwire_validation' 			=> (empty($this->_validation) ? false : $this->_validation),
			'banwire_error' 			=> (empty($this->_error) ? false : $this->_error),
			'banwire_warning' 			=> (empty($this->_warning) ? false : $this->_warning),
			'banwire_configuracion' 		=> Configuration::getMultiple(array(
												'BANWIRE_SANDBOX', 'BANWIRE_TDC_RED', 'BANWIRE_OXXO',
												'BANWIRE_REC', 'BANWIRE_SPEI', 'BANWIRE_TDC_PRO',
												'BANWIRE_USERNAME', 'BANWIRE_CONCEPT',
												'BANWIRE_OXXO_VIG', 'BANWIRE_OXXO_SENDPDF', 'BANWIRE_TDC',
												'BANWIRE_REC_SELECT', 'STATUS_BANWIRE_TDC', 'STATUS_BANWIRE_TDC_REVISION',
												'STATUS_BANWIRE_TDC_DENEGADA', 'STATUS_BANWIRE_TDC_ACEPTADA', 'STATUS_BANWIRE_OXXO_ESPERA',
												'STATUS_BANWIRE_OXXO_CONFIRMADO', 'STATUS_BANWIRE_SPEI_ESPERA',
												'STATUS_BANWIRE_SPEI_CONFIRMADO','STATUS_BANWIRE_REC', 'STATUS_BANWIRE_REC_FALLO',
												'BANWIRE_PROD_DEL_CD', 'BANWIRE_SHIP_MTHD_CD', 'BANWIRE_REC_TRAIL',
												'BANWIRE_REC_RETRYN', 'BANWIRE_REC_RETRYD', 'STATUS_BANWIRE_REC_SCOSTO')),
			'banwire_rec_select' 			=> array(1 => $this->l('Semanal'), 2 => $this->l('Mensual'), 3 => $this->l('Trimestral'), 4 => $this->l('Cuatrimestral'), 6 => $this->l('Semestral'), 7 => $this->l('Anual')),
			'banwire_valores_prod_del_cd' 		=> array('CNC' => $this->l('Comprar y llevar'), 'DCT' => $this->l('Contenidos Digitales'), 'DIG' => $this->l('Bienes Digitales'),'DNP' => $this->l('Fisico y Digital'),'GFT' => $this->l('Certificado de Regalo'),'PHY' => $this->l('Bienes Fisicos'),'REN' => $this->l('Renovacion y Recargas'),'SVC' => $this->l('Servicios')),
			'banwire_valores_ship_mthd_cd' 		=> array('C' => $this->l('Bajo Costo'), 'D' => $this->l('Paqueteria Propia'), 'I' => $this->l('Internacional'), 'M' => $this->l('Militar'), 'N' => $this->l('Dia Siguiente'), 'O' => $this->l('Otros'), 'P' => $this->l('Recoger en Tienda'), 'T' => $this->l('Servicio de Dos Dias'), 'W' => $this->l('Servicio de Tres Dias')),
			'banwire_rec_retry' 			=> array('null' => $this->l('No'), '1' => $this->l('Uno'), '2' => $this->l('Dos'), '3' => $this->l('Tres')),
			'banwire_order_statuses' 		=> $order_statuses,
			'banwire_merchant_country_is_usa' 	=> (Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'US'),
			'banwire_merchant_country_is_mx' 	=> (Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX'),
			'banwire_ps_14' 			=> (version_compare(_PS_VERSION_, '1.5', '<') ? 1 : 0),
			'banwire_b1width' 			=> (version_compare(_PS_VERSION_, '1.5', '>') ? '350' : '300'),
			'banwire_js_files' 			=> stripcslashes('"'._PS_JS_DIR_.'jquery/jquery-ui-1.8.10.custom.min.js","'.$this->_path.'js/colorpicker.js","'.$this->_path.'js/jquery.lightbox_me.js","'.$this->_path.'js/banwire.js'.'"')
		));

		return $this->display(__FILE__, 'views/templates/admin/configuration-mx.tpl');
	}

	private function _saveSettingsProducts()
	{
		if (!isset($_POST['banwire_tdc']) && !isset($_POST['banwire_oxxo']) && !isset($_POST['banwire_spei']) && !isset($_POST['banwire_rec']))
			$this->_error[] = $this->l('Debes activar al menos un metodo de pago');
		if (isset($_POST['banwire_oxxo']) && $_POST['banwire_oxxo_vig'] == '')
			$this->_error[] = $this->l('Si activa OXXO debe elegir días de vigencia para el código de barras.');
		if(isset($_POST['banwire_tdc']) && !isset($_POST['banwire_tdc_red']) && !isset($_POST['banwire_tdc_pro']))
			$this->_error[] = $this->l('Debe activar al menos un método de pago con Tarjeta.');
				if (!isset($_POST['banwire_username']) || $_POST['banwire_username'] == "")
			$this->_error[] = $this->l('Usuario de Banwire requerido.');
		if (!isset($_POST['banwire_concept']) || $_POST['banwire_concept'] == "")
			$this->_error[] = $this->l('Concepto para cuenta de Banwire.');
		if (isset($_POST['banwire_tdc_red'])  && !isset($_POST['banwire_ship_mthd_cd']) && !isset($_POST['banwire_prod_del_cd']))
			$this->_error[] = $this->l('Activo el API de Banwire Shield Debe elegir <b>Tipo de Producto que comercializa</b> y <b>Metodo de Envio</b>.');
		if( ( isset($_POST['banwire_rec_trail']) || isset($_POST['banwire_rec_retryn']) || isset($_POST['banwire_rec_retryd']) ) && (isset($_POST['banwire_rec_select']) && $_POST['banwire_rec_select'] == 1 ) )
			$this->_error[] = $this->l('La recurrencia semanal no puede tener  los parametros de Reintento.');
			
		if (count($this->_error) == 0)
		{
			foreach (array(1 => 'BANWIRE_TDC', 2 => 'BANWIRE_OXXO', 3 => 'BANWIRE_SPEI', 4 => 'BANWIRE_REC') as $banwire_products_id => $banwire_products)
				Configuration::updateValue($banwire_products, (isset($_POST['banwire_products']) && $_POST['banwire_products'] == $banwire_products_id) ? 1 : null);
			
			Configuration::updateValue('BANWIRE_TDC', isset($_POST['banwire_tdc']));
			Configuration::updateValue('BANWIRE_OXXO', isset($_POST['banwire_oxxo']));
			Configuration::updateValue('BANWIRE_SPEI', isset($_POST['banwire_spei']));
			Configuration::updateValue('BANWIRE_REC', isset($_POST['banwire_rec']));
			Configuration::updateValue('BANWIRE_TDC_RED', isset($_POST['banwire_tdc_red']));
			Configuration::updateValue('BANWIRE_TDC_PRO', isset($_POST['banwire_tdc_pro']));
			Configuration::updateValue('BANWIRE_OXXO_SENDPDF', isset($_POST['banwire_oxxo_sendpdf']));
			Configuration::updateValue('BANWIRE_OXXO_VIG', $_POST['banwire_oxxo_vig']);
			Configuration::updateValue('BANWIRE_REC_SELECT', $_POST['banwire_rec_select']);
			
			Configuration::updateValue('STATUS_BANWIRE_TDC', $_POST['status_banwire_tdc']);
			Configuration::updateValue('STATUS_BANWIRE_TDC_REVISION', $_POST['status_banwire_tdc_revision']);
			Configuration::updateValue('STATUS_BANWIRE_TDC_DENEGADA', $_POST['status_banwire_tdc_denegada']);
			Configuration::updateValue('STATUS_BANWIRE_TDC_ACEPTADA', $_POST['status_banwire_tdc_aceptada']);
			Configuration::updateValue('STATUS_BANWIRE_OXXO_ESPERA', $_POST['status_banwire_oxxo_espera']);
			Configuration::updateValue('STATUS_BANWIRE_OXXO_CONFIRMADO', $_POST['status_banwire_oxxo_confirmado']);
			Configuration::updateValue('STATUS_BANWIRE_SPEI_ESPERA', $_POST['status_banwire_spei_espera']);
			Configuration::updateValue('STATUS_BANWIRE_SPEI_CONFIRMADO', $_POST['status_banwire_spei_confirmado']);
			Configuration::updateValue('STATUS_BANWIRE_REC', $_POST['status_banwire_rec']);
			Configuration::updateValue('STATUS_BANWIRE_REC_FALLO', $_POST['status_banwire_rec_fallo']);
			Configuration::updateValue('STATUS_BANWIRE_REC_SCOSTO', $_POST['status_banwire_rec_scosto']);
			Configuration::updateValue('BANWIRE_REC_RETRYN', $_POST['banwire_rec_retryn']);
			Configuration::updateValue('BANWIRE_REC_RETRYD', $_POST['banwire_rec_retryd']);
			Configuration::updateValue('BANWIRE_REC_TRAIL', $_POST['banwire_rec_trail']);
			
			Configuration::updateValue('BANWIRE_USERNAME', $_POST['banwire_username']);
			Configuration::updateValue('BANWIRE_CONCEPT', $_POST['banwire_concept']);
			Configuration::updateValue('BANWIRE_SANDBOX', $_POST['banwire_sandbox']);
			
			Configuration::updateValue('BANWIRE_PROD_DEL_CD', $_POST['banwire_prod_del_cd']);
			Configuration::updateValue('BANWIRE_SHIP_MTHD_CD', $_POST['banwire_ship_mthd_cd']);
			
			$this->_validation[] = $this->l('¡Felicidades, Se ha modificado la configuración de Banwire!');
		}
	}

	public function hookPayment($params)
	{
		/*Cargamops TPL de pagos en Banwire*/
		$this->context->smarty->assign(array(
				'banwire_action' 	=> $this->getModuleLink('banwire', 'payment'),
				'tdc' 			=> Configuration::get('BANWIRE_TDC'),
				'oxxo' 			=> Configuration::get('BANWIRE_OXXO'),
				'spei' 			=> Configuration::get('BANWIRE_SPEI'),
				'rec' 			=> Configuration::get('BANWIRE_REC'),
				'this_path'         	=> $this->getPathUri()
				));
		return $this->display(__FILE__, 'views/templates/hook/standard.tpl');
	}
	
	public function hookPaymentReturn($params) {
		if (!$this->active)
			return;
			
			$smartyArray = array();
			$smartyArray['Método'] 	= $_GET['Banwire_metodo'];
			$smartyArray['Total'] 	= Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false);
			$smartyArray['Orden'] 	= $params['objOrder']->id;

			$imprimir = false;
			$tpl = 'payment_return.tpl';
			
			switch ( $_GET['api'] ) {
				case'tdc':
					if(!Configuration::get('BANWIRE_TDC_RED')){
						$smartyArray['Autorización'] 	= $_GET['Banwire_autorizacion'];
					}elseif(Configuration::get('BANWIRE_TDC_RED') && isset( $_GET['Banwire_referecia']) ){
						$smartyArray['Referencia'] 	= $_GET['Banwire_referecia'];
						$smartyArray['Tarjeta'] 	= $_GET['Banwire_tarjeta'];	
					}else {
						$smartyArray['Challenge'] 	= $_GET['Banwire_challenge'];
					}
					break;
				case 'oxxo':
					$imprimir = true;
					$tpl = 'payment_return_banwire.tpl';	

					$smartyArray['barcode_img'] = $_GET['Banwire_codigo_img'];
					$smartyArray['C_Barras'] = $_GET['Banwire_codigo'];
					$smartyArray['Vigencia'] = $_GET['Fecha_vigencia'];
					break;
				case 'spei':
					$smartyArray['SPEI'] 		= $_GET['Banwire_spei'];
					$smartyArray['Referencia'] 	= $_GET['Banwire_referencia'];
					break;
				case 'rec':
					if(isset($_GET['Banwire_codigo']))
						$smartyArray['Autorización'] = $_GET['Banwire_codigo'];
						
						$smartyArray['Token'] 		= $_GET['Banwire_token'];
						$smartyArray['Siguiente'] 	= $_GET['Fecha_cobro'];	
					break;
				default:
					break;
				
			}

			if($imprimir) $this->context->controller->addJS(($this->_path).'/js/jquery.PrintArea.js');
			
			$this->smarty->assign(array('data' => $smartyArray, 'imprimir' => $imprimir));
			
			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
				$this->smarty->assign('reference', $params['objOrder']->reference);
		
		return $this->display(__FILE__, $tpl);
	}

	public function getModuleLink($module, $controller = 'default', array $params = array(), $ssl = null)
	{
		if (version_compare(_PS_VERSION_, '1.5', '<'))
			$link = Tools::getShopDomainSsl(true)._MODULE_DIR_.$module.'/'.$controller.'?'.http_build_query($params);
		else
			$link = $this->context->link->getModuleLink($module, $controller, $params, $ssl);
			
		return $link;
	}
	
	public function checkCurrency($cart)
	{
		$currency_order = new Currency($cart->id_currency);
		$currencies_module = $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}

}
