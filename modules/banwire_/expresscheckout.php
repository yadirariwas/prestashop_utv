<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');


class banwire_expresscheckout extends Banwireform
{
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		/* Backward compatibility */
		require(_PS_MODULE_DIR_.'banwire/backward_compatibility/backward.php');
		$this->context->smarty->assign('base_dir', __PS_BASE_URI__);

		$this->banwire = new Banwire
	}
}

$validation = new banwire_expresscheckout();
$validation->initContent();

