<?php



/*

 *  @author Adrapok <adrapok@gmail.com>

 *

 *  Modulo de pago Banwire para PrestaShop

 */



class BanwireResponseSPEIModuleFrontController extends ModuleFrontController

{

	

	public function initContent()

	{

		$json = file_get_contents('php://input');

		

		$json = json_decode($json);

				

		if(isset ( $json->ord_id ) )  {

			$this->saveLB($json);

			$order = new Order((int)Order::getOrderByCartId($json->ord_id));

			

			//if($order->total_paid != null && $order->total_paid == $json->amount ){

			if($order->total_paid != null && isset($_POST['CODE_AUTH']) && $_POST['RESPONSE'] == 'ok' ){

				//Obtengo el estatus de la orden

				$resp_order = Db::getInstance()->executeS('SELECT current_state FROM '._DB_PREFIX_.'orders WHERE id_order = "'.(int)$order->id.'"');

				$current_state = (!empty($resp_order) ? $resp_order[0]['current_state'] : null);				

				//Si el estatus de la orden es igual a SPEI: Confirmado (16) duplicamos la orden

				if($current_state == 16){

					$this->getDuplicateOrder($order->id, $json->ord_id);

				}				

			}

			

			if($order->total_paid != null ){				

				$new_history = new OrderHistory();

				$new_history->id_order = (int)$order->id;				

				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_SPEI_CONFIRMADO'), $order, true);

				$new_history->addWithemail(true);

			} else exit('No Mach');

		}else exit('No json');

		exit();

	}

	

	private function saveLB($json) {

		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 

							id_customer = '0', 

							id_cart = '" . $json->ord_id . "', 

							response = '" . serialize($json) . "', 

							send = 'response SPEI', 

							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 

							date_add = NOW();"

						);	

	}

	

	public function getDuplicateOrder($order, $cart){

		//Obtengo el id del cliente

		$resp_customer = Db::getInstance()->executeS('SELECT id_customer FROM '._DB_PREFIX_.'orders WHERE id_order = "'.$order.'"');						

		$customer_id = (!empty($resp_customer) ? $resp_customer[0]['id_customer'] : null);				

		$customer = new Customer($customer_id);

				

		/*		

		//1.- Replicamos el carro de la compra

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'cart (id_shop_group, id_shop, id_carrier, delivery_option, id_lang, id_address_delivery, id_address_invoice, 

			id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, mobile_theme, allow_seperated_package, date_add, date_upd) 

			SELECT id_shop_group, id_shop, id_carrier, delivery_option, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, 

			"'.$customer->secure_key.'", recyclable, gift, gift_message, mobile_theme, allow_seperated_package, now(), now()

			FROM '._DB_PREFIX_.'cart WHERE id_cart = "'.$cart.'"');



		//Obtengo el ultimo id de la tabla de ps_cart

		$result_insert_cart = Db::getInstance()->executeS('SELECT MAX(id_cart) AS last_insert FROM '._DB_PREFIX_.'cart WHERE id_customer = "'.$customer_id.'"');						

		$last_insert_cart = (!empty($result_insert_cart) ? $result_insert_cart[0]['last_insert'] : null);			

			

		//2.- Inserto en la tabla de cart_product

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_address_delivery, id_shop, id_product_attribute, quantity, date_add) 

			SELECT "'.$last_insert_cart.'", id_product, id_address_delivery, id_shop, id_product_attribute, quantity, now()

			FROM '._DB_PREFIX_.'cart_product WHERE id_cart = "'.$cart.'"');

			

		//Inserto en la tabla de ps_cart_cart_rule para los vales que haya tenido el cliente

		Db::getInstance()->Execute('INSERT INTO ps_cart_cart_rule (id_cart, id_cart_rule) 

			SELECT "'.$last_insert_cart.'", id_cart_rule FROM '._DB_PREFIX_.'cart_cart_rule WHERE id_cart = "'.$cart.'"');				

		*/	

			

		//3.- Inserto en la tabla de orders

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'orders (reference, id_shop_group, id_shop, id_carrier, id_lang, id_customer, id_cart, id_currency, id_address_delivery, 

			id_address_invoice, current_state, secure_key, payment, conversion_rate, module, recyclable, gift, gift_message, mobile_theme, shipping_number, total_discounts, 

			total_discounts_tax_incl, total_discounts_tax_excl, total_paid, total_paid_tax_incl, total_paid_tax_excl, total_paid_real, total_products, total_products_wt, 

			total_shipping, total_shipping_tax_incl, total_shipping_tax_excl, carrier_tax_rate, total_wrapping, total_wrapping_tax_incl, total_wrapping_tax_excl, invoice_number, 

			delivery_number, invoice_date, delivery_date, valid, date_add, date_upd) 

			SELECT "'. Tools::passwdGen(9, 'NO_NUMERIC') .'", id_shop_group, id_shop, id_carrier, id_lang, id_customer, "'.$cart.'", id_currency, id_address_delivery, id_address_invoice, current_state, 

			"'.$customer->secure_key.'", payment, conversion_rate, module, recyclable, gift, gift_message, mobile_theme, shipping_number, total_discounts, 

			total_discounts_tax_incl, total_discounts_tax_excl, total_paid, total_paid_tax_incl, total_paid_tax_excl, total_paid_real, total_products, total_products_wt, total_shipping, 

			total_shipping_tax_incl, total_shipping_tax_excl, carrier_tax_rate, total_wrapping, total_wrapping_tax_incl, total_wrapping_tax_excl, invoice_number, delivery_number, 

			invoice_date, delivery_date, valid, now(), now() 

			FROM '._DB_PREFIX_.'orders WHERE id_order = "'.$order.'"');

		

		//Obtengo el ultimo id de la tabla que sera asociado a la tabla de mensajes

		$result_insert_order = Db::getInstance()->executeS('SELECT MAX(id_order) AS last_insert FROM '._DB_PREFIX_.'orders WHERE id_customer = "'.$customer_id.'"');						

		$last_insert_order = (!empty($result_insert_order) ? $result_insert_order[0]['last_insert'] : null);

		

		//4.- Inserto en la tabla del detalle

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'order_detail (id_order, id_order_invoice, id_warehouse, id_shop, product_id, product_attribute_id, 

			product_name, product_quantity, product_quantity_in_stock, product_quantity_refunded, product_quantity_return, product_quantity_reinjected, product_price, 

			reduction_percent, reduction_amount, reduction_amount_tax_incl, reduction_amount_tax_excl, group_reduction, product_quantity_discount, product_ean13, product_upc, 

			product_reference, product_supplier_reference, product_weight, tax_computation_method, tax_name, tax_rate, ecotax, ecotax_tax_rate, discount_quantity_applied, 

			download_hash, download_nb, download_deadline, total_price_tax_incl, total_price_tax_excl, unit_price_tax_incl, unit_price_tax_excl, total_shipping_price_tax_incl, 

			total_shipping_price_tax_excl, purchase_supplier_price, original_product_price) 

			SELECT "'.$last_insert_order.'", id_order_invoice, id_warehouse, id_shop, product_id, product_attribute_id, 

			product_name, product_quantity, product_quantity_in_stock, product_quantity_refunded, product_quantity_return, product_quantity_reinjected, product_price, 

			reduction_percent, reduction_amount, reduction_amount_tax_incl, reduction_amount_tax_excl, group_reduction, product_quantity_discount, product_ean13, product_upc, 

			product_reference, product_supplier_reference, product_weight, tax_computation_method, tax_name, tax_rate, ecotax, ecotax_tax_rate, discount_quantity_applied, 

			download_hash, download_nb, download_deadline, total_price_tax_incl, total_price_tax_excl, unit_price_tax_incl, unit_price_tax_excl, total_shipping_price_tax_incl, 

			total_shipping_price_tax_excl, purchase_supplier_price, original_product_price

			FROM '._DB_PREFIX_.'order_detail WHERE id_order = "'.$order.'"');

			

		//5.- Insertamos en la tabla de messages

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'message (id_cart, id_customer, id_employee, id_order, message, private, date_add) 

			SELECT id_cart, id_customer, id_employee, "'.$last_insert_order.'", message, private, now() 

			FROM '._DB_PREFIX_.'message WHERE id_order = "'.$order.'"');

			

		//Inserto en la tabla de ps_order_history

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order, id_order_state, date_add) VALUES ("'.$last_insert_order.'", 21, now()');

						

					

		//6.- Resto al stock la cantidad del carro

		$cart_product = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = "'.$cart.'"');

		if(!empty($cart_product)){

			foreach($cart_product as $product){

				$id_product = $product[0]['id_product'];

				$quantity = $product[0]['quantity'];

				//Obtengo la cantidad en stock del producto

				$quantity = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'stock_available WHERE id_product = "'.$id_product.'"');

				$new_quantity = (int)$quantity[0]['quantity'] - (int)$quantity;

				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'stock_available SET quantity = "'.$new_quantity.'" WHERE id_product = "'.$id_product.'"');

			}

		}

		

		//7.- Insertamos en la tabla de logs

		Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'duplicate_order_log (id_order, message) VALUES ("'.$last_insert_order.'", "Se ha duplicado la orden, es: SPEI")');	

		

		//Mando llamar la función para el envio del mail

		$this->sendMail($customer_id, $last_insert_order);

		

	}

	

	

	public function sendMail($customer_id, $order){

		

		//require_once('C:/xampp/htdocs/roku/classes/PHPMailer/class.phpmailer.php');

		require_once('/home/canalutv/public_html/classes/PHPMailer/class.phpmailer.php');

		

		//Inicio recuperación de datos para llenar el mail

		//Se obtienen los datos del cliente que realiza el pedido

		$data_customer = Db::getInstance()->executeS('SELECT firstname, lastname, email FROM '._DB_PREFIX_.'customer WHERE id_customer = "'.$customer_id.'"');

		if(!empty($data_customer)){

			$nombre_completo = $data_customer[0]['firstname'] . " " . $data_customer[0]['lastname'];

			$email = $data_customer[0]['email'];

		}



		//Se obtienen datos del pedido

		$data_order = Db::getInstance()->executeS('SELECT reference, DATE_FORMAT(date_add, "%d/%m/%Y") as fecha_creacion FROM '._DB_PREFIX_.'orders WHERE id_order = "'.$order.'"');

		if(!empty($data_order)){

			$referencia = $data_order[0]['reference'];

			$date_add = $data_order[0]['fecha_creacion'];

		}



		//Se obtienen los datos del cuerpo del mensaje

		$data_message = Db::getInstance()->executeS('SELECT message FROM '._DB_PREFIX_.'message WHERE id_order = "'.$order.'"');

		if(!empty($data_message)){

			foreach($data_message as $message){

				$mensaje .= $message['message'] . '<br>';

			}

		}

		//Fin



		//Incio armado del mail

		$mail = new PHPMailer;

		 

		/** Configurar SMTP **/

		$mail->isSMTP();                                    // Indicamos que use SMTP

		$mail->Host = 'smtp.gmail.com';  					// Indicamos los servidores SMTP

		$mail->SMTPAuth = true;                             // Habilitamos la autenticación SMTP

		$mail->Username = 'info@adimpactomexico.com';       // SMTP username

		$mail->Password = '04D%m0:PQw';                       // SMTP password

		$mail->SMTPSecure = 'ssl';                          // Habilitar encriptación TLS o SSL

		$mail->Port = 465;                                  // TCP port

		 

		/** Configurar cabeceras del mensaje **/

		$mail->From = 'info@adimpactomexico.com';     // Correo del remitente

		$mail->FromName = 'Under TV';           	  // Nombre del remitente

		$mail->Subject = 'SPEI: Pago Recurrente';      // Asunto

		 

		/** Incluir destinatarios. El nombre es opcional **/

		$mail->addAddress('undertvmexico@yahoo.com');		
		$mail->addAddress('mane14071@hotmail.com');		
		$mail->addAddress('info@canalutv.mx');		
		$mail->addAddress('utvcanalroku@gmail.com');		
		$mail->addAddress('angel.servin@webmasterproyect.com');		
		$mail->addAddress('honorio0106@gmail.com');		
		//$mail->addAddress('tiagox@mac.com');		

 

		/** Enviarlo en formato HTML **/

		$mail->isHTML(true);                                  

		 

		/** Configurar cuerpo del mensaje **/

		$mail->Body = '<table class="table table-mail" style="width: 100%; margin-top: 10px; -moz-box-shadow: 0 0 5px #afafaf; -webkit-box-shadow: 0 0 5px #afafaf; -o-box-shadow: 0 0 5px #afafaf; box-shadow: 0 0 5px #afafaf; filter: progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5);">

		<tbody>

		<tr>

		<td class="space" style="width: 20px; padding: 7px 0;">&nbsp;</td>

		<td style="padding: 7px 0;" align="center">

		<table class="table" style="width: 100%;" bgcolor="#ffffff">

		<tbody>

		<tr>

		<td class="logo" style="border-bottom: 4px solid #333333; padding: 7px 0;" align="center"></td>

		</tr>

		<tr>

		<td class="titleblock" style="padding: 7px 0;" align="center"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> <span class="title" style="font-weight: 500; font-size: 28px; text-transform: uppercase; line-height: 33px;">Enhorabuena!</span> </span></td>

		</tr>

		<tr>

		<td class="linkbelow" style="padding: 7px 0;"><span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> Tiene un nuevo pedido en su tienda Under TV realizado por el cliente : '.$nombre_completo.' ('.$email.') </span></td>

		</tr>

		<tr>

		<td class="space_footer" style="padding: 0!important;">&nbsp;</td>

		</tr>

		<tr>

		<td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" colspan="3">

		<table class="table" style="width: 100%;">

		<tbody>

		<tr>

		<td style="padding: 7px 0;" width="10">&nbsp;</td>

		<td style="padding: 7px 0;">

		<p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;" data-html-only="1">Detalles del pedido</p>

		<span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"> <span style="color: #777;"> <span style="color: #333;"> <strong>Pedido:</strong> </span> '.$referencia.' Situado en '.$date_add.'<br /><br /> <span style="color: #333;"><strong>Pago:</strong> </span>  banwire<br /><br /> </span> </span></td>

		<td style="padding: 7px 0;" width="10">&nbsp;</td>

		</tr>

		</tbody>

		</table>

		</td>

		</tr>

		<tr>

		<td class="box" style="border: 1px solid #D6D4D4; background-color: #f8f8f8; padding: 7px 0;" colspan="3">

		<table class="table" style="width: 100%;">

		<tbody>

		<tr>

		<td style="padding: 7px 0;" width="10">&nbsp;</td>

		<td style="padding: 7px 0;">

		<p style="border-bottom: 1px solid #D6D4D4; margin: 3px 0 7px; text-transform: uppercase; font-weight: 500; font-size: 18px; padding-bottom: 10px;" data-html-only="1">Mensaje:</p>

		<span style="color: #555454; font-family: Open-sans,sans-serif; font-size: small;"><span style="color: #777;"> '. print_r($mensaje, true) . ' </span> </span></td>

		<td style="padding: 7px 0;" width="10">&nbsp;</td>

		</tr>

		</tbody>

		</table>

		</td>

		</tr>

		<tr>

		<td class="space_footer" style="padding: 0!important;">&nbsp;</td>

		</tr>

		<tr>

		<td class="footer" style="border-top: 4px solid #333333; padding: 7px 0;">

		<p><a style="color: #337ff1;" href="canalutv.mx">Under TV</a></p>

		</td>

		</tr>

		</tbody>

		</table>

		</td>

		<td class="space" style="width: 20px; padding: 7px 0;">&nbsp;</td>

		</tr>

		</tbody>

		</table>

		<p> </p>';



		$mail->AltBody = '';

		 

		/** Para que use el lenguaje español **/

		$mail->setLanguage('es');

		$mail->send();

	}	

	

}

