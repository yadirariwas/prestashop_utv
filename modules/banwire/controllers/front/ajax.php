<?php
/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */
class BanwireAjaxModuleFrontController extends ModuleFrontController
{
	/**
	* @see FrontController::initContent()
	*/
	private $params 		= array();
	private $url_tdc_red 		= 'https://www.banwire.com/api/1/payment/direct';
	private $url_tdc_pro 		= 'https://www.banwire.com/api.pago_pro';
	private $url_oxxo 		= 'https://www.banwire.com/api.oxxo';
	private $url_spei 		= 'https://www.banwire.com/api/1/payment/spei';
	private $url_rec 		= 'https://banwiresecure.com/RecurrenteV4/api/pago_recurrente';
	/* url´s test*/
	private $url_tdc_red_test 	= 'https://test.banwire.com/api/1/payment/direct';
	private $url_tdc_pro_test 	= 'https://test.banwire.com/api.pago_pro';
	private $url_oxxo_test 		= 'https://test.banwire.com/api.oxxo';
	private $url_spei_test 		= 'https://test.banwire.com/api/1/payment/spei';
	private $url_rec_test 		= 'https://banwiresecure.com/RecurrenteV4/api/pago_recurrente';
	//private $url_rec_test 		= 'https://banwiresecure.com/sb/pago_recurrente';
	/*Variables de Banwire*/
	private $url_banwire 		= null;
	private $respuesta 		= null;
	private $api 			= null;
	/* Variables de PS*/
	private $bw_cart 		= array();
	private $bw_customer 		= array();
	private $addresses 		= array();
	private $order 			= array();
	private $productos 		= array();
	
	public function initContent(){	

        $errors = array();
		
        if(!isset($_POST['banwire_api']))
            $errors[] = $this->module->l('No ha seleccionado ningún método de pago de Banwire.');
        if(count($errors) > 0)
			die( Tools::jsonEncode(array('errors' => $errors)));
            
        $this->bw_cart 		= $this->context->cart;
		$this->bw_customer 	= $this->context->customer;
		$this->order 		= new Order((int)Order::getOrderByCartId($this->bw_cart->id));
		$this->productos 	= $this->bw_cart->getProducts();
		$addresses 		= $this->bw_customer->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
		$this->addresses 	= $addresses[0];
		
		if(isset($this->addresses['id_country']) && !empty($this->addresses['id_country'])){
			$this->addresses['country_name'] = $this->addresses['country'];
			$this->addresses['country'] = Country::getIsoById($this->addresses['id_country']);
		}
		if(isset($this->addresses['state_iso']) && !empty($this->addresses['state_iso'])){
			$this->addresses['state_iso'] = substr($this->addresses['state_iso'],0,2);
		}
		$this->api 		= $_POST['banwire_api'];
		if($this->api == 'tdc' && isset($_POST['banwire']['recurrencia']))
            $this->api = 'rec';
		
		
		//echo print_r($this->shipping,true); exit;
		
		$angel=0;
		
		if($this->api=="rec"){
			foreach($_POST['banwire'] as $key => $value){
				$_POST['banwire'][$key]=$_POST['banwire_rec'][$key];
			}
			
			$_POST['select_rec_yes']=$_POST['select_rec_yes_rec'];
			$_POST['banwire_vig_month']=$_POST['banwire_vig_month_rec'];
			$_POST['banwire_vig_year']=$_POST['banwire_vig_year_rec'];
		}
		
		//echo print_r($_POST['banwire'],true);
		//echo $this->api; exit;
		
		
		if($this->api=="tdc" || $this->api=="rec"){
			if(strlen($_POST['banwire']['card_name'])<1){
				$errors[] = "Introduzca el nombre del tarjetahabiente. ";
				$angel=1;
			}
			if($_POST['banwire']['card_type']=="amex" && strlen($_POST['banwire']['card_num'])!=15){
				$errors[] = "Su tarjeta debe tener 15 dígitos. ";
				$angel=1;
			}
			elseif($_POST['banwire']['card_type']!="amex" && strlen($_POST['banwire']['card_num'])!=16){
				$errors[] = "Su tarjeta debe tener 16 dígitos. ";
				$angel=1;
			}
			if($_POST['banwire']['card_type']=="amex" && strlen($_POST['banwire']['card_ccv'])!=4){
				$errors[] = "Su código CCV debe tener 4 dígitos. ";
				$angel=1;
			}
			elseif($_POST['banwire']['card_type']!="amex" && strlen($_POST['banwire']['card_ccv'])!=3){
				$errors[] = "Su código CCV debe tener 3 dígitos. ";
				$angel=1;
			}
		}
		
		//$errors[] = "Prueba Angel";
		
		
		
		///AQUI INICIO
		if($angel!=1){
			
			switch($this->api){
				case 'tdc':
					$this->_engineTDC();
					$this->_paymentBanwire();
					break;
				case 'oxxo':
					$this->_engineOXXO();
					$this->_paymentBanwire();
					break;
				case 'spei':
					$this->_engineSPEI();
					$this->_paymentBanwire();
					break;
				case 'rec':
					$this->api='tdc';
					$this->_engineTDC();
					$this->_paymentBanwire();
					if(Configuration::get('BANWIRE_TDC_RED') && isset($this->respuesta->AUTH_CODE)){
						$this->api='rec';
						$respuesta_tdc = $this->respuesta;
						$this->_engineREC();
						$this->_paymentBanwire();
					}
					break;
				default:
					$this->displayErrorBanwire();
					break;
			}
		
		}
		/////AQUI FIN
			
		if( (isset($this->respuesta->code)) ){
			$errors[] = 'Error ' . $this->respuesta->code . ' ' . (isset($this->respuesta->parameter) ? $this->respuesta->parameter : '') . ': ' . $this->respuesta->message;
		}
		 
		if( isset($this->respuesta->ERROR_MSG) && isset($this->respuesta->ERROR_CODE ) && $this->respuesta->ERROR_CODE != 500 ){
			if($this->respuesta->ERROR_MSG == "denied"){
				$errors[] = "Su tarjeta ha sido rechazada por el banco emisor, por favor comuníquese con su institución financiera.";			
			}
			elseif($this->respuesta->ERROR_MSG == "Denegado"){
				$errors[] = "Su tarjeta ha sido rechazada por su banco. Póngase en contacto con su banco antes de intentar esta compra de nuevo. También puede escoger pagar esta compra en OXXO o SPEI.";	
			}
			else{			
				$errors[] = $this->respuesta->ERROR_CODE . ': ' .$this->respuesta->ERROR_MSG;
			}
		}
		if($this->api == 'rec'){
			if(isset($this->respuesta['RESPONSE']) && $this->respuesta['RESPONSE'] == 'ko'){
				$errors[] = $this->respuesta['MESSAGE'];
			}
		}
		
		if(count($errors) <= 0 && $this->respuesta != null )  {
			$redirect 	= '';
			$currency 	= $this->context->currency;
			$total 		= (float)$this->bw_cart->getOrderTotal(true, Cart::BOTH);
			switch($this->api){
				case 'tdc':
					if ( isset($this->respuesta->ERROR_CODE) && $this->respuesta->ERROR_CODE == 500 ) {
						$order_status_id 	= Configuration::get('STATUS_BANWIRE_TDC_REVISION');
						$mailVars 		= array(
									'{Banwire_metodo}' 		=> 'BANWIRE-TDC',
									'{Banwire_challenge}' 		=> 'Pendiente',
									'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante TDC'));
						$mensaje = $this->module->l('El pago ha pasado a un proceso de validacion. Tendra respuesta de 24 a 72 hrs.');
					}
					
					if(!Configuration::get('BANWIRE_TDC_RED')){
						$order_status_id 	= Configuration::get('STATUS_BANWIRE_TDC');
						$mailVars 		= array(
									'{Banwire_metodo}' 		=> 'BANWIRE-TDC-PRO',
									'{Banwire_autorizacion}' 	=> $this->respuesta->code_auth,
									'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante TDC-PRO'));
						$mensaje = $this->module->l('El pago se confirmo mediante API_PAGO_PRO de Banwire. Con autorización: '. $this->respuesta->code_auth);
					}
					
					if(Configuration::get('BANWIRE_TDC_RED') && isset($this->respuesta->AUTH_CODE)){
						
						$order_status_id 	= Configuration::get('STATUS_BANWIRE_TDC');
						$mailVars 		= array(
									'{Banwire_metodo}' 		=> 'BANWIRE-TDC-RED',
									'{Banwire_tarjeta}' 		=> $this->respuesta->CARD,
									'{Banwire_referecia}' 		=> $this->respuesta->AUTH_CODE,
									'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante Banwire TDC-Shield'));
						$mensaje = $this->module->l('El pago se confirmo mediante TDC-Shield de Banwire. Con autorización: ' . $this->respuesta->AUTH_CODE);
					}
					break;
					
				case 'oxxo':
										
					if(!Configuration::get('BANWIRE_OXXO_SENDPDF')) {
						/* si esta activo el envio de PDF por banwire sera con un formato estandar para todos los usuarios que procesan con OXXO
						en caso de necesitar un PDF personalizado en esta seccion se agregara la llamada a dicho metodo para
						personalizar las variables que envia OXXO Estas son:
						stdClass Object
								(
								[error] => 
								[response] => stdClass Object
								    (
									//barcode_img es la imagen en base64 del codigo de barras generado por oxxo
									[barcode_img] => iVBORw0KGgoAAAANSUhEUgAAASwAAABOAQMAAACg+LnTAAAABlBMVEX///8AAABVwtN+AAABJ0lEQVRIiWP4Twz4wNDAQAQQGFVGkTLG4LCw1JlhWTNTo3xXppoF3QpL9bm6LCw4Y1TZqLJRZaPKRpWNKqOXMgJgYJWxN/FI2FR8UP7++DATI4+NgUDCgYPHm/nl0JRxuAmpOLVyJLGwqLQxCzl5GExUVOJwmbAQXZmXohNQ2SIWFg0vVkWnlkAuIREOjwB0ZQxOik5MLBxKLAwaTpxAZYJgZQ7oyhibFJ1kLAqUnx844SSoaAdSJn78hLM8mjImJrBpCgKKHEBlTGDTFDxcGNGUscCUKXE4yS+EKfNAV8bBBPaCgoAKhxMj3LQATGXgAFESUWFpYhRi8RCYqKii0DKBCU0Z//keUPD+fHz8MX8jj4VBTcKBw8eZ5eeQF1mjymijjLimOwDXuW4n5WEeIAAAAABJRU5ErkJggg==
									//codigo de barras para control
									[barcode] => 21003331552014090100069029
									//ID del pedido, mismo que se envia en la peticion inicial
									[referencia] => 10
									//fecha de vigencia del codigo de barras: numero de días dados de alta en el administrador 
									[fecha_vigencia] => 2014-09-01
									//monto por el cual se hace la solicitud
									[monto] => 69.02
								    )
								
								)
						*/
						//die(Tools::jsonEncode(array('errors' => $this->l('No esta activo el envío de PDF por parte de Banwire. Pongase en contacto con el administrador de la Tienda.'))));
					}
					
					$order_status_id 	= Configuration::get('STATUS_BANWIRE_OXXO_ESPERA');
					//echo print_r($this->respuesta->response,true);
					$mailVars 		= array(
									'{Banwire_metodo}' 		=> 'BANWIRE-OXXO',
									'{Banwire_codigo_img}' 		=> $this->respuesta->response->barcode_img,
									'{Banwire_codigo}' 		=> $this->respuesta->response->barcode,
									'{Fecha_vigencia}'		=> $this->respuesta->response->fecha_vigencia,
									'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante OXXO'));
					$mensaje = $this->module->l('Se ha enviado solicitud de pago para OXXO con fecha limite de pago: ') . $this->respuesta->response->fecha_vigencia;
					
					break;
				case 'spei':
					
					if(isset($this->respuesta->clabe ) ) {
						$order_status_id = Configuration::get('STATUS_BANWIRE_SPEI_ESPERA');
						$mailVars 		= array(
									'{Banwire_metodo}' 		=> 'BANWIRE-SPEI',
									'{Banwire_spei}' 		=> $this->respuesta->clabe,
									'{Banwire_referencia}'		=> isset( $this->respuesta->referencia ) ? $this->respuesta->referencia : 'N/A',
									'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante SPEI'));
						$mensaje = $this->module->l('Se ha enviado clabe SPEI para transferencia.') . ' <br /> <b> Clabe: </b> ' . $this->respuesta->clabe . ' <br /><b>Referencia: </b>' .  (isset($this->respuesta->referencia) ? $this->respuesta->referencia : 'N/A' );
					}
						
					break;
				case 'rec':
					if ( isset($this->respuesta['AUTH_CODE'])){
						$order_status_id 	= Configuration::get('STATUS_BANWIRE_REC');
						$mailVars 		= array(
								'{Banwire_metodo}' 		=> 'BANWIRE RECURRENTE',
								'{Banwire_codigo}' 		=> $this->respuesta['AUTH_CODE'],
								'{Banwire_token}' 		=> $this->respuesta['TOKEN'],
								'{Fecha_cobro}'			=> $this->respuesta['NEXT_PAYMENT'],
								'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago recurrente'));
						$mensaje = $this->module->l('Se ha autorizado recurrente ID : ') . $this->respuesta['AUTH_CODE'] . $this->module->l(' Token: ') . $this->respuesta['TOKEN'] . $this->module->l(' Siguiente cobro') . $this->respuesta['NEXT_PAYMENT'];
					}
					if ( !isset($this->respuesta['AUTH_CODE'])){
						//$order_status_id 	= Configuration::get('STATUS_BANWIRE_REC_SCOSTO');
						//'{Banwire_metodo}' 		=> 'BANWIRE RECURRENTE INICIAL SIN COSTO',
						//'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante recurrente inical sin costo'));
						$order_status_id 	= Configuration::get('STATUS_BANWIRE_REC');
						$mailVars 		= array(
								'{Banwire_metodo}' 		=> 'BANWIRE RECURRENTE',
								'{Banwire_token}' 		=> $this->respuesta['TOKEN'],
								'{Fecha_cobro}'			=> $this->respuesta['NEXT_PAYMENT'],
								'{banwire_address_html}' 	=> str_replace("\n", '<br />', 'Pago recurrente'));
						$mensaje = $this->module->l('Se ha autorizado recurrente ID : ') . $this->respuesta['AUTH_CODE'] . $this->module->l(' Token: ') . $this->respuesta['TOKEN'] . $this->module->l(' Siguiente cobro') . $this->respuesta['NEXT_PAYMENT'];
						//$mensaje = $this->module->l('Se ha guardado recurrente en periodo inicial sin costo con el token: ') . $this->respuesta['TOKEN'] . $this->module->l(' La fecha de cobro inicial sera: '). $this->respuesta['NEXT_PAYMENT'] ; 
					}
					break;
				default:
					Tools::redirect('index.php?controller=order&step=1');
					break;
			}
		} else{
			if($this->respuesta->ERROR_MSG != "Denegado"){
				$errors[] = $this->module->l('No se puede procesar por el momento la transacción. Intente de nuevo mas tarde.');
			}
		}
		
		if(count($errors) > 0)
			die( Tools::jsonEncode(array('errors' => $errors)));	

		$this->module->validateOrder((int)$this->bw_cart->id, $order_status_id, $total, $this->module->name, $mensaje, $mailVars, (int)$currency->id, false, $this->bw_customer->secure_key);
	
	
			$redirectArray = array('id_cart' => (int)$this->context->cart->id, 'id_module' => (int)$this->module->id, 'id_order' => (int)$this->module->currentOrder, 'key' => $this->bw_customer->secure_key);
			foreach ( $mailVars as $index => $value ) {
				$indx = str_replace('{', '', $index);
				$indx = str_replace('}', '', $indx);
				$redirectArray[$indx] = urlencode($value);
			}
			$redirectArray['api'] = $this->api;
			
		if(version_compare(_PS_VERSION_, '1.5', '<'))
			$redirect = Link::getPageLink('order-confirmation.php', $redirectArray);
		else {
			$redirect = __PS_BASE_URI__.'index.php?controller=order-confirmation';
			foreach ( $redirectArray as $key => $value )
				$redirect .= '&' . $key . '=' . $value;
		}

		die( Tools::jsonEncode(array('redirect' => $redirect)));
	}

	private function _engineTDC(){
		
		$banwire 		= $_POST['banwire'];
		
		$this->shipping 	= new Address(intval($this->bw_cart->id_address_delivery));
		
		//echo print_r($this->shipping,true); exit;

		if(isset($this->shipping->id_country) && !empty($this->shipping->id_country)){
			$this->shipping->country_name = $this->shipping->country;
			$this->shipping->country = Country::getIsoById($this->shipping->id_country);
		}

		if(isset($this->shipping->state_iso) && !empty($this->shipping->state_iso)){
			$this->shipping->state_iso = substr($this->shipping->state_iso,0,2);
		}

		$this->url_banwire = $this->url_tdc_pro_test;
		if(Configuration::get('BANWIRE_TDC_RED'))
			$this->url_banwire = $this->url_tdc_red_test;
			
		if(Configuration::get('BANWIRE_TDC_PRO') && Configuration::get('BANWIRE_TDC_RED') && !Configuration::get('BANWIRE_SANDBOX'))
			$this->url_banwire = $this->url_tdc_red;
		if(Configuration::get('BANWIRE_TDC_PRO') && Configuration::get('BANWIRE_TDC_RED') && Configuration::get('BANWIRE_SANDBOX'))
			$this->url_banwire = $this->url_tdc_red_test;
		
		if(!Configuration::get('BANWIRE_TDC_PRO') && Configuration::get('BANWIRE_TDC_RED') && !Configuration::get('BANWIRE_SANDBOX'))
			$this->url_banwire = $this->url_tdc_red;
			
		$postcode_amex 	= ($banwire['card_type'] == 'amex' ? $banwire['postcode'] : $this->addresses['postcode'] );
		$address_amex 	= ($banwire['card_type'] == 'amex' ? $banwire['direction']: $this->addresses['address1'] );
		
		if(!Configuration::get('BANWIRE_TDC_RED')){
			$cardExp 	= ($_POST['banwire_vig_month'] < 10 ? '0'. $_POST['banwire_vig_month'] : $_POST['banwire_vig_month']) . '/' . substr($_POST['banwire_vig_year'], 2);
			
			$this->params = array(
					'response_format' 	=> 'JSON', // Formato de la respues HTTPQUERY | JSON
					'user' 			=> Configuration::get('BANWIRE_USERNAME'), // Nombre de usuario en BanWire.com.
					'reference' 		=> $this->bw_cart->id, // Referencia de la transacción.
					'currency' 		=> 'MXN', // Tipo de moneda MXN | USD.
					'ammount' 		=> $this->bw_cart->getOrderTotal(true, Cart::BOTH), // Monto total a pagar.
					'concept' 		=> configuration::get('BANWIRE_CONCEPT'), // Concepto de pago.
					'card_num'      	=>  $banwire['card_num'], // Número en la tarjeta.
					'card_name'     	=>  $banwire['card_name'], // Nombre en la tarjeta.
					'card_type'     	=>  $banwire['card_type'], // Tipo de tarjeta visa | mastercard | amex.
					'card_exp'      	=>  $cardExp, // Expiración de la tarjeta.
					'card_ccv2'     	=>  $banwire['card_ccv'], // Código de Seguridad de la terjeta.
					'address'       	=>  $address_amex,
					'post_code'     	=>  $postcode_amex,
					'phone'         	=>  $this->addresses['phone_mobile'], // Teléfono del tarjeta habiente.
					'EBT_DEVICEPRINT' 	=> $_POST['EBT_DEVICEPRINT'],
					'mail'          	=>  $this->bw_customer->email // Mail del tarjeta habiente.
				    );
		}
		
		if(Configuration::get('BANWIRE_TDC_RED')){
			$cardExp 	= ($_POST['banwire_vig_month'] < 10 ? '0'. $_POST['banwire_vig_month'] : $_POST['banwire_vig_month']) . substr($_POST['banwire_vig_year'], 2);
			
			$addats['phone']=str_replace(' ', '', $this->addresses['phone_mobile']);
			if(trim($addats['phone'])==""){
				$addats['phone']='11111111';
			}
			if(strlen($addats['phone'])>15){
				$addats['phone']=substr($addats['phone'], 0, 15);
			}
			$addats['phone'] = ereg_replace("[^0-9]", "", $addats['phone']);
			
			$addats['email']=$this->bw_customer->email;
			if(trim($addats['email'])==""){
				$addats['email']='correo@dominio.com';
			}
			if(strlen($addats['email'])>40){
				$addats['email']=substr($addats['email'], 0, 40);
			}
			
			$addats['firstname']=$this->bw_customer->firstname;
			if(trim($addats['firstname'])==""){
				$addats['firstname']='firstname';
			}
			if(strlen($addats['firstname'])>30){
				$addats['firstname']=substr($addats['firstname'], 0, 30);
			}
			
			$addats['lastname']=$this->bw_customer->lastname;
			if(trim($addats['lastname'])==""){
				$addats['lastname']='lastname';
			}
			if(strlen($addats['lastname'])>30){
				$addats['lastname']=substr($addats['lastname'], 0, 30);
			}
			
			if(trim($address)==""){
				$address="address";
			}
			if(strlen($address)>30){
				$address=substr($address, 0, 30);
			}
			
			if(trim($address_amex)==""){
				$address_amex="address";
			}
			if(strlen($address_amex)>30){
				$address_amex=substr($address_amex, 0, 30);
			}
			
			$addats['city']=$this->addresses['city'];
			if(trim($addats['city'])==""){
				$addats['city']="city";
			}
			if(strlen($addats['city'])>20){
				$addats['city']=substr($addats['city'], 0, 20);
			}
			
			$addats['state_iso']=$this->addresses['state_iso'];
			if(trim($addats['state_iso'])==""){
				$addats['state_iso']="state_iso";
			}
			if(strlen($addats['state_iso'])>3){
				$addats['state_iso']=substr($addats['state_iso'], 0, 3);
			}
			
			$addats['postcode']=$this->addresses['postcode'];
			if(trim($addats['postcode'])==""){
				$addats['postcode']='11111';
			}
			if(strlen($addats['postcode'])<5){
				$addats['postcode']=str_pad($addats['postcode'], 5, "0", STR_PAD_LEFT);
			}
			elseif(strlen($addats['postcode'])>7){
				$addats['postcode']=substr($addats['postcode'], 0, 7);
			}
			
			$addats['CUST_CNTRY_CD']="MEX";
			$addats['country']=$this->addresses['country'];
			if(trim($addats['country'])==""){
				$addats['country']="MXN";
			}
			if(strlen($addats['country'])>3){
				$addats['country']=substr($addats['country'], 0, 3);
			}
			if(trim($addats['country'])=="MXN"){
				$addats['CUST_CNTRY_CD']="MEX";
			}
			
			if(trim($postcode)==""){
				$postcode='11111';
			}
			if(strlen($postcode)<5){
				$postcode=str_pad($postcode, 5, "0", STR_PAD_LEFT);
			}
			elseif(strlen($postcode)>7){
				$postcode=substr($postcode, 0, 7);
			}
			
			if(trim($postcode_amex)==""){
				$postcode_amex='11111';
			}
			if(strlen($postcode_amex)<5){
				$postcode_amex=str_pad($postcode_amex, 5, "0", STR_PAD_LEFT);
			}
			elseif(strlen($postcode_amex)>7){
				$postcode_amex=substr($postcode_amex, 0, 7);
			}
			
			$this->params 	= array(
					      'CARD_NUM' 		=> $banwire['card_num'],
					      'CARD_OWN' 		=> ($banwire['card_name'] && strlen($banwire['card_name']) > 30 ? substr($banwire['card_name'], 0, 30) : $banwire['card_name']),
					      'CARD_EXP_DT' 		=> $cardExp,
					      'CARD_CVV'	 	=> $banwire['card_ccv'],
					      'CARD_AVS_ADDR' 		=> $address_amex,
					      'CARD_AVS_ZIPCODE' 	=> $postcode_amex,
					      'CARD_TYPE' 		=> $banwire['card_type'],
					      'ORD_CURR_CD' 		=> 'MXN',
					      'ORD_ID' 			=> $this->bw_cart->id,
					      'ORD_AMT' 		=> $this->bw_cart->getOrderTotal(true, Cart::BOTH),
					      'ORD_CONCEPT' 		=> Configuration::get('BANWIRE_CONCEPT'),
					      'user' 			=> Configuration::get('BANWIRE_USERNAME'),
					      'EBT_PREVCUST' 		=> 'Y',
					      'EBT_DEVICEPRINT' 	=> $_POST['EBT_DEVICEPRINT'],
					      'CUST_IP' 		=> $_SERVER['REMOTE_ADDR'],
					      'ebWEBSITE' 		=> "https://" . $_SERVER['HTTP_HOST'] . ":" . $_SERVER['SERVER_PORT'] . dirname($_SERVER['REQUEST_URI']).'/',
					      'PROD_DEL_CD' 		=> Configuration::get('BANWIRE_PROD_DEL_CD'),
					      'NOTIFY_URL' 		=> $this->module->getModuleLink('banwire', 'responseTDC'),
					      'CUST_USER_ID' 		=> $this->bw_customer->id,
					      'CUST_PHONE' 		=> $addats['phone'],
					      'CUST_EMAIL' 		=> $addats['email'],
					      'CUST_FNAME' 		=> $addats['firstname'],
					      'CUST_LNAME' 		=> $addats['lastname'],
					      'CUST_MNAME' 		=> substr($addats['lastname'], 0,1),
					      'CUST_HOME_PHONE' 	=> $addats['phone'],
					      'CUST_WORK_PHONE' 	=> $addats['phone'],
					      'CUST_ADDR1' 		=> $address,
					      'CUST_CITY' 		=> $addats['city'],
					      'CUST_STPR_CD' 		=> $addats['state_iso'],
					      'CUST_POSTAL_CD' 		=> $addats['postcode'],
					      'CUST_CNTRY_CD' 		=> $addats['CUST_CNTRY_CD'],
					      'SHIP_ID' 		=> 'delivery',
					      'SHIP_HOME_PHONE' 	=> $addats['phone'],
					      'SHIP_EMAIL' 		=> $addats['email'],
					      'SHIP_FNAME' 		=> $addats['firstname'],
					      'SHIP_LNAME' 	 	=> $addats['lastname'],
					      'SHIP_MNAME' 	 	=> substr($addats['lastname'], 0,1),
					      'SHIP_MTHD_CD' 		=> Configuration::get('BANWIRE_SHIP_MTHD_CD'),
					      'SHIP_ADDR1' 		=> $address,
					      'SHIP_CITY' 		=> $addats['city'],
					      'SHIP_STPR_CD' 		=> $addats['state_iso'],
					      'SHIP_POSTAL_CD' 		=> $addats['postcode'],
					      'SHIP_CNTRY_CD' 		=> $addats['CUST_CNTRY_CD'],
					      );
			
			foreach ( $this->productos as $index => $product)
			{
				if(strlen($product['name'])>30){
					$product['name']=substr($product['name'], 0, 30);	
				}
				$this->params['ITEM_QTY'. ($index + 1)] 	= $product['quantity'];
				$this->params['ITEM_AMT' . ($index + 1)] 	= $product['total'];
				$this->params['ITEM_CST_AMT' . ($index + 1)] 	= $product['price'];
				$this->params['ITEM_DESC' . ($index + 1)] 	= $product['name'];
			}
		}
		
	}
	private function _engineOXXO(){
		$this->url_banwire = $this->url_oxxo_test;
		if(!Configuration::get('BANWIRE_SANDBOX'))
			$this->url_banwire = $this->url_oxxo;
		
		$this->params = array(
					'usuario' 	=> Configuration::get('BANWIRE_USERNAME'), // Nombre del usuario en BanWire.
					'referencia' 	=> $this->bw_cart->id, // Referencia de la transacción.
					'dias_vigencia' => 3, // Días que tendrá para realizar el pago, actualmente indefinido.
					'monto' 	=> $this->bw_cart->getOrderTotal(true, Cart::BOTH), // Monto a cobrar, máximo 10000.
					'url_respuesta' => $this->module->getModuleLink('banwire', 'responseOXXO'), // URL a donde se mandara el POST de la respuesta.
					'cliente' 	=> $this->bw_customer->firstname . ' ' . $this->bw_customer->lastname , // Nombre del cliente.
					'formato' 	=> 'JSON',  // Formato de la comunicación JSON | HTTPQUERY.
					'email' 	=> $this->bw_customer->email, // Si sendPDF es TRUE, manda el recibo de pago a este correo.
					'sendPDF' 	=> Configuration::get('BANWIRE_OXXO_SENDPDF') // Indica si BanWire envia el mail de pago.
				      );
	}
	
	private function _engineSPEI(){
		$this->url_banwire = $this->url_spei_test;
		if(!Configuration::get('BANWIRE_SANDBOX'))
			$this->url_banwire = $this->url_spei;
			
		$customer 	= array(
					'user_id' => $this->bw_customer->id,
					'name' 		=> $this->bw_customer->firstname . ' ' .  $this->bw_customer->lastname,
					'phone' 	=> ereg_replace("[^0-9]", "", $this->addresses['phone_mobile']),
					'email' 	=> $this->bw_customer->email,
					'ip' 		=> $_SERVER['REMOTE_ADDR']
					);
		
		$items 		= array();
		foreach( $this->productos as $product )
		{
			$items[] = array(
					 'quantity' 	=> $product['quantity'],
					 'unit_price' 	=> $product['price'],
					 'amount' 	=> $product['total'],
					 'description' 	=> $product['name']
					 );	
		}
		
		$this->params = array(
				      'user' 		=> Configuration::get('BANWIRE_USERNAME'),
				      'ord_id' 		=> $this->bw_cart->id,
				      'ord_amount' 	=> $this->bw_cart->getOrderTotal(true, Cart::BOTH),
				      'ord_concept' 	=> Configuration::get('BANWIRE_CONCEPT'),
				      'notify_url' 	=> $this->module->getModuleLink('banwire', 'responseSPEI'),
				      'customer' 	=> $customer,
				      'items' 		=> $items
				      );
	}
	
	private function _engineREC(){
		
		$banwire = $_POST['banwire_rec'];
		$_POST['select_rec_yes'] = $_POST['select_rec_yes_rec'];
		$_POST['banwire_vig_month'] = $_POST['banwire_vig_month_rec'];
		$_POST['anwire_vig_year'] = $_POST['anwire_vig_year_rec'];
		
		//echo "<pre><small>".print_r($_POST,true)."</small></pre>";
		
		$this->url_banwire = $this->url_rec_test;
		if( !Configuration::get('BANWIRE_SANDBOX') )
			$this->url_banwire = $this->url_rec;
		
		$postcode_amex 	= ($banwire['card_type'] == 'amex' ? $banwire['postcode'] : $this->addresses['postcode'] );
		$address_amex 	= ($banwire['card_type'] == 'amex' ? $banwire['direction']: $this->addresses['address1'] );
		
		$addats['phone']=str_replace(' ', '', $this->addresses['phone_mobile']);
		if(trim($addats['phone'])==""){
			$addats['phone']='11111111';
		}
		if(strlen($addats['phone'])>15){
			$addats['phone']=substr($addats['phone'], 0, 15);
		}
		$addats['phone'] = ereg_replace("[^0-9]", "", $addats['phone']);
		
		$addats['email']=$this->bw_customer->email;
		if(trim($addats['email'])==""){
			$addats['email']='correo@dominio.com';
		}
		if(strlen($addats['email'])>40){
			$addats['email']=substr($addats['email'], 0, 40);
		}
		
		$addats['firstname']=$this->bw_customer->firstname;
		if(trim($addats['firstname'])==""){
			$addats['firstname']='firstname';
		}
		if(strlen($addats['firstname'])>30){
			$addats['firstname']=substr($addats['firstname'], 0, 30);
		}
		
		$addats['lastname']=$this->bw_customer->lastname;
		if(trim($addats['lastname'])==""){
			$addats['lastname']='lastname';
		}
		if(strlen($addats['lastname'])>30){
			$addats['lastname']=substr($addats['lastname'], 0, 30);
		}
		
		if(trim($address)==""){
			$address="address";
		}
		if(strlen($address)>30){
			$address=substr($address, 0, 30);
		}
		
		if(trim($address_amex)==""){
			$address_amex="address";
		}
		if(strlen($address_amex)>30){
			$address_amex=substr($address_amex, 0, 30);
		}
		
		$addats['city']=$this->addresses['city'];
		if(trim($addats['city'])==""){
			$addats['city']="city";
		}
		if(strlen($addats['city'])>20){
			$addats['city']=substr($addats['city'], 0, 20);
		}
		
		$addats['state_iso']=$this->addresses['state_iso'];
		if(trim($addats['state_iso'])==""){
			$addats['state_iso']="state_iso";
		}
		if(strlen($addats['state_iso'])>3){
			$addats['state_iso']=substr($addats['state_iso'], 0, 3);
		}
		
		$addats['postcode']=$this->addresses['postcode'];
		if(trim($addats['postcode'])==""){
			$addats['postcode']='11111';
		}
		if(strlen($addats['postcode'])<5){
			$addats['postcode']=str_pad($addats['postcode'], 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($addats['postcode'])>7){
			$addats['postcode']=substr($addats['postcode'], 0, 7);
		}
		
		$addats['CUST_CNTRY_CD']="MEX";
		$addats['country']=$this->addresses['country'];
		if(trim($addats['country'])==""){
			$addats['country']="MXN";
		}
		if(strlen($addats['country'])>3){
			$addats['country']=substr($addats['country'], 0, 3);
		}
		if(trim($addats['country'])=="MXN"){
			$addats['CUST_CNTRY_CD']="MEX";
		}
		
		if(trim($postcode)==""){
			$postcode='11111';
		}
		if(strlen($postcode)<5){
			$postcode=str_pad($postcode, 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($postcode)>7){
			$postcode=substr($postcode, 0, 7);
		}
		
		if(trim($postcode_amex)==""){
			$postcode_amex='11111';
		}
		if(strlen($postcode_amex)<5){
			$postcode_amex=str_pad($postcode_amex, 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($postcode_amex)>7){
			$postcode_amex=substr($postcode_amex, 0, 7);
		}
		
		$this->params 	= array(
					
					"USER"			=> Configuration::get('BANWIRE_USERNAME'), 
					"ORD_CURR_CD"		=> "MXN", 
					"CARD_NUM"		=> $banwire['card_num'], 
					"ORD_DTM" 		=> $this->bw_cart->date_add,
					"CARD_OWN" 		=> ($banwire['card_name'] && strlen($banwire['card_name']) > 30 ? substr($banwire['card_name'], 0, 30) : $banwire['card_name']),
					"CARD_TYPE" 		=> $banwire['card_type'], 
					"CARD_EXP_MM" 		=> ( $_POST['banwire_vig_month'] < 10 ? '0' . $_POST['banwire_vig_month'] : $_POST['banwire_vig_month'] ), 
					"CARD_EXP_YY" 		=> substr($_POST['banwire_vig_year'], 2), 
					"CARD_CVV" 		=> $banwire['card_ccv'], 
					"CARD_AVS_ADDR" 	=> $address_amex, 
					"CARD_AVS_ZIPCODE" 	=> $postcode_amex, 
					"CUST_PHONE" 		=> $addats['phone'],
					"CUST_EMAIL" 		=> $addats['email'],
					"CUST_FNAME" 		=> $addats['firstname'],
					"CUST_LNAME" 		=> $addats['lastname'],
					"CUST_MNAME" 		=> substr($addats['lastname'], 0, 1),
					"CUST_HOME_PHONE" 	=> $addats['phone'],
					"CUST_WORK_PHONE" 	=> $addats['phone'],
					"CUST_ADDR1" 		=> $address,
					"CUST_ADDR2" 		=> ( $this->addresses['address2'] != "" ? $this->addresses['address2'] : $address ),
					"CUST_CITY" 		=> $addats['city'], 
					"CUST_STPR_CD" 		=> $addats['state_iso'],
					"CUST_CNTRY_CD" 	=> $addats['CUST_CNTRY_CD'], 
					"CUST_POSTAL_CD" 	=> $addats['postcode'], 
					"CUST_USER_ID" 		=> $this->bw_customer->id, 
					"EMAIL_IMAGE" 		=> "https://www.banwire.com/home/img/banwire_logo.png", 
					"EMAIL_NOTIFY" 		=> $this->bw_customer->email, 
					"CUST_IP" 		=> $_SERVER['REMOTE_ADDR'],
					"ebWEBSITE" 		=> "https://" . $_SERVER['HTTP_HOST'] . ":" . $_SERVER['SERVER_PORT'] . dirname($_SERVER['REQUEST_URI']).'/',
					"EBT_DEVICEPRINT" 	=> $_POST['EBT_DEVICEPRINT'],
					'ORD_AMT' 		=> $this->bw_cart->getOrderTotal(true, Cart::BOTH),
					//"ORD_AMT" 		=> $this->bw_cart->getOrderTotal(true, Cart::BOTH) / $banwire['recurrencias'],
					"ORD_CONCEPT" 		=> Configuration::get('BANWIRE_CONCEPT'),
					"ITEM_DESC1" 		=> 'Order ' . $this->bw_cart->id . ': ' . Configuration::get('BANWIRE_CONCEPT'),
					"ORD_ID" 		=> $this->bw_cart->id, 
					"TYPE_PAYMENT" 		=> Configuration::get('BANWIRE_REC_SELECT'), 
					"NUMBER_PAYMENT" 	=> $banwire['recurrencias'],
					"PROD_DEL_CD" 		=> Configuration::get('BANWIRE_PROD_DEL_CD'), 
					"VAR_AD1" 		=> "",
					"VAR_AD2" 		=> "", 
					"URL_NOTIFY" 		=> $this->module->getModuleLink('banwire', 'responseREC'),
					"RETRY_NUMBER" 		=> Configuration::get('BANWIRE_REC_RETRYN'), 
					"RETRY_DAY" 		=> Configuration::get('BANWIRE_REC_RETRYD'), 
					"TRAIL" 		=> Configuration::get('BANWIRE_REC_TRAIL'),
					"START_DATE" 		=> date("Y-m-d", strtotime(date('m/d/Y')." +".Configuration::get('BANWIRE_REC_SELECT')))
					
				);

	}
	
	private function _paymentBanwire()
	{
		if($this->api == 'spei')
			$paramsSpei = json_encode($this->params);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url_banwire);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; WINDOWS; .NET CLR 1.1.4322)');
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		if($this->api == 'spei')
			curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsSpei);
		else
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->params));
		if(defined('CURLOPT_ENCODING'))curl_setopt($ch, CURLOPT_ENCODING, "");
		
		$buffer = curl_exec($ch);
		$this->saveLog($buffer);
		
		if($this->api == 'rec'){
			$kv = explode("&", $buffer);
				foreach($kv as $key => $val){
					$varval = explode("=", $val);
					$_response[$varval[0]] = urldecode($varval[1]);
				}
			$this->respuesta = $_response;
		}else{
			$var	= json_decode($buffer);
			$this->respuesta = $var;
		}
	}
	private function saveLog($respuesta){
		$parametros = $this->params;
		if(isset($parametros['CARD_CVV']))
			unset($parametros['CARD_CVV']);
		
		if(isset($parametros['card_ccv2']))
			unset($parametros['card_ccv2']);
		
		if(isset($parametros['CARD_NUM']) && strlen($parametros['CARD_NUM'] ) > 4 )
			$parametros['CARD_NUM'] = substr($parametros['CARD_NUM'], -4);
		
		if(isset($parametros['card_num']) && strlen( $parametros['card_num'] ) > 4 )
			$parametros['card_num'] = substr($parametros['card_num'], -4);
			
		return Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 
							id_customer = '" . $this->bw_customer->id . "', 
							id_cart = '" . $this->bw_cart->id . "', 
							response = '" . $respuesta . "', 
							send = '" . serialize($parametros) . "', 
							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 
							date_add = NOW();"
						);	
	}
}
