<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

class BanwireResponseOXXOModuleFrontController extends ModuleFrontController
{
	
	public function initContent()
	{
		if(isset ($_POST['referencia']) )  {
			$this->saveLB($_POST);
			$order = new Order((int)Order::getOrderByCartId($_POST['referencia']));
			
			if($order->total_paid != null ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_BANWIRE_OXXO_CONFIRMADO'), $order, true);
				$new_history->addWithemail(true);
			} else exit('No Mach');
		}else exit('No post');
		exit();
	}
	
	private function saveLB($post) {
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "banwire_log SET 
							id_customer = '0', 
							id_cart = '" . $post['referencia'] . "', 
							response = '" . serialize($post) . "', 
							send = 'response OXXO', 
							mode = '" . (Configuration::get('BANWIRE_SANDBOX') ? 'Desarrollo' : 'Produccion' ). "', 
							date_add = NOW();"
						);	
	}
	
	
}
