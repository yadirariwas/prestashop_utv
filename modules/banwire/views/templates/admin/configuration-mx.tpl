{*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
*}

{if $banwire_ps_14}
<script type="text/javascript">
		{literal}
		$(document).ready(function() {
			var scripts = [{/literal}{$banwire_js_files}{literal}];
			for(var i = 0; i < scripts.length; i++) {
				$.getScript(scripts[i], function() {banwire_init()});
			}
		});
		{/literal}
</script>
{/if}
<div class="banwire-module-wrapper">
	<div class="banwire-module-header">
		<img src="{$banwire_tracking|escape:'htmlall':'UTF-8'}" alt="" style="display: none;" />
		<a rel="external" href="https://www.banwire.com/" target="_blank"><img class="banwire-logo" alt="" src="{$module_dir}/img/logo.png" /></a>
		<span class="banwire-module-intro">{l s='Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO, SPEI.' mod='banwire'}<br />
		<a class="banwire-module-create-btn L" rel="external" href="https://banwire.com/login/" target="_blank"><span>{l s='Ir a Banwire' mod='banwire'}</span></a></span>
	</div>
	<div class="banwire-module-wrap">
		<div class="banwire-module-col2">
			<div class="banwire-module-col2inner">
				<h3>{l s='Online & Mobile Payments.' mod='banwire'}</h3>
				<div style="line-height: 9px; width: 194px; float: left;">
				<img class="banwire-cc" alt="" src="{$module_dir}/img/tarjetas.png" style="float: left;" />
				</div>
				<div style="line-height: 9px; width: 255px; float: left;">
						<img class="banwire-cc" alt="" src="{$module_dir}/img/efectivo_oxxo.png" style="float: left;" />
				</div>
				{if $banwire_merchant_country_is_usa}
					<div style="line-height: 9px; width: 194px; float: left;">
						<img src="{$module_dir}/img/ebank.png" />
					</div>
				{/if}
			</div>
		</div>	
	</div>
	{if $banwire_validation}
		<div class="conf">
			{foreach from=$banwire_validation item=validation}
				{$validation|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	{if $banwire_error}
		<div class="error">
			{foreach from=$banwire_error item=error}
				{$error|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	{if $banwire_warning}
		<div class="info">
			{foreach from=$banwire_warning item=warning}
				{$warning|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	<form action="{$banwire_form_link|escape:'htmlall':'UTF-8'}" method="post">
		<fieldset>
			<legend><img src="{$module_dir}/img/banwire_shield.png" alt="" />{l s='Banwire API´s' mod='banwire'}</legend>
			<a href="https://banwire.com" class="banwire-module-btn right resetMargin" target="_blank">{l s='Conoce cada una de las API´s de Banwire' mod='banwire'}</a>
			<h4>{l s='Activa/desactiva el API de Banwire necesaria' mod='banwire'}</h4>
			<div class="banwire-usa-threecol">
				<div class="banwire-usa-product first fixCol{if $banwire_configuracion.BANWIRE_TDC} banwire-usa-product-active{/if}">
					<h4>{l s='Tarjeta de Crédito o Débito' mod='banwire'}</h4>
					<div class="box-content-payment" >
						<input type='checkbox' name='banwire_tdc_red' id='banwire_tdc_red' {if $banwire_configuracion.BANWIRE_TDC_RED} checked="checked"{/if} /> <label for="banwire_tdc_red"> {l s='Banwire Shield' mod='banwire'}</label></br>
						<input type='checkbox' name='banwire_tdc_pro' id='banwire_tdc_pro' {if $banwire_configuracion.BANWIRE_TDC_PRO} checked="checked"{/if} /> <label for="banwire_tdc_pro"> {l s='Banwire Pago PRO' mod='banwire'}</label></br>
						<label for="status_banwire_tdc"> {l s='Pago completado:' mod='banwire'}</label></br>
						<select name='status_banwire_tdc' id='status_banwire_tdc' style="width:200px">
						{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_TDC == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
						<label for="status_banwire_tdc_revision"> {l s='Challenge: En revisión' mod='banwire'}</label></br>
						<select name='status_banwire_tdc_revision' id='status_banwire_tdc_revision' style="width:200px">
						{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_TDC_REVISION == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
						<label for="status_banwire_tdc_denegada"> {l s='Challenge: Denegado' mod='banwire'}</label></br>
						<select name='status_banwire_tdc_denegada' id='status_banwire_tdc_denegada' style="width:200px">
						{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_TDC_DENEGADA == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
						<label for="status_banwire_tdc_aceptada"> {l s='Challenge: Aceptado' mod='banwire'}</label></br>
						<select name='status_banwire_tdc_aceptada' id='status_banwire_tdc_aceptada' style="width:200px">
						{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_TDC_ACEPTADA == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
					</div>
					<center><input type="checkbox" name="banwire_tdc" id="banwire_tdc" {if $banwire_configuracion.BANWIRE_TDC} checked="checked"{/if} /> <label for="banwire_tdc"> {l s='Enabled' mod='banwire'}</label></center>
				</div>
				<div class="banwire-usa-product fixCol{if $banwire_configuracion.BANWIRE_OXXO} banwire-usa-product-active{/if}">
					<h4>{l s='OXXO' mod='banwire'}</h4>
					<div class="box-content-payment" ><input type="checkbox" name="banwire_oxxo_sendpdf" id="banwire_oxxo_sendpdf" {if $banwire_configuracion.BANWIRE_OXXO_SENDPDF} checked="checked"{/if} /> <label for="banwire_oxxo_sendpdf"> {l s='Enviar PDF' mod='banwire'}</label><br>
						<label for="banwire_oxxo_vig"> {l s='Vigencia' mod='banwire'}</label><input type="text" name="banwire_oxxo_vig" id="banwire_oxxo_vig" size="5" value = "{$banwire_configuracion.BANWIRE_OXXO_VIG}" /> <br>
						<label for="status_banwire_oxxo_espera"> {l s='OXXO: Espera de Pago' mod='banwire'}</label></br>
							<select name='status_banwire_oxxo_espera' id='status_banwire_oxxo_espera' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_OXXO_ESPERA == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
						<label for="status_banwire_oxxo_confirmado"> {l s='Challenge: Aceptado' mod='banwire'}</label></br>
							<select name='status_banwire_oxxo_confirmado' id='status_banwire_oxxo_confirmado' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_OXXO_CONFIRMADO == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
					</div>
					<center><input type="checkbox" name="banwire_oxxo" id="banwire_oxxo_vig" {if $banwire_configuracion.BANWIRE_OXXO} checked="checked"{/if} /> <label for="banwire_oxxo"> {l s='Enabled' mod='banwire'}</label></center>
				</div>
				<div class="banwire-usa-product last fixCol{if $banwire_configuracion.BANWIRE_SPEI} banwire-usa-product-active{/if}">
					<h4>{l s='SPEI' mod='banwire'}</h4>
					<div class="box-content-payment" >{l s='Activa pagos via tranferencia bancaria referenciada mediante SPEI-FAST (solo repblica Mexicana) ' mod='banwire'}</br>
						<label for="status_banwire_spei_espera"> {l s='SPEI: En Espera de Transferencia' mod='banwire'}</label></br>
							<select name='status_banwire_spei_espera' id='status_banwire_spei_espera' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_SPEI_ESPERA == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
						<label for="status_banwire_spei_confirmado"> {l s='SPEI: Confirmado' mod='banwire'}</label></br>
							<select name='status_banwire_spei_confirmado' id='status_banwire_spei_confirmado' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_SPEI_CONFIRMADO == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
					</div>
					<center><input type="checkbox" name="banwire_spei" id="banwire_spei" {if $banwire_configuracion.BANWIRE_SPEI} checked="checked"{/if} /> <label for="banwire_spei"> {l s='Enabled' mod='banwire'}</label></center>
				</div>
			</div>
			<div class="banwire-usa-onecol">
				<div class="banwire-usa-product_eco fixCol{if $banwire_configuracion.BANWIRE_REC} banwire-usa-product-active{/if}">
					<h4>{l s='Pagos Recurrentes (V4).' mod='banwire'}</h4>
					<div class="box-content-payment" >
						<label for="banwire_tdc_rec_select"> {l s='Recurrencia' mod='banwire'}</label>
						<select name='banwire_rec_select' id='banwire_rec_select' >
						{foreach from = $banwire_rec_select item = recurrencia key = key}
								{if $banwire_configuracion.BANWIRE_REC_SELECT == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select></br>
						<label for="status_banwire_rec"> {l s='En Pago Recurrente' mod='banwire'}</label></br>
							<select name='status_banwire_rec' id='status_banwire_rec' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_REC == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
						<label for="status_banwire_rec_fallo"> {l s='Fallo Pago Recurrente' mod='banwire'}</label></br>
							<select name='status_banwire_rec_fallo' id='status_banwire_rec_fallo' style="width:200px">
							{foreach from = $banwire_order_statuses item = recurrencia key = key}
								{if $banwire_configuracion.STATUS_BANWIRE_REC_FALLO == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
							
						
						<div id='datos_rec'>
							<label for="status_banwire_rec_scosto"> {l s='Recurrente Inicio sin costo' mod='banwire'}</label></br>
							<sup>*Solo si activa Inicio sin costo</sup><br />
								<select name='status_banwire_rec_scosto' id='status_banwire_rec_scosto' style="width:200px">
								{foreach from = $banwire_order_statuses item = recurrencia key = key}
									{if $banwire_configuracion.STATUS_BANWIRE_REC_SCOSTO == $key }
										<option value="{$key}" selected='selected'>{$recurrencia}</option>
									{else}
										<option value="{$key}" >{$recurrencia}</option>
									{/if}
								{/foreach}
								</select>	
							<label for="banwire_rec_retryn"> {l s='Activar número de reintentos:' mod='banwire'}</label></br>
								<select name='banwire_rec_retryn' id='banwire_rec_retryn' style="width:100px" >
								{foreach from = $banwire_rec_retry item = recurrencia key = key}
									{if $banwire_configuracion.BANWIRE_REC_RETRYN == $key }
										<option value="{$key}" selected='selected'>{$recurrencia}</option>
									{else}
										<option value="{$key}" >{$recurrencia}</option>
									{/if}
								{/foreach}
								</select></br>
							<label for="banwire_rec_retryd"> {l s='Activar días de reintentos:' mod='banwire'}</label></br>
								<select name='banwire_rec_retryd' id='banwire_rec_retryd' style="width:100px" >
								{foreach from = $banwire_rec_retry item = recurrencia key = key}
									{if $banwire_configuracion.BANWIRE_REC_RETRYD == $key }
										<option value="{$key}" selected='selected'>{$recurrencia}</option>
									{else}
										<option value="{$key}" >{$recurrencia}</option>
									{/if}
								{/foreach}
								</select></br>
							<label for="banwire_rec_trail"> {l s='Iniciar Recurrencia un mes despues:' mod='banwire'}</label></br>
								{if $banwire_configuracion.BANWIRE_REC_TRAIL == 1 }
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_traily' value="1" checked='checked' /> <label for="banwire_rec_traily"> {l s='Si' mod='banwire'}</label>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_trailn' value="null" /> <label for="banwire_rec_trailn"> {l s='No' mod='banwire'}</label>
								{else}
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_traily' value="1" /> <label for="banwire_rec_traily"> {l s='Si' mod='banwire'}</label>
									<input type= "radio" name = 'banwire_rec_trail' id = 'banwire_rec_trailn' value="null" checked='checked' /> <label for="banwire_rec_trailn"> {l s='No' mod='banwire'}</label>
								{/if}
						</div>
					</div>
					<center><input type="checkbox" id="banwire_rec" name="banwire_rec" {if $banwire_configuracion.BANWIRE_REC} checked="checked"{/if} /> <label for="banwire_rec"> {l s='Enabled' mod='banwire'}</label></center>
				</div>
			</div>
		</fieldset>
	<br />
		<fieldset>
			<legend><img src="{$module_dir}img/settings.gif" alt="" /><span>{l s='Configuración general de API Banwire' mod='banwire'}</span></legend>
			<div id="banwire-usa-basic-settings-table">
				<label for="banwire_sandbox_on">{l s='Desarrollo / Producción' mod='banwire'}</label>
				<div class="margin-form PT4">
					<input type="radio" name="banwire_sandbox" id="banwire_sandbox_on" value="0"{if $banwire_configuracion.BANWIRE_SANDBOX == 0} checked="checked"{/if} /> <label for="banwire_sandbox_on" class="resetLabel">{l s='Producción' mod='banwire'}</label>
					<input type="radio" name="banwire_sandbox" id="banwire_sandbox_off" value="1"{if $banwire_configuracion.BANWIRE_SANDBOX == 1} checked="checked"{/if} /> <label for="banwire_sandbox_off" class="resetLabel">{l s='Desarrollo (Sandbox)' mod='banwire'}</label>
					<p>{l s='En modo de Desarrollo no sera visible la transacción en el administrador de Banwire:' mod='banwire'}<br /></p>
				</div>
				<label for="banwire_username">{l s='Usuario en Banwire:' mod='banwire'}</label>
				<div class="margin-form">
					<input type="text" name="banwire_username" class="input-text" value="{$banwire_configuracion.BANWIRE_USERNAME}" /> <sup>*</sup>
				</div>
				<label for="banwire_concept">{l s='Concepto:' mod='banwire'}</label></td>
				<div class="margin-form">
					<input type="text" name="banwire_concept" class="input-text" value="{$banwire_configuracion.BANWIRE_CONCEPT}" /> <sup>*</sup>
				</div>
				
				<label for="banwire_prod_del_cd">{l s='Tipo de Producto que comercializa:' mod='banwire'}</label>
				</br<sup>{l s='* Este campo es obligatorio si activa Banwire-Shield' mod='banwire'}</sup>
				<div class="margin-form">
					<select name='banwire_prod_del_cd' id='banwire_prod_del_cd' style="width:200px">
						{foreach from = $banwire_valores_prod_del_cd item = recurrencia key = key}
							{if $banwire_configuracion.BANWIRE_PROD_DEL_CD == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
							{else}
								<option value="{$key}" >{$recurrencia}</option>
							{/if}
						{/foreach}
					</select>
				</div>
				<label for="banwire_ship_mthd_cd">{l s='Metodo de Envio (BW-Shield):' mod='banwire'}</label>
				</br<sup>{l s='* Este campo es obligatorio si activa Banwire-Shield' mod='banwire'}</sup>
				<div class="margin-form">
					<select name='banwire_ship_mthd_cd' id='banwire_ship_mthd_cd' style="width:200px">
						{foreach from = $banwire_valores_ship_mthd_cd item = recurrencia key = key}
							{if $banwire_configuracion.BANWIRE_SHIP_MTHD_CD == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
							{else}
								<option value="{$key}" >{$recurrencia}</option>
							{/if}
						{/foreach}
					</select>
				</div>				
			</div>
			
			<div class="clear centerText">
				<input type="submit" name="SubmitProducts" class="button MB15" value="{l s='Modificar Configuración' mod='banwire'}" />
			</div>
			<span class="small"><sup style="color: red;">*</sup> {l s='Campos requeridos' mod='banwire'}</span>
		</fieldset>
	</form>
</div>
{if $banwire_merchant_country_is_mx}
	<script type="text/javascript">
		{literal}
		$(document).ready(function() {
			$('#banwire_rec_select').bind('change', function(){
				
				if($(this).val() == 1 )
					$("#datos_rec").css('display', 'none');
				else
					$("#datos_rec").css('display', 'block');
			})
			$('#content table.table tbody tr th span').html('banwiremx');
			
			
		});
		{/literal}
	</script>
{/if}