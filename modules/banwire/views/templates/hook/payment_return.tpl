{*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}modules/banwire/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}modules/banwire/css/layout.css" type="text/css" media="all">
                
<div id="content_confiramtion">
	{assign var=total value=$data.TotalBruto|replace:"$":""|replace:" ":""|replace:".":""|replace:",":""}
	<img src="http://v2.afilio.com.br/sale.php?pid=1792&order_id={$data.Orden}&order_price={$total}" border="0" width="1" height="1" />
    <div id='msg_confirmation'><span>{l s='¡GRACIAS POR SU COMPRA!' mod='banwire'}</span><br>{l s='El proceso de pago a través de BanWire ha sido completado, a continuación los detalles de su pago:' mod='banwire'}</div>
    <div class='data_confirmation'>

        {foreach from = $data item = value key = key}
		{if $key == "SPEI"}	
		<div class="row_data"><label><b>Clabe Interbancaria: </b></label> {$value}</div>
		<div class="row_data"><label><b>Pasos a seguir: </b></label><br> 1. Ir a banca online personal<br>2. Dar de alta la Clabe Interbancaria: <b>{$value}</b><br>3. El nombre del Banco es (Es obligatorio ponerlo): <b>Sistema de tranferencias y pagos</b><br>4. Persona Moral: Under TV<br><br></div>
	    {elseif $key != "barcode_img"}	
		<div class="row_data"><label><b>{$key}: </b></label> {$value}</div>
	    {/if}
		{if $key == "SPEI"}	
		
	    {/if}
        {/foreach}
			
	{if isset($data.Banwire_codigo_img) }
	    <div class="row_data"><label>&nbsp;</label> <img src="data:image/jpg;base64,{$_GET.Banwire_codigo_img}"></div>
	{/if}
    </div>
	
</div>

{if isset($imprimir) && $imprimir }
<br/><p><a class="print_confirmation button-exclusive btn btn-default" href="#" title="Imprimir"><span>Imprimir</span></a></p><br/>
<script>
	function print_confirmation(){
		var tmp = $("#content_confiramtion").clone();	
		$("#msg_confirmation",tmp).remove();
		tmp.printArea();
		return false;
	}
	$(".print_confirmation").click(print_confirmation);
</script>
{/if}
