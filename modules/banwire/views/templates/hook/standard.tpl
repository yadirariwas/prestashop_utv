{*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}/css/layout.css" type="text/css" media="all">         
	{assign var='precio' value=$total_price}
	<a href="{$banwire_action|escape:'htmlall':'UTF-8'}" title="{l s='Elige tu método de pago Banwire' mod='banwire'}" class="btn btn-default{* button button-medium *}">
		
		<img src="https://www.banwire.com/home/img/banwire_logo.png" alt="{l s='Elige tu método de pago Banwire' mod='banwire'}" width="130" height="58"/>
		<img src="http://canalutv.mx/modules/banwire/img/credit_card.png" alt="{l s='Elige tu método de pago Banwire' mod='banwire'}"/>
		{l s='Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO, SPEI.' mod='banwire'}
		{*<span>Pagar &nbsp;<i class="icon-chevron-right right"></i></span>*}
	</a>




	{if $page_name == 'order'}
		<script type='text/javascript'>
			if('{$precio}' > 5000){
				//alert(confirm("El monto es de: {$precio} �desea continuar?"));
				if (!confirm("El monto es de: {displayPrice price=$total_price}, desea continuar?")) {
					alert('Regrese al paso 1 para adecuar su compra.');
				}
			}
		</script>		
	{/if}
						