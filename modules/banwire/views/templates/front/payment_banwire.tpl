<link rel="stylesheet" href="{$this_path}/css/banwire.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}/css/layout.css" type="text/css" media="all">
{assign var='esregalo' value=$cart->getisRegalo($cookie->id_customer)}
<script>
			$('#fancybox_inline').fancybox({
			    'speedOut': 150,
			    'modal': {
				'enableEscapeButton': true
			    },
			    'centerOnScroll': true,
			    'overlayColor': '#40E0FF',
			    'overlayOpacity': .5,
			    'transitionIn': 'none'
			});
			
			var tipo = $("#banwire_card_type");
			tipo.live('change', function(){
			/*
				if(tipo.val() == 'amex')
					$("#banwire_amex").css('display', 'block');
				else
					$("#banwire_amex").css('display', 'none');
			*/
			});
			$("#button_payment").live('click', function(){
				$.ajax({
					url:"{$link->getModuleLink('banwire', 'ajax', [], true)}",
					data: $("#form_banwire").serialize(),
					type: 'post',
					dataType: 'json',
					beforeSend:function(){
						$("#fancybox_inline").trigger('click');
                        $("#button_payment").css('disabled', true);
					},
					success: function(data){
						if (data.errors){
							html ='';
							$.each(data.errors, function(a, b){
								html += '<span class="renglon" > ' + b + '</span>';
							});
							$("#errors").html(html).css("display", "block");
                                                        $.fancybox.close();
                                                        $("#button_payment").css('disabled', false);        
						}
						if (data.redirect) {
							$(location).attr('href', data.redirect);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						      $.fancybox.close();
                                                      $("#button_payment").css('disabled', false);
					}
				});
			});
			$("#payment_banwire_tdc").css('display', 'block');
			$("#payment_banwire_rec").css('display', 'none');
			$("#payment_banwire_oxxo").css('display', 'none');
			$("#payment_banwire_spei").css('display', 'none');	

			//$("#banwire_rec").css('display', 'none');        /*               
        function selectRecYN(value) {			$("#banwire_rex").css('display', 'none');			if (value == 1 )				$("#banwire_rec").css('display', 'block');
		}
		*/
                function newPaymentMethod(api){
                    if(api == 'oxxo'){
                        $("#payment_banwire_oxxo").css('display', 'block');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('oxxo');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").addClass("active");
                        $("#tab_spei").removeClass("active");
                    }
                    if(api == 'tdc'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'block');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('tdc');
                        $("#tab_cc").addClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").removeClass("active");
                    }
					
					if(api == 'rec'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'block');
                        $("#payment_banwire_spei").css('display', 'none');
                        $("#banwire_api").val('rec');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").addClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").removeClass("active");
                    }
                    if(api == 'spei'){
                        $("#payment_banwire_oxxo").css('display', 'none');
                        $("#payment_banwire_tdc").css('display', 'none');
						$("#payment_banwire_rec").css('display', 'none');
                        $("#payment_banwire_spei").css('display', 'block');
                        $("#banwire_api").val('spei');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").removeClass("active");
                        $("#tab_spei").addClass("active");
                    }
                }
		// basic configuration
		var io_install_flash = false;
		// do not install Flash
		var io_install_stm = false;
		// do not require install of Active X
		var io_exclude_stm = 12;
		// do not run Active X under IE 8 platforms
		var io_enable_rip = true;
		// collect Real IP information
		var io_bb_callback = function (bb, isComplete){ // populate hidden form fields in both forms
		    var login_field = document.getElementById("EBT_DEVICEPRINT");
		    if (login_field)
				login_field.value = bb;
		};
		
		$(function() {
			var is_rec = "{$cookie->isrecurrente}";
			if ((is_rec.length > 0)){
				$("#api_rec").trigger('click');
				$("#tab_cc").css("display", 'none');
				$("#tab_oxxo").css("display", 'none');
				$("#tab_spei").css("display", 'none');			    
			}
			
			if("{$esregalo}"){
				$("#tab_oxxo").css("display", 'none');
				$("#tab_spei").css("display", 'none');				
			}
		});
        </script>
	<script language="javascript" src=https://mpsnare.iesnare.com/snare.js></script>	
	{capture name=path}
		<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='banwire'}">{l s='Checkout' mod='banwire'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Banwire payment' mod='banwire'}
	{/capture}
	{assign var='current_step' value='payment'}
	{include file="$tpl_dir./order-steps.tpl"}
		{assign var='cartProducts' value=$cart->getProducts()}
	{assign var='idProducto' value=$cartProducts[0]['id_product']}
	{assign var='total' value=$cookie->price_total}
	
	{if $nbProducts <= 0}
		<p class="warning">{l s='Your shopping cart is empty.' mod='banwire'}</p>
	{else}
		<div id='errors' style="display: none"></div>
		<form id="form_banwire">
            <input type="hidden" value="tdc" name='banwire_api' id = 'banwire_api' />			<div class='header-title-payments'>
				<ul>
					{if $banwire_tdc }							<li id="tab_cc" class="active"><a tab = 'payment_banwire_tdc' id = 'paymnet_banwire_tdc_btn' > <input type="radio" name="banwire_api" id='api_tdc' value="tdc" onclick = "newPaymentMethod('tdc')" checked = checked/><label for="api_tdc">{l s='PAGO TARJETA' mod='banwire'}</label></a></li> 					{/if}
					{if $idProducto == 4 || $idProducto == 9}
						{if $banwire_rec }	
							<li id="tab_rec"><a tab = 'payment_banwire_rec' id = 'paymnet_banwire_rec_btn' > <input type="radio" name="banwire_api" id='api_rec' value="rec" onclick = "newPaymentMethod('rec')" /><label for="api_rec">{l s='DOMICILIAR PAGO' mod='banwire'}</label></a></li> 
						{/if}
					{/if}					{if $banwire_oxxo}						<li id="tab_oxxo"> <a tab = 'payment_banwire_oxxo' id ='payment_banwire_oxxo_btn' ><input type="radio" name="banwire_api" id='api_oxxo' value='oxxo' onclick = "newPaymentMethod('oxxo')" /><label for="api_oxxo">{l s='PAGO OXXO' m='banwire'}</label></a></li> 					{/if}					{if $banwire_spei}							<li id="tab_spei"><a tab = 'payment_banwire_spei' id = 'paymnet_banwire_spei_btn' ><input type="radio" name="banwire_api" id='api_spei' value='spei' onclick = "newPaymentMethod('spei')" /><label for="api_spei">{l s='PAGO SPEI' mod='banwire'}</label></a></li> 
					{/if}
				</ul>
            </div>
            <div id='content-payments'>
				<input type="hidden" id='EBT_DEVICEPRINT' name='EBT_DEVICEPRINT' />				{if $banwire_tdc}						<div class = 'content-description-payment' id = 'payment_banwire_tdc'>						<fieldset class='section_banwire_form'>							<legend><h3>{l s='Pago Tarjeta' mod='banwire'}</h3></legend>							<div class="logos-tarjeta"><img src="{$module_dir}img/logos_pagos_tarjetas.jpg" class="wb"/></div>							<div class="banwire_field">								<label>{l s='Nombre en Tarjeta: ' mod='banwire'}</label>								<input type="text" name='banwire[card_name]' />
								</br><span class="form_info">{l s='Poner el nombre tal cual, como sale en su tarjeta.'}</span><br><br>							</div>
							<div class="banwire_field">
								<label>{l s='Número de Tarjeta: ' mod='banwire'}</label>
								<input placeholder='1234123412341234' type="text"  name='banwire[card_num]' />
								</br><span class="form_info">{l s='Poner los dìgitos tal cual y como vienen en su tarjeta.'}</span><br><br>
							</div>
							<div class="banwire_field">
								<label>{l s='Tipo de Tarjeta: ' mod='banwire'}</label>
								<select  name='banwire[card_type]' id = 'banwire_card_type' onchange="if(this.value=='amex') document.getElementById('banwire_amex').style.display='block'; else document.getElementById('banwire_amex').style.display='none';">
									<option value="visa">VISA</option>									<option value="mastercard">MasterCard</option>									<option value="amex">AmericanExpress</option>								</select>
								<br><br>
							</div>
							<div class="banwire_field">								<label>{l s='Fecha de Vigencia: ' mod='banwire'}</label>
								<select name='banwire_vig_month' id='banwire_vig_month' >									{foreach from = $months item = recurrencia key = key}										<option value="{$key}" >{$recurrencia}</option>									{/foreach}								</select>
								<select name='banwire_vig_year' id='banwire_vig_year' >
									{foreach from = $years item = recurrencia}										<option value="{$recurrencia}" >{$recurrencia}</option>									{/foreach}
								</select>
								<br><br>
							</div>
							<!--
							<div class="banwire_field">
								<label>{l s='Código CCV: ' mod='banwire'}</label>
								<input type="password" maxlength="4" size="5" name='banwire[card_ccv]' />
							</div>
							-->
                            <div class="banwire_field">
								<label>{l s='Código CCV: ' mod='banwire'}</label>
                                <input type="password" maxlength="4" size="5" name='banwire[card_ccv]' />
								<a class="iframe2" href="{$link->getCMSLink('15')}?content_only=1">
									<b>¿Qué es CCV?</b>
								</a>
								<script type="text/javascript">
									$(document).ready(function(){
										$("a.iframe2").fancybox({
										'autoScale'  : false,
										'transitionIn'  : 'none',
										'transitionOut'  : 'none',
										'width'  : 420,
										'height'  : 1200,
										'type'  : 'iframe'
										});
									});
								</script>
                            </div>							
							
							<div id="banwire_amex" style="display: none">
								<span class="help">{l s='* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.' mod='banwire'}</span>
								<div class="banwire_field">
									<label>{l s='Código Postal: ' mod='banwire'}</label>
									<input type="text"  name='banwire[postcode]' />
								</div>
								
								<div class="banwire_field">
									<label>{l s='Dirección: ' mod='banwire'}</label>
									<input type="text" name='banwire[direction]' />
								</div>
								
							</div>
						</fieldset>
					</div>
				{/if}
				
				{if $idProducto == 4 || $idProducto == 9}
				{if $banwire_rec}
					<div class = 'content-description-payment' id = 'payment_banwire_rec' style="display: none">
						<fieldset class='section_banwire_form'>
							<legend><h3>{l s='Domiciliar Pago TDC Banwire' mod='banwire'}</h3></legend>
							<div class="logos-tarjeta"><img src="{$module_dir}img/logos_pagos_tarjetas.jpg"/></div>
							<div class="banwire_field">
								<label>{l s='Nombre en Tarjeta: ' mod='banwire'}</label>
								<input type="text" name='banwire_rec[card_name]' />
							</div>
							<div class="banwire_field">
								<label>{l s='Número de Tarjeta: ' mod='banwire'}</label>
								<input type="text"  name='banwire_rec[card_num]' />
							</div>
							<div class="banwire_field">
								<label>{l s='Tipo de Tarjeta: ' mod='banwire'}</label>
								<select  name='banwire_rec[card_type]' id = 'banwire_card_type' onchange="if(this.value=='amex') document.getElementById('banwire_rec_amex').style.display='block'; else document.getElementById('banwire_rec_amex').style.display='none';">
									<option value="visa">VISA</option>
									<option value="mastercard">MasterCard</option>
									<option value="amex">AmericanExpress</option>
								</select>
							</div>
							<div class="banwire_field">
								<label>{l s='Fecha de Vigencia: ' mod='banwire'}</label>
								<select name='banwire_vig_month_rec' id='banwire_vig_month' >
									{foreach from = $months item = recurrencia key = key}
										<option value="{$key}" >{$recurrencia}</option>
									{/foreach}
								</select>
								<select name='banwire_vig_year_rec' id='banwire_vig_year' >
									{foreach from = $years item = recurrencia}
										<option value="{$recurrencia}" >{$recurrencia}</option>
									{/foreach}
								</select>
							</div>
							<div class="banwire_field">
								<label>{l s='Código CCV: ' mod='banwire'}</label>
								<input type="password" maxlength="4" size="5" name='banwire_rec[card_ccv]' />
							</div>
							
							<div id='banwire_rec'>
								<div class="banwire_field">
									<label>{l s='Monto a domiciliar: ' mod='banwire'}</label>
									<label>$ {$total|number_format:2:".":","}</label>
								</div>
								<div class="banwire_field">
									<label>{l s='Recurrencia: ' mod='banwire'}</label>
									<label>{* $banwire_rec_sel *}30 Días</label>
								</div>
								<div class="banwire_field">
									<label>{l s='Inicio Recurrencia: ' mod='banwire'}</label>
									<label>{$cart->angel_f|date_format: "%d/%m/%Y"}</label>
								</div>
								<div class="banwire_field">
									<input type="hidden" maxlength="4" size="5" name='banwire_rec[recurrencias]' value='IND' />
									<input type="hidden" maxlength="1" size="1" name='select_rec_yes_rec' value='1' />
									{*<input type="hidden" maxlength="10" size="10" name='banwire_rec[pago_inicial]' value='{$cart->angel_f}' />*}
								</div>
							</div>
							
							<div id="banwire_rec_amex" style="display: none">
								<span class="help">{l s='* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.' mod='banwire'}</span>
								<div class="banwire_field">
									<label>{l s='Código Postal: ' mod='banwire'}</label>
									<input type="text"  name='banwire_rec[postcode]' />
								</div>
								<div class="banwire_field">
									<label>{l s='Dirección: ' mod='banwire'}</label>
									<input type="text" name='banwire_rec[direction]' />
								</div>
							</div>
						</fieldset>
					</div>
				{/if}				{/if}
			    {if $banwire_oxxo}	
					<div class='content-description-payment' id= 'payment_banwire_oxxo' style="display: none">
						<div class="logos-oxxo"><img src="{$module_dir}img/logos_pagos_oxxo.jpg"/></div>
						<p><b>{l s='Al escoger pagar con OXXO se generará y enviara un cupón de pago al correo de tu cuenta.' mod='banwire'}</b></p><br>
						<p>{l s='Este cupón es válido únicamente para realizar el pago correspondiente y dentro de la fecha de vigencia establecida. La acreditación del mismo es a las 24 hrs. de realizado, hasta entonces el pago podrá ser corroborado por el vendedor.' mod='banwire'}</p><br>
						<p>{l s='El pago se verá reflejado en tienda dentro de las siguientes 72 horas después de haberlo realizado.' mod='banwire'}</p><br>
						<p><b><br>{l s='Imprime el cupón de manera clara y legible, usa de preferencia impresora láser, consérvalo en buen estado sin tachar ó doblar la parte del código de barras. En caso de no ser legible, la tienda puede rechazar el pago correspondiente.' mod='banwire'}</b></p>
					</div>
			    {/if}				{if $banwire_spei}					<div class="content-description-payment" id = 'payment_banwire_spei' style="display: none">
						<div class="logos-spei"><img src="{$module_dir}img/logos_pagos_spei.jpg"/></div>						<p>{l s='Al escoger pagar transferencia se generará una cuenta CLABE (La CLABE cuenta con 18 dígitos) con la cual podrás realizar el pago desde tu portal de banca electrónica o acudir a tu banco y realizar un SPEI en ventanilla.' mod='banwire'}</p><br>
						<p><b>{l s='• Copia la cuenta CLABE (La CLABE cuenta con 18 dígitos) que te es asignada.' mod='banwire'}</b></p>
						<p>
							<b>{l s='• Ingresa a tu banca en línea y realiza una transferencia a esta cuenta CLABE.' mod='banwire'} 
							<!--<sup>-->
								{l s='[Si es la primera vez que realizas un pago a través de SPEI FAST tendrás que dar de alta esta CLABE para poder realizar la transferencia].' mod='banwire'} </b>
							<!--</sup>-->
						</p>
						<p><b>{l s='• No olvides indicar NOMBRE Y APELLIDOS del USUARIO con el que está registrada la cuenta.' mod='banwire'} </b></p>
						<p><b>{l s='• Listo, tu pago ha sido completado, el vendedor recibirá de inmediato la notificación de tu pago.' mod='banwire'}</b></p>					</div>				{/if}
            </div>	
			<p class="cart_navigation" id="cart_navigation">
				<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">{l s='Volver' mod='banwire'}</a>
				<label id="button_payment"  class="exclusive_large" >{l s='Confirmar Pago' mod='banwire'}</label>
			</p>
		</form>
	{/if}
	<div style="display:none">
		<a id="fancybox_inline" href="#data" style="display:none"></a>
		<div id="data">
			<img src="{$module_dir}img/BanWire-Payment-Process.gif" alt="BanWire Security">                
		</div>
	</div>