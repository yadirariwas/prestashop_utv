{*
 *  @authorRanchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}modules/payu/css/payu.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}modules/payu/css/layout.css" type="text/css" media="all">
                
<div id="content_confiramtion">
    <div id='msg_confirmation'><span>{l s='¡GRACIAS POR SU COMPRA!' mod='payu'}</span><br>{l s='El proceso de pago a través de payu ha sido completado, a continuación los detalles de su pago:' mod='payu'}</div>
    <div class='data_confirmation'>

	<center>
	<h2 style="font-size: 24px;">Formato de pago en tiendas OXXO</h2>
	<br/><p>Para realizar tu pago, imprime y presenta este comprobante en cualquier tienda OXXO de México</p><br/><br/>
	</center>

	<div style="margin: 0 0 0 50px;">
		<div class="row_data"><label><b>Orden: </b></label> {$data.Orden}</div>
		<div class="row_data"><label><b>Total: </b></label> {$data.Total}</div>
		<div class="row_data"><label><b>Código de barras: </b></label> {$data.C_Barras}</div>
		<div class="row_data"><label><b>Vigencia: </b></label> {$data.Vigencia}</div>
	</div>
	<center>
		<iframe src="{$data.barcode_url}" height="900px" width="800px"></iframe>
	</center>
	<br/><br/><p style="text-align:left;">*Imprime el cupón de manera clara y legible, usa de preferencia impresora
	láser, Consérvalo en buen estado sin tachar ó doblar la parte del código de
	barras. En caso de no ser legible, la tienda puede rechazar el pago
	correspondiente.</p>

	</div>

    </div>
</div>
