{*
 *  @authorRanchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}modules/payupayment/css/payu.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}modules/payupayment/css/layout.css" type="text/css" media="all">
                
<div id="content_confiramtion">
    <div id='msg_confirmation'><span>{l s='¡GRACIAS POR SU COMPRA!' mod='payu'}</span><br>{l s='El proceso de pago a través de payu ha sido completado, a continuación los detalles de su pago:' mod='payu'}</div>
    <div class='data_confirmation'>
        {foreach from = $data item = value key = key}
	    {if $key != "barcode_img"}	
		<div class="row_data"><label><b>{$key}: </b></label> {$value}</div>
	    {/if}
        {/foreach}
	
	{if isset($data.barcode_img) }
	    <div class="row_data"><label>&nbsp;</label> <img src="data:image/jpg;base64,{$data.barcode_img}"></div>
	{/if}
    </div>
</div>

{if isset($imprimir) && $imprimir }
<br/><p><a class="print_confirmation button-exclusive btn btn-default" href="#" title="Imprimir"><span>Imprimir</span></a></p><br/>
<script>
	function print_confirmation(){
		var tmp = $("#content_confiramtion").clone();	
		$("#msg_confirmation",tmp).remove();
		tmp.printArea();
		return false;
	}
	$(".print_confirmation").click(print_confirmation);
</script>
{/if}
