{*
 *  @author Ranchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 *
*}
<link rel="stylesheet" href="{$this_path}/css/payu.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}/css/layout.css" type="text/css" media="all">
{if $cookie->id_customer eq 2 or $cookie->id_customer eq 27}                
<p class="payment_module">
	<a href="{$payu_action|escape:'htmlall':'UTF-8'}" title="{l s='Elige tu método de pago Payu' mod='payu'}">
		<img src="{$this_path}/img/logo.png" alt="{l s='Elige tu método de pago Payu' mod='payu'}" width="130" height="58"/>
		{l s='Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO.' mod='payu'}
	</a>
</p>
{/if}