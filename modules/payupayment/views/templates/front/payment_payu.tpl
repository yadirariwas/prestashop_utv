<link rel="stylesheet" href="{$this_path}css/payu.css" type="text/css" media="all">
<link rel="stylesheet" href="{$this_path}css/layout.css" type="text/css" media="all">
<script>
			$('#fancybox_inline').fancybox({
			    'speedOut': 150,
			    'modal': {
				'enableEscapeButton': true
			    },
			    'centerOnScroll': true,
			    'overlayColor': '#40E0FF',
			    'overlayOpacity': .5,
			    'transitionIn': 'none'
			});
			$("#button_payment").live('click', function(){
				$.ajax({
					url:"{$link->getModuleLink('payupayment', 'ajax', [], true)}",
					data: $("#form_payu").serialize(),
					type: 'post',
					dataType: 'json',
					beforeSend:function(){
						$("#fancybox_inline").trigger('click');
                        $("#button_payment").css('disabled', true);
					},
					success: function(data){
						if (data.errors){
							html ='';
							$.each(data.errors, function(a, b){
								html += '<span class="renglon" > ' + b + '</span>';
							});
							$("#errors").html(html).css("display", "block");
                                                        $.fancybox.close();
                                                        $("#button_payment").css('disabled', false);        
						}
						if (data.redirect) {
							$(location).attr('href', data.redirect);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						      $.fancybox.close();
                                                      $("#button_payment").css('disabled', false);
					}
				});
			});
			$("#payment_payu_tdc").css('display', 'block');
			$("#payment_payu_rec").css('display', 'none');
			$("#payment_payu_oxxo").css('display', 'none');
                function newPaymentMethod(api){
                    if(api == 'oxxo'){
                        $("#payment_payu_oxxo").css('display', 'block');
                        $("#payment_payu_tdc").css('display', 'none');
						$("#payment_payu_rec").css('display', 'none');
                        
                        $("#payu_api").val('oxxo');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").addClass("active");
                        
                    }
                    if(api == 'tdc'){
                        $("#payment_payu_oxxo").css('display', 'none');
                        $("#payment_payu_tdc").css('display', 'block');
						$("#payment_payu_rec").css('display', 'none');
                        
                        $("#payu_api").val('tdc');
                        $("#tab_cc").addClass("active");
						$("#tab_rec").removeClass("active");
                        $("#tab_oxxo").removeClass("active");
                        
                    }
					
					if(api == 'rec'){
                        $("#payment_payu_oxxo").css('display', 'none');
                        $("#payment_payu_tdc").css('display', 'none');
						$("#payment_payu_rec").css('display', 'block');
                        
                        $("#payu_api").val('rec');
                        $("#tab_cc").removeClass("active");
						$("#tab_rec").addClass("active");
                        $("#tab_oxxo").removeClass("active");
                        
                    }
                }
		// basic configuration
		var io_install_flash = false;
		// do not install Flash
		var io_install_stm = false;
		// do not require install of Active X
		var io_exclude_stm = 12;
		// do not run Active X under IE 8 platforms
		var io_enable_rip = true;
		// collect Real IP information
		var io_bb_callback = function (bb, isComplete){ // populate hidden form fields in both forms
		    var login_field = document.getElementById("EBT_DEVICEPRINT");
		    if (login_field)
				login_field.value = bb;
		};
	    
        </script>
	<script language="javascript" src=https://ci-mpsnare.iovation.com/snare.js></script>
	
	{capture name=path}
		<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='payu'}">{l s='Checkout' mod='payu'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Payu payment' mod='payu'}
	{/capture}
	{assign var='current_step' value='payment'}
	{include file="$tpl_dir./order-steps.tpl"}

	{if $nbProducts <= 0}
		<p class="warning">{l s='Your shopping cart is empty.' mod='payu'}</p>
	{else}		
		<div id='errors' style="display: none"></div>
		<form id="form_payu">
            <input type="hidden" value="tdc" name='payu_api' id = 'payu_api' />			<div class='header-title-payments'>
				<ul>
					{if $payu_tdc }							<li id="tab_cc" class="active"><a tab = 'payment_payu_tdc' id = 'paymnet_payu_tdc_btn' > <input type="radio" name="payu_api" id='api_tdc' value="tdc" onclick = "newPaymentMethod('tdc')" checked = checked/><label for="api_tdc">{l s='PAGO TARJETA' mod='payu'}</label></a></li> 					{/if}
					
					{if $payu_rec }	
						<li id="tab_rec"><a tab = 'payment_payu_rec' id = 'paymnet_payu_rec_btn' > <input type="radio" name="payu_api" id='api_rec' value="rec" onclick = "newPaymentMethod('rec')" /><label for="api_rec">{l s='DOMICILIAR PAGO' mod='payu'}</label></a></li> 
					{/if}
										{if $payu_oxxo}						<li id="tab_oxxo"> <a tab = 'payment_payu_oxxo' id ='payment_payu_oxxo_btn' ><input type="radio" name="payu_api" id='api_oxxo' value='oxxo' onclick = "newPaymentMethod('oxxo')" /><label for="api_oxxo">{l s='PAGO OXXO' m='payu'}</label></a></li> 					{/if}
				</ul>
            </div>
            <div id='content-payments'>
				<input type="hidden" id='EBT_DEVICEPRINT' name='EBT_DEVICEPRINT' />				{if $payu_tdc}						<div class = 'content-description-payment' id = 'payment_payu_tdc'>						<fieldset class='section_payu_form'>							<legend><h3>{l s='Pago Tarjeta' mod='payu'}</h3></legend>							<div class="logos-tarjeta"><img src="{$module_dir}img/logos_pagos_tarjetas.jpg" class="wb"/></div>							<div class="payu_field">								<label>{l s='Nombre en Tarjeta: ' mod='payu'}</label>								<input type="text" name='payu[card_name]' />
								</br><span class="form_info">{l s='Poner el nombre tal cual, como sale en su tarjeta.'}</span><br><br>							</div>
							<div class="payu_field">
								<label>{l s='Número de Tarjeta: ' mod='payu'}</label>
								<input placeholder='1234 1234 1234 1234' type="text"  name='payu[card_num]' />
								</br><span class="form_info">{l s='Poner los dìgitos tal cual y como vienen en su tarjeta.'}</span><br><br>
							</div>
							<div class="payu_field">
								<label>{l s='Tipo de Tarjeta: ' mod='payu'}</label>
								<select  name='payu[card_type]' id = 'payu_card_type' onchange="if(this.value=='amex') document.getElementById('payu_amex').style.display='block'; else document.getElementById('payu_amex').style.display='none';">
									<option value="VISA">VISA</option>									<option value="MASTERCARD">MasterCard</option>									{*<option value="AMEX">AmericanExpress</option>*}								</select>
								<br><br>
							</div>
							<div class="payu_field">								<label>{l s='Fecha de Vigencia: ' mod='payu'}</label>
								<select name='payu_vig_month' id='payu_vig_month' >									{foreach from = $months item = recurrencia key = key}										<option value="{$key}" >{$recurrencia}</option>									{/foreach}								</select>
								<select name='payu_vig_year' id='payu_vig_year' >
									{foreach from = $years item = recurrencia}										<option value="{$recurrencia}" >{$recurrencia}</option>									{/foreach}
								</select>
								<br><br>
							</div>
                            <div class="payu_field">
								<label>{l s='Código CCV: ' mod='payu'}</label>
                                <input type="password" maxlength="4" size="5" name='payu[card_ccv]' />
								
                            </div>							
							
							<div id="payu_amex" style="display: none">
								<span class="help">{l s='* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.' mod='payu'}</span>
								<div class="payu_field">
									<label>{l s='Código Postal: ' mod='payu'}</label>
									<input type="text"  name='payu[postcode]' />
								</div>
								
								<div class="payu_field">
									<label>{l s='Dirección: ' mod='payu'}</label>
									<input type="text" name='payu[direction]' />
								</div>
								
							</div>
						</fieldset>
					</div>
				{/if}
				
				{if $payu_rec}
					<div class = 'content-description-payment' id = 'payment_payu_rec' style="display: none">
						<fieldset class='section_payu_form'>
							<legend><h3>{l s='Domiciliar Pago TDC Payu' mod='payu'}</h3></legend>
							<div class="logos-tarjeta"><img src="{$module_dir}img/logos_pagos_tarjetas.jpg"/></div>
							<div class="payu_field">
								<label>{l s='Nombre en Tarjeta: ' mod='payu'}</label>
								<input type="text" name='payu_rec[card_name]' />
							</div>
							<div class="payu_field">
								<label>{l s='Número de Tarjeta: ' mod='payu'}</label>
								<input type="text"  name='payu_rec[card_num]' />
							</div>
							<div class="payu_field">
								<label>{l s='Tipo de Tarjeta: ' mod='payu'}</label>
								<select  name='payu_rec[card_type]' id = 'payu_card_type' onchange="if(this.value=='amex') document.getElementById('payu_rec_amex').style.display='block'; else document.getElementById('payu_rec_amex').style.display='none';">
									<option value="visa">VISA</option>
									<option value="mastercard">MasterCard</option>
									<option value="amex">AmericanExpress</option>
								</select>
							</div>
							<div class="payu_field">
								<label>{l s='Fecha de Vigencia: ' mod='payu'}</label>
								<select name='payu_vig_month_rec' id='payu_vig_month' >
									{foreach from = $months item = recurrencia key = key}
										<option value="{$key}" >{$recurrencia}</option>
									{/foreach}
								</select>
								<select name='payu_vig_year_rec' id='payu_vig_year' >
									{foreach from = $years item = recurrencia}
										<option value="{$recurrencia}" >{$recurrencia}</option>
									{/foreach}
								</select>
							</div>
							<div class="payu_field">
								<label>{l s='Código CCV: ' mod='payu'}</label>
								<input type="password" maxlength="4" size="5" name='payu_rec[card_ccv]' />
							</div>
							
							<div id='payu_rec'>
								<div class="payu_field">
									<label>{l s='Monto a domiciliar: ' mod='payu'}</label>
									<label>$ {$total|number_format:2:".":","}</label>
								</div>
								<div class="payu_field">
									<label>{l s='Recurrencia: ' mod='payu'}</label>
									<label>{$payu_rec_sel}</label>
								</div>
								<div class="payu_field">
									<label>{l s='Inicio Recurrencia: ' mod='payu'}</label>
									<label>{$cart->angel_f|date_format: "%d/%m/%Y"}</label>
								</div>
								<div class="payu_field">
									<input type="hidden" maxlength="4" size="5" name='payu_rec[recurrencias]' value='IND' />
									<input type="hidden" maxlength="1" size="1" name='select_rec_yes_rec' value='1' />
									<input type="hidden" maxlength="10" size="10" name='payu_rec[pago_inicial]' value='{$cart->angel_f}' />
								</div>
							</div>
							
							<div id="payu_rec_amex" style="display: none">
								<span class="help">{l s='* Estos datos son de la terminal amex y son tal y como vienen en sus estado de cuenta.' mod='payu'}</span>
								<div class="payu_field">
									<label>{l s='Código Postal: ' mod='payu'}</label>
									<input type="text"  name='payu_rec[postcode]' />
								</div>
								<div class="payu_field">
									<label>{l s='Dirección: ' mod='payu'}</label>
									<input type="text" name='payu_rec[direction]' />
								</div>
							</div>
						</fieldset>
					</div>
				{/if}
			    {if $payu_oxxo}	
					<div class='content-description-payment' id= 'payment_payu_oxxo' style="display: none">
						<div class="logos-oxxo"><img src="{$module_dir}img/logos_pagos_oxxo.jpg"/></div>
						<p><b>{l s='Al escoger pagar con OXXO se generará y enviara un cupón de pago al correo de tu cuenta.' mod='payu'}</b></p><br>
						<p>{l s='Este cupón es válido únicamente para realizar el pago correspondiente y dentro de la fecha de vigencia establecida. La acreditación del mismo es a las 24 hrs. de realizado, hasta entonces el pago podrá ser corroborado por el vendedor.' mod='payu'}</p><br>
						<p>{l s='El pago se verá reflejado en tienda dentro de las siguientes 72 horas después de haberlo realizado.' mod='payu'}</p><br>
						<p><b><br>{l s='Imprime el cupón de manera clara y legible, usa de preferencia impresora láser, consérvalo en buen estado sin tachar ó doblar la parte del código de barras. En caso de no ser legible, la tienda puede rechazar el pago correspondiente.' mod='payu'}</b></p>
					</div>
			    {/if}
            </div>	
			<p class="cart_navigation" id="cart_navigation">
				<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">{l s='Volver' mod='payu'}</a>
				<label id="button_payment"  class="exclusive_large" >{l s='Confirmar Pago' mod='payu'}</label>
			</p>
		</form>
	{/if}
	<div style="display:none">
		<a id="fancybox_inline" href="#data" style="display:none"></a>
		<div id="data">
			<img src="{$module_dir}img/Payu-Payment-Process.gif" alt="Payu Security">                
		</div>
	</div>