{*
 *  @author Ranchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
*}

<div class="payu-module-wrapper">
	<div class="payu-module-header">
		<img src="{$payu_tracking|escape:'htmlall':'UTF-8'}" alt="" style="display: none;" />
		<a rel="external" href="http://payu.com.mx/" target="_blank"><img class="payu-logo" alt="" src="{$module_dir}img/logo.png" /></a>
		<span class="payu-module-intro">{l s='Pago seguro con: Tarjeta de Crédito/Débito Visa/MasterCard/Amex, Tiendas OXXO.' mod='payu'}<br />
		<a class="payu-module-create-btn L" rel="external" href="https://payu.com.mx/login/" target="_blank"><span>{l s='Ir a Payu' mod='payu'}</span></a></span>
	</div>
	{if $payu_validation}
		<div class="conf">
			{foreach from=$payu_validation item=validation}
				{$validation|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	{if $payu_error}
		<div class="error">
			{foreach from=$payu_error item=error}
				{$error|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	{if $payu_warning}
		<div class="info">
			{foreach from=$payu_warning item=warning}
				{$warning|escape:'htmlall':'UTF-8'}<br />
			{/foreach}
		</div>
	{/if}
	<form action="{$payu_form_link|escape:'htmlall':'UTF-8'}" method="post">
		<fieldset>
			<h4>{l s='Activa/desactiva el API de Payu necesaria' mod='payu'}</h4>
			<div class="payu-usa-threecol">
				<div class="payu-usa-product first fixCol{if $payu_configuracion.PAYU_TDC} payu-usa-product-active{/if}">
					<h4>{l s='Tarjeta de Crédito o Débito' mod='payu'}</h4>
					<div class="box-content-payment" >
						<label for="status_payu_tdc_aceptada"> {l s='TDC: Aceptado' mod='payu'}</label></br>
						<select name='status_payu_tdc_aceptada' id='status_payu_tdc_aceptada' style="width:200px">
						{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_TDC_ACEPTADA == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
						<label for="status_payu_tdc_denegada"> {l s='TDC: Denegado' mod='payu'}</label></br>
						<select name='status_payu_tdc_denegada' id='status_payu_tdc_denegada' style="width:200px">
						{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_TDC_DENEGADA == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
						<label for="status_payu_tdc_pendiente"> {l s='TDC: Pendiente' mod='payu'}</label></br>
						<select name='status_payu_tdc_pendiente' id='status_payu_tdc_pendiente' style="width:200px">
						{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_TDC_PENDIENTE == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select>
					</div>
					<center><input type="checkbox" name="payu_tdc" id="payu_tdc" {if $payu_configuracion.PAYU_TDC} checked="checked"{/if} /> <label for="payu_tdc"> {l s='Enabled' mod='payu'}</label></center>
				</div>
				<div class="payu-usa-product fixCol{if $payu_configuracion.PAYU_OXXO} payu-usa-product-active{/if}">
					<h4>{l s='OXXO' mod='payu'}</h4>
					<div class="box-content-payment" >
						<label for="payu_oxxo_vig"> {l s='Vigencia' mod='payu'}</label><input type="text" name="payu_oxxo_vig" id="payu_oxxo_vig" size="5" value = "{$payu_configuracion.PAYU_OXXO_VIG}" /> <br>
						<label for="status_payu_oxxo_espera"> {l s='OXXO: Espera de Pago' mod='payu'}</label></br>
							<select name='status_payu_oxxo_espera' id='status_payu_oxxo_espera' style="width:200px">
							{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_OXXO_ESPERA == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
						<label for="status_payu_oxxo_confirmado"> {l s='OXXO: Pago Confirmado' mod='payu'}</label></br>
							<select name='status_payu_oxxo_confirmado' id='status_payu_oxxo_confirmado' style="width:200px">
							{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_OXXO_CONFIRMADO == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
					</div>
					<center><input type="checkbox" name="payu_oxxo" id="payu_oxxo_vig" {if $payu_configuracion.PAYU_OXXO} checked="checked"{/if} /> <label for="payu_oxxo"> {l s='Enabled' mod='payu'}</label></center>
				</div>
				
			</div>
			
			<div class="payu-usa-onecol">
				<div class="payu-usa-product_eco fixCol{if $payu_configuracion.PAYU_REC} payu-usa-product-active{/if}">
					<h4>{l s='Pagos Recurrentes.' mod='payu'}</h4>
					<div class="box-content-payment" >
						<label for="payu_tdc_rec_select"> {l s='Recurrencia' mod='payu'}</label>
						<select name='payu_rec_select' id='payu_rec_select' >
						{foreach from = $payu_rec_select item = recurrencia key = key}
								{if $payu_configuracion.PAYU_REC_SELECT == $key }
								<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
								<option value="{$key}" >{$recurrencia}</option>
								{/if}
						{/foreach}
						</select></br>
						<label for="status_payu_rec"> {l s='En Pago Recurrente' mod='payu'}</label></br>
							<select name='status_payu_rec' id='status_payu_rec' style="width:200px">
							{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_REC == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
						<label for="status_payu_rec_fallo"> {l s='Fallo Pago Recurrente' mod='payu'}</label></br>
							<select name='status_payu_rec_fallo' id='status_payu_rec_fallo' style="width:200px">
							{foreach from = $payu_order_statuses item = recurrencia key = key}
								{if $payu_configuracion.STATUS_PAYU_REC_FALLO == $key }
									<option value="{$key}" selected='selected'>{$recurrencia}</option>
								{else}
									<option value="{$key}" >{$recurrencia}</option>
								{/if}
							{/foreach}
							</select>
					</div>
					<center><input type="checkbox" id="payu_rec" name="payu_rec" {if $payu_configuracion.PAYU_REC} checked="checked"{/if} /> <label for="payu_rec"> {l s='Enabled' mod='payu'}</label></center>
				</div>
				
			</div>
		</fieldset>
	<br />
		<fieldset>
			<legend><img src="{$module_dir}img/settings.gif" alt="" /><span>{l s='Configuración general de API Payu' mod='payu'}</span></legend>
			<div id="payu-usa-basic-settings-table">
				<label for="payu_sandbox_on">{l s='Desarrollo / Producción' mod='payu'}</label>
				<div class="margin-form PT4">
					<input type="radio" name="payu_sandbox" id="payu_sandbox_on" value="0"{if $payu_configuracion.PAYU_SANDBOX == 0} checked="checked"{/if} /> <label for="payu_sandbox_on" class="resetLabel">{l s='Producción' mod='payu'}</label>
					<input type="radio" name="payu_sandbox" id="payu_sandbox_off" value="1"{if $payu_configuracion.PAYU_SANDBOX == 1} checked="checked"{/if} /> <label for="payu_sandbox_off" class="resetLabel">{l s='Desarrollo (Sandbox)' mod='payu'}</label>
					<p>{l s='En modo de Desarrollo no sera visible la transacción en el administrador de Payu:' mod='payu'}<br /></p>
				</div>
				<label for="payu_merchant">{l s='API_KEY:' mod='api_key'}</label>
				<div class="margin-form">
					<input type="text" name="payu_apikey" class="input-text" value="{$payu_configuracion.PAYU_APIKEY}" /> <sup>*</sup>
				</div>
				
				<label for="payu_merchant">{l s='API_LOGIN:' mod='api_login'}</label>
				<div class="margin-form">
					<input type="text" name="payu_apilogin" class="input-text" value="{$payu_configuracion.PAYU_APILOGIN}" /> <sup>*</sup>
				</div>
				
				<label for="payu_merchant">{l s='MERCHANT_ID:' mod='merchant_id'}</label>
				<div class="margin-form">
					<input type="text" name="payu_merchant_id" class="input-text" value="{$payu_configuracion.PAYU_MERCHANT_ID}" /> <sup>*</sup>
				</div>
				
				<label for="payu_merchant">{l s='ACCOUNT_ID:' mod='acount_id'}</label>
				<div class="margin-form">
					<input type="text" name="payu_acount_id" class="input-text" value="{$payu_configuracion.PAYU_ACCOUNT_ID}" /> <sup>*</sup>
				</div>
				
				<label for="payu_concept">{l s='Concepto:' mod='payu'}</label></td>
				<div class="margin-form">
					<input type="text" name="payu_concept" class="input-text" value="{$payu_configuracion.PAYU_CONCEPT}" /> <sup>*</sup>
				</div>
				<input type="hidden" name="payu_prod_del_cd" class="input-text" value="CNC" />
				<input type="hidden" name="payu_ship_mthd_cd" class="input-text" value="T" />
			</div>
			
			<div class="clear centerText">
				<input type="submit" name="SubmitProducts" class="button MB15" value="{l s='Modificar Configuración' mod='payu'}" />
			</div>
			<span class="small"><sup style="color: red;">*</sup> {l s='Campos requeridos' mod='payu'}</span>
		</fieldset>
	</form>
</div>
{if $payu_merchant_country_is_mx}
	<script type="text/javascript">
		{literal}
		$(document).ready(function() {
			$('#payu_rec_select').bind('change', function(){
				
				if($(this).val() == 1 )
					$("#datos_rec").css('display', 'none');
				else
					$("#datos_rec").css('display', 'block');
			})
			$('#content table.table tbody tr th span').html('payumx');
			
			
		});
		{/literal}
	</script>
{/if}