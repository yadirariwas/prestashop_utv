<?php

/*
 *  @authorRanchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 */
if (!defined('_PS_VERSION_'))
	exit;

class PayuPayment extends PaymentModule
{

	private $_error = array();
	private $_validation = array();
	private $_shop_country = array();
	
	public $ACCOUNT_ID = null;
	public $REFERENCE_CODE = null;
	//public static $language = SupportedLanguages::ES;
	
	public $display_column_left = false;

	public function __construct()
	{
		
		$this->name 		= 'payupayment';
		$this->version 		= '1.0';
		$this->author 		= 'R15';
		$this->className 	= 'payupayment';
		$this->tab 		= 'payments_gateways';

		parent::__construct();
		
		$this->APIKEY = Configuration::get('PAYU_APIKEY');
		$this->APILOGIN = Configuration::get('PAYU_APILOGIN');
		$this->MERCHANT_ID = Configuration::get('PAYU_MERCHANT_ID');
		$this->ACCOUNT_ID = Configuration::get('PAYU_ACCOUNT_ID');
		$this->REFERENCE_CODE = Configuration::get('PAYU_REFERENCE_CODE');
		$this->DESCRIPTION = Configuration::get('PAYU_CONCEPT');
		
		$this->_shop_country = new Country((int)Configuration::get('PS_SHOP_COUNTRY_ID'));
		$this->displayName = 'Payu Payment';
		$this->confirmUninstall = $this->l('¿Seguro que desea eliminar este modulo?');
		
		/* Backward compatibility */
		require(_PS_MODULE_DIR_.'payupayment/backward_compatibility/backward.php');
		$this->context->smarty->assign('base_dir', __PS_BASE_URI__);
	}

	public function install()
	{
		/* The cURL PHP extension must be enabled to use this module */
		if (!function_exists('curl_version'))
		{
			$this->_errors[] = $this->l('Lo Sentimos, este modulo requiere la extensión cURL PHP (http://www.php.net/curl), no se encuentra activo en su servidor. Por favor comuniquese con su proveedor de hosting.');
			return false;
		}
			
		Configuration::updateValue('PAYU_SANDBOX', true);
		
		Configuration::updateValue('PAYU_APIKEY', 'Apikey_Payu');
		Configuration::updateValue('PAYU_APILOGIN', 'ApiLogin_Payu');
		Configuration::updateValue('PAYU_MERCHANT_ID', 'MERCHANT_Payu');
		
		Configuration::updateValue('PAYU_ACCOUNT_ID', 'Acount_Id_Payu');
		Configuration::updateValue('PAYU_CONCEPT', 'Pago en Línea');
		Configuration::updateValue('PAYU_OXXO_VIG', 3);
		Configuration::updateValue('PAYU_REC_SELECT', 1);
		
		Configuration::updateValue('STATUS_PAYU_TDC_DENEGADA', 8);
		Configuration::updateValue('STATUS_PAYU_TDC_ACEPTADA', 2);
		Configuration::updateValue('STATUS_PAYU_TDC_PENDIENTE', 2);
		Configuration::updateValue('STATUS_PAYU_OXXO_ESPERA', 8);
		Configuration::updateValue('STATUS_PAYU_OXXO_CONFIRMADO', 2);
		Configuration::updateValue('STATUS_PAYU_REC', 2);
		Configuration::updateValue('STATUS_PAYU_REC_FALLO', 8);
		
		/* API´s Disponibles */
		Configuration::updateValue('PAYU_TDC', true);
		Configuration::updateValue('PAYU_OXXO', true);
		Configuration::updateValue('PAYU_REC', false);
		
		/* Configuración de datos para API's*/
		if(	version_compare(_PS_VERSION_, '1.5', '<')){
			Configuration::updateValue('PS_SHOP_COUNTRY','MX');
			Configuration::updateValue('PAYU_SHOP_COUNTRY','MX');
		}else{
			Configuration::updateValue('PS_SHOP_COUNTRY_ID',145);
			Configuration::updateValue('PAYU_SHOP_COUNTRY','MX');
		}
		
		return parent::install() && $this->registerHook('payment') && $this->_installDb() && $this->_installStatuses() && $this->registerHook('paymentReturn');
	}
	

	private function _installDb()
	{
		return Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'payu_log` (
			`id_payu_log` int(11) NOT NULL AUTO_INCREMENT,
			`id_customer` int(11) unsigned NOT NULL,
			`id_cart` int(11) unsigned NOT NULL,
			`response` text NOT NULL,
			`send` text NOT NULL,
			`mode` varchar(10) NOT NULL,
			`date_add` datetime NOT NULL,
		PRIMARY KEY (`id_payu_log`));');
	}
	
	private function _installStatuses(){
		$languages = Language::getLanguages(false);
		
		/*OXXO*/
		Db::getInstance()->Execute( "INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#f76900';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='OXXO: Espera de Pago';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='OXXO: Pago Confirmado';" );
		}
		
		/*TDC*/
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Pagado';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#59c300';" );
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='TDC: Denegado';" );
		}
		
		/*REC*/
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#59c300'; ");
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='REC: En Pago Recurrete';" );
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state SET  module_name = 'payupayment', color = '#f76900'; ");
		$id = Db::getInstance()->insert_id();
		foreach($languages as $language) {
			Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "order_state_lang SET id_order_state = '" . (int)$id . "', id_lang = '" . $language['id_lang'] . "', name='REC: Fallo Pago Recurrente';" );
		}
		
		return true;
	}
	
	public function uninstall()
	{
		
		Configuration::updateValue('PAYU_APIKEY', 'Apikey_Payu');
		Configuration::updateValue('PAYU_APILOGIN', 'ApiLogin_Payu');
		Configuration::updateValue('PAYU_MERCHANT_ID', 'MERCHANT_Payu');
		
		Configuration::updateValue('PAYU_ACCOUNT_ID', 'Acount_Id_Payu');
		Configuration::updateValue('PAYU_CONCEPT', 'Pago en Línea');
		Configuration::updateValue('PAYU_OXXO_VIG', 3);
		Configuration::updateValue('PAYU_REC_SELECT', 1);
		
		/*STATUS DE PEDIDO EN 0*/
		Configuration::updateValue('STATUS_PAYU_TDC_DENEGADA', 8);
		Configuration::updateValue('STATUS_PAYU_TDC_ACEPTADA', 2);
		Configuration::updateValue('STATUS_PAYU_TDC_PENDIENTE', 2);
		Configuration::updateValue('STATUS_PAYU_OXXO_ESPERA', 8);
		Configuration::updateValue('STATUS_PAYU_OXXO_CONFIRMADO', 2);
		Configuration::updateValue('STATUS_PAYU_REC', 2);
		Configuration::updateValue('STATUS_PAYU_REC_FALLO', 8);
		
		/* API´s Disponibles */
		Configuration::updateValue('PAYU_TDC', true);
		Configuration::updateValue('PAYU_OXXO', true);
		Configuration::updateValue('PAYU_REC', false);
		
		$keys_to_uninstall = array('PAYU_MERCHANT_ID','PAYU_APILOGIN','PAYU_APIKEY','PAYU_ACCOUNT_ID','PAYU_CONCEPT','PAYU_OXXO_VIG',
					   'PAYU_TDC','PAYU_OXXO', 'PAYU_REC_SELECT', 'PAYU_REC','STATUS_PAYU_TDC_DENEGADA', 'STATUS_PAYU_TDC_ACEPTADA', 'STATUS_PAYU_TDC_PENDIENTE', 'STATUS_PAYU_OXXO_ESPERA',
					   'STATUS_PAYU_OXXO_CONFIRMADO','STATUS_PAYU_REC', 'STATUS_PAYU_REC_FALLO','PAYU_SANDBOX', 'CONF_PAYU_VAR_FOREIGN', 'PAYU_SHOP_COUNTRY', 'CONF_PAYU_FIXED', 'CONF_PAYU_VAR', 'CONF_PAYU_FIXED_FOREIGN'
					);
		$result = true;
		
		$estatus_payu = Db::getInstance()->executeS("SELECT * FROM " . _DB_PREFIX_ . "order_state WHERE module_name LIKE 'payupayment' ");
		foreach($estatus_payu as $estatus )
			if(Db::getInstance()->Execute("DELETE FROM " . _DB_PREFIX_ . "order_state_lang WHERE id_order_state = " . $estatus['id_order_state']))
				Db::getInstance()->Execute("DELETE FROM " . _DB_PREFIX_ . "order_state WHERE id_order_state = " . $estatus['id_order_state']);
				
		foreach ($keys_to_uninstall as $key_to_uninstall)
			$result &= Configuration::deleteByName($key_to_uninstall);
		/* descomentar la siguiente linea para eliminar los detalles del log de Payu*/
		$result &= Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'payu_log`'); 
		
		return $result && parent::uninstall();
	}

	public function getContent()
	{
		/* Loading CSS and JS files */
		if(isset($this->context->controller)){
			$this->context->controller->addCSS(array($this->_path.'css/payu.css'));
			$this->context->controller->addJS(array(_PS_JS_DIR_.'jquery/jquery-ui-1.8.10.custom.min.js', $this->_path.'js/colorpicker.js', $this->_path.'js/jquery.lightbox_me.js', $this->_path.'js/payu.js'));
		}

		/* Update the Configuration option values depending on which form has been submitted */
		if ((Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX') && Tools::isSubmit('SubmitBasicSettings'))
		{
			$this->_saveSettingsProducts();
			unset($this->_validation[count($this->_validation) - 1]);
		}
		elseif (Tools::isSubmit('SubmitProducts'))
			$this->_saveSettingsProducts();

		$order_statuses = array();	
		$statuses = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang = 1 ');
		
		foreach($statuses as $status)
			$order_statuses[$status['id_order_state']] = $status['name'];
			
		$this->context->smarty->assign(array(
			'payu_tracking' 			=> 'http://www.prestashop.com/modules/payu.png?url_site='.Tools::safeOutput($_SERVER['SERVER_NAME']).'&id_lang='.(int)$this->context->cookie->id_lang,
			'payu_form_link' 			=> './index.php?tab=AdminModules&configure=payupayment&token='.Tools::getAdminTokenLite('AdminModules').'&tab_module='.$this->tab.'&module_name=payupayment',
			'payu_ssl' 				=> Configuration::get('PS_SSL_ENABLED'),
			'payu_validation' 			=> (empty($this->_validation) ? false : $this->_validation),
			'payu_error' 			=> (empty($this->_error) ? false : $this->_error),
			'payu_warning' 			=> (empty($this->_warning) ? false : $this->_warning),
			'payu_configuracion' 		=> Configuration::getMultiple(array(
											   'PAYU_SANDBOX','PAYU_MERCHANT_ID','PAYU_APILOGIN','PAYU_APIKEY','PAYU_ACCOUNT_ID','PAYU_CONCEPT','PAYU_OXXO_VIG',
											   'PAYU_TDC','PAYU_OXXO', 'PAYU_REC',
											   'STATUS_PAYU_TDC_DENEGADA', 'STATUS_PAYU_TDC_ACEPTADA', 'STATUS_PAYU_TDC_PENDIENTE', 'STATUS_PAYU_OXXO_ESPERA',
											   'STATUS_PAYU_OXXO_CONFIRMADO','PAYU_REC_SELECT','STATUS_PAYU_REC', 'STATUS_PAYU_REC_FALLO')),
			'payu_rec_select' 			=> array(1 => $this->l('Diaria'), 2 => $this->l('Mensual'), 3 => $this->l('Anual')),
			'payu_order_statuses' 		=> $order_statuses,
			'payu_merchant_country_is_usa' 	=> (Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'US'),
			'payu_merchant_country_is_mx' 	=> (Validate::isLoadedObject($this->_shop_country) && $this->_shop_country->iso_code == 'MX'),
			'payu_ps_14' 			=> (version_compare(_PS_VERSION_, '1.5', '<') ? 1 : 0),
			'payu_b1width' 			=> (version_compare(_PS_VERSION_, '1.5', '>') ? '350' : '300'),
			'payu_js_files' 			=> stripcslashes('"'._PS_JS_DIR_.'jquery/jquery-ui-1.8.10.custom.min.js","'.$this->_path.'js/colorpicker.js","'.$this->_path.'js/jquery.lightbox_me.js","'.$this->_path.'js/payu.js'.'"')
		));

		return $this->display(__FILE__, 'views/templates/admin/configuration-mx.tpl');
	}

	private function _saveSettingsProducts()
	{
		if (!isset($_POST['payu_tdc']) && !isset($_POST['payu_oxxo']) && !isset($_POST['payu_rec']))
			$this->_error[] = $this->l('Debes activar al menos un metodo de pago');
		if (isset($_POST['payu_oxxo']) && $_POST['payu_oxxo_vig'] == '')
			$this->_error[] = $this->l('Si activa OXXO debe elegir días de vigencia para el código de barras.');
		if (!isset($_POST['payu_apikey']) || $_POST['payu_apikey'] == "")
			$this->_error[] = $this->l('PAYU_APIKEY de Payu requerido.');
		if (!isset($_POST['payu_apilogin']) || $_POST['payu_apilogin'] == "")
			$this->_error[] = $this->l('APILOGIN de Payu requerido.');
		if (!isset($_POST['payu_merchant_id']) || $_POST['payu_merchant_id'] == "")
			$this->_error[] = $this->l('MERCHANT_ID de Payu requerido.');
		if (!isset($_POST['payu_acount_id']) || $_POST['payu_acount_id'] == "")
			$this->_error[] = $this->l('ACCOUNT_ID de Payu requerido.');
		if (!isset($_POST['payu_concept']) || $_POST['payu_concept'] == "")
			$this->_error[] = $this->l('Concepto para cuenta de Payu.');
			
		if (count($this->_error) == 0)
		{
			foreach (array(1 => 'PAYU_TDC', 2 => 'PAYU_OXXO', 3 => 'PAYU_REC') as $payu_products_id => $payu_products)
				Configuration::updateValue($payu_products, (isset($_POST['payu_products']) && $_POST['payu_products'] == $payu_products_id) ? 1 : null);
			
			Configuration::updateValue('PAYU_TDC', isset($_POST['payu_tdc']));
			Configuration::updateValue('PAYU_OXXO', isset($_POST['payu_oxxo']));
			Configuration::updateValue('PAYU_REC', isset($_POST['payu_rec']));
			Configuration::updateValue('PAYU_OXXO_VIG', $_POST['payu_oxxo_vig']);
			Configuration::updateValue('PAYU_REC_SELECT', $_POST['payu_rec_select']);
			
			Configuration::updateValue('STATUS_PAYU_TDC_DENEGADA', $_POST['status_payu_tdc_denegada']);
			Configuration::updateValue('STATUS_PAYU_TDC_ACEPTADA', $_POST['status_payu_tdc_aceptada']);
			Configuration::updateValue('STATUS_PAYU_TDC_PENDIENTE', $_POST['status_payu_tdc_pendiente']);
			Configuration::updateValue('STATUS_PAYU_OXXO_ESPERA', $_POST['status_payu_oxxo_espera']);
			Configuration::updateValue('STATUS_PAYU_OXXO_CONFIRMADO', $_POST['status_payu_oxxo_confirmado']);
			Configuration::updateValue('STATUS_PAYU_REC', $_POST['status_payu_rec']);
			Configuration::updateValue('STATUS_PAYU_REC_FALLO', $_POST['status_payu_rec_fallo']);
			
			$keys_to_uninstall = array('PAYU_MERCHANT_ID','PAYU_APILOGIN','PAYU_APIKEY','PAYU_ACCOUNT_ID','PAYU_CONCEPT','PAYU_OXXO_VIG',
					   'PAYU_TDC','PAYU_OXXO', 'PAYU_REC_SELECT', 'PAYU_REC','STATUS_PAYU_TDC_DENEGADA', 'STATUS_PAYU_TDC_ACEPTADA', 'STATUS_PAYU_TDC_PENDIENTE', 'STATUS_PAYU_OXXO_ESPERA',
					   'STATUS_PAYU_OXXO_CONFIRMADO','STATUS_PAYU_REC', 'STATUS_PAYU_REC_FALLO','PAYU_SANDBOX', 'CONF_PAYU_VAR_FOREIGN', 'PAYU_SHOP_COUNTRY', 'CONF_PAYU_FIXED', 'CONF_PAYU_VAR', 'CONF_PAYU_FIXED_FOREIGN');
			
			Configuration::updateValue('PAYU_APIKEY', $_POST['payu_apikey']);
			Configuration::updateValue('PAYU_APILOGIN', $_POST['payu_apilogin']);
			Configuration::updateValue('PAYU_MERCHANT_ID', $_POST['payu_merchant_id']);
			
			Configuration::updateValue('PAYU_ACCOUNT_ID', $_POST['payu_acount_id']);
			Configuration::updateValue('PAYU_CONCEPT', $_POST['payu_concept']);
			Configuration::updateValue('PAYU_SANDBOX', $_POST['payu_sandbox']);
			
			$this->_validation[] = $this->l('¡Felicidades, Se ha modificado la configuración de Payu!');
		}
	}

	public function hookPayment($params)
	{
		/*Cargamops TPL de pagos en Payu*/
		$this->context->smarty->assign(array(
				'payu_action' 	=> $this->getModuleLink('payupayment', 'payment'),
				'tdc' 			=> Configuration::get('PAYU_TDC'),
				'oxxo' 			=> Configuration::get('PAYU_OXXO'),
				'rec' 			=> Configuration::get('PAYU_REC'),
				'this_path'         	=> $this->getPathUri()
				));
		return $this->display(__FILE__, 'views/templates/hook/standard.tpl');
	}
	
	public function hookPaymentReturn($params) {
		if (!$this->active)
			return;
			
			$smartyArray = array();
			$smartyArray['Método'] 	= $_GET['Payu_metodo'];
			$smartyArray['Total'] 	= Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false);
			$smartyArray['Orden'] 	= $params['objOrder']->id;

			$imprimir = false;
			$tpl = 'payment_return.tpl';
			
			switch ( $_GET['api'] ) {
				case'tdc':
					$smartyArray['Autorización'] 	= $_GET['Payu_autorizacion'];
					break;
				case 'oxxo':
					$imprimir = true;
					$tpl = 'payment_return_payu.tpl';	

					$smartyArray['barcode_url'] = $_GET['Payu_url'];
					$smartyArray['C_Barras'] = $_GET['Payu_codigo'];
					$smartyArray['Vigencia'] = $_GET['Fecha_vigencia'];
					$smartyArray['Vigencia'] = $_GET['Fecha_vigencia'];
					break;
				case 'rec':
					if(isset($_GET['Payu_codigo']))
						$smartyArray['Autorización'] = $_GET['Payu_codigo'];
						$smartyArray['Token'] 		= $_GET['Payu_token'];
						$smartyArray['Siguiente'] 	= $_GET['Fecha_cobro'];	
					break;
				default:
					break;
				
			}

			if($imprimir) $this->context->controller->addJS(($this->_path).'/js/jquery.PrintArea.js');
			
			$this->smarty->assign(array('data' => $smartyArray, 'imprimir' => $imprimir));
			
			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
				$this->smarty->assign('reference', $params['objOrder']->reference);
		
		return $this->display(__FILE__, $tpl);
	}

	public function getModuleLink($module, $controller = 'default', array $params = array(), $ssl = null)
	{
		if (version_compare(_PS_VERSION_, '1.5', '<'))
			$link = Tools::getShopDomainSsl(true)._MODULE_DIR_.$module.'/'.$controller.'?'.http_build_query($params);
		else
			$link = $this->context->link->getModuleLink($module, $controller, $params, $ssl);
			
		return $link;
	}
	
	public function checkCurrency($cart)
	{
		$currency_order = new Currency($cart->id_currency);
		$currencies_module = $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}

}
