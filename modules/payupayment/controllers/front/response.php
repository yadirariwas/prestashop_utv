<?php

/*
 *  @author Adrapok <adrapok@gmail.com>
 *
 *  Modulo de pago Banwire para PrestaShop
 */

class PayuPaymentResponseModuleFrontController extends ModuleFrontController
{
	
	public function initContent()
	{
		$file = fopen("payu_response.txt", "w");
		if(isset($_POST)) fwrite($file, print_r($_POST,true));
		fwrite($file, " FIN RESPONSE ");
		fclose($file);
		
		exit;
		
		if(isset ($_POST['response_code_pol']) )  {
			$this->saveLB($_POST);
			$order = new Order((int)Order::getOrderByCartId($_POST['reference_sale']));
			
			if($order->total_paid != null ){
				$new_history = new OrderHistory();
				$new_history->id_order = (int)$order->id;
				$new_history->changeIdOrderState(Configuration::get('STATUS_PAYU_OXXO_CONFIRMADO'), $order, true);
				$new_history->addWithemail(true);
			} else exit('No Mach');
		}else exit('No post');
		exit();
	}
	
	private function saveLB($post) {
		
		if(Configuration::get('PAYU_SANDBOX')=="0"){
			$mode="Desarrollo"; 
		}
		else{
			$mode="Produccion"; 
		}
		
		Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "payu_log_log SET 
							id_customer = '0', 
							id_cart = '" . $post['referencia'] . "', 
							response = '" . serialize($post) . "', 
							send = 'response OXXO', 
							mode = '" . $mode. "', 
							date_add = NOW();"
						);	
	}
	
	
}
