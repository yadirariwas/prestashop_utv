<?php

/*
 *  @author Ranchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 */
class PayuPaymentPaymentModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $display_column_left = false;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
                parent::initContent();
                if(	version_compare(_PS_VERSION_, '1.5', '<')){
			$css_files = array($this->_path.'css/payu.css');

			foreach($css_files as $cssfile){
				echo 	'<link type="text/css" rel="stylesheet" href="'.$cssfile.'" />';
			}

		}
		
		$cart = $this->context->cart;
                    
		if (!$this->module->checkCurrency($cart))
			Tools::redirect('index.php?controller=order');
                
                $cards = array('visa' => 'VISA', 'mastercar' => 'MasterCard', 'amex' => 'AmericanExpress');
                $months = array(1 => '01 - '.$this->module->l('Enero'), 2 => '02 - '.$this->module->l('Febrero'), 3 => '03 - '.$this->module->l('Marzo'), 4 => '04 - '.$this->module->l('Abril'), 5 => '05 - '.$this->module->l('Mayo'), 6 => '06 - '.$this->module->l('Junio'), 7 => '07 - '.$this->module->l('Julio'), 8 => '08 - '.$this->module->l('Agosto'), 9 => '09 - '.$this->module->l('Septiembre'), 10 => '10 - '.$this->module->l('Octubre'), 11 => '11 - '.$this->module->l('Noviembre'), 12 => '12 - '.$this->module->l('Diciembre'));
                $years = array();
                for($i = date('Y'); $i <= date('Y') + 10; $i++)
                    $years[] = $i;
                
                $recurrencias = array(1 => $this->module->l('Semanal'), 2 => $this->module->l('Mensual'), 3 => $this->module->l('Trimestral'), 4 => $this->module->l('Cuatrimestral'), 6 => $this->module->l('Semestral'), 7 => $this->module->l('Anual'));
		
                $this->context->smarty->assign(array(
                        'payu_sandbox'   => Configuration::get('PAYU_SANDBOX'),
                        'payu_tdc'       => Configuration::get('PAYU_TDC'),
                        'payu_oxxo'      => Configuration::get('PAYU_OXXO'),
                        'payu_spei'      => Configuration::get('PAYU_SPEI'),
                        'payu_rec'       => Configuration::get('PAYU_REC'),
                        'payu_rec_sel'   => $recurrencias[Configuration::get('PAYU_REC_SELECT')],
                        'cards'             => $cards,
                        'years'             => $years,
                        'months'            => $months,
                        'module_dir'        => $this->module->getPathUri(),
			'nbProducts'        => $cart->nbProducts(),
			'cust_currency'     => $cart->id_currency,
			'currencies'        => $this->module->getCurrency((int)$cart->id_currency),
			'total'             => $cart->getOrderTotal(true, Cart::BOTH),
			'this_path'         => $this->module->getPathUri(),
			'this_path_bw'      => $this->module->getPathUri(),
			'this_path_ssl'     => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/'
		));

		$this->setTemplate('payment_payu.tpl');
	}
}
