<?php
/*
 *  @author Ranchero 15 <angel.servin@gmail.com>
 *
 *  Modulo de pago Payu para PrestaShop
 */
 
 

 
class payuPaymentAjaxModuleFrontController extends ModuleFrontController
{
	/**
	* @see FrontController::initContent()
	*/
	private $params 		= array();
	/*Variables de Payu*/
	private $response 		= null;
	
	/* Variables de PS*/
	private $py_cart 		= array();
	private $py_customer 		= array();
	private $addresses 		= array();
	private $order 			= array();
	private $productos 		= array();
	
	public function initContent(){	
		$errors = array();
            
		if(!isset($_POST['payu_api'])){
			$errors[] = $this->module->l('No ha seleccionado ningún método de pago de Payu.');
		}
		
		if(count($errors) > 0){
			die( Tools::jsonEncode(array('errors' => $errors)));
		}
            
        $this->py_cart 		= $this->context->cart;
		$this->py_customer 	= $this->context->customer;
		$this->order 		= new Order((int)Order::getOrderByCartId($this->py_cart->id));
		$this->productos 	= $this->py_cart->getProducts();
		$addresses 		= $this->py_customer->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
		$this->addresses 	= $addresses[0];
		if(isset($this->addresses['id_country']) && !empty($this->addresses['id_country'])){
			$this->addresses['country_name'] = $this->addresses['country'];
			$this->addresses['country'] = Country::getIsoById($this->addresses['id_country']);
		}
		if(isset($this->addresses['state_iso']) && !empty($this->addresses['state_iso'])){
			$this->addresses['state_iso'] = substr($this->addresses['state_iso'],0,2);
		}
		
		$this->api = $_POST['payu_api'];
		
		switch($this->api){
			case 'tdc':
				$response=$this->_engineTDC();
				break;
			case 'oxxo':
				$response=$this->_engineOXXO();
				break;
		}
		
		$redirect 	= '';
		$currency 	= $this->context->currency;
		$total 		= (float)$this->py_cart->getOrderTotal(true, Cart::BOTH);
		
		if( (isset($response->code)) ){
			if($response->transactionResponse->state=="DECLINED"){
				switch($response->transactionResponse->responseCode){
					case 'PAYMENT_NETWORK_REJECTED'	: $errors[] = 'TRANSACCION DECLINADA: Transacción rechazada por entidad financiera'; break;
					case 'ENTITY_DECLINED'	: $errors[] = 'TRANSACCION DECLINADA: Transacción rechazada por el banco'; break;
					case 'INSUFFICIENT_FUNDS'	: $errors[] = 'TRANSACCION DECLINADA: Fondos insuficientes'; break;
					case 'INVALID_CARD'	: $errors[] = 'TRANSACCION DECLINADA: Tarjeta inválida'; break;
					case 'CONTACT_THE_ENTITY'	: $errors[] = 'TRANSACCION DECLINADA: Contactar entidad financiera'; break;
					case 'BANK_ACCOUNT_ACTIVATION_ERROR'	: $errors[] = 'TRANSACCION DECLINADA: Débito automático no permitido'; break;
					case 'BANK_ACCOUNT_NOT_AUTHORIZED_FOR_AUTOMATIC_DEBIT'	: $errors[] = 'TRANSACCION DECLINADA: Débito automático no permitido'; break;
					case 'INVALID_AGENCY_BANK_ACCOUNT'	: $errors[] = 'TRANSACCION DECLINADA: Débito automático no permitido'; break;
					case 'INVALID_BANK_ACCOUNT'	: $errors[] = 'TRANSACCION DECLINADA: Débito automático no permitido'; break;
					case 'INVALID_BANK'	: $errors[] = 'TRANSACCION DECLINADA: Débito automático no permitido'; break;
					case 'EXPIRED_CARD'	: $errors[] = 'TRANSACCION DECLINADA: Tarjeta vencida'; break;
					case 'RESTRICTED_CARD'	: $errors[] = 'TRANSACCION DECLINADA: Tarjeta restringida'; break;
					case 'INVALID_EXPIRATION_DATE_OR_SECURITY_CODE'	: $errors[] = 'TRANSACCION DECLINADA: Fecha de expiración o código de seguridadinválidos'; break;
					case 'REPEAT_TRANSACTION'	: $errors[] = 'TRANSACCION DECLINADA: Reintentar pago'; break;
					case 'INVALID_TRANSACTION'	: $errors[] = 'TRANSACCION DECLINADA: Transacción inválida'; break;
					case 'EXCEEDED_AMOUNT'	: $errors[] = 'TRANSACCION DECLINADA: El valor excede el máximo permitido por la entidad'; break;
					case 'ABANDONED_TRANSACTION'	: $errors[] = 'TRANSACCION DECLINADA: Transacción abandonada por el pagador'; break;
					case 'CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS'	: $errors[] = 'TRANSACCION DECLINADA: Tarjeta no autorizada para comprar por internet'; break;
					case 'ANTIFRAUD_REJECTED'	: $errors[] = 'TRANSACCION DECLINADA: Transacción rechazada por sospecha de fraude'; break;
					case 'DIGITAL_CERTIFICATE_NOT_FOUND'	: $errors[] = 'TRANSACCION DECLINADA: Certificado digital no encotnrado'; break;
					case 'BANK_UNREACHABLE'	: $errors[] = 'TRANSACCION DECLINADA: Error tratando de cominicarse con el banco'; break;
					case 'ENTITY_MESSAGING_ERROR'	: $errors[] = 'TRANSACCION DECLINADA: Error comunicándose con la entidad financiera'; break;
					case 'NOT_ACCEPTED_TRANSACTION'	: $errors[] = 'TRANSACCION DECLINADA: Transacción no permitida al tarjetahabiente'; break;
					case 'INTERNAL_PAYMENT_PROVIDER_ERROR'	: $errors[] = 'TRANSACCION DECLINADA: Error'; break;
					case 'INACTIVE_PAYMENT_PROVIDER'	: $errors[] = 'TRANSACCION DECLINADA: Error'; break;
				}
			}
			elseif($response->transactionResponse->state=="ERROR"){
				switch($response->transactionResponse->responseCode){
					case 'ERROR'	: $errors[] = 'ERROR: Error'; break;
					case 'ERROR_CONVERTING_TRANSACTION_AMOUNTS'	: $errors[] = 'ERROR: Error'; break;
					case 'BANK_ACCOUNT_ACTIVATION_ERROR'	: $errors[] = 'ERROR: Error'; break;
					case 'FIX_NOT_REQUIRED'	: $errors[] = 'ERROR: Error'; break;
					case 'AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL'	: $errors[] = 'ERROR: Error'; break;
					case 'AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL'	: $errors[] = 'ERROR: Error'; break;
					case 'AUTOMATIC_FIXED_NOT_SUPPORTED'	: $errors[] = 'ERROR: Error'; break;
					case 'NOT_FIXED_FOR_ERROR_STATE'	: $errors[] = 'ERROR: Error'; break;
					case 'ERROR_FIXING_AND_REVERSING'	: $errors[] = 'ERROR: Error'; break;
					case 'ERROR_FIXING_INCOMPLETE_DATA'	: $errors[] = 'ERROR: Error'; break;
					case 'PAYMENT_NETWORK_BAD_RESPONSE'	: $errors[] = 'ERROR: Error'; break;
					case 'PAYMENT_NETWORK_NO_CONNECTION'	: $errors[] = 'ERROR: No fue posible establecer comunicación con la entidad financiera'; break;
					case 'PAYMENT_NETWORK_NO_RESPONSE'	: $errors[] = 'ERROR: No se recibió respuesta de la entidad financiera'; break;
				}
			}
			elseif($response->transactionResponse->state=="EXPIRED"){
				switch($response->transactionResponse->responseCode){
					case 'EXPIRED_TRANSACTION'	: $errors[] = 'STATUS: Transacción expirada'; break;
				}
			}
			elseif($response->transactionResponse->state=="PENDING"){
				switch($response->transactionResponse->responseCode){
					case 'PENDING_TRANSACTION_REVIEW'	: $mensaje2 = 'STATUS: Transacción en validación manual'; break;
					case 'PENDING_TRANSACTION_CONFIRMATION'	: $mensaje2 = 'STATUS: Recibo de pago generado. En espera de pago'; break;
					case 'PENDING_TRANSACTION_TRANSMISSION'	: $mensaje2 = 'STATUS: Transacción no permitida'; break;
					case 'PENDING_PAYMENT_IN_ENTITY'	: $mensaje2 = 'STATUS: Recibo de pago generado. En espera de pago'; break;
					case 'PENDING_PAYMENT_IN_BANK'	: $mensaje2 = 'STATUS: Recibo de pago generado. En espera de pago'; break;
					case 'PENDING_SENT_TO_FINANCIAL_ENTITY'	: $mensaje2 = 'STATUS: '; break;
					case 'PENDING_AWAITING_PSE_CONFIRMATION'	: $mensaje2 = 'STATUS: En espera de confirmación de PSE'; break;
					case 'PENDING_NOTIFYING_ENTITY'	: $mensaje2 = 'STATUS: Recibo de pago generado. En espera de pago'; break;
				}
			}
			
			
			switch($this->api){
				case 'tdc':
					
					if($response->transactionResponse->state=="APPROVED"){
						$order_status_id 	= Configuration::get('STATUS_PAYU_TDC_ACEPTADA');
						$mailVars 		= array(
								'{Payu_metodo}' 		=> 'PAYU-TDC',
								'{Payu_autorizacion}' 	=> $response->transactionResponse->responseCode,
								'{payu_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante TDC'));
						$mensaje = $this->module->l('El pago se confirmo mediante TDC de Payu. Con autorización: '). $response->transactionResponse->responseCode;
					}
					elseif($response->transactionResponse->state=="DECLINED"){
						$order_status_id 	= Configuration::get('STATUS_PAYU_TDC_DENEGADA');
					}
					elseif($response->transactionResponse->state=="EXPIRED"){
						$order_status_id 	= Configuration::get('STATUS_PAYU_TDC_EXPIRADA');
					}
					elseif($response->transactionResponse->state=="PENDING"){
						$order_status_id 	= Configuration::get('STATUS_PAYU_TDC_PENDIENTE');  
						$mailVars 		= array(
								'{Payu_metodo}' 		=> 'PAYU-TDC',
								'{Payu_autorizacion}' 	=> $response->transactionResponse->responseCode,
								'{payu_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante TDC'));
						$mensaje = $this->module->l('El pago se mediante TDC de Payu se encuentra Pendiente de Autorización. '). $mensaje2;
					}
					break;
				case 'oxxo':
					if($response->transactionResponse->state=="PENDING"){
						$order_status_id 	= Configuration::get('STATUS_PAYU_OXXO_ESPERA');
						$mailVars 		= array(
										'{Payu_metodo}' 		=> 'PAYU-OXXO',
										'{Payu_url}' 			=> $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML,
										'{Payu_codigo}' 		=> $response->transactionResponse->extraParameters->BAR_CODE,
										'{Fecha_vigencia}'		=> $response->transactionResponse->extraParameters->EXPIRATION_DATE,
										'{payu_address_html}' 	=> str_replace("\n", '<br />', 'Pago mediante OXXO'));
						$mensaje =  $mensaje2;
						
					}
					break;
			}
		}
		//echo "-".$order_status_id."- ".print_r($response,true);
		if(count($errors) <= 0 )  {
			$this->module->validateOrder((int)$this->py_cart->id, $order_status_id, $total, $this->module->name, $mensaje, $mailVars, (int)$currency->id, false, $this->py_customer->secure_key);
		}
		else{
			die( Tools::jsonEncode(array('errors' => $errors)));	
		}
		
		$redirectArray = array('id_cart' => (int)$this->context->cart->id, 'id_module' => (int)$this->module->id, 'id_order' => (int)$this->module->currentOrder, 'key' => $this->py_customer->secure_key);
		foreach ( $mailVars as $index => $value ) {
			$indx = str_replace('{', '', $index);
			$indx = str_replace('}', '', $indx);
			$redirectArray[$indx] = urlencode($value);
		}
		$redirectArray['api'] = $this->api;
			
		if(version_compare(_PS_VERSION_, '1.5', '<')){
			$redirect = Link::getPageLink('order-confirmation.php', $redirectArray);
		}
		else {
			$redirect = __PS_BASE_URI__.'index.php?controller=order-confirmation';
			foreach ( $redirectArray as $key => $value ){
				$redirect .= '&' . $key . '=' . $value;
			}
		}

		die( Tools::jsonEncode(array('redirect' => $redirect)));
		
		
	}

	private function _engineTDC(){
		
		require_once dirname(__FILE__).'/../../lib/PayU.php';
		
		PayU::$apiKey = Configuration::get('PAYU_APIKEY'); 
		PayU::$apiLogin = Configuration::get('PAYU_APILOGIN'); 
		PayU::$merchantId = Configuration::get('PAYU_MERCHANT_ID'); 
		PayU::$language = SupportedLanguages::ES; 
		if(Configuration::get('PAYU_SANDBOX')=="0"){
			PayU::$isTest = false; 
		}
		else{
			PayU::$isTest = true; 
		}
		
		$payu 		= $_POST['payu'];
		
		$this->shipping = new Address(intval($this->py_cart->id_address_delivery));
		if(isset($this->shipping->id_country) && !empty($this->shipping->id_country)){
			$this->shipping->country_name = $this->shipping->country;
			$this->shipping->country = Country::getIsoById($this->shipping->id_country);
		}

		if(isset($this->shipping->state_iso) && !empty($this->shipping->state_iso)){
			$this->shipping->state_iso = substr($this->shipping->state_iso,0,2);
		}
			
		$postcode_amex 	= ($payu['card_type'] == 'amex' ? $payu['postcode'] : $this->addresses['postcode'] );
		$address_amex 	= ($payu['card_type'] == 'amex' ? $payu['direction']: $this->addresses['address1'] );
		
		$cardExp 	= $_POST['payu_vig_year']."/".($_POST['payu_vig_month'] < 10 ? '0'. $_POST['payu_vig_month'] : $_POST['payu_vig_month']);
			
		$addats['phone']=str_replace(' ', '', $this->addresses['phone_mobile']);
		if(trim($addats['phone'])==""){
			$addats['phone']='11111111';
		}
		if(strlen($addats['phone'])>15){
			$addats['phone']=substr($addats['phone'], 0, 15);
		}
		$addats['phone'] = @ereg_replace("[^0-9]", "", $addats['phone']);
		
		$addats['email']=$this->py_customer->email;
		if(trim($addats['email'])==""){
			$addats['email']='correo@dominio.com';
		}
		if(strlen($addats['email'])>40){
			$addats['email']=substr($addats['email'], 0, 40);
		}
		
		$addats['firstname']=$this->py_customer->firstname;
		if(trim($addats['firstname'])==""){
			$addats['firstname']='firstname';
		}
		if(strlen($addats['firstname'])>30){
			$addats['firstname']=substr($addats['firstname'], 0, 30);
		}
		
		$addats['lastname']=$this->py_customer->lastname;
		if(trim($addats['lastname'])==""){
			$addats['lastname']='lastname';
		}
		if(strlen($addats['lastname'])>30){
			$addats['lastname']=substr($addats['lastname'], 0, 30);
		}
		
		if(!isset($address) || trim($address)==""){
			$address="address";
		}
		if(strlen($address)>30){
			$address=substr($address, 0, 30);
		}
		
		if(trim($address_amex)==""){
			$address_amex="address";
		}
		if(strlen($address_amex)>30){
			$address_amex=substr($address_amex, 0, 30);
		}
		
		$addats['city']=$this->addresses['city'];
		if(trim($addats['city'])==""){
			$addats['city']="city";
		}
		if(strlen($addats['city'])>20){
			$addats['city']=substr($addats['city'], 0, 20);
		}
		
		$addats['state_iso']=$this->addresses['state_iso'];
		if(trim($addats['state_iso'])==""){
			$addats['state_iso']="state_iso";
		}
		if(strlen($addats['state_iso'])>3){
			$addats['state_iso']=substr($addats['state_iso'], 0, 3);
		}
		
		$postcode=$addats['postcode']=$this->addresses['postcode'];
		if(trim($addats['postcode'])==""){
			$addats['postcode']='11111';
		}
		if(strlen($addats['postcode'])<5){
			$addats['postcode']=str_pad($addats['postcode'], 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($addats['postcode'])>7){
			$addats['postcode']=substr($addats['postcode'], 0, 7);
		}
		
		$addats['CUST_CNTRY_CD']="MX";
		$addats['country']=$this->addresses['country'];
		if(trim($addats['country'])==""){
			$addats['country']="MX";
		}
		if(strlen($addats['country'])>3){
			$addats['country']=substr($addats['country'], 0, 3);
		}
		
		if(!isset($postcode) || trim($postcode)==""){
			$postcode='11111';
		}
		if(strlen($postcode)<5){
			$postcode=str_pad($postcode, 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($postcode)>7){
			$postcode=substr($postcode, 0, 7);
		}
		
		if(trim($postcode_amex)==""){
			$postcode_amex='11111';
		}
		if(strlen($postcode_amex)<5){
			$postcode_amex=str_pad($postcode_amex, 5, "0", STR_PAD_LEFT);
		}
		elseif(strlen($postcode_amex)>7){
			$postcode_amex=substr($postcode_amex, 0, 7);
		}
		
		if($payu['card_type']=="VISA"){
			$card_type = PaymentMethods::VISA;
		}
		elseif($payu['card_type']=="MASTERCARD"){
			$card_type = PaymentMethods::MASTERCARD;
		}
		elseif($payu['card_type']=="AMEX"){
			$card_type = PaymentMethods::AMEX;
		}
			
		$parameters = array(
			//Ingrese aquí el identificador de la cuenta.
			PayUParameters::ACCOUNT_ID => Configuration::get('PAYU_ACCOUNT_ID'),
			//Ingrese aquí el código de referencia.
			PayUParameters::REFERENCE_CODE => "orderrr".$this->py_cart->id,
			//Ingrese aquí la descripción.
			PayUParameters::DESCRIPTION => Configuration::get('PAYU_CONCEPT'),
				
			// -- Valores --
			//Ingrese aquí el valor.        
			PayUParameters::VALUE => $this->py_cart->getOrderTotal(true, Cart::BOTH),
			//Ingrese aquí la moneda.
			PayUParameters::CURRENCY => "MXN",
				
			// -- Comprador 
			//Ingrese aquí el nombre del comprador.
			PayUParameters::BUYER_NAME => $payu['card_name'],
			//Ingrese aquí el email del comprador.
			PayUParameters::BUYER_EMAIL => $addats['email'],
			//Ingrese aquí el teléfono de contacto del comprador.
			PayUParameters::BUYER_CONTACT_PHONE => $addats['phone'],
			//Ingrese aquí el documento de contacto del comprador.
			PayUParameters::BUYER_DNI => $this->addresses["dni"],  ///***********
			//Ingrese aquí la dirección del comprador.
			PayUParameters::BUYER_STREET => $address,
			PayUParameters::BUYER_STREET_2 => "",
			PayUParameters::BUYER_CITY => $addats['city'],
			PayUParameters::BUYER_STATE => $this->addresses['country'],
			PayUParameters::BUYER_COUNTRY => $addats['CUST_CNTRY_CD'],
			PayUParameters::BUYER_POSTAL_CODE => $addats['postcode'],
			PayUParameters::BUYER_PHONE => $addats['phone'], 
			
			// -- pagador --
			//Ingrese aquí el nombre del pagador.
			PayUParameters::PAYER_NAME => $payu['card_name'],
			//Ingrese aquí el email del pagador.
			PayUParameters::PAYER_EMAIL => $addats['email'],
			//Ingrese aquí el teléfono de contacto del pagador.
			PayUParameters::PAYER_CONTACT_PHONE => $addats['phone'],
			//Ingrese aquí el documento de contacto del pagador.
			PayUParameters::PAYER_DNI => $this->addresses["dni"],  ///***********
			//OPCIONAL fecha de nacimiento del pagador YYYY-MM-DD, importante para autorización de pagos en México.
			PayUParameters::PAYER_BIRTHDATE => '1980-06-22',

			//Ingrese aquí la dirección del pagador.
			PayUParameters::PAYER_STREET => $address,
			PayUParameters::PAYER_STREET_2 => "",
			PayUParameters::PAYER_CITY => $addats['city'],
			PayUParameters::PAYER_STATE => $this->addresses['country'],
			PayUParameters::PAYER_COUNTRY => $this->addresses['country'],
			PayUParameters::PAYER_POSTAL_CODE => $addats['postcode'],
			PayUParameters::PAYER_PHONE => $addats['phone'],
			
			// -- Datos de la tarjeta de crédito -- 
			//Ingrese aquí el número de la tarjeta de crédito
			PayUParameters::CREDIT_CARD_NUMBER => $payu['card_num'],
			//Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
			PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $cardExp,
			//Ingrese aquí el código de seguridad de la tarjeta de crédito
			PayUParameters::CREDIT_CARD_SECURITY_CODE=> $payu['card_ccv'],
			//Ingrese aquí el nombre de la tarjeta de crédito
			//PaymentMethods::VISA||PaymentMethods::MASTERCARD||PaymentMethods::AMEX  
			PayUParameters::PAYMENT_METHOD => $card_type,
			
			//Ingrese aquí el número de cuotas.
			PayUParameters::INSTALLMENTS_NUMBER => "1",
			//Ingrese aquí el nombre del pais.
			PayUParameters::COUNTRY => PayUCountries::MX,
			
			//Session id del device.
			PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
			//IP del pagadador
			PayUParameters::IP_ADDRESS => $_SERVER['REMOTE_ADDR'],
			//Cookie de la sesión actual.
			PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
			//Cookie de la sesión actual.        
			PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
		);
			
		///echo print_r($parameters,true); exit;
			
		$response = PayUPayments::doAuthorizationAndCapture($parameters);
			//echo "HOLA".print_r($response,true); exit;
			
		$this->saveLog($parameters,$response);
		return $response;
		
		//echo print_r($payu,true); exit;
		
	}
	
	private function _engineOXXO(){
		require_once dirname(__FILE__).'/../../lib/PayU.php';
		
		PayU::$apiKey = Configuration::get('PAYU_APIKEY'); 
		PayU::$apiLogin = Configuration::get('PAYU_APILOGIN'); 
		PayU::$merchantId = Configuration::get('PAYU_MERCHANT_ID'); 
		PayU::$language = SupportedLanguages::ES; 
		if(Configuration::get('PAYU_SANDBOX')=="0"){
			PayU::$isTest = false; 
		}
		else{
			PayU::$isTest = true; 
		}
		
		$dt = date("Y-m-d");
		$exp=  date( "Y-m-d", strtotime( "$dt +".Configuration::get('PAYU_OXXO_VIG')." day" ) );

		$parameters = array(
			PayUParameters::ACCOUNT_ID => Configuration::get('PAYU_ACCOUNT_ID'),
			PayUParameters::REFERENCE_CODE => $this->py_cart->id,
			PayUParameters::DESCRIPTION => Configuration::get('PAYU_CONCEPT'),
			
			PayUParameters::VALUE => $this->py_cart->getOrderTotal(true, Cart::BOTH),
			PayUParameters::CURRENCY => "MXN",
			
			PayUParameters::BUYER_EMAIL => $this->py_customer->email,
			PayUParameters::PAYER_NAME => $this->py_customer->firstname . ' ' . $this->py_customer->lastname,
			PayUParameters::PAYER_DNI=> "5415668464654",
			
			//"SANTANDER"||"SCOTIABANK"||"IXE"||"BANCOMER"||PaymentMethods::OXXO||PaymentMethods::SEVEN_ELEVEN
			PayUParameters::PAYMENT_METHOD => PaymentMethods::OXXO,
		   
			PayUParameters::COUNTRY => PayUCountries::MX,
			
			
			//Ingrese aquí la fecha de expiración. Sólo para OXXO y SEVEN_ELEVEN
			PayUParameters::EXPIRATION_DATE => $exp."T00:00:00",
			PayUParameters::IP_ADDRESS =>  "127.0.0.1",
		   
		);
		
		$response = PayUPayments::doAuthorizationAndCapture($parameters);
		
		if($response){
			//echo print_r($response,true); exit;
			$response->transactionResponse->orderId;
			$response->transactionResponse->transactionId;
			$response->transactionResponse->state;
			if($response->transactionResponse->state=="PENDING"){
				$response->transactionResponse->pendingReason;
				$response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;
				$response->transactionResponse->extraParameters->REFERENCE;
			}
			$response->transactionResponse->responseCode;	
			$this->saveLog($parameters,$response);
			return $response;
		}
		
		return false;
		
 	}
	
	private function saveLog($parametros,$respuesta){
		if(isset($parametros['creditCardSecurityCode']))
			unset($parametros['creditCardSecurityCode']);
		
		if(isset($parametros['creditCardNumber']) && strlen($parametros['creditCardNumber'] ) > 4 )
			$parametros['creditCardNumber'] = substr($parametros['creditCardNumber'], -4);
		
		if(isset($parametros['creditCardNumber']) && strlen( $parametros['creditCardNumber'] ) > 4 )
			$parametros['creditCardNumber'] = substr($parametros['creditCardNumber'], -4);
		
		//echo print_r($respuesta,true); exit;
		
		if(Configuration::get('PAYU_SANDBOX')=="0"){
			$mode="Desarrollo"; 
		}
		else{
			$mode="Produccion"; 
		}
			
		return Db::getInstance()->Execute("INSERT INTO " . _DB_PREFIX_ . "payu_log SET 
							id_customer = '" . $this->py_customer->id . "', 
							id_cart = '" . $this->py_cart->id . "', 
							response = '" . serialize($respuesta) . "', 
							send = '" . serialize($parametros) . "', 
							mode = '" .$mode. "', 
							date_add = NOW();"
						);	
	}
}
