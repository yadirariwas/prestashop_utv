<?php
/* 
 * Gestion des traductions internes
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2014
*/
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');

	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;

	$filename = 'TRANSLATIONS';
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	if ($_GET['key'] != $Key) 
		die('Bad request...Wrong key.');
		
	else
	{
		$language = Language::getLanguage(intval(Configuration::get('PS_LANG_DEFAULT')));
		$iso = $language['iso_code'];
		$trans_lang = _PS_MODULE_DIR_.'newsletteradmin/lang/lang_'.$iso.'.php';
			
		if(file_exists ($trans_lang)) 
		{
			include $trans_lang;
			
			echo '
				<!DOCTYPE html >
				<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
				<script language="javascript">
				function PopupCentrer(page,largeur,hauteur,options) {  var top=(screen.height-hauteur)/2;  var left=(screen.width-largeur)/2;  window.open(page,"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options);}
				</script>
				<title>'.trans('Translations').'</title>
				</head>
				<body>
					<div class="newsfield" >
						<div style="float: right; color:green">Secure key is ok </div>
				<h1><img src="logo.gif"  alt="" title="Newsletter" />&nbsp;'.trans('Edit your files translations').'</h1>
				<h4>'.trans('The fields can\'t be empty, thank you for filling them').'</h4>
				<br/><br/>  
				';
				
			$filesToTranslate = array(
							'cron/list.php' => 'CRON',
							'logs/list.php' => 'LOGS',
							'scripts/files/track.php' => 'TRACK',
							'scripts/manage/newsletters/list.php' => 'BACKUPS',
							'bounces.php' => 'BOUNCES',
							'cron.php' => 'CRON',
							'history.php' => 'HISTORY',
							'import.php' => 'IMPORT',
							'import2.php' => 'IMPORT',
							'translate.php' => 'TRANSLATE',
							'translations.php' => 'TRANSLATIONS',
							'truncate.php' => 'TRUNCATE',
							'update.php' => 'UPDATE',
							'upgrade.php' => 'UPGRADE',
							'templates/list.php' => 'TEMPLATES'
							);
						
			$final = array();			
				
			foreach ($filesToTranslate AS $file => $str)
			{
				$contents = phps2array($file);
				$resu = array();

				foreach ($contents AS $key => $value)
				{
					$a = php_parser(html_entity_decode($value));
					if (isset($a[0]))		//si il y a plus d'une traduction par phrase on les rajoute au tableau
						$resu[] = $a;
				}

				$s = array_flat( $resu);
				
				if(($key = array_search('",\'/',$s)) or ($key = array_search('(.*?)',$s)))
					unset( $s[$key]);
				sort($s); // on trie le tableau en ordre alphabétique

				$s = array_flip($s); // on inverse clés/valeurs

				foreach( $s as $k => $v ) //On complète la clé avec le nom du fichier et on nettoie le code
				{
					$s[$str.' '.str_replace('&nbsp;',' ',$k)] = $s[$k];
					$s[$str.' '.str_replace('&nbsp;',' ',$k)]  = str_replace('&nbsp;',' ',$k);
					unset($s[$k]);
				}
						
				//l'ensemble des expressions à traduire se trouvent dans $final
				$final = array_merge($final,$s);
			} // fin de la boucle parsant chaque fichier

			// on recherche les clés invalides
			foreach( $_LANG as $k => $v ) 
			{
				if (!isset ($final[$k]))
					unset($_LANG[$k]); // si obsolète, on supprime la clé et sa valeur
			}
			foreach( $final as $k => $v ) 
			{
				if(!array_key_exists($k, $_LANG))
					$_LANG[$k] = $v; // on ajoute les manquantes
			}
			ksort($_LANG);
			
			$str_output = '		
				<form action="update.php?key='.$Key.'" method="post"><div>
				<table cellpadding="10">';
			foreach ($_LANG AS $name => $value)
			{
				$str_output .= '<tr><td style="width: 40%; text-align:justify;">'.stripslashes($name).'</td><td style="width: 489px">
								<input type="hidden" name="key['.md5($name).']" value="'.stripslashes($name).'" />';
					
				$value2 = explode(' ', $name, 2);
				$height = (strlen($name)/5)<= 16 ? 16: ceil((strlen($name)/3.5)/16)*16; //ajustement des boites
				$background = ($value2[1] == $value) ? 'background-color:rgb(236, 193, 193);' : '';
						$str_output .= '
						<textarea rows="0" style=" '.$background.' width: 489px; height: '.$height.'px; font-family: Arial;" name="name['.md5($name).']">'.stripslashes($value).'</textarea>';
						if($iso != 'en')
							$str_output .= '</td><td><img src="img/google.gif" title="'.trans('Suggest with Google ?').'" onClick="PopupCentrer(\'../newsletteradmin/translate.php?expr='.urlencode($value2[1]).'&to='.$iso.'\',400,'.$height.',\'menubar=no, status=no\')"/>';
				
				$str_output .= '
				
				</td></tr>';
			}
			$str_output .= '
					</table>
				</div>
			</fieldset>';	
				
		}else 
			die('<br/><br/><center>Your file '.$trans_lang.' not exists!, <br/>please use "Configure" in your module install<br/><input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" /></center>');
		echo $str_output;

		echo '<br /><center><input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" />&nbsp;&nbsp;<input type="submit" name="update" value="'.trans('Update translations').'" class="button" /></center></form>
			<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div></div>
			</html>';

	}

?>			