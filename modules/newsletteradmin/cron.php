#!/usr/local/bin/php
<?php
/* 
 * @Cron task
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 02/2014
*/
ignore_user_abort(true);
set_time_limit (0);

include(dirname(__FILE__).'/../../config/settings.inc.php');
include(dirname(__FILE__).'/functions.php');
require_once(dirname(__FILE__).'/html2text.php');
include(dirname(__FILE__).'/classes/NewsMail.php');

		dbconnect();
		$req = mysql_query("SELECT iso_code FROM "._DB_PREFIX_."lang WHERE id_lang = (SELECT MIN(value) FROM "._DB_PREFIX_."configuration WHERE name ='PS_LANG_DEFAULT')");
		$iso = mysql_fetch_array($req);
		define('_TRANS_LANG_',$iso[0]);
		$filename = 'CRON';
		$cron = '';
		$crondir  = dirname(__FILE__).'/cron/'; 
		
		//Vérification de la présence du fichier de sauvegarde de session:
		$dir = opendir($crondir);	
		if(sizeof(glob($crondir.'*')) <= 3)
		{ 
			$body = trans('No newsletter in the queue.');
			echo '<br/>'.utf8_decode($body);
			die();
		}
		$n = 0;
			
		while($fichier = readdir($dir)) 
		{
			if( $fichier != '.' && $fichier != '..' && $fichier != 'index.php' && $fichier != 'list.php' && $fichier != 'supp.php')
			{
				//récupération des données provenant du fichier:
				$cron = @unserialize(implode('',file($crondir.$fichier)));
				++$n;
				
				if(!empty($cron['finalList']))
				{
					//intégration de ces données:
					$info = utf8_decode(trans('Sending scheduled from a cron job').' -> '.trans('File').' n°:'.$n.' ('.$fichier.')');
					cronlog($info.chr(13));
					$mail = new NewsMail();
					$subject = $cron['POST']['subject_email'];
					$from = utf8_decode($cron['POST']["from"]);
					$fromName = utf8_decode($cron['POST']["fromname"]);
					$toName = utf8_decode($cron['POST']["toname"]);
					$address = utf8_decode($cron['POST']["address"]);
					$configuration = $cron['POST']["configuration"];
					$wait = $cron['POST']["wait_time"]!= 0 ?(int)(60/$cron['POST']["wait_time"]): 2;
			
					if (isset($cron["send"])) $num_sent = $cron["send"]; //ajout des envoyés aux restants
						else $num_sent = 0;
								
					if (!isset($cron["total"])) 
						$cron["total"] = 0;
								
					$shop_id = $cron['shop_id'];
					$url = $cron['url'];
					$id_lang = $cron['lang_id'];
					$num_to_sent =  count($cron['finalList'],0);
					$view = str_replace(" ","_", $view);
					$view = remove_accents($subject);
					$view = str_replace("'","_", $view);
					$view = str_replace("%"," per cent", $view);
					
					$req1 ="SELECT MAX(id_campaign),time FROM "._DB_PREFIX_."mailing_history WHERE id_shop = '$shop_id'";
					$res = mysql_query($req1);
					$id_campaign = mysql_result($res,0);
					$req2 =mysql_query("SELECT time FROM "._DB_PREFIX_."mailing_history WHERE id_shop = '$shop_id' and id_campaign = ".$id_campaign);
					$res2 = mysql_fetch_array($req2);
					if($res2['time'] != 'Progress')
					{			
						$id_campaign++;
						$sql ="insert into "._DB_PREFIX_."mailing_history ( id_campaign, subject, date, time, num_sent) values('$id_campaign', '$view','In','Progress','')";
						mysql_query($sql);
					}
					if($id_campaign >= '11') {
						$sql  = "DELETE from "._DB_PREFIX_."mailing_sent WHERE id_campaign <= ($id_campaign -10)"; 
						mysql_query($sql);
						$sql ="OPTIMIZE TABLE "._DB_PREFIX_."mailing_sent";
						mysql_query($sql);
					}
					
					//on boucle sur toutes les adresses
					foreach ($cron['finalList'] as $key=> $value)
					{	
						$email = $value['email'];
						$firstname = (isset($value['firstname'])) ? $value['firstname']:'';
						$lastname = (isset($value['lastname'])) ? $value['lastname']:'';
						$category 	= $value['category']; 
						if(isset($value['id_gender'])) 
							$gender = $value['id_gender']; 
						else 
							$gender ='';
						if ($gender == 1) 
							$gender = trans('Mr');
						elseif ($gender ==2) 
							$gender = trans('Ms');
						else 
							$gender = '';
									
								
						$link = '<a href="'.$url.'newsletters/'.$view.'.html">'.trans('Click here to read this email in your browser').'</a>';
						$verif_url0 = $url.'?SID='.base64_encode($email).'&action=0&fc=module&module=newsletteradmin&controller=subscription&id_lang='.$id_lang;
						$verif_url1 = $url.'?SID='.base64_encode($email).'&action=1&fc=module&module=newsletteradmin&controller=subscription&id_lang='.$id_lang;
						$sub = '<a href="'.$verif_url0.'">'.trans('Subscribe').'</a>';
						$unsub = '<a href="'.$verif_url1.'">'.trans('Unsubscribe').'</a>';
							
						$message 	= $cron['POST']["body_email"];
						$message 	= str_replace('%FIRSTNAME%', $firstname, $message);
						$message 	= str_replace('%CIVILITY%', $gender, $message);			
						$message 	= str_replace('%LASTNAME%',  $lastname, $message);
						$message 	= str_replace('%MAIL%',      $email, $message);
						$message 	= str_replace('%LINK%',      $link, $message);
						$message 	= str_replace('%SUB%',     $sub, $message);
						$message 	= str_replace('%UNSUB%',     $unsub, $message);
								
						$subject 	= str_replace('%CIVILITY%', $gender, $subject);			
						$subject 	= str_replace('%FIRSTNAME%', $firstname, $subject);
						$subject 	= str_replace('%LASTNAME%',  $lastname, $subject);
						$subject 	= str_replace('%MAIL%',      $email, $subject);
						$css 		= stripslashes($cron['POST']["css"]);

						$date 	= date("d-m-Y");
						$time 	= date("H\h i\m s\s");
						$start1 = utf8_decode(trans('Sending the mail,on:'));
						$start2 = utf8_decode(trans('at'));
						$start 	= $start1.' '.$date.' '.$start2.' '.$time.' -> ';	
						cronlog($start);
								
						//save
						$track = '<img src="'.$url.'track.php?id_shop='.$shop_id.'&email='.$email.'&id_campaign='.$id_campaign.'&subject='.$view.'" border="0" width="0" height="0" />';
						$message.= $track;
								
						//Envoi du mail aux destinataires
						$mail_dir = dirname(__FILE__).'/mails/';
						$h2t = new html2text(utf8_decode($message));
						$text = $h2t->getText();
						$send 		= NewsMail::send(null, 'newsletter',$subject, array('{css}' => $css,'{message}' => stripslashes($message),'{message2}' => $text), strtolower($email),$toName, $from, $fromName, null, null, $mail_dir, false, $shop_id, null,$configuration);
						//limite le blocage dû aux envois trop rapides
						$dom = explode("@",$email);
						$domain = $dom[1];
						if($domain == 'wanadoo.fr' || $domain == 'orange.fr' || $domain == 'laposte.net')
							sleep(5);
						else
							sleep($wait);
						$cron["total"]++;
										
						if (!$send)
						{ 
							echo '<br />Error!'.utf8_decode(trans('Invalid email address')).': '.$email;
							$suc= '('.$cron["total"].') Fail: '.$category.' -> '.$email.' ('.utf8_decode(trans('Invalid email address')).')';
							unset($cron['finalList'][$key]);
						} 
						else 
						{
							$suc= '('.$cron["total"].') Ok: '.$category.' -> '.$email;
							$today = date('d-m-Y H:i:s');
							$num_sent++;
							$sql ="insert into "._DB_PREFIX_."mailing_sent (  id_shop, id_campaign, email, date) values('$shop_id','$id_campaign', '$email','$today')";
							mysql_query($sql);
							$cron["send"] = $num_sent;	
							unset($cron['finalList'][$key]);//on dégage le destinataire
							$srzed = serialize($cron); //sécurité pour reprendre les envois si la connexion plante
							$fh = fopen($crondir.$fichier,'w+');
							fwrite($fh,$srzed);
							fclose($fh);
						}	
						cronlog(html_entity_decode($suc.chr(13)));				
					}		
							
					$sql ="UPDATE "._DB_PREFIX_."mailing_history SET date = '".date("d-m-Y")."', time = '".date("H:i:s")."', num_sent = '$num_sent' WHERE id_campaign = '$id_campaign'";
					mysql_query($sql);
					
					//on supprime le fichier
					unlink($crondir.$fichier);
								
					//envoi d'un mail à l'administrateur
					if($num_sent != $cron["total"]) 
						$error = trans('WARNING: You have one or more errors, see your logs.');
					else 
						$error ='';
						
					$body = $info."\n".utf8_decode(trans('Sending messages finished at').': '.date("H:i:s").' '.trans('on').': '.date("d-m-Y")."\n".trans('File').' n°:'.$n.' ('.$fichier.') -> '.trans('Total number of messages sent').': '.$num_sent.'/'.$cron["total"]);

					mail_info($body."\n".$error,$configuration );
					cronlog(html_entity_decode($body.chr(13).chr(13)));
					unset($cron);
					
					echo '<br/>'.html_entity_decode($body.chr(13)).'<br/>'.utf8_decode(trans('Your session has been destroyed')); 
				} 
				else  
				{
					unlink($crondir.$fichier);
					die('No recipients or invalid file!');
				}
			}	
		}
		closedir($dir);
		exit();

		
	
	function mail_info($body,$configuration )
	{
		$headers ='From: Cron Report for '.$configuration['PS_SHOP_NAME'].'<'.$configuration['PS_SHOP_EMAIL'].'>'."\n"; 
		$headers .='Content-Type: text/plain; charset="iso-8859-1"'."\n"; 
		$headers .='Content-Transfer-Encoding: 8bit';
		mail($configuration['PS_SHOP_EMAIL'], utf8_decode(trans('Sending newsletter planned for')).' '.$configuration['PS_SHOP_NAME'], html_entity_decode($body), $headers);
	}
	
	function remove_accents($str, $charset='utf-8')//to remove specials characters in filenames
	{
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); 
		$str = preg_replace('#&[^;]+;#', '', $str);
		$str = str_replace("\\","", $str);
		$str = str_replace("/","", $str);
		return $str;
	}
?>