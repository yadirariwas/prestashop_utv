<?php
/* 
 * Récupération des traductions Google
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2013
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
$filename = 'TRANSLATE';
		echo '
			<!DOCTYPE html>
			<head>
			<script langbrowserge="javascript">eval(setTimeout(\'window.close()\',8000));</script>
			<meta http-equiv="content-type" content="text/html; charset=utf-8" />
			<title>'.trans('Google suggestion').'</title>
			</head> 
			<body>
			';
		$browser = array (
					"Mozilla/5.0 (Windows; U; Windows NT 6.0; fr; rv:1.9.1b1) Gecko/20081007 Firefox/3.1b1",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.0",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/0.4.154.18 Safari/525.19",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13",
					"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
					"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.40607)",
					"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322)",
					"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; Alexa Toolbar; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
					"Mozilla/45.0 (compatible; MSIE 6.0; Windows NT 5.1)",
					"Mozilla/4.08 (compatible; MSIE 6.0; Windows NT 5.1)",
					"Mozilla/4.01 (compatible; MSIE 6.0; Windows NT 5.1)"
					);
    
    function getRandomUserAgent ( ) {
        srand((double)microtime()*1000000);
        global $browser;
        return $browser[rand(0,count($browser)-1)];
    }
    
    function getContent ($url) {
    
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, getRandomUserAgent());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

        $output = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        if ($output === false || $info != 200) {
          $output = null;
        }
        return $output;
     
    }
	
	class GoogleTranslate {
		
		public $lastResult = "";
		private $langFrom;
		private $langTo;
		private static $urlFormat = "http://translate.google.com/translate_a/t?client=t&text=%s&hl=en&sl=%s&tl=%s&ie=UTF-8&oe=UTF-8&multires=1&otf=1&pc=1&trs=1&ssel=3&tsel=6&sc=1";
		public function __construct($from = "en", $to = "fr") {
			$this->setLangFrom($from)->setLangTo($to);
		}
		public function setLangFrom($lang) {
			$this->langFrom = $lang;
			return $this;
		}
		public function setLangTo($lang) {
			$this->langTo = $lang;
			return $this;
		}
		public static final function makeCurl($url, array $params = array(), $cookieSet = false) {
			if (!$cookieSet) {
				$cookie = tempnam(sys_get_temp_dir(), "CURLCOOKIE");
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = curl_exec($ch);
				// Clean up temporary file
				unset($ch);
				unlink($cookie);
				return $output;
			}
			
			$queryString = http_build_query($params);
			$curl = curl_init($url . "?" . $queryString);
			curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($curl);
			
			return $output;
		}
		public function translate($string) {
			return $this->lastResult = self::staticTranslate($string, $this->langFrom, $this->langTo);
		}
		public static function staticTranslate($string, $from, $to) {
			$url = sprintf(self::$urlFormat, rawurlencode($string), $from, $to);
			$result = preg_replace('!,+!', ',', self::makeCurl($url)); // remove repeated commas (causing JSON syntax error)
			$result = str_replace ("[,", "[", $result);
			$resultArray = json_decode($result, true);
			$finalResult = "";
			if (!empty($resultArray[0])) {
				foreach ($resultArray[0] as $results) {
					$finalResult .= $results[0];
				}
				return $finalResult;
			}
			return false;
		}
	}
    function translate($expression, $from, $to) {
		if ($translate = GoogleTranslate::staticTranslate($expression, $from, $to))
			return $translate;
		else {
			$f = getContent("http://mymemory.translated.net/api/get?q=" . urlencode($expression) . "&langpair=$from|$to");
			$response = json_decode($f);
			return $response->responseData->translatedText;
		}
        
    }
    
	$a = translate($_GET['expr'],'en',$_GET['to']);
	echo $a.
	'</body>';
?>