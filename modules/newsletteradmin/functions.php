<?php
/*
 * Regroupement de certaines fonctions communes 
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2014
*/
	  
	function dbconnect() //deprecated in 1.5 only use for CLI requests
	{
		$hostName = _DB_SERVER_;
		$userName = _DB_USER_;
		$password = _DB_PASSWD_;
		$dbName = _DB_NAME_;

		$req = mysql_connect($hostName,$userName,$password) or die('Connection to the server failed');
		@mysql_select_db($dbName,$req) or die('No such database exist');
	}
	
	function trans($string)
	{
		global $filename;
		
		if (class_exists('Language'))
		{
			$language = Language::getLanguage(intval(Configuration::get('PS_LANG_DEFAULT')));
			$iso = $language['iso_code'];
		}
		else
		{
			$iso = _TRANS_LANG_;
		}

		$trans_lang = dirname(__FILE__).'/lang/lang_'.$iso.'.php';
			
		if(file_exists ($trans_lang)) 
		{
			include $trans_lang;

			if (isset ($_LANG[$filename.' '.html_entity_decode($string)])) 
				return html_entity_decode($_LANG[$filename.' '.$string]);
			else
				return $string;
		}
		else 
		{
			echo 'Warning, Your translation file not exists! Please re-configure your module';
			return $string;
		}
	}

	function errolog($log_message)
	{
		$log = '../modules/newsletteradmin/install_errors.txt' ;
		$wlog=fopen($log, 'a+');
		$log_message = str_replace("&#039;","'", $log_message);
		$r = chr(13);
		fwrite($wlog,$log_message.$r);
		fclose($wlog);
	}
	
	function nlog($log_message)
	{
		$date = date("d-m-Y");
		$time = date("H\h i\m s\s");
		$log = '../modules/newsletteradmin/logs/'.$date.'.txt' ;
		$wlog=fopen($log, 'a+');
		$log_message = str_replace("&#039;","'", $log_message);
		fwrite($wlog,$log_message);
		fclose($wlog);
	}
		
	function cronlog($log_message)
	{
		$date = date("d-m-Y");
		$time = date("H\h i\m s\s");
		$log = dirname(__FILE__).'/logs/'.$date.'.txt' ;
		$wlog=fopen($log, 'a+');
		$log_message = str_replace("&#039;","'", $log_message);
		fwrite($wlog,$log_message);
		fclose($wlog);
	}

	function cmp($a,$b) 
	{
		if ($a[1] == $b[1])
			return 0;
		return ($a[1] < $b[1]) ? 1 : -1 ;
	}

	function rotate_logs()
	{
		$tab = array();
		$sauvegardes = _PS_MODULE_DIR_ .'newsletteradmin/logs/';
		
		foreach (glob($sauvegardes."*") as $path) { 
			$docs[$path] = filemtime($path);
		} 
		arsort($docs);

		foreach ($docs as $path=>$name) 
		{
			if(basename($path) != "." && basename($path) != ".."&& basename($path) != "index.php"&& basename($path) != "list.php"&& basename($path) != "supp.php")
				$tab[] = basename($path);
		}
		
		$key = '';
		foreach ($tab as $key=>$tt) 
		{
			if($key >9) unlink($sauvegardes.basename($tab[$key]));
		}
	
	}
	
	function deltree($dir)  //suppression d'un r�pertoire et de son contenu(sous-r�pertoires y compris)
	{ 
		$current_dir = opendir($dir); 
		  
		while($entryname = readdir($current_dir))  
		{ 
			if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!=".."))  
				deltree("${dir}/${entryname}");  
			elseif($entryname != "." and $entryname!="..") 
				unlink("${dir}/${entryname}"); 
		}
		  
		closedir($current_dir); 
		@rmdir(${dir}); 
	}
	

	function makeAll($dir, $mode = 0777, $recursive = true) 
	{
		if( is_null($dir) || $dir === "" )
			return FALSE;
		if( is_dir($dir) || $dir === "/" )
			return TRUE;
		if( makeAll(dirname($dir), $mode, $recursive) )
			return mkdir($dir, $mode);
		return FALSE;
	}
	
	function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0644)) 
	{ 
		$result=false; 
			        
		if (is_file($source)) 
		{ 
			if ($dest[strlen($dest)-1]=='/') 
			{ 
			    if (!file_exists($dest)) 
			        makeAll($dest,$options['folderPermission'],true); 
			    $__dest=$dest."/".basename($source); 
			} 
			else { 
			    $__dest=$dest;
				$result .= $__dest.'...<br/>';				
			} 
			        $result=copy($source, $__dest);
					chmod($__dest,$options['filePermission']); 
			        
		} 
		elseif(is_dir($source)) 
		{ 
			if ($dest[strlen($dest)-1]=='/') 
			{ 
			    if ($source[strlen($source)-1]=='/')
				{ 
			       //Copy only contents 
			    } 
				else 
				{ 
			        //Change parent itself and its contents 
			        $dest=$dest.basename($source); 
			        @mkdir($dest); 
			        chmod($dest,$options['folderPermission']); 
			    } 
			} else { 
			    if ($source[strlen($source)-1]=='/') 
				{ 
					//Copy parent directory with new name and all its content 
			        @mkdir($dest,$options['folderPermission']); 
			    } else { 
			        //Copy parent directory with new name and all its content 
			        @mkdir($dest,$options['folderPermission']); 
			    } 
			} 
			
			$dirHandle=opendir($source); 
			    while($file=readdir($dirHandle)) 
			        { 
			            if($file!="." && $file!="..") 
			            { 
			                if(!is_dir($source."/".$file))
							{ 
			                    $__dest=$dest."/".$file; 
			                } else { 
			                    $__dest=$dest."/".$file; 
			                } 
							$result=smartCopy($source."/".$file, $__dest, $options);
			            } 
					} 
			closedir($dirHandle);            
		} else {
			$result=false; 
		} 
		return $result; 
	}
	

	
	function stepto($t,$step,$error) 
	{ 
		echo '<meta http-equiv="refresh" content="'.$t.';URL='.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&install=1&step='.$step.'&token='.Tools::getAdminTokenLite('AdminNewsletter').'&error='.$error.'">';
	}
		
		
		
	function report($id_campaign,$id_shop)
	{
				$req3 = Db::getInstance()->ExecuteS("SELECT email FROM "._DB_PREFIX_."mailing_sent WHERE id_campaign ='$id_campaign' AND id_shop = ".$id_shop);
				$req2 = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."mailing_history WHERE id_campaign ='$id_campaign' AND id_shop = ".$id_shop);
					if ($req2 == null)
						die('No Campaign!');
					else
					{				
						$nbrs_enreg = Db::getInstance()->getValue("SELECT COUNT(*) AS compt FROM "._DB_PREFIX_."mailing_track WHERE id_campaign ='$id_campaign' AND id_shop = ".$id_shop);
						$name 		= $req2[0]['subject'];
						$num_sent 	= $req2[0]['num_sent']!= 0 ? $req2[0]['num_sent'] : count($req3);
						$date_sent 	= $req2[0]['date'];
						$time_sent 	= $req2[0]['time'];
						
						$arrayIdentifiants   = array();
						$arrayIdentifiants[] = $name;
						$arrayIdentifiants[] = $date_sent;
						$arrayIdentifiants[] = $time_sent;
						$arrayIdentifiants[] = $num_sent;	
						$arrayIdentifiants[] = $nbrs_enreg;
				 
						return $arrayIdentifiants;
					}	
	}
		
	function email_parser($str)
	{
		$str = strtolower($str);
		$pattern = '`[a-z0-9][-_\.a-z0-9]+[a-z0-9]@[-\.a-z0-9]+[a-z]`';
		
		preg_match_all($pattern, $str, $matches); 
		$out = $matches[0]; 
		$out = array_unique($out); 
		sort($out); 
		return $out;
	}
	
	function debug($var) 
	{
		$debug = debug_backtrace();
		echo '<p>&nbsp;</p><p><a href="#" onclick="$(this).parent().next(\'ol\').slideToggle(); return false;"><strong>' . $debug[0]['file'] . ' </strong> l.' . $debug[0]['line'] . '</a></p><ol style="display:none;">';
		
		foreach ($debug as $k => $v)
		{
			if ($k > 0) 
				echo '<li><strong>' . $v['file'] . '</strong> l.' . $v['line'] . '</li>';
		}
		echo '</ol>';
		echo '<pre>';
		print_r($var);
		echo '</pre>';		
	}

	function php_parser($page) //recherche les champs � traduire
	{
		preg_match_all('/'.preg_quote("trans('",'/').'(.*?)'.preg_quote("')", '/').'/i', stripslashes($page), $m);
				
		return $m[1];
				
	}
		
	function phps2array($fileName) // transforme le fichier texte en array
	{ 
		$n = explode(PHP_EOL, htmlspecialchars(strip_tags(highlight_file($fileName, true))));
				
		return  $n;
			
	}
		
	function array_flat( $a, $s = array( ), $l = 0 ) //reduit le tableau � 1 dimension sans doublons
	{ 
		# check if this is an array 
		if( !is_array( $a ) )                           
			return $s; 
		# go through the array values 
		foreach( $a as $k => $v ) 
		{ 
			# check if the contained values are arrays 
			if( !is_array( $v ) ) 
			{ 
				# store the value 
				$s[ ] = $v; 
				# move to the next node 
				continue; 
			} 
			# increment depth level 
			$l++; 
			# replace the content of stored values 
			$s = array_flat($v, $s, $l ); 
			# decrement depth level 
			$l--; 
		} 
		# get only unique values 
		if( $l == 0 ) 
			$s = array_values( array_unique( $s ) ); 

		return $s; 
	}	
		
	function secureslashes($string) 
	{
		if (get_magic_quotes_gpc()) 
			return stripslashes($string);
		else 
			return addslashes($string);
	}