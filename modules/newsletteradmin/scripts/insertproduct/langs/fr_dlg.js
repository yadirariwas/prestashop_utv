tinyMCE.addI18n('fr.insertproduct_dlg',{
	title : 'Importer des produits',
	search_desc : 'Rechercher', 
	selection_desc : 'Produits sélectionnés',
	style_desc : 'Style',
	search_panel_form_category_label : 'Catégorie',
	search_panel_form_currency_label : 'Devise',
	search_panel_form_keyword_label : 'Mot clé',
	search_panel_form_add_to_cart_label_1 : 'Lien',
	search_panel_form_add_to_cart_label_2 : '"Ajouter au panier"',
	new_search : 'Nouvelle recherche',
	search : 'Rechercher', 
	insert : 'Insérer', 
	alreadySelected : 'Ce produit est déjà sélectionné', 
	noneSelected : 'Aucun produit n\'est sélectionné', 
	notfound : 'Cette catégorie ne contient pas de produits'
});
