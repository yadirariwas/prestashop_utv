<?php
include_once(dirname(__FILE__).'/../../../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../../../init.php');

$context = Context :: getContext();
if (Shop::isFeatureActive())
	Shop::setContext(Shop::CONTEXT_SHOP);
$id_lang = (int)$context->language->id;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#insertproduct_dlg.title}</title>
<?php	
if (version_compare(_PS_VERSION_,'1.6','>=')) 
	echo '
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="tiny_mce_popup.js"></script>
	<script type="text/javascript" src="utils/mctabs.js"></script>
	<script type="text/javascript" src="utils/form_utils.js"></script>';
else 
	echo '
	<script type="text/javascript" src="../../../../js/jquery/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="../../../../js/tiny_mce/tiny_mce_popup.js"></script>
	<script type="text/javascript" src="../../../../js/tiny_mce/utils/mctabs.js"></script>
	<script type="text/javascript" src="../../../../js/tiny_mce/utils/form_utils.js"></script>'; 
?>
	<script type="text/javascript">
		var p_InsertProductDialog_id_lang = "<?php $id_lang ?>";
	</script>
	<script type="text/javascript" src="js/insertproduct.js"></script>
</head>
<body>
<form onsubmit="InsertProductDialog.insert();return false;" action="#">
	<div class="tabs">
		<ul>
			<li id="search_tab" class="current"><span><a href="javascript:InsertProductDialog.switchMode('search');" onmousedown="return false;">{#insertproduct_dlg.search_desc}</a></span></li>
			<li id="selection_tab"><span><a href="javascript:InsertProductDialog.switchMode('selection');" onmousedown="return false;">{#insertproduct_dlg.selection_desc}</a></span></li>
		</ul>
	</div>

	<div class="panel_wrapper">
		<div id="search_panel" class="panel">
			<div id="search_panel_form">
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td><label for="search_panel_form_category">{#insertproduct_dlg.search_panel_form_category_label}</label></td>
						<td><select name="search_panel_form_category" id="search_panel_form_category"><?php
$categories = Category::getCategories(intval($id_lang));
Category::recurseCategory($categories, (int)Configuration::get('PS_HOME_CATEGORY'),1,(int)Configuration::get('PS_HOME_CATEGORY'));
?></select></td>
					</tr>
					<tr>
						<td>
							{#insertproduct_dlg.search_panel_form_add_to_cart_label_1}<br/>
							{#insertproduct_dlg.search_panel_form_add_to_cart_label_2}</td>
						<td>
							<input type="radio" id="search_panel_form_add_to_cart_enabled" name="search_panel_form_add_to_cart" value="1" checked="checked" style="border: 0 none" /> <label for="search_panel_form_add_to_cart_enabled"><img src="img/enabled.gif" /></label><br/>
							<input type="radio" id="search_panel_form_add_to_cart_disabled" name="search_panel_form_add_to_cart" value="0" style="border: 0 none" /> <label for="search_panel_form_add_to_cart_disabled"><img src="img/disabled.gif" /></label>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div>
								<input type="button" id="search" value="{#insertproduct_dlg.search}" onclick="InsertProductDialog.searchProductsResult();" />
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="search_panel_result" style="display:none;">
				<div id="search_panel_result_result">
				</div>
				<div>
					<input type="button" id="newsearch" value="{#insertproduct_dlg.new_search}" onclick="InsertProductDialog.searchProductsForm();" />
				</div>
			</div>
		</div>

		<div id="selection_panel" class="panel">
			<div id="selection_panel_result">
			</div>
		</div>

	</div>

	<div class="mceActionPanel">
		<div style="left: 20px;position: fixed;bottom: 10px;">
			<input type="submit" id="insert" name="insert" value="{#insertproduct_dlg.insert}" />
		</div>

		<div style="left: 200px;position: fixed;bottom: 10px;">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
		</div>
	</div>

</form>

</body>
</html>
