tinyMCEPopup.requireLangPack();

var p_InsertProductDialog_searchProducts;
var p_InsertProductDialog_selectedProducts;

function f_InsertProductDialog_selectedIndex(a_id_product){
	for(var l_cpt=0 ; l_cpt<p_InsertProductDialog_selectedProducts.length ; l_cpt++)
		if(p_InsertProductDialog_selectedProducts[l_cpt].attr('id')==a_id_product)
			return l_cpt;
	return -1;
}

var InsertProductDialog = {

	init : function(ed) {
		var f = document.forms[0], m = tinyMCEPopup.getWindowArg("mode");
		p_InsertProductDialog_selectedProducts = new Array();
		this.switchMode('search');

	},

	switchMode : function(m) {
		var f, lm = this.lastMode;

		if (lm != m) {
			mcTabs.displayTab(m + '_tab',  m + '_panel');
			this.lastMode = m;
		}
	}, 

	searchProductsResult : function() {
		f = document.forms[0];
		$.post('insertproduct-callback.php', 
			{
				id_lang: p_InsertProductDialog_id_lang,
				id_category: f['search_panel_form_category'].value, 
				add_to_cart: f['search_panel_form_add_to_cart'][0].checked, 
			}, 
			function(a_xmlData){
				if($(a_xmlData).find('return').attr('value')=='ok'){
					p_InsertProductDialog_searchProducts = new Array();
					$('#search_panel_form').hide();
					$('#search_panel_result').show();
					$.each($(a_xmlData).find('product'), function(a_cpt, a_product){
						p_InsertProductDialog_searchProducts[a_cpt] = $(a_product);
						$('#search_panel_result_result').append('<div id="search_product_' + $(a_product).attr('id') + '">' + 
											'	<table border="0" cellspacing="0" cellpadding="2"><tr>' + 
											'		<td><a href="javascript:InsertProductDialog.selectionProductAdd(' + a_cpt + ')"><img src="img/add.gif" border="0"></a></td>' + 
											'		<td><img src="' + $(a_product).attr('img') + '" /></td>' + 
											'		<td>' + $(a_product).find('name').text() + '</td>' + 
											'	</tr></table>' + 
											'</div>');
					});

					if(p_InsertProductDialog_searchProducts.length == 0){
						var ed = tinyMCEPopup.editor;
						tinyMCEPopup.alert(ed.getLang('insertproduct_dlg.notfound'));
						$('#search_panel_result').hide();
						$('#search_panel_form').show();
					}

				}
			});

	}, 

	searchProductsForm : function() {
		$('#search_panel_result').hide();
		$('#search_panel_result_result').html('');
		$('#search_panel_form').show();
	},


	selectionProductAdd : function(a_cpt) {
		var l_product = p_InsertProductDialog_searchProducts[a_cpt];
		if(f_InsertProductDialog_selectedIndex(l_product.attr('id'))==-1){
			p_InsertProductDialog_selectedProducts.push(l_product);
			$('#selection_panel_result').append(	'<div id="selection_product_' + l_product.attr('id') + '">' + 
								'	<table border="0" cellspacing="0" cellpadding="2"><tr>' + 
								'		<td><a href="javascript:InsertProductDialog.selectionProductDelete(' + l_product.attr('id') + ')"><img src="img/delete.gif" border="0"></a></td>' + 
								'		<td><img src="' + l_product.attr('img') + '" /></td>' + 
								'		<td>' + l_product.find('name').text() + '</td>' + 
								'	</tr></table>' + 
								'</div>');
		}
		else{
			var ed = tinyMCEPopup.editor;
			tinyMCEPopup.alert(ed.getLang('insertproduct_dlg.alreadySelected'));
		}
		$('#search_product_' + l_product.attr('id')).hide();
	},

	selectionProductDelete : function(a_id_product) {
		var l_cpt = f_InsertProductDialog_selectedIndex(a_id_product);
		p_InsertProductDialog_selectedProducts = p_InsertProductDialog_selectedProducts.slice(0, l_cpt).concat(p_InsertProductDialog_selectedProducts.slice(l_cpt+1, p_InsertProductDialog_selectedProducts.length));
		$('#selection_product_' + a_id_product).remove();
		if($('#search_product_' + a_id_product).length)
			$('#search_product_' + a_id_product).show();
	}, 

	insert : function() {
		if(p_InsertProductDialog_selectedProducts.length==0){
			var ed = tinyMCEPopup.editor;
			tinyMCEPopup.alert(ed.getLang('insertproduct_dlg.noneSelected'));
		}
		else{
			var l_InsertContent = '<ul id="product_list">';
			for(var l_cpt=0 ; l_cpt<p_InsertProductDialog_selectedProducts.length ; l_cpt++){
				var l_product = p_InsertProductDialog_selectedProducts[l_cpt];

				var l_li_class = 'ajax_block_product'
				if(l_cpt == 0)
					l_li_class += ' first_item';
				else if(l_cpt==p_InsertProductDialog_selectedProducts.length-1)
					l_li_class += ' last_item';
				
				if(l_cpt%2 == 0)
					l_li_class += ' alternate_item';
				else
					l_li_class += ' item';

				l_InsertContent += '<li class="' + l_li_class + '" style="list-style-type: none;">' + l_product.find('html').text() + '</li>';

			}
			l_InsertContent += '</ul>';
			tinyMCEPopup.editor.execCommand('mceInsertContent', false, l_InsertContent);
			tinyMCEPopup.close();
		}
	}

};

tinyMCEPopup.onInit.add(InsertProductDialog.init, InsertProductDialog);
