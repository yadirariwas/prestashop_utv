<?php
/**
 * Tableau récapitulatif des 10 derniers envois depuis le Back-Office Prestashop  
 * @category admin 
 * @copyright eolia@o2switch.net 
 * @Author Eolia  02/2014  compatible PS 1.5.x.x Only! 
 * @version 3.3
 *
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');

$filename = 'HISTORY';
$Key = Configuration::get('NEWSLETTER_KEY_CODE');

if (@$_GET['key'] != $Key) 
	die('Bad request...Wrong key.');
else 
{
	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;

			echo'<!DOCTYPE html">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
				<title>'.trans('Results of the latest campaigns').'</title>
			</head>
				<body>
					<div class="newsfield" >
							<div style="float: right; color:green">Secure key is ok </div>
								<h1>'.trans('Results of the latest campaigns').'</h1>
									<div style="margin-left: 12px">
										<TABLE border="3" style=" font-size:14px; text-align:center; width:100%">
										<TR>
											<TD><b>Id</b> </TD>
											<TD><b>'.trans('Name').':</b> </TD>
											<TD><b>'.trans('Date').':</b> </TD>
											<TD><b>'.trans('Time').':</b> </TD>
											<TD><b>'.trans('Sent').':</b> </TD>
											<TD><b>'.trans('Received').':</b> </TD>
											<TD><b>'.trans('Impact').':</b> </TD>
										</TR><br/>';
			//Nombre de campagnes
			$req3 = Db::getInstance()->ExecuteS("SELECT `id_campaign` FROM "._DB_PREFIX_."mailing_history WHERE id_shop = ".$_GET['id_shop']." ORDER BY id_campaign");
			$min = Db::getInstance()->ExecuteS("SELECT MIN(`id_campaign`) as min FROM "._DB_PREFIX_."mailing_history WHERE id_shop = ".$_GET['id_shop']);
			
			foreach($req3 as $key)
			{
				foreach($key as $id_campaign)
				{
					// Clean the table after 10 campaigns
					if ($id_campaign >10)
					{				
						$id_camp_old = ($id_campaign - 10);
						if ($min[0]['min'] < $id_camp_old)
						{
							Db::getInstance()->Execute("DELETE  FROM "._DB_PREFIX_."mailing_track WHERE id_campaign <='".$id_camp_old."' AND id_shop = '".$_GET['id_shop']."'");
							Db::getInstance()->Execute("DELETE  FROM "._DB_PREFIX_."mailing_history WHERE id_campaign <='".$id_camp_old."' AND id_shop = '".$_GET['id_shop']."'");
							Db::getInstance()->Execute("OPTIMIZE TABLE "._DB_PREFIX_."mailing_history");
							Db::getInstance()->Execute("OPTIMIZE TABLE "._DB_PREFIX_."mailing_track");
						}
					}
					
					list($name,$date_sent,$time_sent,$num_sent,$nbrs_enreg) = report($id_campaign,$_GET['id_shop']);
					$impact=($nbrs_enreg/$num_sent)*100;
					$ratio = number_format($impact, 2, ',', ' ');
				
					echo'
										<TR style="font-size:12px;">
											<TD>'.$id_campaign.'</TD>
											<TD>'.$name.'</TD>
											<TD>'.$date_sent.'</TD>	
											<TD>'.$time_sent.'</TD>
											<TD>'.$num_sent.'</TD>
											<TD>'.$nbrs_enreg.'</TD>
											<TD>'.$ratio.' %</TD>
										</TR>';	
					
				}
			}
			echo '						</TABLE>
									</div>
									<br/>
								<form><input type="button" class="button" value=" OK " onclick="window.close()">
								</form>
								<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
							</div>
						</body>
				</html>';
						
					
}
?>			
			