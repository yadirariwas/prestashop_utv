<?php
/* 
 * Gestion des Cron 
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2013
*/
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
include('../../../config/config.inc.php');
include('../functions.php');

	$filename = 'CRON';
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	if (@$_GET['key'] != $Key) 
		die('Bad request...Wrong key.');
	else {

			echo'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="../views/css/newsletteradmin.css"/>
				<title>'.trans('Manage your Cron tasks').'</title>
			</head>
			<body>
				<div class="newsfield" >
					<div style="float: right; color:green">Secure key is ok </div>
							<h1>'.trans('Your cron pending tasks').'</h1>
										<div style="margin-left: 12px">';
							
			// Lecture des fichiers contenus dans le répertoire
			$rep = dirname(__FILE__);
			$dir = opendir($rep);
				while (false !==($file = readdir($dir)))
				{
					if($file != "." && $file != ".."&& $file != "index.php"&& $file != "list.php"&& $file != "supp.php")
					{	
						// Insertion du nom du fichier dans le tableau nommé tb
						$tb[] = "$file";
					}
				}

			// Fin de la recherche dans le fichier
			clearstatcache();

			// Tri du tableau par ordre alphabétique
			if (!isset($tb)) {
				$fail = trans('Your cron directory is empty').'!';
				echo $fail.'<br/><br/>
					<form><input type="button" class="button" value=" OK " onclick="window.close()"></form>
					<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div></div></div>';
				return('error!');}
				asort($tb);

			// Affichage du tableau ligne par ligne
				reset($tb);
				while(list($key,$val) = each($tb)){ 
					$val = str_replace("'","_", $val);
					$url=  '
					<TABLE><TR>
										<TD WIDTH=120px >&nbsp;&nbsp;'.$val.'&nbsp;&nbsp;
										</TD>
										<TD WIDTH=10px><a href="#" onclick="if(confirm(\''.trans('Are you sure to delete this cron task').'? ('.$val.')\')) document.location.href=\'supp.php?key='.$Key.'&file='.$val.'\'"><img src="../../../img/admin/delete.gif" border="0" alt="Delete" title="'.trans('Delete this task').'" width="16px"></a>
										</TABLE>';
					echo $url;
				}	
				echo '					<br/>
											<form><input type="button" class="button" value=" OK " onclick="window.close()"></form>
											<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
										</div>
								</div>
							</body>
					</html>';
		}
?>