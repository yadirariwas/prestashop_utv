<?php

	// Init
	$sql = array();
	
	$sql[] = "DROP TABLE IF EXISTS `"._DB_PREFIX_."mailing_track`"; 		 
	$sql[] = "DROP TABLE IF EXISTS `"._DB_PREFIX_."mailing_history`"; 
	$sql[] = "DROP TABLE IF EXISTS `"._DB_PREFIX_."mailing_sent`"; 
	$sql[] = "DROP TABLE IF EXISTS `"._DB_PREFIX_."mailing_import`"; 
	$sql[] = "DROP TABLE IF EXISTS `temp`"; 