<?php
/**
* CSV Export Orders v1.0
* @author kik-off.com <support@kik-off.com>
* @file csvexportorders.php
**/

if (!defined('_PS_VERSION_'))
	exit;

class CsvExportOrders extends Module
{
	private $_html = '';
	private $_error = '';

	public function __construct()
	{
		$this->name = 'csvexportorders';
		$this->tab = 'administration';
		$this->version = '1.0';
		$this->author = 'kik-off.com';
		
		parent::__construct();

		$this->bootstrap = true;
		$this->need_instance = 0;
		$this->meta_title = $this->l('CSV Export Orders');
		$this->displayName = $this->l('CSV Export Orders');
		$this->description = $this->l('Export orders in a csv file.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if (!parent::install())
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

	public function getContent()
	{
		$this->_html = '
		<style type="text/css">
		    #customer_result {
    		    background: none repeat scroll 0 0 white;
    		    border-left: 1px solid #ccc;
				border-right: 1px solid #ccc;
				border-bottom: 1px solid #ccc;
				border-radius: 3px;
				box-shadow: 0 2px 5px #ccc;
				-moz-border-radius: 3px;
				-webkit-border-radius: 3px;
    		    list-style: none outside none;
    		    max-height: 150px;
    		    overflow-x: hidden;
    		    overflow-y: scroll;
    		    padding: 0;
				margin: 0;
    		    position: absolute;
    		    z-index: 10;
		    }
			#customer_result li {
			    display: block;
				widthd: 100%;
				border-bottom: 1px solid #ededed;
			}
			#customer_result li:last-child {
				border-bottom: none;
			}
			#customer_result li a,
			#customer_result li span {
			    display: block;
				padding: 4px 8px;
			}
			#customer_result li a:hover {
			    text-decoration: none;
			}
		</style>
		<script type="text/javascript">
		    $(document).ready(function(){
				$("a.list-toolbar-btn").not("#desc-csvexportorders-delete").attr("href", "'.
				    $this->context->link->getAdminLink('AdminModules', false)
					.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'");
			});
			
			$(document).on("keyup", "#customer", function(e) {
				e.preventDefault();
				if($(this).val() == 0)
				{
					$("#id_customer").val("");
					if($("#customer_result").length > 0)
						$("#customer_result").remove();
				}
				else { searchCustomers(); }
            });
			
			function searchCustomers()
	        {
				$.ajax({
			        type:"POST",
			        url : "'.$this->context->link->getAdminLink('AdminModules', true).'&configure=csvexportorders&tab_module=administration&module_name=csvexportorders",
			        async: true,
			        dataType: "json",
			        data : {
						controller: "AdminModules",
						module_name: "csvexportorders",
						ajax: "1",
				        action: "findCustomers",
				        find_customer: $("#customer").val()
					},
			        success : function(data)
			        {
						if(data.found)
				        {
    						var customer_html = "";
						
						    if($("#customer_result").length == 0)
						       $("#customer").after("<ul id=\'customer_result\' class=\'fixed-width-xxl\'></ul>");

							$.each(data.customers, function() {
								customer_html += "<li><a class=\'customer\' href=\'#\' rel=\'" + this.id_customer + "\'>" + this.firstname + " " + this.lastname + "</a></li>";
							});

						    $("#customer_result").html(customer_html);
							
						    $("a.customer").click(function(){
				                var name = $(this).text();
								var rel = $(this).attr("rel");
					            $("#customer").val(name);
								$("#id_customer").val(rel);
					            $("#customer_result").remove();
				            });
						}
						else
						{
						    var noResults = "'.$this->l('No results').'";
							if($("#customer_result").length == 0)
							    $("#customer").after("<ul id=\'customer_result\' class=\'fixed-width-xxl\'></ul>");
							
							$("#customer_result").html("<li><span>" + noResults + "</span></li>");
							$("#id_customer").val("");
						}
				    }
		        });
	        }
		</script>';
		
		if (Tools::isSubmit('csvexportordersFormSubmit'))
		{
			$reference = Tools::getValue('reference');
			$id_customer = Tools::getValue('id_customer');
			$order_state = Tools::getValue('order_state');
			$group = Tools::getValue('group');
			$from = Tools::getValue('from');
			$to = Tools::getValue('to');

			if (!$this->_getOrdersDetails($id_customer, $order_state, $group, $from, $to, $reference))
			    $this->_error .= $this->displayError($this->l('Cannot generate csv file.'));
		}
		else if (Tools::isSubmit('deletecsvexportorders') && Tools::getValue('file'))
		{
		    $file_name = Tools::getValue('file');

			if (!$this->deleteCsvFile($file_name))
			    $this->_error .= $this->displayError($this->l('Cannot delete file.'));
			else
			    $this->_html .= $this->displayConfirmation($this->l('File deleted succefully.'));
		}
		else if (Tools::isSubmit('deleteallcsvexportorders'))
		{
		    $file_name = Tools::getValue('file');

			if (!$this->deleteCsvFiles())
			    $this->_error .= $this->displayError($this->l('Cannot delete files.'));
			else
			    $this->_html .= $this->displayConfirmation($this->l('Files deleted succefully.'));
		}
		else if (Tools::isSubmit('downloadcsvexportorders'))
		{
		    $file_name = Tools::getValue('file');
			
			if (!$this->downloadCsvFile($file_name))
			    $this->_error .= $this->displayError($this->l('Cannot download file.'));
		}
		else if (Tools::isSubmit('submitBulkdeletecsvexportorders'))
		{
		    $files_name = Tools::getValue('csvexportordersBox');

			if (!$this->deleteCsvFile($files_name))
			    $this->_error .= $this->displayError($this->l('Cannot delete files.'));
			else
			    $this->_html .= $this->displayConfirmation($this->l('Files deleted succefully.'));
		}
		if (Tools::getValue('ajax') == 1 && Tools::getValue('action') == 'findCustomers')
		{
			$this->ajaxProcessSearchCustomers();
		}
		
		if(isset($this->_error) && $this->_error)
		    $this->_html .= $this->_error;

		$helper = $this->initForm();

		$helper->fields_value['reference'] = '';
		$helper->fields_value['customer'] = '';
		$helper->fields_value['id_customer'] = '';
		$helper->fields_value['order_state'] = 0;
		$helper->fields_value['group'] = 0;
		$helper->fields_value['from'] = '';
		$helper->fields_value['to'] = '';

		$this->_html .= $helper->generateForm($this->fields_form);

		$this->_html .= $this->renderList();
		
		$this->displayKikoffField();

		return $this->_html;
	}
	
	public function displayKikoffField()
	{	
		$helper_list = New HelperList();
		@$helper->token = Tools::getAdminTokenLite('AdminOrders');
		$this->_html .= '
		<style type="text/css">#imprint {width: 100%; text-align: right;}#imprint-logo {float: left;} #imprint img {vertical-align: top;}</style>
		<div id="imprint" class="panel">
			<span id="imprint-logo"><a href="http://kik-off.com" target="_blank" title="http://kik-off.com"><img src="'.$this->_path.'logo.gif" alt="" /></a> '.$this->displayName.' v'.$this->version.'</span>
			<span><a href="index.php?controller=AdminOrders&token='.$helper->token.'" title="http://kik-off.com">Regresar</a></span>
		</div>';

		return $this->_html;
	}

	protected function initForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$order_states = OrderState::getOrderStates($this->context->language->id);
		$order_state_options = array();
		$order_state_options[0] = array('id_option' => 0, 'name' => $this->l('Select a order state'));
		foreach($order_states as $order_state)
		    $order_state_options[] = array(
			    'id_option' => $order_state['id_order_state'],
				'name' => $order_state['name']
			);

		$groups = Group::getGroups($this->context->language->id);
		$groups_options = array();
		$groups_options[0] = array('id_option' => 0, 'name' => $this->l('Select a group'));
		foreach($groups as $group)
		    $groups_options[] = array(
			    'id_option' => $group['id_group'],
				'name' => $group['name']
			);

		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->displayName
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Reference'),
					'name' => 'reference',
					'class' => 'fixed-width-xxl'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Customer'),
					'name' => 'customer',
					'autocomplete' => false,
					'id' => 'customer',
					'class' => 'fixed-width-xxl',
					'hint' => $this->l('Can be id customer, name or email.')
				),
				array(
					'type' => 'hidden',
					'name' => 'id_customer',
					'id' => 'id_customer',
				),
				array(
                    'type' => 'select',
                    'label' => $this->l('Order state'),
                    'id' => 'order_state',
					'name' => 'order_state',
					'class' => 'fixed-width-xxl',
					'options' => array(
                        'query' => $order_state_options,
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                ),
				array(
					'type' => 'select',
                    'label' => $this->l('Group'),
                    'id' => 'group',
					'name' => 'group',
					'class' => 'fixed-width-xxl',
					'options' => array(
                        'query' => $groups_options,
                        'id' => 'id_option',
                        'name' => 'name'
                    ),
                ),
				array(
                    'type' => 'date',
                    'label' => $this->l('From'),
                    'id' => 'from',
					'name' => 'from'
                ),
				array(
                    'type' => 'date',
                    'label' => $this->l('To'),
                    'id' => 'to',
					'name' => 'to'
                ),
			),
			'submit' => array(
                'title' => $this->l('Export'),
                'class' => 'btn btn-default pull-right',
				'icon' => 'process-icon-export'
            )
		);

		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name;
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->toolbar_scroll = true;
		$helper->title = $this->displayName;
		$helper->submit_action = $this->name.'FormSubmit';

		return $helper;
	}

	protected function renderList()
	{
		$fields_list = array(
			'file' => array(
				'title' => $this->l('File'),
				'search' => false,
			),
			'size' => array(
				'title' => $this->l('Size'),
				'search' => false,
			)
		);

		$helper_list = New HelperList();
		$helper_list->module = $this;
		$helper_list->title = $this->l('CSV generated');
		$helper_list->shopLinkType = '';
		$helper_list->no_link = true;
		$helper_list->show_toolbar = true;
		$helper_list->simple_header = false;
		$helper_list->identifier = 'file';
		$helper_list->table = 'csvexportorders';
		$helper_list->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name;
		$helper_list->token = Tools::getAdminTokenLite('AdminModules');
		
		$helper_list->toolbar_btn = array(
			'delete' => array(
				'href' => $helper_list->currentIndex.'&deleteallcsvexportorders&token='.$helper_list->token,
				'desc' => $this->l('Delete all'),
				'icon' => 'icon-trash',
				'confirm' => $this->l('Delete all items?'),
			)
		);
		
		$files = $this->getCsvFiles();
		
		if ($files)
		{
		    $helper_list->actions = array('download', 'delete');
		    $helper_list->force_show_bulk_actions = true;
		
		    $helper_list->bulk_actions = array(
			    'delete' => array(
				    'text' => $this->l('Delete selected'),
				    'icon' => 'icon-trash',
				    'confirm' => $this->l('Delete selected items?'),
			    )
		    );
		
		    $helper_list->specificConfirmDelete = false;
		}

		$helper_list->listTotal = count($files);

		$page = ($page = Tools::getValue('submitFilter'.$helper_list->table)) ? $page : 1;
		$pagination = ($pagination = Tools::getValue($helper_list->table.'_pagination')) ? $pagination : 20;
		$files = $this->paginateCsvFiles($files, $page, $pagination);

		return $helper_list->generateList($files, $fields_list);
	}

	protected function _getOrdersDetails($id_customer = null, $order_state = null, $group = null, $from = null, $to = null, $reference = null)
	{	
		if (isset($from) && $from && !$to)
			return $this->_error .= $this->displayError($this->l('To field is required.'));
        if (isset($to) && $to && !$from)
			return $this->_error .= $this->displayError($this->l('From field is required.'));
		
		/*$sql = 'SELECT o.`id_order` FROM `'._DB_PREFIX_.'orders` o
			'.($group > 0 ? 'LEFT JOIN `'._DB_PREFIX_.'customer_group` cg ON (cg.`id_group` = '.(int)$group.')' : '').'
			WHERE `id_shop` = '.(int)$this->context->shop->id.'
			'.($id_customer ? ' AND o.`id_customer` = '.(int)$id_customer : '').'
			'.($order_state > 0 ? ' AND o.`current_state` = '.(int)$order_state : '').'
			'.($group > 0 ? ' AND cg.`id_group` = '.(int)$group : '').'
			'.($from && $to ? ' AND o.`date_add` BETWEEN "'.$from.' 00:00:00 " AND "'.$to.' 23:59:59"' : '').' OR o.current_state = 4
			'.($reference ? ' AND o.`reference` LIKE "%'.$reference.'%"' : '').'
		';
		
		echo $sql;
		exit;*/
		
		$results = Db::getInstance()->executeS('
		    SELECT o.`id_order` FROM `'._DB_PREFIX_.'orders` o
			'.($group > 0 ? 'LEFT JOIN `'._DB_PREFIX_.'customer_group` cg ON (cg.`id_group` = '.(int)$group.')' : '').'
			WHERE `id_shop` = '.(int)$this->context->shop->id.'
			'.($id_customer ? ' AND o.`id_customer` = '.(int)$id_customer : '').'
			'.($order_state > 0 ? ' AND o.`current_state` = '.(int)$order_state : '').'
			'.($group > 0 ? ' AND cg.`id_group` = '.(int)$group : '').'
			'.($from && $to ? ' AND o.`date_add` BETWEEN "'.$from.' 00:00:00 " AND "'.$to.' 23:59:59"' : '').'
			'.($reference ? ' AND o.`reference` LIKE "%'.$reference.'%"' : '').'
		');

		if (!$results)
			return $this->_error .= $this->displayError($this->l('No records found.'));

		$orders = array();

		foreach($results as $i => $result)
        {
            $order = new Order((int)$result['id_order']);
			$customer = new Customer($order->id_customer);
			$phones = $this->getCustomerPhones($order->id_customer);

            $orders[$i]['id_order'] = $order->id;
			$orders[$i]['id_cart'] = $order->id_cart;
            $orders[$i]['reference'] = $order->reference;
			$orders[$i]['id_customer'] = $order->id_customer;
			$orders[$i]['firstname'] = $customer->firstname;
			$orders[$i]['lastname'] = $customer->lastname;
			$orders[$i]['email'] = $customer->email;
			$orders[$i]['phone'] = $phones[0]['phone'];
			$orders[$i]['phone_mobile'] = $phones[0]['phone_mobile'];
			$orders[$i]['valid'] = ($order->valid == 1 ? $this->l('Yes') : $this->l('No'));
			$orders[$i]['invoice_date'] = $order->invoice_date;
			$orders[$i]['date_add'] = $order->date_add;
			$orders[$i]['current_state'] = $this->getOrderStateNameById($order->current_state);
			$orders[$i]['carrier'] = $this->getCarrierNameById($order->id_carrier);

			$products = $order->getProductsDetail();

			foreach($products as $product)
			{
			    $order_products[$i]['products_array'][] = $product['product_name'].', '.$this->l('Quantity').' : '.$product['product_quantity'];
			}

			$orders[$i]['products'] = implode(', ', $order_products[$i]['products_array']);
			$orders[$i]['total'] = Tools::displayPrice($order->total_paid_tax_incl, (int)$order->id_currency);
        }

		$this->processCreateCsv($orders);
	}
	
	public function displayDownloadLink($token = null, $id, $name = null)
	{		
		$this->smarty->assign(array(
			'id' => $id,
			'href' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&file='.$id.'&downloadcsvexportorders&token='.($token != null ? $token : $this->token),
			'token' => $token != null ? $token : $this->token,
			'action' => $this->l('Download')
		));

		return $this->display(__FILE__, 'views/templates/admin/list_action_download.tpl');
	}

	protected function getCustomerPhones($id_customer)
	{
	    $result = Db::getInstance()->executeS('
		    SELECT `phone`, `phone_mobile` FROM `'._DB_PREFIX_.'address`
			WHERE `id_customer` = '.(int)$id_customer
		);

		if($result)
		    return $result;
		return false;
	}

	protected function getCarrierNameById($id_carrier)
	{
	    $result = Db::getInstance()->executeS('
		    SELECT `name` FROM `'._DB_PREFIX_.'carrier`
			WHERE `id_carrier` = '.(int)$id_carrier
		);

		if($result)
		    return $result[0]['name'];
		return false;
	}

	protected function getOrderStateNameById($id_order_state)
	{
	    $result = Db::getInstance()->executeS('
		    SELECT `name` FROM `'._DB_PREFIX_.'order_state_lang`
			WHERE `id_order_state` = '.(int)$id_order_state.' AND `id_lang` = '.(int)$this->context->language->id
		);

		if($result)
		    return $result[0]['name'];
		return false;
	}

	protected function processCreateCsv($orders, $text_delimiter = ';')
	{
		if (ob_get_level() && ob_get_length() > 0)
			ob_clean();

		if (!$orders)
		    exit;

		$file_name = $this->name.'_'.date('Y-m-d_His').'.csv';
		$file = _PS_BASE_URL_.__PS_BASE_URI__.'modules/csvexportorders/csv/'.$file_name;

		$headers = array(
		    $this->l('ID Order'),
			$this->l('Referencia Banwire'),
			$this->l('Reference'),
			$this->l('ID Customer'),
			$this->l('Firstname'),
			$this->l('Lastname'),
			$this->l('Email'),
			$this->l('Phone'),
			$this->l('Mobile phone'),
			$this->l('Valid'),
			$this->l('Invoice date'),
			$this->l('Date add'),
			$this->l('Order state'),
			$this->l('Carrier'),
			$this->l('Products'),
			$this->l('Total')
		);

		$export = array_merge(array($headers), $orders);

		$fd = fopen($this->getLocalPath().'csv/'.$file_name, 'w+');

		foreach ($export as $data)
		{
			$lines = implode($text_delimiter, $data);
			$lines .= "\n";
			fwrite($fd, $lines, 4096);
		}

		fclose($fd);
		Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&file='.$file_name.'&downloadcsvexportorders');
	}

	protected function downloadCsvFile($file_name)
	{
		$file_uri = $this->getLocalPath().'csv/'.$file_name;

		header('Content-type: text/csv');
		header('Content-Type: application/force-download; charset=UTF-8');
		header('Cache-Control: no-store, no-cache');
		header('Content-disposition: attachment; filename='.$file_name);
		readfile($file_uri);
		exit;
	}

	private function getCsvFiles()
	{
		$files = array_reverse(glob($this->getLocalPath().'csv/*.csv', GLOB_BRACE));

		if (!$files)
		    return array();

		$datas = array();

		foreach ($files as $i => $file)
		{
		    $datas[$i]['file'] = str_replace($this->getLocalPath().'csv/', '', $file);
			$datas[$i]['size'] = round((filesize($file) / 1024), 2).' MB';
		}

		return $datas;
	}

	private function paginateCsvFiles($files, $page = 1, $pagination = 20)
	{
		if (!$files)
		    return array();

		if(count($files) > $pagination)
			$files = array_slice($files, $pagination * ($page - 1), $pagination);

		return $files;
	}

	private function deleteCsvFiles()
	{
		$files = glob($this->getLocalPath().'csv/*.csv', GLOB_BRACE);

		if (!$files)
		    return false;

		foreach ($files as $file)
		    @unlink($file);

		return true;
	}
	
	private function deleteCsvFile($files)
	{
		if (is_array($files))
		{
		    foreach($files as $file)
			{
			    $file_uri = $this->getLocalPath().'csv/'.$file;
				@unlink($file_uri);
			}
			return true;
		}
		else
		{
		    $file_uri = $this->getLocalPath().'csv/'.$files;

		    if (!file_exists($file_uri))
		        return false;
		    else
		        unlink($file_uri);
		        return true;
		}
	}
	
	public function ajaxProcessFindCustomers()
	{
		$searches = explode(' ', Tools::getValue('find_customer'));
		$customers = array();
		$searches = array_unique($searches);
		foreach ($searches as $search)
		{
			if (!empty($search) && is_numeric($search) && $results = $this->findById((int)$search))
			{
			    foreach ($results as $result)
					$customers[$result['id_customer']] = $result;
			}
			else if (!empty($search) && !is_numeric($search) && $results = $this->findByNameOrEmail($search))
			{
				foreach ($results as $result)
					$customers[$result['id_customer']] = $result;
			}
		}

		if (count($customers))
			$to_return = array(
				'customers' => $customers,
				'found' => true
			);
		else
			$to_return = array('found' => false);

		die(Tools::jsonEncode($to_return));
	}
	
	private function findByNameOrEmail($query)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`
				WHERE (
						`email` LIKE \'%'.pSQL($query).'%\'
						OR `lastname` LIKE \'%'.pSQL($query).'%\'
						OR `firstname` LIKE \'%'.pSQL($query).'%\'
					)'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);
		return Db::getInstance()->executeS($sql);
	}
	
	private function findById($query)
	{
		$sql = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`
				WHERE `id_customer` LIKE \'%'.pSQL((int)$query).'%\'
					'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);
		return Db::getInstance()->executeS($sql);
	}
}