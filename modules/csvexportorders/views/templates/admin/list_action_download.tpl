{*
* CSV Export Orders v1.0
* @author kik-off.com <support@kik-off.com>
* @file list_action_download.tpl
*}
<a href="{$href|escape:'html':'UTF-8'}" title="{$action|escape:'html':'UTF-8'}" >
	<i class="icon-cloud-download"></i> {$action|escape:'html':'UTF-8'}
</a>