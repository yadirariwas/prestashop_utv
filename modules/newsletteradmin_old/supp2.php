<?php
/* 
 * Suppression des adresses mail de la liste d'envoi
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2014
*/
require_once(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');	
session_start();

	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;
		
	$ref = $_GET['key'];
	
	array_splice($_SESSION['newsletter']['finalList'], $ref, 1);
	 header('Content-type: text/html; charset= utf8'); 
	if (empty($_SESSION['newsletter']['finalList']))
		echo '<meta HTTP-EQUIV="Refresh" content="0;">';
		
	else 
	{
		echo '<b>'.count($_SESSION['newsletter']['finalList']).'</b></br></br><table id="table1">';
		foreach($_SESSION['newsletter']['finalList'] as $keyList => $maillist)
		//while(list($keyList,$maillist) = each($_SESSION['newsletter']['finalList']))
			{
				echo '	
					<TR>
					<TD style=" width=300px;border-width:1px;border-style:solid;border-color:#CCCCCC;">&nbsp;',$maillist['category'],'</TD>
					<TD style=" width=250px;border-width:1px;border-style:solid;border-color:#CCCCCC;">&nbsp;N°',$keyList,':&nbsp;',$maillist['email'],'&nbsp;&nbsp;
					</TD>
					<TD WIDTH=10px>
					<a href="#" onclick="ajaxExec(\'../modules/newsletteradmin/supp2.php?key=',$keyList,'\',\'resultat\');"><img src="../img/admin/delete.gif" border="0" alt="Delete" title="Delete this entry" width="16px"></a>
					</TR>
					';
			}
		echo ' </table>';
	}

?>