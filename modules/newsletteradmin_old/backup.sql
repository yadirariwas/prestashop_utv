-- Eolia NewsletterAdmin SQL DataBase Dump
-- Server: localhost -- Database: canalrok_roku --
-- Dump of NewsletterAdmin tables on 03-Jul-2015


--
-- Table structure of ps_mailing_history
--

CREATE TABLE IF NOT EXISTS `ps_mailing_history` (
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_campaign` int(5) NOT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(10) NOT NULL,
  `time` varchar(8) NOT NULL,
  `num_sent` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Insertions in the table ps_mailing_history
--
INSERT INTO `ps_mailing_history` (`id_shop`, `id_campaign`, `subject`, `date`, `time`, `num_sent`) VALUES 
(1, 1, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '12-53-44', 3),
(1, 2, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '12-57-19', 1),
(1, 3, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '13-03-03', 3),
(1, 4, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '13-17-04', 2),
(1, 5, 'Boletin_de_Miercoles_01_Julio_2015', '01-07-2015', '11-32-55', 2),
(1, 6, 'Boletin_de_Miercoles_01_Julio_2015', '01-07-2015', '11-41-21', 4);


--
-- Table structure of ps_mailing_import
--

CREATE TABLE IF NOT EXISTS `ps_mailing_import` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `email` varchar(128) NOT NULL DEFAULT '',
  `lastname` varchar(32) DEFAULT '',
  `firstname` varchar(32) DEFAULT '',
  `group` varchar(32) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `id_shop` (`id_shop`,`email`,`group`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


--
-- Insertions in the table ps_mailing_import
--
INSERT INTO `ps_mailing_import` (`ID`, `id_shop`, `email`, `lastname`, `firstname`, `group`) VALUES 
(1, 1, 'angel.servin@gmail.com', 'Angel', 'Servin', 'temp'),
(2, 1, 'sebastian.carry@gmail.com', 'Pablo', 'Carrito', 'temp'),
(3, 1, 'angel@adimpactomexico.com', 'Angel', 'Servin', 'temp'),
(4, 1, 'pablo@adimpactomexico.com', 'Pablo', 'Carrito', 'temp');


--
-- Table structure of ps_mailing_sent
--

CREATE TABLE IF NOT EXISTS `ps_mailing_sent` (
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_campaign` varchar(5) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `date` varchar(20) NOT NULL,
  `dateReceived` varchar(20) NOT NULL,
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Insertions in the table ps_mailing_sent
--
INSERT INTO `ps_mailing_sent` (`id_shop`, `id_campaign`, `email`, `date`, `dateReceived`) VALUES 
(1, 1, 'angel@adimpactomexico.com', '23-06-2015 12:53:36', ''),
(1, 1, 'alejandro@adimpactomexico.com', '23-06-2015 12:53:40', ''),
(1, 1, 'ea_richard@live.com.mx', '23-06-2015 12:53:44', ''),
(1, 2, 'angel.servin@gmail.com', '23-06-2015 12:57:19', ''),
(1, 3, 'angel.servin@gmail.com', '23-06-2015 13:02:58', '23-06-2015 13:03:10'),
(1, 3, 'alejandro@adimpactomexico.com', '23-06-2015 13:03:00', ''),
(1, 3, 'ea_richard@live.com.mx', '23-06-2015 13:03:03', ''),
(1, 4, 'angel.servin@gmail.com', '23-06-2015 13:17:01', '23-06-2015 13:17:11'),
(1, 4, 'sebatian.carry@gmail.com', '23-06-2015 13:17:04', ''),
(1, 5, 'angel.servin@gmail.com', '01-07-2015 11:32:51', '01-07-2015 11:33:01'),
(1, 5, 'sebatian.carry@gmail.com', '01-07-2015 11:32:55', ''),
(1, 6, 'angel.servin@gmail.com', '01-07-2015 11:41:07', ''),
(1, 6, 'sebastian.carry@gmail.com', '01-07-2015 11:41:11', ''),
(1, 6, 'angel@adimpactomexico.com', '01-07-2015 11:41:16', ''),
(1, 6, 'pablo@adimpactomexico.com', '01-07-2015 11:41:21', '01-07-2015 12:27:46');


--
-- Table structure of ps_mailing_track
--

CREATE TABLE IF NOT EXISTS `ps_mailing_track` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `ipAddress` varchar(12) NOT NULL DEFAULT '',
  `id_campaign` varchar(5) NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '0',
  `postDate` varchar(10) NOT NULL DEFAULT '',
  `postTime` varchar(8) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


--
-- Insertions in the table ps_mailing_track
--
INSERT INTO `ps_mailing_track` (`ID`, `id_shop`, `ipAddress`, `id_campaign`, `subject`, `postDate`, `postTime`, `email`) VALUES 
(1, 1, '66.102.7.130', 3, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '13-03-10', 'angel.servin@gmail.com'),
(2, 1, '66.102.7.130', 4, 'Boletin_de_Martes_23_Junio_2015', '23-06-2015', '13-17-11', 'angel.servin@gmail.com'),
(3, 1, '66.102.7.130', 5, 'Boletin_de_Miercoles_01_Julio_2015', '01-07-2015', '11-33-01', 'angel.servin@gmail.com'),
(4, 1, '66.102.7.130', 6, 'Boletin_de_Miercoles_01_Julio_2015', '01-07-2015', '12-27-46', 'pablo@adimpactomexico.com');


