<?php
/* 
 * Gestion des backups 
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2013
*/
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include(dirname(__FILE__).'/../config/config.inc.php');
include(dirname(__FILE__).'/../modules/newsletteradmin/functions.php');

	$filename = 'BACKUPS';
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	if (@$_GET['key'] != $Key) 
		die('Bad request...Wrong key.');

	else {
			echo'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="../modules/newsletteradmin/views/css/newsletteradmin.css"/>
				<title>'.trans('Manage your backups').'</title>
			</head>
			<body>
				<div class="newsfield" >
					<div style="float: right; color:green">Secure key is ok </div>
									<h1>'.trans('Your backups').'</h1>
										<div style="margin-left: 12px">';
									
			// Lecture des fichiers contenus dans le r�pertoire
			$rep = dirname(__FILE__);
			$dir = opendir($rep);
				while (false !==($file = readdir($dir)))
				{
					if($file != "." && $file != ".."&& $file != "index.php"&& $file != "list.php"&& $file != "index.html"&& $file != "supp.php")
					{	
						// Insertion du nom du fichier dans le tableau nomm� tb
						$tb[] = "$file";
					}
				}

			// Fin de la recherche dans le fichier
			clearstatcache();

			// Tri du tableau par ordre alphab�tique
			if (!isset($tb)) 
			{
				echo '<center>'.trans('Your backups directory is empty').'!
				<br/><br/><form><input type="button" class="button" value=" OK " onclick="window.close()"></form></center>
				<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div></div></div>';
			}
			else 
			{
				asort($tb);

				// Affichage du tableau ligne par ligne
				reset($tb);
				echo '<br/>';
				while(list($key,$val) = each($tb))
				{ 
					$val = str_replace("'","_", $val);
					$url=  '<a href="#" onclick=" document.location.href=\''.$val.'\'"><img src="../img/admin/details.gif" alt="View" title="'.trans('View this newsletter').'" border="0" width="16px"><a href="#" onclick="if(confirm(\''.trans('Are you sure to delete this newsletter').'? ('.$val.')\')) document.location.href=\'supp.php?key='.$Key.'&file='.$val.'\'"><img src="../img/admin/delete.gif" alt="Delete" border="0" title="'.trans('Delete this newsletter').'" width="16px"></a>&nbsp;&nbsp;'.$val.'<br/>';
					echo $url;
				}	
				echo '						<br/>
											<form><input type="button" class="button" value=" OK " onclick="window.close()"></form>
										<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
									</div>
								</div>
							</body>
					</html>';
			}
		}
?>