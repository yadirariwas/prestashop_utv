tinyMCE.addI18n('en.insertproduct_dlg',{
	desc : 'Import products',
	title : 'Import products',
	search_desc : 'Search', 
	selection_desc : 'Selection',
	style_desc : 'Style',
	search_panel_form_category_label : 'Category',
	search_panel_form_currency_label : 'Currency',
	search_panel_form_keyword_label : 'Keyword',
	search_panel_form_add_to_cart_label_1 : '"Add to cart"',
	search_panel_form_add_to_cart_label_2 : 'link',
	new_search : 'New search',
	search : 'Search', 
	insert : 'Insert', 
	alreadySelected : 'This product is already selected', 
	noneSelected : 'No product is selected', 
	notfound : 'No product found in this scategory' 
});
