tinyMCE.addI18n('br.insertproduct_dlg',{
	desc : 'Importar produtos',
	title : 'Imporart produtos',
	search_desc : 'Busca por', 
	selection_desc : 'Seleção',
	style_desc : 'Estilo',
	search_panel_form_category_label : 'Categoria',
	search_panel_form_currency_label : 'Moeda',
	search_panel_form_keyword_label : 'Keyword',
	search_panel_form_add_to_cart_label_1 : '"Ver"',
	search_panel_form_add_to_cart_label_2 : 'link',
	new_search : 'Nova pesquisa',
	search : 'Busca por', 
	insert : 'Inserir', 
	alreadySelected : 'Este produto já está selecionado', 
	noneSelected : 'Nenhum produto é seleccionado', 
	notfound : 'Nenhum produto encontrado nesta categoria' 
});
