<?php
ini_set('display_errors', 0);
error_reporting(0);
include_once(dirname(__FILE__).'/../../../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../../../init.php');
// Creating a ghost context
$context = Context::getContext();
$valid_admin = Employee::getEmployeesByProfile(1);
$context->employee = $valid_admin[0]['id_employee'];
//create controller type only to avoid Tools error...
$context->controller = new stdClass;
$context->controller->controller_type = 'back';
if (Shop::isFeatureActive())
	Shop::setContext(Shop::CONTEXT_SHOP);
$id_lang =  $context->language->id;
$keyword = Tools::getValue('keyword', false);
$id_category = Tools::getValue('id_category', false);
$keyword = Tools::getValue('keyword', false);
$add_to_cart = Tools::getValue('search_panel_form_add_to_cart', false);
$category = new Category((int)$id_category, $id_lang, 2);
$products_list = Product::getProducts($id_lang, 0, 100, 'name', 'asc', (int)$id_category, true, $context);
$items = Product::getProductsProperties($id_lang, $products_list);
$products = array();
$currency = new Currency($context->currency->id);
$protocol = Configuration::get('PS_SSL_ENABLED') ? 'https' : 'http';
if ($items)
	foreach ($items as $item)
	{
		$link = new Link;
		$id_image = Db::getInstance()->getValue('SELECT im.id_image FROM ' . _DB_PREFIX_ . 'image as im 
												LEFT JOIN ' . _DB_PREFIX_ . 'image_shop as ims ON (ims.id_image = im.id_image)
												WHERE im.id_product = '.$item['id_product'].' AND ims.cover = 1 AND id_shop ='.$context->shop->id);
		$default = version_compare(_PS_VERSION_,'1.6','>=') ? 'cart_default' : 'small_default';
		$item['image_mini'] = $protocol.'://'.$link->getImageLink($item['link_rewrite'], $id_image, $default);
		$item['image_link'] = $protocol.'://'.$link->getImageLink($item['link_rewrite'], $id_image, 'home_default');
		$item['link'] = str_replace('_PS_BASE_URL_/', $protocol.'://'.$_SERVER['SERVER_NAME'].__PS_BASE_URI__, $item['link']);
		$item['price'] = Tools::displayPrice($item['price'], $currency, false, $context);
		$item['price_tax_exc'] = Tools::displayPrice($item['price_tax_exc'], $currency, false, $context);
		$products[] = $item ;
	}

if (file_exists(_PS_THEME_DIR_.'lang/'.Language::getIsoById($id_lang).'.php'))
	$translationsFile = _PS_THEME_DIR_.'lang/'.Language::getIsoById($id_lang).'.php';
elseif(file_exists(_PS_ALL_THEMES_DIR_.'prestashop/lang/'.Language::getIsoById($id_lang).'.php'))
	$translationsFile = _PS_ALL_THEMES_DIR_.'prestashop/lang/'.Language::getIsoById($id_lang).'.php';
else
	$translationsFile = '';

if ($translationsFile != '')
	@include_once($translationsFile);

function smartyTranslate_tinyMCE($params, &$smarty)
{
	global $_LANG, $id_lang;

	if (!isset($params['js'])) $params['js'] = 0;
	if (!isset($params['mod'])) $params['mod'] = false;

	$string = str_replace('\'', '\\\'', $params['s']);
	$key = $params['mod'].'_'.md5($string);

	return (is_array($_LANG) AND key_exists($key, $_LANG)) ? ($params['js'] ? addslashes($_LANG[$key]) : stripslashes($_LANG[$key])) : $params['s'];

}
$smarty->registerPlugin('function', 'l_tinyMCE', 'smartyTranslate_tinyMCE');
$smarty->assign(array(
        'products' => $products,
		'base_dir' => getShopUrl($context),
		'add_to_cart' => $add_to_cart
));

function getShopUrl($context)
{
	$ShopUrl = Db::getInstance()->getRow("
	SELECT
		domain,
		domain_ssl,
		physical_uri,
		virtual_uri
	FROM
		`"._DB_PREFIX_."shop_url`
	WHERE  id_shop = ".$context->shop->id); 
	if(Configuration::get('PS_SSL_ENABLED') || (!empty($_SERVER['HTTPS'])&& strtolower($_SERVER['HTTPS']) != 'off'))
		return 'https://'.$ShopUrl['domain_ssl'].$ShopUrl['physical_uri'].$ShopUrl['virtual_uri'];
	else
		return 'http://'.$ShopUrl['domain'].$ShopUrl['physical_uri'].$ShopUrl['virtual_uri'];
}

header ("content-type: text/xml");

$smarty->display(dirname(__FILE__).'/insertproduct-callback.tpl');

?>