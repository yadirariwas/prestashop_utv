<?php

/**
 * Envoi de Newsletter depuis le Back-Office Prestashop 
 * @category admin
 * @copyright eolia@eolia.o2switch.net
 * @author  Eolia  27/06/2015  >= PS 1.5 ONLY !
 * @version 5.7
 *
 */
 

if (!defined('_PS_VERSION_'))
	exit;
if( _PS_VERSION_ < '1.5.0.0') {
	echo '<span style="color: red;"><b>Fatal Error: </b>  This version of Newsletteradmin runs on Prestashop 1.5 and more only, please download the right version to your shop!</span>';
	exit;
	}

set_time_limit (0);
session_start() ;
global $filename;
require_once(_PS_ROOT_DIR_.'/modules/newsletteradmin/functions.php');
require_once(_PS_ROOT_DIR_.'/modules/newsletteradmin/html2text.php');
include(_PS_ROOT_DIR_.'/modules/newsletteradmin/classes/NewsMail.php');

class AdminNewsletterController extends AdminController
{
	public function __construct()
	{
		
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_SHOP);
			  
		parent::__construct();	
		 	
		$this->lang = false;
		$this->context = Context::getContext();
		$this->context->controller->addCSS(_MODULE_DIR_.'newsletteradmin/views/css/newsletteradmin.css');
		
	}

	public $_returnHTML = '';
	public $_html = '';
	
	public function initContent()
	{		
		$this->show_toolbar = false;
		$this->display = 'view';
		
		parent::initContent();	
	}
	
	public function renderView()
	{		
		if(strlen($this->_returnHTML) <= 0)	
			$content = $this->setForm();
		else
			$content = '';			
			
		return parent::renderView().$this->_returnHTML.$content;
	}
	
	public function l($string, $class = NULL, $addslashes = false, $htmlentities = true) 
	{
		return Translate::getModuleTranslation('newsletteradmin', $string, 'AdminNewsletter');
	}	

	public function setForm()
	{
		global $currentIndex, $cookie, $Key;

		if (Tools::getValue('send') != 1 && Tools::getValue('install') != 1)
		{		
			Configuration::updateGlobalValue('NEWSLETTER_KEY_CODE', Tools::getAdminTokenLite('AdminNewsletter'));
			$Key = Configuration::get('NEWSLETTER_KEY_CODE');
			$shop_server  = $_SERVER['HTTP_HOST'];
			$updateOk = Configuration::get('Admin_Newsletter_Last_Version')? Configuration::get('Admin_Newsletter_Last_Version'):0;
			$language = Language::getLanguage(intval(Configuration::get('PS_LANG_DEFAULT')));
			$iso = $this->context->language->iso_code;
			//count email list
			$countCustomersSub = $this->countCustomersSub();
			$countBirthCustomers = $this->countBirthCustomers();
			$countCustomers =$this->countCustomers();
			$countOptCustomers =$this->countOptCustomers();
			$countBlockSub = $this->countBlockSub();
			$countNoReadCustomers = $this->countNoReadCustomers();
			$countCsvCustomers = $this->countCsvCustomers();

			$tagtl = Tools::getAdminTokenLite('AdminModules');
			$tagt2 = Tools::getAdminTokenLite('AdminTranslations');
			$_SESSION['tagtl'] = $tagtl;
			$flag = Configuration::get('PS_LANG_DEFAULT'); 
			$help_link ='http://www.eolia.o2switch.net/newsletter/download/counter.php?f=MODULE%20%20NEWSLETTER%20par%20Eolia.pdf';
					if ($iso != 'fr') $help_link ='http://www.eolia.o2switch.net/newsletter/download/counter.php?f=MODULE%20%20NEWSLETTER%20by%20Eolia.pdf';

			//Verify if the module version is configured and up to date 
			$nwl_version = Configuration::getGlobalValue ('Admin_Newsletter_Version');//current version
			$last_update = Configuration::get('Admin_Newsletter_Last_Version');
			if($nwl_version == '0.0')
			{
				Tools::redirectAdmin('index.php?controller=AdminNewsletter&install=1&token='.Tools::getAdminTokenLite('AdminNewsletter'));	
			}
			else
			{ 
				if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP))
					$this->displayWarning('<span style="color: red;"><b>'.$this->l('Warning:').' </b> '.$this->l(' You have to select a shop before send a newsletter!').'</span>');
				else
				{
					$this->html =  '
						<div class="newsfield" >
								<h2 style="padding:10px 30px 0px 30px"><img src="../modules/newsletteradmin/logo.gif" style=" margin-bottom: 6px; ">&nbsp;&nbsp;NewsletterAdmin - V. '.$nwl_version. '&nbsp;&nbsp;
								<div style="float: right;padding:0px 40px">
									<a href="'.$help_link.'"><img src="../modules/newsletteradmin/img/help.gif" width="55" height="26" alt="help" title="'.$this->l('Help about this module').'"></a>
								</div></h2>
								
								<div style="float: right;padding:69px 10px 0px 30px">'.$this->l('Manage translations').': 
									<a href="index.php?controller=AdminTranslations&lang='.$iso.'&type=modules&token='.$tagt2.'" style="margin-left:5px"><img src="../img/l/'.$flag.'.jpg" alt="'.$iso.'" title="'.$iso.'"></a>
									<br/> '.$this->l('Translation attachments').': 
									<a href="../modules/newsletteradmin/translations.php?key='.$Key.'" style="margin-left:5px"><img src="../img/l/'.$flag.'.jpg" alt="'.$iso.'" title="'.$iso.'"></a>
									<br/> '.$this->l('Module reset').': 
									<a href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&install=1&token='.Tools::getAdminTokenLite('AdminNewsletter').'"><img src="../img/admin/cog.gif" alt="Reset" title="'.$this->l('Reset').'"></a>
								</div>';
				
					if(!$last_update || $last_update > $nwl_version)
					{	
						try
						{
							$feed = 'http://www.eolia.o2switch.net/newsletter/xml/version2.xml';
							$ch = curl_init($feed);
							curl_setopt($ch, CURLOPT_URL, $feed);
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch, CURLOPT_USERAGENT ,"Mozilla 5.0 for ".$_GET['controller']);
							$data = curl_exec($ch);
							curl_close($ch);
						}
						catch(Exception $e)
						{
							$this->html .= $e->message;
						}	
						if (strlen(utf8_decode($data)) > 540) 
						{
							$xml = new SimpleXMLElement($data);
							$num = $xml->version->num;
							$name = $xml->version->name;
							$link = $xml->download->link;
							$desc = $xml->version->desc->$iso;				
							if ($nwl_version < $num)
							{
								Configuration::updateGlobalValue('Newsletter_Token', $tagtl);
								Configuration::updateGlobalValue('tokenNews', Tools::getAdminTokenLite('AdminNewsletter'));
								Configuration::updateGlobalValue('Admin_Index', $_SERVER["PHP_SELF"]);
								Configuration::updateGlobalValue('Admin_Newsletter_Last_Version', $num);
				
								$this->displayWarning('<center><font color="red"><b>'.$this->l('Warning:').' </b> '.$this->l('Your version is out of date ').'!</font><br/> '.$this->l('Click here to auto-upgrade your module').': <br/><a href="../modules/newsletteradmin/upgrade.php?controller='.Tools::getValue('controller').'&step=0&key='.$Key.'" style="margin-left:5px"><img src="../modules/newsletteradmin/img/up.gif" width="56" height="56" alt="Upgrade" title="'.$this->l('Download and install the last version').': &#13;'.$name.'"></a><br/><br/>'.$desc.'<br/>'.$this->l('If auto-upgrade fails, Please download the latest version and install it').': <a href="'.$link.'">'.$name.'</a><br/><br/></center>');
								$this->html .=  '
								<p style="font-size:11px; padding:0px 30px; color:red">'.$this->l('Your version is out of date ').'!</p>
								<div style="margin-top:50px;"> </div>
								';
							}
							else
								$this->html .= '
								<div style="font-size:11px; padding:0px 30px; color:green">'.$this->l('Your version is up to date ').'</div>
								<div style="margin-top:50px;"> </div>';
						}
						else 
							$this->html .= '<center><div class="alert"><font color="red">'.$this->l('Sorry, No information of new version avalaible!').'</font><br/>'.$this->l(' Please try later  ').'
							</font></div></center>'; 
					}
					else
						$this->html .= '
								<div style="font-size:11px; padding:0px 30px; color:green">'.$this->l('Your version is up to date ').'</div>
								<div style="margin-top:50px;"> </div>';
								
					$this->context->controller->addJS(_MODULE_DIR_.'newsletteradmin/views/js/newsletteradmin.js');//waiting JQUERY loading
					$this->context->controller->addJqueryUi('ui.datepicker');
					$this->html .= '
								<fieldset>
								  <legend class="hide" title="Click to show/hide">'.$this->l('Import csv file').':</legend>
									<div style="display: none;">
									<form action="'._MODULE_DIR_.'newsletteradmin/import.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" method="post" enctype="multipart/form-data" name="form1"> 
											'.$this->l('Import email list from csv file').': <input type="file" name="file" />&nbsp;
											<input type="submit" name="record" class="button" value="'.$this->l('Record').'" title="'.$this->l('Choose a csv or txt file').' &#13; '.$this->l('Max: 3 columns').' &#13; '.$this->l('email;lastname;firstname').'"> 
											<input type="submit" name="save" class="button" value="'.$this->l('Record and Save').'" title="'.$this->l('Choose a csv or txt file').' &#13; '.$this->l('Max: 3 columns').' &#13; '.$this->l('email;lastname;firstname').'"> 
									</form>	
									</div>
								</fieldset></br>';
					$_POST = isset($_SESSION['newsletter']['POST']) ? $_SESSION['newsletter']['POST'] : null;
					
					//date settings for no-english contries
					$par1 = $this->l('en_EN.utf8');
					$par2 = $this->l('eng');
					setlocale (LC_TIME, $par1,$par2); 					
					$day = ucwords (strftime("%A %d %B %Y"));
					if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')
						$day = utf8_encode($day);
						
					$this->html .= $this->listrep().'<div style="float:right; padding:23px 80px 10px 80px">' .  		      
							$this->l('You can use the following variables in your message (only for registered customers):') .'</br>'.
							$this->l('Variables:') 	. '   '	.
							$this->l('%CIVILITY%') . ' - ' . 
							$this->l('%FIRSTNAME%') . ' - ' .				
							$this->l('%LASTNAME%') 	. ' - ' . 				
							$this->l('%MAIL%') 	. '</br>' . 
							$this->l('These variables are used to create a link to view the page in a browser and create the unsubscription link:') .'</br>'.	
							$this->l('Variables:') 	. '   '	.
							$this->l('%LINK%') . ' - ' .
							$this->l('%SUB%') . ' - ' .
							$this->l('%UNSUB%') . '</div><br/>
								
							<form action="' . $currentIndex . '&token=' . $this->token .'&send=1" method="post">
								<fieldset>
									<legend class="none">'.$this->l('Subject').':</legend><br/>
									<input type="text" value="'.$this->l('Newsletter of').' '.$day.'" name="subject_email" style="width: 300px" />
								</fieldset>			
								<br/>
								<fieldset>
									<legend>'.$this->l('Message').':</legend>
								<br/>';
								$tiny = '../js/tiny_mce';
								file_exists($tiny.'/langs/'.$iso.'.js')? $iso : 'en';
								$this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');

								$supVersion = file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/themes/advanced/skins/cirkuit/content.css') ? true : false;
					$this->html .= '
								<div  class="mce">
								  <script type="text/javascript">
								  tinymce.PluginManager.load("insertproduct", "'._MODULE_DIR_.'newsletteradmin/scripts/insertproduct/editor_plugin.js");';
					if(version_compare(_PS_VERSION_,'1.6','>='))
						$this->html .= 'tinymce.PluginManager.load("compat3x", "'._MODULE_DIR_.'newsletteradmin/scripts/insertproduct/plugins/compat3x/plugin.min.js");';
								  
					$this->html .= 'tinyMCE.init({
										mode : "textareas",
										editor_selector : "mceEditor",';
					if($supVersion)
					{
						$this->html .= '		
										theme : "advanced",	
										skin : "cirkuit",
										plugins : "pagebreak,style,table,advimage,advlink,inlinepopups,preview,media,emotions,searchreplace,contextmenu,paste,fullscreen,template,insertproduct",
										// Theme options
										theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,emotions,preview",
										theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
										theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
										theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,template,pagebreak,insertproduct",
										Spellchecker_report_misspellings : true,
										theme_advanced_toolbar_location : "top",
										theme_advanced_toolbar_align : "left",
										theme_advanced_statusbar_location : "bottom",
										theme_advanced_resizing : true,';
					} 
					else 
					{
						$this->html .= '
										theme : "modern",	
										skin : "prestashop",
										plugins : "compat3x colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor template emoticons preview insertproduct",
										toolbar1 : "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image,emoticons,preview,insertproduct",
										toolbar2: "",';
					}
					$this->html .= '
										content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
										document_base_url : "'.__PS_BASE_URI__.'",
										width: "640px",
										height: "400px",
										font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
										elements : "nourlconvert,ajaxfilemanager",
										file_browser_callback : "ajaxfilemanager",
										entity_encoding: "named",
										convert_urls : false,
										language : "'.$iso.'",
										template_popup_width : 800,
										template_popup_height : 600,
										template_external_list_url : "'._MODULE_DIR_.'newsletteradmin/lists/template_list.php",
										templates : "'._MODULE_DIR_.'newsletteradmin/lists/template_list2.php"
										});
										function ajaxfilemanager(field_name, url, type, win) {
											var ajaxfilemanagerurl = "'.dirname($_SERVER["PHP_SELF"]).'/ajaxfilemanager/ajaxfilemanager.php";
											switch (type) {
												case "image":
													break;
												case "media":
													break;
												case "flash":
													break;
												case "file":
													break;
												default:
													return false;
											}
											tinyMCE.activeEditor.windowManager.open({
												url: "'.dirname($_SERVER["PHP_SELF"]).'/ajaxfilemanager/ajaxfilemanager.php",
												width: 782,
												height: 440,
												inline : "yes",
												close_previous : "no"
											},{
												window : win,
												input : field_name
											});
										}	
									$(document).ready(function(){ 
										$("#selectcss").change(function () {
												var option = $(this).val();
												var css = option.replace("\'", "\"");
												$("#css").html(css);
										});		
									});								
								  </script>
							<textarea  id="editornews" class="mceEditor" name="body_email" style="width: 100%; height: 380px;">				
									  '.Tools::getValue('body_email').'
							</textarea>
							</div>
							<div class="css">
							<h3 class="explain"><a title="'.$this->l('This feature adds a css content in the header of your newsletter').'"> '.$this->l('Add your css content').':</a></h3>
							<div id="selectTitle">';
					//liste les css						
					$dir = _PS_ROOT_DIR_.'/modules/newsletteradmin/templates/';
					$file ='';
					$this->html .='<select id="selectcss" name="fichiersCss">
						<option value="" disabled selected>'.$this->l('Choose a file:').'</option>';
						
					if (is_dir($dir)) 
					{
						if ($dh = opendir($dir)) 
						{
							while (($file = readdir($dh)) !== false) 
							{
								$name	= explode(".",$file);
								if($name[1] == 'css')
								{
									$value2= strip_tags(file_get_contents($dir.$file));
									$this->html .= '<option value="'.str_replace('"','\'',$value2).'">'.$file.'</option>'."\n";
								}
							}
							closedir($dh);
						}
					}
					
					$this->html .= '</select>&nbsp;<input type="button" class="button" value=" '.$this->l('Clear').' " onclick="cleartxt()" /><br><br></div>						
							<textarea id ="css" name="css">'.Tools::getValue('css').'</textarea>
							</div>
							<div style="clear:both;"><br /></div>    
							<div align="left">
							</fieldset></br>
							<fieldset>
							<legend class="hide" title="Click to show/hide">'.$this->l('Configuration').'</legend>
							<div style="display:none">						 
								  <h3> '.$this->l('Define parameters').': </h3><div style="margin-left: 15px; font-size:11px"> '.$this->l('Note: If you leave the fields blank, defaults settings will be used').'</div>
								<div style="margin-left: 12px">
								<TABLE WIDTH=100%>
								<TR>
									<TD>'.$this->l('Emails per minute:').' <input type="text" name="wait_time" size="2" value="60" maxlength="2" /></TD>
								</TR>
								<br/><TR>
									<TD style="padding-top:16px;"> '.$this->l('Email from').': <input type="text" value=" '.Configuration::get('NEWSLETTER_FROM').'" name="from" size="30" />
									</TD>
									<TD WIDTH=30px ></TD> 
									<TD style="padding-top:16px;"> '.$this->l('Email from name').': <input type="text" value=" '.Configuration::get('NEWSLETTER_FROM_NAME').'" name="fromname" size="30" />
									</TD>
									<TD WIDTH=30px></TD>  
									<TD style="padding-top:16px;"> '.$this->l('Recipients').': <input type="text" value=" '.Configuration::get('NEWSLETTER_TO_NAME').'" name="toname" size="30" /><br />
									</TD>
								</TR><br/>
								<TR style="font-size:11px;text-align:left">
									<TD>'.$this->l('Example').': No-reply@'.$shop_server.'</TD><TD></TD>		
									<TD>'.$this->l('Example').': Press Division</TD><TD></TD> 
									<TD>'.$this->l('Example').': Mailing List</TD>
								</TR>
								</div>				
								</TABLE></div><br/>
								</div>
								<div>
							</fieldset></br>
							<fieldset>
								<legend class="hide" title="Click to show/hide">'.$this->l('Choice of recipients').':</legend>
								<div style="display: none;">
								<h3> '.$this->l('Test').' </h3>
								  <div style="margin-left: 15px">
									  <input type="checkbox" name="sTeste" value="1" /> '.$this->l('Mailer Test').' <br /><br /> &nbsp;&nbsp;&nbsp;
									  '.$this->l('Email:').' <input type="text" value="'. Configuration::get('PS_SHOP_EMAIL').'" name="sMailTest" size="30" /></label> <br /><br />
								  </div>
								<h3> '.$this->l('Registered visitors to blocknewsletter').' </h3>
								  <div style="margin-left: 15px">
									  <input type="checkbox" name="$sNewsletter" value="1" /> '.$this->l('Newsletter Block Subscribers').'&nbsp;(<b>'.$countBlockSub.'</b>) <br /><br /> 
								  </div>
								<h3> '.$this->l('By gender').' </h3>
								  <div style="margin-left: 15px">
									<input type="radio" name="sexCustomers" value="0"  checked="checked" onClick="hide();"/> '.$this->l('None').'<br/>
									<input type="radio" name="sexCustomers" value="1"  /> '.$this->l('Womens').'&nbsp;(<b>'.$this->countsex(2).'</b>) <br/>
									<input type="radio" name="sexCustomers" value="2"  /> '.$this->l('Mens').'&nbsp;(<b>'.$this->countsex(1).'</b>) <br/>
								  </div>
								<h3> '.$this->l('By lang').' </h3>
								  <div style="margin-left: 15px">';
									 $langs = Language::getLanguages(false);
									 foreach ($langs as $lang)
										   {
											   $lang_name = $lang['name'];
											   $id_lang = $lang['id_lang'];
											   $count_lang = $this->countlang($id_lang);
											   $this->html .=  "<input type=\"checkbox\" name=\"customerslang[]\" value=\"$id_lang\"  /> $lang_name <strong>($count_lang)</strong><br/>" ;
											}
					$this->html .= '</div>
					
								<h3> '.$this->l('Customers').' </h3>
								  <div style="margin-left: 15px">		
									  <input type="radio" name="sCustomers" value="0"  checked="checked" onClick="hide();hiden();"/> '.$this->l('None').'<br/><br/>
									  <input type="radio" name="sCustomers" value="1"  onClick="hide();hiden();"/> '.$this->l('All Customers').'&nbsp;(<b>'.$countCustomers.'</b>) <br/><br/>
									  <input type="radio" name="sCustomers" value="2"  onClick="hide();hiden();"/> '.$this->l('Customers who signed up for Newsletter').'&nbsp;(<b>'.$countCustomersSub.'</b>) <br/><br/>
									<input type="radio" name="sCustomers" value="5" onClick="hide();hiden();"/> '.$this->l('Customers who signed up for Opt-in').'&nbsp;(<b>'.$countOptCustomers.'</b>) <br/><br/>
									  <input type="radio" name="sCustomers" value="3" id="sCustomers3" onClick="hide();hiden();"/> '.$this->l('Customers whose birthdate is between').': &nbsp;
									  <input type="text" size="6" maxlength="5" class="datepicker" id="date_from" name="date_from" value="'.date("d-m").'"onClick="hide();document.getElementById(\'sCustomers3\').checked=true;"/>&nbsp;'.$this->l('and').'&nbsp;
									  <input type="text" size="6" maxlength="5" class="datepicker" id="date_to" name="date_to" value="'.date("d-m",strtotime("+1 month")).'"onClick="hide();document.getElementById(\'sCustomers3\').checked=true;"/>&nbsp;(<b>'.$countBirthCustomers.'</b>)<br/><div style="font-size:11px; margin-left: 15px">( '.$this->l('Format: d-m').' )</div><br/>
									<input type="radio" name="sCustomers" value="4" onClick="display();hiden();" /> '.$this->l('Customers who belong to one or more of these groups:').':
										<p id="collapse" style="margin-left: 15px"><br/>';
										 $sql = "SELECT distinct(c.id_customer), c.active, c.deleted,c.newsletter,
										 cg.id_customer, cg.id_group as group2, count(cg.id_customer) as compte, gl.id_group,gl.name as nom, gl.id_lang
										   FROM  "._DB_PREFIX_."customer as c,
										   "._DB_PREFIX_."customer_group as cg,
										   "._DB_PREFIX_."group_lang as gl
										   WHERE c.active !=0
										   AND c.deleted !=1
										   AND c.newsletter !=0
										   AND c.id_customer=cg.id_customer
										   AND cg.id_group=gl.id_group
										   AND c.id_shop = ".$this->context->shop->id."
										   AND gl.id_lang=".$this->context->language->id."
										   GROUP BY cg.id_group";
									 $datas = DB::getInstance()->ExecuteS($sql);
									 foreach ($datas as $data)
										   {
											   $name2=$data["nom"];
											   $compte2=$data["compte"];
											   $id_group2= $data["group2"];
											   $this->html .=  "<input type=\"checkbox\" name=\"groupname[]\" value=\"$id_group2\"  /> $name2 <strong>($compte2)</strong>&nbsp;&nbsp;" ;
											}
					$this->html .= '
								</p><br/><br/>
								<input type="radio" name="sCustomers" value="6"  onClick="hide();hiden();"/> '.$this->l('Customers who have not read the latest newsletter').'&nbsp;(<b>'.$countNoReadCustomers.'</b>)<br/><br/>
								<input type="radio" name="sCustomers" value="7" onClick="hide();hiden();" /> '.$this->l('Email list imported from csv file:').'&nbsp;(<b>'.$countCsvCustomers.'</b>)<br/><br/>';
								$query = 'SELECT count(*) as nb FROM  '._DB_PREFIX_.'mailing_import WHERE `group` != "temp" AND id_shop = '.$this->context->shop->id; 
								$is = Db::getInstance()->getValue($query);
								
					$active = ($is > 0) ? '' : 'disabled';		
					$this->html .= '<input type="radio" name="sCustomers" value="8" onClick="play();hide();" '.$active.' /> '.$this->l('Imported CSV Groups').'&nbsp;(<b>'.$is.'</b>) :	
								<p id="cache" style="margin-left: 15px"><br/>';
								
					if ($is != 0) 
					{	
						$sql1 = 'SELECT distinct `ID`, `group`  , count(ID) as compte
								FROM  '._DB_PREFIX_.'mailing_import 
								WHERE `group` != "temp"
								AND id_shop = '.$this->context->shop->id.'
								GROUP BY `group`';
									  
						$datas = DB::getInstance()->ExecuteS($sql1);

						foreach ($datas as $data)
						{
							$csvname=$data["group"];
							$csvcompte=$data["compte"];
							$this->html .= '<input type="checkbox" name="groupcsv[]" value="'.$csvname.'"  /> '.$csvname.' <strong>('.$csvcompte.')</strong><a href="#" onclick="if(confirm(\''.$this->l('Are you sure to delete this Group').'? ('.$csvname.')\')) document.location.href=\''._MODULE_DIR_.'newsletteradmin/supp.php?key='.$Key.'&group='.$csvname.'&id_shop='.$this->context->shop->id.'\'"><img src="../img/admin/cross.png" alt="Delete" title="'.$this->l('Delete this Group').'" width="16px"></a>&nbsp;&nbsp;' ;
						}
					}
					
					$this->html .= '</p>
										</div>
									</div> 
									</fieldset></br>
									<div align="right"> 
										<input type="checkbox" name="template_save" value="1"  /> '.$this->l('Save this mail as template').'<br/>	
										<input type="checkbox" name="no_save" value="1"  /> '.$this->l('Do not save this campaign (no backups, no tracking)').'<br/><br/>	
										<input type="submit" class="button" name="maillist" value="&nbsp;'.$this->l(' Continue ').' &nbsp;" />
									</div><br/> 								
									<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div>
									
							
						<div style="height:10px;">&nbsp;</div>';
					$news_url = 'http://www.eolia.o2switch.net/pss_cms_content.php?cms=9&iso=fr';
					$opts = array(
							  'http'=>array(
								'method'=>"GET",
								'header'=>"User-agent: Mozilla 5.0 for ".$_GET['controller'])
							);
					$context = stream_context_create($opts);
					$data = file_get_contents($news_url, false, $context);
					if (strlen(utf8_decode($data)) > 540)
						$this->html .= '<fieldset>
								<legend><img style="vertical-align:middle;"/>'.$this->l('Informations').'</legend>'.$data;
					else 
						$this->html .= '<center>News Server Error!</center>';
								
					$this->html .='	
							</fieldset>
							</form></div>
								</div>';
							
					unset( $_SESSION['newsletter'] );
					$_SESSION['newsletter']['POST'] = $_POST;
					
					return $this->html;
				}
			}//Install Ok
		}
	}

	public function postProcess()
	{
		global $currentIndex;
		
		if (Tools::getValue('send') == 1)
		{	
			if( !isset($_SESSION['newsletter']) OR empty($_SESSION['newsletter']['finalList']) )
			{
				$sCustomers 	= Tools::getValue('sCustomers');
				$sexCustomers	= Tools::getValue('sexCustomers');
				$langCustomers  = Tools::getValue('customerslang');
				$sNewsletter 	= Tools::getValue('$sNewsletter');
				$sTeste         = Tools::getValue('sTeste');
				$sendList       = Array();
		
				/** Mailer test **/
				if( $sTeste)
				{
					if (Validate::isEmail(Tools::getValue('sMailTest'))) 
					{
						$array[]   = array( 'email' => Tools::getValue('sMailTest'), 'firstname' => $this->l('Firstname test'), 'lastname' => $this->l('Lastname test'), 'category' => $this->l('Test') );
						$sendList  = array_merge($sendList,  $array );
					}
					else 
					{
						$this->displayWarning('<center>'.$this->l('Verify your email\'s syntax, this address is not a valid email address!').' ('.Tools::getValue('sMailTest').')</center>');
								
						$fail= Tools::getValue('sMailTest').': '.html_entity_decode($this->l('Verify your email\'s syntax, this address is not a valid email address!'));	
						nlog($fail.chr(13));
						$_POST['send']  =   0;
						return;
						}
				}
							
				switch($sexCustomers)
				{
					case '1':
						$sexcustomers 		    = $this->getSexCustomers(2);
						foreach ($sexcustomers as $cle => $value)
							$sexcustomers[$cle]['category'] = $this->l('females Customers');
						$sendList           = (count($sexcustomers) > 0) ? array_merge($sendList, $sexcustomers) : $sendList;

					break;
					
					case '2':
						$sexcustomers         = $this->getSexCustomers(1);
						foreach ($sexcustomers as $cle => $value)
							$sexcustomers[$cle]['category'] = $this->l('Males Customers');
						$sendList           = (count($sexcustomers) > 0) ? array_merge($sendList, $sexcustomers) : $sendList;

					break;
					}

				if($langCustomers) {
					foreach ($langCustomers as $cust){ 
						$customers          = $this->getCustomerByLang($cust);
						foreach ($customers as $cle => $value)
							$customers[$cle]['category'] = $this->l('Customers by lang').': '.Language::getIsoById($cust);
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;
					}		
				}
				
				/** BEGIN Customers **/
				switch($sCustomers)
				{
				  case '1':
					$customers 		    = $this->getCustomers();
					foreach ($customers as $cle => $value)
						$customers[$cle]['category'] = $this->l('All Customers');
					$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '2':
					$customers          = $this->getNewsletteremails();
					foreach ($customers as $cle => $value)
							$customers[$cle]['category'] = $this->l('Customers who signed up for Newsletter');
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '3':
					$customers          = $this->getBirthdayCustomers( Tools::getValue('dateBirthday') );
					foreach ($customers as $cle => $value)
							$customers[$cle]['category'] = $this->l('Birthday Customers');
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '4':			
					$custom = Tools::getValue('groupname');
						foreach ($custom as $cust){
							$customers          = $this->getGroupCustomers($cust);
							foreach ($customers as $cle => $value)
								$customers[$cle]['category'] = 'Groupe N:'.trim($cust);
							$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;
						}		

					break;
				  case '5':
					$customers          = $this->getOptCustomers();
					foreach ($customers as $cle => $value)
							$customers[$cle]['category'] = $this->l('Customers who signed up for Opt-in');
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '6':
					$customers          = $this->getNoReadCustomers();
					foreach ($customers as $cle => $value)
							$customers[$cle]['category'] = $this->l('Customers who have not read the latest newsletter');
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '7':
					$customers          = $this->getCsvCustomers();
					foreach ($customers as $cle => $value)
							$customers[$cle]['category'] =  $this->l('Email list imported from csv file:');
						$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;

					break;
				  case '8':
					$custom = Tools::getValue('groupcsv');
							foreach ($custom as $cust){
								$customers          = $this->getCsvGroupCustomers($cust);
								foreach ($customers as $cle => $value)
									$customers[$cle]['category'] = $this->l('Group list imported from csv file').': '.$cust;
								$sendList           = (count($customers) > 0) ? array_merge($sendList, $customers) : $sendList;
								}

					break;
					
				  }
				/** END Customers **/
			
				/** BEGIN Block Subscribers **/
				if ($sNewsletter)
				{
					  $blockSubscribers   = $this->getBlockSubscribers();
					  foreach ($blockSubscribers as $cle => $value)
							$blockSubscribers[$cle]['category'] = $this->l('Newsletter Block Subscribers');
						$sendList           = (count($blockSubscribers) > 0) ? array_merge($sendList, $blockSubscribers) : $sendList;
				}
				/** END Block Subscribers **/
				
				if (empty($sendList))
				{
					$errorMessage = $this->l('No recipients, with selected criteria!');
					$this->context->controller->errors[] = $errorMessage;
					$this->backSend($currentIndex,$errorMessage);
					return;
				}

				if (count($sendList) >= 2000)
					$finalList = $sendList ;
				else
				{
					$finalList = Array();
					$doublons = Array();
					foreach ($sendList as $item)
					{
						if (!$this->recursive_in_array($item['email'],$finalList))
							$finalList[] = $item;
						else
							$doublons[] = $item['email'].' found!';
					}
				}

				/**VERIFY if subject is present!**/
				$subject = Tools::getValue('subject_email');
				
				if (empty($subject))
				{
					$errorMessage = $this->l('Email subject do not be empty!');
					$this->context->controller->errors[] = $errorMessage;
					$this->backSend($currentIndex,$errorMessage);
					return;
				}
				
				$no_save = trim(Tools::getValue('no_save'));
				$_SESSION['newsletter']['no_save'] = $no_save;
				$template_save = trim(Tools::getValue('template_save'));
				$_SESSION['newsletter']['template_save'] = $template_save;
				
				$Result['total']            =   0;
				$Result['failed']           =   0;
				$Result['success']          =   0;
				$ArrayFailed                =   array();
				$key                        =   0;
				$output                     =   '';
				$check_division             =   60 / ( intval($_POST['wait_time']) > 0 ? intval($_POST['wait_time']) : 2 ) ;
				$wait                       =   intval( $check_division ) > 0 ? $check_division : 30;
				$from 						= 	trim(Tools::getValue('from'));
				
				if(empty($from))  
					$from = configuration::get('PS_SHOP_EMAIL');
					
				if (!Validate::isEmail($from)) 
					die('Wrong sender address format! : '.$from);
				$_SESSION['newsletter']['from']      	  =   $from;
				
				$fromName = trim(Tools::getValue('fromname'));
				if(empty($fromName)) 
					$fromName = configuration::get('PS_SHOP_NAME');
				$_SESSION['newsletter']['fromName']  	  =   $fromName;
				
				$toName = trim(Tools::getValue('toname'));
				if(empty($toName)) 
					$toName = Null ;
				$_SESSION['newsletter']['toName']    	  =   $toName;
				
				$address = trim(Tools::getValue('address'));
				if(empty($address)) 
					$address = Null ;
				$_SESSION['newsletter']['address']    	  =   $address;				
				$_SESSION['newsletter']['check']          =   TRUE;
				$_SESSION['newsletter']['finalList']      =   $finalList;
				$_SESSION['newsletter']['total']          =   $Result['total'];
				$_SESSION['newsletter']['failed']         =   $Result['failed'];
				$_SESSION['newsletter']['success']        =   $Result['success'];
				$_SESSION['newsletter']['ArrayFailed']    =   $ArrayFailed;
				$_SESSION['newsletter']['POST']           =   $_POST;
				$_SESSION['newsletter']['GET']            =   $_GET;
				$_SESSION['newsletter']['key']            =   $key;
				$_SESSION['newsletter']['output']         =   $output;
				$_SESSION['newsletter']['wait_time']      =   $_POST['wait_time'];
				$_SESSION['newsletter']['body_email']     =   Tools::getValue('body_email');
				$_SESSION['newsletter']['subject_email']  =   Tools::getValue('subject_email');
				$_SESSION['newsletter']['css'] 			  =   addslashes(Tools::getValue('css'));
				$_SESSION['newsletter']['url'] 		   	  =   $this->getShopUrl();
				$_SESSION['newsletter']['shop_id'] 		  =   $this->context->shop->id;	
				$_SESSION['newsletter']['lang_id'] 		  =   $this->context->language->id;
			}
			else
			{
			  $finalList                  =   $_SESSION['newsletter']['finalList'];
			  $Result['total']            =   $_SESSION['newsletter']['total'];
			  $Result['failed']           =   $_SESSION['newsletter']['failed'];
			  $Result['success']          =   $_SESSION['newsletter']['success'];
			  $ArrayFailed                =   $_SESSION['newsletter']['ArrayFailed'];
			  $_POST                      =   $_SESSION['newsletter']['POST'];
			  $_GET                       =   $_SESSION['newsletter']['GET'];
			  $key                        =   $_SESSION['newsletter']['key'];
			  $output                     =   $_SESSION['newsletter']['output'];
			  
			  $check_division             =   60 / ( intval($_POST['wait_time']) > 0 ? intval($_POST['wait_time']) : 1 ) ;
			  $wait                       =   intval( $check_division ) > 0 ? $check_division : 30;
			  
			}
			
			$value      = $finalList[$key];
			$email 		= $value['email'];	
			
					
		// MAILING LIST MANAGEMENT
			if (($finalList[$key]!=null) and (Validate::isEmail($email)))
			{
				if(isset($doublons)) 
				{
					$supp = count($doublons);
					$text_doublon = '&nbsp;('.$supp.'&nbsp;'.$this->l('duplicates deleted').')';
				}
				else
				{
					$supp = 0;
					$text_doublon = '';
				}
				$this->_returnHTML .= '
									<div class="newsfield">
									<div id="droop" style="display:none;position:fixed; top:0; left:0; width:100%; height: 100%; background-color:rgba(0,0,0,0.66); z-index: 2000; text-align : center;"><span style=" display: inline-block; margin : 250px auto; background-color : #F18028;color : white; font-size: 3em; padding:10px;border-radius: 8px 8px;box-shadow: 0px 0px 10px 3px rgba(0, 0, 0, 0.4);"'.$this->l('Please wait...').'</span></div>
									</br>
									<div class="preview_left">
									<H1> '.$this->l('Content preview').'</H1>
									<form action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
									<input type="submit" class="button"  value="&nbsp;'.$this->l('Modify').'&nbsp;"  />
									</form><br />';
									
				$preview = '		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
									<style type="text/css">
									'.htmlspecialchars_decode(stripslashes(str_replace('\'','"',$_POST['css']))).'
									</style>
									<tbody>'.stripslashes($_POST['body_email']).'</tbody>';
				$tempfile = '../modules/newsletteradmin/page.htm';
				file_put_contents($tempfile, $preview);
				$this->_returnHTML .= '					
									<IFRAME src="'.$tempfile.'" style=" width: 100%; height: 100%; border: none; "></IFRAME>
									</div>
									<div class="preview_right">
									<H1> '.$this->l('Your Mailing List').'</H1>
									<form style=" display: inline-block;" action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
										<input type="submit" class="button"  value="&nbsp;'.$this->l('Cancel').'&nbsp;"  />
									</form>
									<form style=" display: inline-block;margin-left:20%;margin-bottom: 16px;" action="' . $currentIndex . '&token=' . $this->token .'&send=2" method="post" >
									
											<input type="submit" class="button" name="send_newsletter" value="&nbsp;'.$this->l(' Send Mails ').' &nbsp;" />
											<input type="submit" class="button" name="cron"  value="&nbsp;'.$this->l(' Generate Cron ').' &nbsp;"  />
									
									</form>	
									<center><b>
										  '.$this->l('Addresses generated').$text_doublon.':
										<div id="resultat">
										'.count($finalList).'</b></br /><br />
										<TABLE id="table1">';
										foreach($finalList as $keyList => $maillist)
										{
											$this->_returnHTML .='	
												<TR>
												<TD style=" width=300px;border-width:1px;border-style:solid;border-color:#CCCCCC;">&nbsp;'.$maillist['category'].'</TD>
												<TD style=" width=250px;border-width:1px;border-style:solid;border-color:#CCCCCC;">&nbsp;N°'.$keyList.':&nbsp;'.$maillist['email'].'&nbsp;&nbsp;
												</TD>
												<TD WIDTH=10px>
												<a href="#" onclick="ajaxExec(\'../modules/newsletteradmin/supp2.php?key='.$keyList.'\',\'resultat\');"><img src="../img/admin/delete.gif" border="0" alt="Delete" title="'.$this->l('Delete this entry').'" width="16px"></a>
												</TR>
												';
										}
				
				$this->_returnHTML .= '
									</TABLE>
									</div>
									</center>
									<script type="text/javascript">
									function ajaxExec(url,targetId) {
										var numList = '.count($finalList).';
										if (numList > 2000) {
											document.getElementById("droop").style.display="block";
										}
										document.getElementById(targetId).innerHTML = "";
										var XHR = null;
										if(document.all && !window.opera) { 
											var XhrObj = new ActiveXObject("Microsoft.XMLHTTP") ;  }  
										else {   
											var XHR = new XMLHttpRequest(); }
										
										XHR.open(\'POST\', url, true);
										XHR.setRequestHeader(\'Content-Type\', \'application/x-www-form-urlencoded\');	
										XHR.onreadystatechange = function (){

											if(XHR.readyState == 4 && XHR.status == 200){	
												document.getElementById(targetId).innerHTML = XHR.responseText;
												document.getElementById("droop").style.display="none";				
											}
										};
										XHR.send();
									}
									</script> 										
									<br/>
									<br/><br/>
									';
									
			}
			else
			{
				$this->_returnHTML .= '<div class="alert">'.$this->l('Verify your email\'s syntax, this address is not a valid email address!').' ('.$email.')</div>';
				$fail= html_entity_decode($email.$this->l('Verify your email\'s syntax, this address is not a valid email address!'));	
				nlog($fail.chr(13));
				$_POST['send']  =   0;
				
				return $this->display();
			}
	
		//Mailing list ok!	
		}
		elseif ((Tools::getValue('send') == 2) && isset($_SESSION['newsletter']))
		{	
			if (isset($_POST['cron']))
				$crontask = 1;
			if (isset($_POST['send_newsletter']) or isset($_SESSION['newsletter']['send_newsletter']))
			{
				$send_newsletter = 1;
				$_SESSION['newsletter']['send_newsletter'] = 1;
			}
				
			if (isset($_SESSION['newsletter']))
			{	
				$html 					= 	$_SESSION['newsletter']['body_email'];
				$subject 				= 	$_SESSION['newsletter']['subject_email'];
				$css					=	$_SESSION['newsletter']['css'];
				$finalList              =   $_SESSION['newsletter']['finalList'];
				$Result['total']        =   $_SESSION['newsletter']['total'];
				$Result['failed']       =   $_SESSION['newsletter']['failed'];
				$Result['success']     	=   $_SESSION['newsletter']['success'];
				$ArrayFailed            =   $_SESSION['newsletter']['ArrayFailed'];
				$_POST                  =   $_SESSION['newsletter']['POST'];
				$_GET                  	=   $_SESSION['newsletter']['GET'];
				$key 					=   $_SESSION['newsletter']['key'];
				$output                	=   $_SESSION['newsletter']['output'];
				$from      				=   $_SESSION['newsletter']['from'];
				$fromName      			=   $_SESSION['newsletter']['fromName'];
				$toName      			=   $_SESSION['newsletter']['toName'];
				$address      			=   $_SESSION['newsletter']['address'];
				$no_save 				= 	$_SESSION['newsletter']['no_save'];
				$template_save 			= 	$_SESSION['newsletter']['template_save'];
				$check_division         =   60 / ( intval($_SESSION['newsletter']['wait_time']) > 0 ? intval($_SESSION['newsletter']['wait_time']) : 1 ) ;
				$wait                   =   intval( $check_division ) > 0 ? $check_division : 30;
				
				$value  = $finalList[$key];
			
				//%VARIABLES%
				$gender =  isset($value['id_gender']) ? $value['id_gender'] : '';
				switch($gender)
				{
					case '1':
						$gender = $this->l('Mr');
						break;	
					case '2':
						$gender = $this->l('Mme');
						break;	
					default:
						$gender = '';
						break;	
				}
				
				$firstname 	= isset($value['firstname']) ? $value['firstname'] : '';
				$lastname 	= isset($value['lastname']) ? $value['lastname'] : '';
				$email 	= $value['email'];
				
				$verif_url0 = $this->getShopUrl().'?SID='.base64_encode($email).'&action=0&fc=module&module=newsletteradmin&controller=subscription&id_lang='.$this->context->language->id;
				$verif_url1 = $this->getShopUrl().'?SID='.base64_encode($email).'&action=1&fc=module&module=newsletteradmin&controller=subscription&id_lang='.$this->context->language->id;
				$view = self::remove_accents($subject);
				$view = str_replace(" ","_", $view);
				$view = str_replace("'","_", $view);
				$view = str_replace("%"," per cent", $view);
				$link = '<a href="'.$this->getShopUrl().'newsletters/'.$view.'.html">'.$this->l('Click here to read this email in your browser').'</a>';
				$sub = '<a href="'.$verif_url0.'">'.$this->l('Subscribe').'</a>';
				$unsub = '<a href="'.$verif_url1.'">'.$this->l('Unsubscribe').'</a>';
		
				$html 	= str_replace($this->l('%FIRSTNAME%'),'%FIRSTNAME%', $html);
				$html 	= str_replace($this->l('%CIVILITY%'),'%CIVILITY%', $html);			
				$html 	= str_replace($this->l('%LASTNAME%'),'%LASTNAME%', $html);
				$html 	= str_replace($this->l('%MAIL%'),'%MAIL%', $html);
				$html 	= str_replace($this->l('%LINK%'),'%LINK%', $html);
				$html 	= str_replace($this->l('%SUB%'),'%SUB%', $html);
				$html 	= str_replace($this->l('%UNSUB%'),'%UNSUB%', $html);
						
				$subject 	= str_replace($this->l('%CIVILITY%'), '%CIVILITY%', $subject);			
				$subject 	= str_replace($this->l('%FIRSTNAME%'), '%FIRSTNAME%', $subject);
				$subject 	= str_replace($this->l('%LASTNAME%'),  '%LASTNAME%', $subject);
				$subject 	= str_replace($this->l('%MAIL%'), '%MAIL%', $subject);		
				$_SESSION['newsletter']['POST']['body_email'] = $html ;
				$_SESSION['newsletter']['POST']['subject_email'] = $subject;
				
				//CRON TASK				
				if (isset($crontask )) 
					{
						if ($finalList[$key]!=null)
						{
							$configuration = Configuration::getMultiple(array(
								'PS_SHOP_EMAIL',
								'PS_MAIL_METHOD',
								'PS_MAIL_SERVER',
								'PS_MAIL_USER',
								'PS_MAIL_PASSWD',
								'PS_SHOP_NAME',
								'PS_MAIL_SMTP_ENCRYPTION',
								'PS_MAIL_SMTP_PORT',
								'PS_MAIL_TYPE',
								'PS_MAIL_DOMAIN'
							), null, null, $this->context->shop->id);
							$_SESSION['newsletter']['POST']['configuration'] = $configuration;
							
							$srzed = serialize($_SESSION['newsletter']);
							$fh = fopen(_PS_MODULE_DIR_ . 'newsletteradmin/cron/'.date('m-d-His'),'w+');
							fwrite($fh,$srzed);
							fclose($fh)or die("erreur");
							
							$this->displayWarning('Info: '.$this->l('You can generate a cron task here:').' <a href="http://www.generateit.net/cron-job/">www.generateit.net/cron-job/</a>');
							
							$this->_returnHTML .= '
								<div class="newsfield">
								</br>
								<center>
								  <H1> '.$this->l('Cron job generated').' !</H1>
									</br>'.count($finalList).'  '.$this->l('addresses generated').'.</br>'.$this->l('The path to your script is here').': <b>'. _PS_MODULE_DIR_ . 'newsletteradmin/cron.php</b>
									</br></br>
									<div class="alert">'.$this->l('Do not forget to configure your server for this task!').'
									</div>
									'.$this->l('example').':<p>/usr/bin/curl -s '.$this->getShopUrl().'modules/newsletteradmin/cron.php </br>
											/usr/bin/lynx -source '.$this->getShopUrl().'modules/newsletteradmin/cron.php </br>
											/usr/bin/wget -o/dev/null -O- '.$this->getShopUrl().'modules/newsletteradmin/cron.php </br></p>
											<p>'.$this->l('Or use an external service as:').' <a href="https://www.easycron.com/cron-job-tutorials/how-to-set-up-cron-job-for-prestashop-emailing-newsletter"> Cron Service</a></p>
								</center>
								<form action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
									<div align="right"><br /><input type="submit" name="back" value="&nbsp;'.$this->l('Back').'&nbsp;" class="button" />
									</div>
								</form>
							';
							$suc= html_entity_decode($this->l('Cron job generated')).' ('.date('m-d-His').')';	
							nlog($suc.chr(13));
						}
						if($template_save == 1)
							{
								$template_dir =_PS_MODULE_DIR_.'newsletteradmin/templates/';
								if (!file_exists ($template_dir)) 
									$this->_returnHTML .= 'Error the directory '.$template_dir.' don\'t exists!';
								else 
								{
									$temp=fopen($template_dir.$view.'.html', 'w+');
									fputs($temp,stripslashes($_POST['body_email']));
									fclose($temp);
									if(!empty($css)) {
										$temp=fopen($template_dir.$view.'.css', 'w+');
										fputs($temp,stripslashes($css));
										fclose($temp);
									}
								}
							}
							
							if($no_save == 1) 
							{
								$this->_returnHTML .= $this->l('No backup required for this session!').'<br/><br/>';
								$fail= 'Info: '.html_entity_decode($this->l('No backup required for this session!'));	
								nlog($fail.chr(13));
							}
							else
							{
								if (empty($_POST['body_email']) ) 
									$this->_returnHTML .= 'Backup: '.$this->l('Hum, hum...Your message is empty... Any backup required'); 
								else 
								{
									//backup only if the directory exist
									$rep = _PS_ROOT_DIR_.'/newsletters/';
									$fichier = _PS_ROOT_DIR_.'/newsletters/'.$view.'.html';
									if (file_exists ($rep)) 
									{
										$this->_returnHTML .= 'Backup: Ok !<br/>';
										if(file_exists($fichier) and ($no_save != 1)) 
										{
											$this->displayWarning('
												<div>'.$this->l('Info: You have crashed an existing backup!').'
												</div>');
														
											$fail= html_entity_decode($this->l('Info: You have crashed an existing backup!'));	
											nlog($fail.chr(13));
										}
										if (isset ($fichier))
										{
											$saven=fopen($fichier, 'w+');
												
											$html 	= $_SESSION['newsletter']['POST']['body_email'];
											$html 	= str_replace('%FIRSTNAME%','',$html);
											$html 	= str_replace('%LASTNAME%','' ,$html);
											$html 	= str_replace('%MAIL%','' ,$html);
											$html 	= str_replace('%LINK%',''  ,$html);
											$html 	= str_replace('%CIVILITY%',''  ,$html);
											$html 	= str_replace('%SUB%',''  ,$html);
											$html 	= str_replace('%UNSUB%',''  ,$html);	
											if(!empty($track))
												$html 	= str_replace($track,'',$html);
					
											fputs($saven,  stripslashes('<!DOCTYPE html>
											<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">'.stripslashes($css).'</style><title>'.$subject.'</title></head><body><center>'.$html.'</center></body></html>'));
											fclose($saven);
										}	
									}
									else
										$this->_returnHTML .= $this->l('FAILED').'<br/><div class="alert"><b>'.$this->l('Warning:') .'</b></br>'.$this->l('Your newsletter has not been saved!') .'</br>'.$this->l(' If you want to keep archives re-configure your module here').': <a href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&install=1&token='.Tools::getAdminTokenLite('AdminNewsletter').'"><img src="../img/admin/cog.gif" alt="Reset" title="'.$this->l('Reset').'"></a></div>';
								}
								$this->_returnHTML .= '</div>';
							}
						
						session_destroy();
						unset($_POST);
						unset($_GET);
						return $this->_returnHTML;
							
				//END OF CRON TASK	
				}
				
				//SEND MAILS				
				if(isset($send_newsletter) && isset($_SESSION['newsletter'])) 
				{
					$mail 	= new NewsMail();
					$send   = true;
					$html 	= str_replace('%FIRSTNAME%', $firstname, $html);
					$html 	= str_replace('%CIVILITY%', $gender, $html);			
					$html 	= str_replace('%LASTNAME%',  $lastname, $html);
					$html 	= str_replace('%MAIL%',      $email, $html);
					$html 	= str_replace('%LINK%',      $link, $html);
					$html 	= str_replace('%SUB%',     $sub, $html);
					$html 	= str_replace('%UNSUB%',     $unsub, $html);
							
					$subject 	= str_replace('%CIVILITY%', $gender, $subject);			
					$subject 	= str_replace('%FIRSTNAME%', $firstname, $subject);
					$subject 	= str_replace('%LASTNAME%',  $lastname, $subject);
					$subject 	= str_replace('%MAIL%',      $email, $subject);
					
					$date = date("d-m-Y");
					$time = date("H\h i\m s\s");
					$start1 = html_entity_decode($this->l('Sending the mail,on:'));
					$start2 = html_entity_decode($this->l('at'));
					$start = $start1.' '.$date.' '.$start2.' '.$time.' -> ';	
					nlog($start);
					
					if ($no_save != 1)
					{
						$id_campaign = Db::getInstance()->getValue("SELECT MAX(id_campaign) FROM "._DB_PREFIX_."mailing_history WHERE id_shop = ".$this->context->shop->id);
						$nbrs_enreg = Db::getInstance()->getValue("SELECT COUNT(*) AS compt FROM "._DB_PREFIX_."mailing_history WHERE id_campaign ='$id_campaign' AND id_shop = ".$this->context->shop->id);
						$id_campaign++;
						$this->_returnHTML .= '<div class="newsfield"><h3>'.$this->l('Campaign').' n° '.$id_campaign.'&nbsp;&nbsp;</h3>';
						if($id_campaign >= '11')
							Db::getInstance()->Execute("DELETE FROM `"._DB_PREFIX_."mailing_sent` WHERE `id_campaign` <= ($id_campaign -10)  AND `id_shop` = ".$this->context->shop->id); 
						$track = '<img src="'.$this->getShopUrl().'track.php?id_shop='.$this->context->shop->id.'&email='.$email.'&id_campaign='.$id_campaign.'&subject='.$view.'"  width="0" height="0" alt="track link" />';
						$html.= $track;
					}
					
					if (($finalList[$key]!=null) )
					{
						$mail_dir = _PS_MODULE_DIR_ . 'newsletteradmin/mails/';

						$h2t = new html2text($html);
						$text = $h2t->getText();
						
						$send 		= NewsMail::send(null, 'newsletter', $subject, array('{css}' => stripslashes($css),'{message}' => stripslashes($html),'{message2}' => $text),strtolower($email), $toName, $from, $fromName, null, null, $mail_dir, false, $this->context->shop->id, null,null);
						if ($no_save != 1)
						{	
							$today = date('d-m-Y H:i:s');
							Db::getInstance()->Execute("INSERT INTO `"._DB_PREFIX_."mailing_sent` ( `id_shop`, `id_campaign`, `email`, `date`) values(".$this->context->shop->id.",'$id_campaign', '$email','$today')");
						}
						$suc= 'Ok: '.$finalList[$key]['category'].' -> '.$email;
						nlog($suc.chr(13));
						
					}else
					{
						$this->displayWarning($this->l('Your session is finished!'));
						$fail= html_entity_decode($this->l('Your session is finished!'));	
						nlog($fail.chr(13));
						$_POST['send']  =   0;
						$this->_returnHTML .= '	<form action="' . $currentIndex . '&token=' . $this->token .'&send=0" method="post" >
												<div align="center"><br />
												<input type="submit" name="back" value="&nbsp;'.$this->l('Back').'&nbsp;" class="button" />
												</div><br/>
												<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div>
												</form>';
						session_destroy();
						
						return $this->setForm();							
					}
				
					$output .= '
						<tr>
						<td>&nbsp;' . $firstname . '</td>
						<td>&nbsp;' . $lastname . '</td>
						<td>&nbsp;' . $email . '</td>
						<td align="center">';
						
					//begining the backup...
					if ($send) 
					{
						$Result['success']++;
						$output .= "<font color=\"GREEN\"> ".$this->l('SUCCESS!')." </font> <br>";
					} 
					else
					{
						$this->_returnHTML .= $this->l('Ok!').'<br/><br/>';
						$ArrayFailed[]  =   $value;
						$Result['failed']++;
						$output .=  '<object type="audio/wav" width="0" height="0" data="../modules/newsletteradmin/siren.wav">
										<param name="filename" value="../modules/newsletteradmin/siren.wav" />
										<param name="autostart" value="true" />
										<param name="loop" value="false" />
									</object>
									<font color="RED"> '.$this->l('FAILED').' </font><br/>';
					}
						
					$output .=  ' </td></tr>';
					$Result['total']++;
					$key++;
					$_GET['send'] = 2;
					$_SESSION['newsletter']['key']            =   $key;
					$_SESSION['newsletter']['output']         =   $output;
					$_SESSION['newsletter']['total']          =   $Result['total'];
					$_SESSION['newsletter']['failed']         =   $Result['failed'];
					$_SESSION['newsletter']['success']        =   $Result['success'];
					
					$nbv = count($finalList);
						
					$ouput_foot     =    '<tr><td colspan="4">&nbsp;</td></tr>';
					$ouput_foot    .=    '<tr><td colspan="2"  ></td><td><b>&nbsp; '.$this->l('Total sent / Total to be sent').'</b></td><td align="center">'.$Result['total'].'/'.$nbv.'</td><tr>';
					$ouput_foot    .=    '<tr><td colspan="2"  ></td><td><b>&nbsp; '.$this->l('Total successfully').'</b></td><td align="center"><font color="GREEN">'.$Result['success'].'</font></td><tr>';
					$ouput_foot    .=    '<tr><td colspan="2"  ></td><td><b>&nbsp; '.$this->l('Total failure').'</b></td><td align="center"><font color="RED">'.$Result['failed'].'</font></td><tr>';
					$percent = ($Result['total']/$nbv)*100;
					$progress = number_format($percent, 2, ',', ' ');
					$this->_returnHTML .= '
							<style type="text/css">
								.prog-border {height: 20px;border: 1px solid #DDD;}
								.prog-bar {height: 20px;background: url("../modules/newsletteradmin/img/bar.gif") repeat-x;width:'.$percent.'%;height:100%; }
								.barre span{width:'.$percent.'%;text-align:center;float:left;margin-top:3px;}
							</style>
							<div class="prog-border">
								<div class="prog-bar" style="width:'.$percent.'%; text-align: center;">
									<div class="barre">
										<span style="color:white;">'.$progress.' %   ('.$Result['total'].'/'.$nbv.')</span>
									</div>
								</div>
							</div>
							<br/>';
					
						if( isset($finalList[$key]) )
						{	
							$this->_returnHTML .= '
									<meta http-equiv="refresh" content="'.$wait.'" />
									<div>'.$this->l('Wait...').'
										<div id="waiting">
											<p class="center"><img src="../img/loader.gif" alt="" /> '.$this->l('Sending Emails...').'</p>
										</div>
									</div>';
							$button = '';			
						} 
						else 
						{
						//backups if required
							$this->_returnHTML .= 'Backup: ';
							if($template_save == 1)
							{
								$template_dir =_PS_MODULE_DIR_.'/newsletteradmin/templates/';
								if (!file_exists ($template_dir)) 
									$this->_returnHTML .= 'Error the directory '.$template_dir.' don\'t exists!';
								else 
								{
									$temp=fopen($template_dir.$view.'.html', 'w+');
									fputs($temp,stripslashes($_POST['body_email']));
									fclose($temp);
									if(!empty($css)) {
										$temp=fopen($template_dir.$view.'.css', 'w+');
										fputs($temp,stripslashes($css));
										fclose($temp);
									}
								}
							}
							
							if($no_save == 1) 
							{
								$this->_returnHTML .= $this->l('No backup required for this session!').'<br/><br/>';
								$fail= 'Info: '.html_entity_decode($this->l('No backup required for this session!'));	
								nlog($fail.chr(13));
							}
							else
							{
								if (empty($_POST['body_email']) ) 
									$this->_returnHTML .= $this->l('Hum, hum...Your message is empty... Any backup required'); 
								else 
								{
									//backup only if the directory exist
									$rep = _PS_ROOT_DIR_.'/newsletters/';
									$fichier = _PS_ROOT_DIR_.'/newsletters/'.$view.'.html';
									if (file_exists ($rep)) 
									{
										$this->_returnHTML .= 'Ok<br/>';
										if(file_exists($fichier) and ($no_save != 1)) 
										{
											$this->displayWarning('
												<div>'.$this->l('Info: You have crashed an existing backup!').'
												</div>');
														
											$fail= html_entity_decode($this->l('Info: You have crashed an existing backup!'));	
											nlog($fail.chr(13));
										}
										if (isset ($fichier))
										{
											$saven=fopen($fichier, 'w+');
												
											$html 	= $_SESSION['newsletter']['POST']['body_email'];
											$html 	= str_replace('%FIRSTNAME%','',$html);
											$html 	= str_replace('%LASTNAME%','' ,$html);
											$html 	= str_replace('%MAIL%','' ,$html);
											$html 	= str_replace('%LINK%',''  ,$html);
											$html 	= str_replace('%CIVILITY%',''  ,$html);
											$html 	= str_replace('%SUB%',''  ,$html);
											$html 	= str_replace('%UNSUB%',''  ,$html);	
											$html 	= str_replace($track,'',$html);	
					
											fputs($saven,  stripslashes('<!DOCTYPE html>
											<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">'.stripslashes($css).'</style><title>'.$subject.'</title></head><body><center>'.$html.'</center></body></html>'));
											fclose($saven);
										}	
									}
									else
										$this->_returnHTML .= $this->l('FAILED').'<br/><div class="alert"><b>'.$this->l('Warning:') .'</b></br>'.$this->l('Your newsletter has not been saved!') .'</br>'.$this->l(' If you want to keep archives re-configure your module here').': <a href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&install=1&token='.Tools::getAdminTokenLite('AdminNewsletter').'"><img src="../img/admin/cog.gif" alt="Reset" title="'.$this->l('Reset').'"></a></div>';
								}
							}
							
							Configuration::updateValue('NEWSLETTER_FROM', $from);
							Configuration::updateValue('NEWSLETTER_FROM_NAME', $fromName);
							Configuration::updateValue('NEWSLETTER_TO_NAME', $toName);
							Configuration::updateValue('NEWSLETTER_BOUNCE', $address);

							$this->_returnHTML .= '
									<center>
										<div class="conf" style="width:200px">'.$this->l('Finished!').'
										</div>
									</center>
									<object type="audio/wav" width="0" height="0" data="../modules/newsletteradmin/succes.wav">
										<param name="filename" value="../modules/newsletteradmin/succes.wav" />
										<param name="autostart" value="true" />
										<param name="loop" value="false" />
									</object>';
								
							//Store the number of mails sent								
							$num_sent = $Result['total'];
							$date = date("d-m-Y");
							$time = date("H-i-s");
							if ($no_save != 1)
							{
								$sql ="insert into "._DB_PREFIX_."mailing_history ( id_shop,id_campaign, subject, date, time, num_sent) values(".$this->context->shop->id.",'$id_campaign', '$view','$date','$time','$num_sent')";
								Db::getInstance()->Execute($sql);
							}
							$finish = html_entity_decode($this->l('Total successfully')).': '.$Result['success'];
							nlog($finish.chr(13));
							
							if ($Result['failed']> 0)
							{					
								$num_fail = $Result['failed'];					
								$failed = html_entity_decode($this->l('Total failed')).': '.$num_fail;
								nlog($failed.chr(13));
							}
							session_destroy();
						}
		
					$ouput_header = '
										<fieldset>
											 <legend>'.$this->l('Report').'</legend><br>
												<table border="1" width="100%" style="border-collapse: collapse;" cellpadding="2" bordercolor="#e0d0b1" >
													<tr>
												<td align="center"><b>'.$this->l('FIRSTNAME').'</b></td>
												<td align="center"><b>'.$this->l('LASTNAME').'</b></td>
												<td align="center"><b>'.$this->l('E-MAIL').'</b></td>
												<td align="center"><b>'.$this->l('STATUS').'</b></td>
													</tr>';
		
					$ouput_foot	.=    	'</table>
											</fieldset>
												<form action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
													<div align="right"><br />
													<input type="submit" value="&nbsp;'.$this->l('Back').'&nbsp;" class="button" />
													</div><br/>
													<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div>
												</form>
												</div>';
						
					$this->_returnHTML .= $ouput_header;
					$this->_returnHTML .= $output;
					$this->_returnHTML .= $ouput_foot;
							
					rotate_logs();
				}
			
			}else
			{
				$this->displayWarning($this->l('Your session is finished!'));
				$fail= html_entity_decode($this->l('Your session is finished!'));	
				nlog($fail.chr(13));
				$this->_returnHTML .= '<form action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
												<div align="center"><br />
												<input type="submit" value="&nbsp;'.$this->l('Back').'&nbsp;" class="button" />
												</div><br/>
												<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div>
											</form>';
				session_destroy();
				
				return $this->setForm();							
			}
		
		}
		
		//INSTALL
		elseif (Tools::getValue('install') == 1) {

			$ver = '5.7';
			$bottom = '<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div></fieldset></form>';
			$error = Tools::getValue('error');
			
			$this->_returnHTML .= '
							<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
								<fieldset style="background:#EBEDF4;border:1px solid #CCCED7;color: #585A69; font-size:1em;margin:50px;padding:1em">
									<legend style="background: none repeat scroll 0 0 #EBEDF4;border: 1px solid #CCCED7;font-weight: 700; margin: 0;  padding: 0.2em 0.5em;  text-align: left;"><img src="../modules/newsletteradmin/logo.gif"  alt="" title="Newsletter" /> <a>'.$this->l('Copying files to your server').' </a></legend>
										
										<br/>
											<div id="message" style="display:none;"><p class="center"><img src="../img/loader.gif" alt="" /> '.$this->l('Copying files').'...</p><br/></div>
											';
				
			if (Tools::getValue('step') > 6) 
			{
				$this->_returnHTML .= 'Are you crazy, what do you try ?'.$bottom;	
				stepto(3,0,$error);
			}
		
			if (Tools::getValue('step') == 0)
			{	
				$error = 0;
				$this->_returnHTML .= '	<script>document.getElementById("message").style.display = "block";</script>'.$this->l('Create your language file').'...<br/><br/>'.$bottom;
				stepto(2,1,$error);	
			}
			
			if (Tools::getValue('step') == 1)
			{
				if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
					$rewrited_url = __PS_BASE_URI__;
					
				clearstatcache();
				Configuration::updateGlobalValue('Admin_Newsletter_Version', $ver);
				
				$language = Language::getLanguage(intval(Configuration::get('PS_LANG_DEFAULT')));
				$iso = $language['iso_code'];
				
				if (empty($iso)){ 
					$this->_returnHTML .= '
						Please configure your local language before continue in Preferences/Localization and retry
						<br/><br/>
						<center>
							<input type="button" class="button" value="'.$this->l('Go to the module').'" onClick="javascript:history.go(-1)" />
						</center>'.$bottom;
					$error++;
				}
				
				$rep_lang = _PS_MODULE_DIR_.'newsletteradmin/lang';
				$files = 'scripts';
				$trans_lang = $rep_lang.'/lang_'.$iso.'.php';
				$uni_lang = $rep_lang.'/uni_lang.php';
				$temp_lang = $rep_lang.'temp.php';
				
				if (!file_exists ($trans_lang))
				{
					if (file_exists($uni_lang)) {
						copy($uni_lang,$temp_lang); 
						rename($temp_lang,$trans_lang);
						$this->_returnHTML .= '<b>'.$this->l('A translation file for your language has been created here').':</b> '.$trans_lang.'<br/>'.$this->l('You can edit it later to translate the expressions in your language').'.';
					}
					else {

						$this->_returnHTML .='<font color="red">Error: The files uni_lang.php is missing! <br/><br/>Verify your archive in '.$rep_lang.'</font><br/>';
						$error++;
						errolog('Error: The file uni_lang.php is missing! Verify your archive in '.$rep_lang);
						}
				} 
				else  
					$this->_returnHTML .= '<b>'.$this->l('A translation file for your language is here').':</b> '.$trans_lang.'<br/><br/>';

				$this->_returnHTML .= '	<script>document.getElementById("message").style.display = "block";</script>'.$bottom;	
				
				if (file_exists('../modules/upgrade_PSNewsLetter.zip')) 
					stepto(3,2,$error);
				else { 
					if (file_exists(_PS_MODULE_DIR_.'newsletteradmin_old'))
						stepto(2,3,$error);
					else
						stepto(2,4,$error);
				}				
			}
			
			if (Tools::getValue('step') == 2) //Clean after auto-update
			{				
				if(unlink('../modules/upgrade_PSNewsLetter.zip')) {
					$this->_returnHTML .= '<br/>'.$this->l('Installation files successfully deleted').'!<br/><br/>
					<script>document.getElementById("message").style.display = "block";</script>';
				} 
				else {
					$error++;
					errolog('Error: The file '._PS_MODULE_DIR_.'upgrade_PSNewsLetter.zip is not erasable, delete it yourself.');
				}

				if (file_exists(_PS_MODULE_DIR_.'newsletteradmin_old'))
					stepto(2,3,$error);
				else
					stepto(2,4,$error);
			}
			
			if (Tools::getValue('step') == 3) //Recovery templates
			{
				if (file_exists(_PS_MODULE_DIR_.'newsletteradmin_old/templates')) 
				{
					$right = substr(sprintf('%o', fileperms(_PS_MODULE_DIR_.'newsletteradmin/templates')), -4);
					if ($right >= 700) { 
						$this->_returnHTML .= '<font color="green">'.$this->l('OK, you are allowed to write on the folder').': '._PS_MODULE_DIR_.'newsletteradmin/templates --> CHMOD: '.$right.'</font><br/>';
						smartCopy(_PS_MODULE_DIR_.'newsletteradmin_old/templates', _PS_MODULE_DIR_.'newsletteradmin/templates', $options=array('folderPermission'=>0755,'filePermission'=>0644));
						$this->_returnHTML .= $this->l('Your old templates have been saved').'
						</br><b>'.$this->l('The file').' '._PS_MODULE_DIR_.'newsletteradmin_old/templates '.$this->l('was successfully copied to').' '._PS_MODULE_DIR_.'newsletteradmin/templates<p align="center">'.$this->l('Finish!').'</p>
						<br/></b><br/>'; 
						sleep(1);
					}
					else {
						$this->_returnHTML .= '<b><font color="red">'.$this->l('Error!').' '.$this->l('You are not allowed to write on the folder').': '._PS_MODULE_DIR_.'newsletteradmin  <p align="center">'.$this->l('FAIL!').'</p></font></b><br/><br/>
						<center>
						<input type="button" class="button" value="'.$this->l('Back').'" onClick="javascript:history.go(-1)" />
						</center>';
						$error++;
						errolog('Error: '.$this->l('You are not allowed to write on the folder').': '._PS_MODULE_DIR_.'newsletteradmin/templates' );
					} 
					$this->_returnHTML .= '	<script>document.getElementById("message").style.display = "block";</script>'.$bottom;
					stepto(3,4,$error);
				}
			}
			
			if (Tools::getValue('step') == 4)
			{					
				$file = _PS_MODULE_DIR_.'newsletteradmin/scripts/files/track.php';
				$files2 =  _PS_ROOT_DIR_;
							
				if (!is_file($file)) {
					$this->_returnHTML .= '<font color="red">'. $this->l('Error: The file').' '. $file.' '.$this->l('not exist! Verify your archive').'</font><br/>';
					$error++;
					errolog( $this->l('Error: The file').' '. $file.' '.$this->l('not exist! Verify your archive'));
				}
				$right = substr(sprintf('%o', fileperms($files2)), -4);
			
				if ($right >= 700) { 
					$this->_returnHTML .= '<font color="green">'.$this->l('OK, you are allowed to write on the folder').': '.$files2.' --> CHMOD: '.$right.'</font><br/>';
					if(copy($file, $files2.'/track.php'))
						$this->_returnHTML .= '<b>'.$this->l('The file').' '.$file.' '.$this->l('was successfully copied to').' '.$files2.'<p align="center">'.$this->l('Finish!').'</p><br/></b><br/>';
		
				} 
				else {
					$this->_returnHTML .= '<b><font color="red">'.$this->l('Error!').' '.$this->l('You are not allowed to write on the folder').': '.$files2.'<p align="center">'.$this->l('FAIL!').'</p></font></b><br/><br/>
									<center><input type="button" class="button" value="'.$this->l('Back').'" onClick="javascript:history.go(-1)" /></center>';
					$error++;
					errolog($this->l('Error!').' '.$this->l('You are not allowed to write on the folder').': '.$files2);
				}
									 
				$this->_returnHTML .= '	<script>document.getElementById("message").style.display = "block";</script>'.$bottom;
				stepto(3,5,$error);
			}
			
			if (Tools::getValue('step') == 5)
			{						
				$files = _PS_MODULE_DIR_.'newsletteradmin/scripts/manage';
				$files2 =  _PS_ROOT_DIR_;
							
				if (!is_dir($files)) {
					$this->_returnHTML .= '<font color="red">'. $this->l('Error: The directory').' '. $files.' '.$this->l('not exist! Verify your archive').'</font><br/>';
					$error++;
					errolog($this->l('Error: The directory').' '. $files.' '.$this->l('not exist! Verify your archive'));
				}			

				$right = substr(sprintf('%o', fileperms($files2)), -4);
								
				if ($right >= 700) { 
					$this->_returnHTML .= '<font color="green">'.$this->l('OK, you are allowed to write on the folder').': '.$files2.' --> CHMOD: '.$right.'</font><br/>';
					smartCopy($files, $files2, $options=array('folderPermission'=>0755,'filePermission'=>0644));
					$this->_returnHTML .= '<b>'.$this->l('The entire file').' '.$files.' '.$this->l('was successfully copied to').' '.$files2.'<p align="center">'.$this->l('Finish!').'</p><br/></b><br/>'; 
				}else 
					{
						$this->_returnHTML .= '<b><font color="red">'.$this->l('Error!').' '.$this->l('You are not allowed to write on the folder').': '.$files2.'<p align="center">'.$this->l('FAIL!').'</p></font></b><br/><br/>
									<center><input type="button" class="button" value="'.$this->l('Back').'" onClick="javascript:history.go(-1)" /></center>';
						$error++;
						errolog($this->l('Error!').' '.$this->l('You are not allowed to write on the folder').': '.$files2);
					} 

				$this->_returnHTML .= '	<script>document.getElementById("message").style.display = "block";</script>'.$bottom;
				stepto(3,6,$error);
			}
			
			if (Tools::getValue('step') == 6)
			{	
				Configuration::deleteByName('Newsletter_Token');
				Configuration::deleteByName('Admin_Index');	
				Configuration::deleteByName('tokenNews');
				if(is_dir(_PS_MODULE_DIR_.'newsletteradmin_old')){
					@smartCopy(_PS_MODULE_DIR_.'newsletteradmin_old/logs', _PS_MODULE_DIR_.'newsletteradmin/logs', $options=array('folderPermission'=>0644,'filePermission'=>0755));
					@smartCopy(_PS_MODULE_DIR_.'newsletteradmin_old/mails', _PS_MODULE_DIR_.'newsletteradmin/mails', $options=array('folderPermission'=>0644,'filePermission'=>0755));
					@smartCopy(_PS_MODULE_DIR_.'newsletteradmin_old/lang', _PS_MODULE_DIR_.'newsletteradmin/lang', $options=array('folderPermission'=>0644,'filePermission'=>0755));
					@smartCopy(_PS_MODULE_DIR_.'newsletteradmin_old/cron', _PS_MODULE_DIR_.'newsletteradmin/cron', $options=array('folderPermission'=>0644,'filePe44ission'=>0755));
					deltree(_PS_MODULE_DIR_.'newsletteradmin_old');
				}
				if($error == 0) {
					$this->_returnHTML .=  $this->l('End of install without any error, you are a good guy!').'
					<center><a class="button" href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&token='.Tools::getAdminTokenLite('AdminNewsletter').'">
						'.$this->l('Go to the module!').'</a>
					</center>'.$bottom.'<br/><center><img src="http://eolia.o2switch.net/newsletter/Email-Marketing-300x300.jpg"/></center>';
				} else {
					$this->_returnHTML .=  $this->l('Install finished but you have some errors').': ('.$error.')<br/>';
					$lines = file('../modules/newsletteradmin/install_errors.txt');
						foreach ($lines as $lineNumber => $lineContent)
						{
							$this->_returnHTML .= '<br/>'.($lineNumber + 1).' -  '.$lineContent;
						}
					$this->_returnHTML .= '<br/><br/><center><a class="button" href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&token='.Tools::getAdminTokenLite('AdminNewsletter').'">
						'.$this->l('Go to the module!').'</a>
					</center>'.$bottom;
					unlink('../modules/newsletteradmin/install_errors.txt');
				}
			}
		}
		
		return $this->_returnHTML;
	}

	private function countBirthCustomers() 
	{
		$date = date("m-d");
		$date_to = date("m-d", strtotime("+1 month"));
		if ($date_to < $date) {
			$where = " ( DATE_FORMAT(birthday, '%m-%d') BETWEEN '".$date."' and '12-31'
					OR DATE_FORMAT(birthday, '%m-%d') BETWEEN '01-01' and '".$date_to."')";		
		}
		elseif ($date_to == $date) {
			$where = " DATE_FORMAT(birthday, '%m-%d') = '".$date."'";
		}
		else {
			$where = " DATE_FORMAT(birthday, '%m-%d') BETWEEN '".$date."' and '".$date_to."'";
		}
		$query = "
			SELECT count(*) as nb
			FROM  `"._DB_PREFIX_."customer` 
			WHERE `newsletter` = 1
			AND active != 0
			AND deleted != 1
			AND id_shop = ".(int)$this->context->shop->id."
			AND ".$where
			;
		
		return Db::getInstance()->getValue($query);
	}
		
	private function getBirthdayCustomers()
	{
		
		$date_from = date_format(date_create_from_format('d-m', Tools::getValue('date_from')), 'm-d');
		$date_to = date_format(date_create_from_format('d-m', Tools::getValue('date_to')), 'm-d');
		
		if ($date_to < $date_from) {
			$where = " ( DATE_FORMAT(birthday, '%m-%d') BETWEEN '".$date_from."' and '12-31'
					OR DATE_FORMAT(birthday, '%m-%d') BETWEEN '01-01' and '".$date_to."')";		
		}
		elseif ($date_to == $date_from) {
			$where = " DATE_FORMAT(birthday, '%d-%m') = '".Tools::getValue('date_from')."'";
		}
		else {
			$where = " DATE_FORMAT(birthday, '%m-%d') BETWEEN '".$date_from."' and '".$date_to."'";
		}

		return Db::getInstance()->ExecuteS("
	        SELECT
				id_gender,
	            email,
	            lastname,
	            firstname
	        FROM `"._DB_PREFIX_."customer`
	        WHERE `newsletter` = 1
			AND active != 0
			AND deleted != 1
			AND id_shop = ".(int)$this->context->shop->id."
			AND ".$where);
	}
	
	private function countBlockSub() 
	{
		$return = Db::getInstance()->getValue('
			SELECT count(*) as nb
			FROM `'._DB_PREFIX_.'newsletter`
			WHERE active != 0
			AND id_shop = '.(int)$this->context->shop->id);			
		return ($return == false) ? $this->l('The BlockNewsletter is not installed or no subscriber') : $return;
	}

	private function getBlockSubscribers()
	{
		return Db::getInstance()->ExecuteS("
			SELECT email
			FROM `"._DB_PREFIX_."newsletter` 
			WHERE active != 0
			AND id_shop = ".(int)$this->context->shop->id);
	}
	
	private function countsex($id_gender) 
	{
		return Db::getInstance()->getValue('
			SELECT count(*) as nb
			FROM `'._DB_PREFIX_.'customer` 
			WHERE active != 0
			AND deleted != 1
			AND id_shop = '.(int)$this->context->shop->id.'
			AND id_gender = '.(int)$id_gender);			
	}
	
	private function getSexCustomers($id_gender)
	{
		return Db::getInstance()->ExecuteS("
			SELECT
				id_gender,
				email,
				lastname,
				firstname
			FROM `"._DB_PREFIX_."customer` 
	        WHERE deleted != 1
			AND active != 0
			AND id_shop = ".(int)$this->context->shop->id."
			AND id_gender = ".(int)$id_gender);      
	}
	
	private function countlang($id_lang) 
	{
		return Db::getInstance()->getValue('
			SELECT count(email) as nb
			FROM `'._DB_PREFIX_.'customer` 
			WHERE active = 1
			AND `newsletter` = 1
			AND deleted != 1
			AND id_shop = '.(int)$this->context->shop->id.'
			AND id_lang = '.(int)$id_lang);			
	}
	
	private function getCustomerByLang($id_lang)
	{
		return Db::getInstance()->ExecuteS('
			SELECT
				email,
				lastname,
				firstname
			FROM `'._DB_PREFIX_.'customer` 
	        WHERE deleted != 1
			AND `newsletter` = 1
			AND active != 0
			AND id_shop = '.(int)$this->context->shop->id.'
			AND id_lang = '.(int)$id_lang);	     
	}
	
	private function countCustomers() 
	{	
		return Db::getInstance()->getValue('
			SELECT count(*) as nb
			FROM `'._DB_PREFIX_.'customer` 
			WHERE active != 0
			AND id_shop = '.(int)$this->context->shop->id.'
			AND deleted != 1');
	}	
	
	private function getCustomers()
	{
		return Db::getInstance()->ExecuteS("
			SELECT
				id_gender,
				email,
				lastname,
				firstname
			FROM `"._DB_PREFIX_."customer` 
			WHERE deleted != 1
			AND active != 0
			AND id_shop = ".(int)$this->context->shop->id);      
	}
	
	private function countCustomersSub() 
	{			
		return Db::getInstance()->getValue('
			SELECT count(*) as nb
			FROM `'._DB_PREFIX_.'customer` 
			WHERE `newsletter` = 1
			AND active != 0
			AND deleted != 1
			AND id_shop = '.(int)$this->context->shop->id);
	}
		
	private function getNewsletteremails()
	{
		return Db::getInstance()->ExecuteS("
			SELECT
				id_gender,
				email,
				lastname,
				firstname
			FROM `"._DB_PREFIX_."customer`
			WHERE deleted != 1
			AND active != 0
			AND newsletter != 0
			AND id_shop = ".(int)$this->context->shop->id);  
	}
	
	private function countOptCustomers() 
	{	
		return Db::getInstance()->getValue('
			SELECT count(*) as nb
			FROM  `'._DB_PREFIX_.'customer` 
			WHERE `optin` = 1
			AND `newsletter` = 1
			AND  active != 0
			AND deleted != 1
			AND id_shop = '.(int)$this->context->shop->id);
	}
	
	private function getOptCustomers()
	{
		return Db::getInstance()->ExecuteS("
			SELECT
				id_gender,
				email,
				lastname,
				firstname
			FROM `"._DB_PREFIX_."customer`
			WHERE deleted != 1
			AND active != 0
			AND newsletter != 0
			AND `optin` = 1
			AND id_shop = ".(int)$this->context->shop->id);  
	}

	private function countNoReadCustomers() 
	{
		$id_campaign 	= Db::getInstance()->getValue('SELECT MAX(id_campaign) FROM '._DB_PREFIX_.'mailing_history WHERE id_shop = '.$this->context->shop->id);
		$nbr_sent 		= Db::getInstance()->getValue("select count(*) FROM `"._DB_PREFIX_."mailing_sent` as m WHERE m.id_campaign = '$id_campaign' AND id_shop = ".$this->context->shop->id);
		$nbr_received = Db::getInstance()->getValue("select count(*) FROM `"._DB_PREFIX_."mailing_track` as t WHERE t.id_campaign = '$id_campaign' AND id_shop = ".$this->context->shop->id);
		$result = ($nbr_sent - $nbr_received);	
		if ($nbr_received>$nbr_sent) 
			$result = '0';
		if ($nbr_received == 0)
			$result = $this->l('Please wait, no email has been opened');
		return ($result); 
	}
	
	private function getNoReadCustomers()
	{
		$id_campaign = Db::getInstance()->getValue("SELECT MAX(id_campaign) FROM "._DB_PREFIX_."mailing_history WHERE id_shop = ".$this->context->shop->id);

		return Db::getInstance()->ExecuteS("
			SELECT DISTINCT s.email 
			FROM 	
				`"._DB_PREFIX_."mailing_sent` as s,
				`"._DB_PREFIX_."mailing_track` as t
			WHERE s.id_campaign = ".(int)$id_campaign."
		    AND t.id_campaign = ".(int)$id_campaign."
		    AND s.email != t.email
		    AND s.id_shop = ".(int)$this->context->shop->id); 
	}
	
  	private function getGroupCustomers($valueGroup) 
    {
		return	Db::getInstance()->ExecuteS("
			SELECT 
				c.id_gender,c.email,c.lastname,c.firstname,c.id_customer,
				cg.id_customer, cg.id_group
			FROM 
				`"._DB_PREFIX_."customer` as c,
				`"._DB_PREFIX_."customer_group` as cg
			WHERE c.id_customer = cg.id_customer
			AND cg.id_group = ".(int)$valueGroup."
			AND c.deleted != 1
			AND c.active != 0
			AND c.newsletter != 0
			AND c.id_shop = ".(int)$this->context->shop->id);
    }
	
	private function countCsvCustomers() 
	{	
		return Db::getInstance()->getValue('
			SELECT count(*) as nb 
			FROM  `'._DB_PREFIX_.'mailing_import` 
			WHERE id_shop = '.(int)$this->context->shop->id.' 
			AND `group` = "temp"');
	}	
	
	private function getCsvCustomers()
	{
		return Db::getInstance()->ExecuteS("
			SELECT
				email,
	            lastname,
	            firstname
			FROM `"._DB_PREFIX_."mailing_import`
			WHERE id_shop = ".(int)$this->context->shop->id." 
			AND `group` = 'temp'");      
	}
	
	private function getCsvGroupCustomers($valuecsv)
	{

		return Db::getInstance()->ExecuteS("
		SELECT
			email,
            lastname,
            firstname
		FROM `"._DB_PREFIX_."mailing_import` 
		WHERE `group` = '".$valuecsv."'
		AND id_shop = ".(int)$this->context->shop->id);      
	}
	
	private function listrep()
	{
		global $Key;
		$template_dir = '../modules/newsletteradmin/templates/';
		$rep = _PS_ROOT_DIR_.'/newsletters';
		if (file_exists ($rep))	
		{
			$this->html .= '
	
				<fieldset>
				<legend class="hide" title="Click to show/hide">'.$this->l('Archives').':</legend>	
				<div style="display:none">
				<form name="redirect" > 
				<div>'.$this->l('Remember to replace the variables in your text if you copy/paste an archive in your message').'</div>
				<div>'.$this->l('Yours backups are stored in the created directory:').'<b>'.$rep.'</b></div>
				</br>
				<div style="margin-left:15px; float:right; font-size:12px; min-width:400px;">';
				
			//report of the last campaign
			$this->html .=  $this->l('Emails read in the last campaign');	
			$id_campaign =Db::getInstance()->getValue("SELECT MAX(id_campaign) FROM "._DB_PREFIX_."mailing_history WHERE id_shop = ".$this->context->shop->id);
			if ($id_campaign <= 0)
				$this->html .=  ':<p style="color:red; text-align:center;">'.$this->l('No campaign registered!').'</p><br/></div>';
			else
			{
				$nbrs_enreg = Db::getInstance()->getValue("SELECT COUNT(*) as compt FROM "._DB_PREFIX_."mailing_track WHERE id_campaign ='$id_campaign' AND id_shop = ".$this->context->shop->id);		
				$res2 = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."mailing_history WHERE id_campaign ='$id_campaign' AND id_shop = ".$this->context->shop->id);			
				$num_sent = $res2['num_sent'];
				$date_sent = $res2['date'];
				$impact = ($nbrs_enreg/$num_sent)*100;
				$ratio = number_format($impact, 2, ',', ' ');
				$this->html .= " N° ".$id_campaign."<b> >> </b>".$date_sent.": ".$nbrs_enreg."/".$num_sent."&nbsp;&nbsp;(<b>".$ratio."%</b>)<br/><br/>
					<a href=\"../modules/newsletteradmin/history.php?key=".$Key."&id_shop=".$this->context->shop->id."\" target=popup class=\"button\" onclick=\"window.open('','popup','width=750,height=434,left=50,top=50')\">".$this->l('See reports')."</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href=\"../modules/newsletteradmin/truncate.php?key=".$Key."&id_shop=".$this->context->shop->id."\" target=popup class=\"button\" onclick=\"if(!confirm('".$this->l('Are you sure to delete your results ?')." ')){return false }else{window.open('','popup', 'width=350, height=200, left=50, top=50')} \">".$this->l('Drop results') ."</a></div>";
			}
				
			$this->html .= $this->l('See or delete your backups').':</br><br/>
					<a href="../newsletters/list.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" target="popup" class="button" onclick="window.open(\'\',\'popup\',\'width=750,height=380,left=50,top=50,resizable=yes, scrollbars=yes\')" title="'.$this->l('See or delete your backups').'">'.$this->l('Manage backups').'</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="'.$template_dir.'list.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" target="popup" class="button" onclick="window.open(\'\',\'popup\',\'width=750,height=380,left=50,top=300,resizable=yes, scrollbars=yes\')" title="'.$this->l('See or delete your templates (html and css files)').'">'.$this->l('Manage templates').'</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="../modules/newsletteradmin/logs/list.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" target="popup" class="button" onclick="window.open(\'\',\'popup\',\'width=750,height=380,left=50,top=50,resizable=yes, scrollbars=yes\')" title="'.$this->l('See or delete your logs').'">'.$this->l('View logs').'</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="../modules/newsletteradmin/cron/list.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" target="popup" class="button" onclick="window.open(\'\',\'popup\',\'width=750,height=380,left=50,top=50,resizable=yes, scrollbars=yes\')" title="'.$this->l('See or delete your scheduled tasks').'">'.$this->l('View Cron Tasks').'</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="../modules/newsletteradmin/bounces.php?key='.$Key.'&id_shop='.$this->context->shop->id.'" target="popup" class="button" onclick="window.open(\'\',\'popup\',\'width=950,height=450,left=50,top=50,resizable=yes, scrollbars=yes\')" title="'.$this->l('Delete invalid imported emails in your database').'">'.$this->l('Clean bounces').'</a>
					</form><br/>
					</div>
					</fieldset></br>';
				    	
		}
		else
		{	
			$this->displayWarning('

				<center><b>'.$this->l('WARNING: Archives unavailable!') .'</b>
				</br>'.$this->l('The directory /newsletters don\'t exist!') .'</br>'.$this->l(' If you want to keep archives re-configure your module here').': <a href="'.$_SERVER["PHP_SELF"].'?controller=AdminNewsletter&install=1&step=4&token='.Tools::getAdminTokenLite('AdminNewsletter').'&error=0"><img src="../img/admin/cog.gif" alt="Reset" title="'.$this->l('Reset').'"></a></center>');
		}
	}
			
	function remove_accents($str, $charset='utf-8')//to remove specials characters in filenames
	{
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); 
		$str = preg_replace('#&[^;]+;#', '', $str);
		$str = str_replace("\\","-", $str);
		$str = str_replace("/","-", $str);
		return $str;
	}
	
	private function getShopUrl()
	{
		$ShopUrl = Db::getInstance()->getRow("
		SELECT
			domain,
			domain_ssl,
            physical_uri,
            virtual_uri
		FROM
			`"._DB_PREFIX_."shop_url`
        WHERE  id_shop = ".$this->context->shop->id); 
		if(Configuration::get('PS_SSL_ENABLED') || (!empty($_SERVER['HTTPS'])&& strtolower($_SERVER['HTTPS']) != 'off'))
			return 'https://'.$ShopUrl['domain_ssl'].$ShopUrl['physical_uri'].$ShopUrl['virtual_uri'];
		else
			return 'http://'.$ShopUrl['domain'].$ShopUrl['physical_uri'].$ShopUrl['virtual_uri'];
	}
	
	function backSend($currentIndex,$errorMessage)
	{
		
		$this->_returnHTML .= '
			<form action="' . $currentIndex . '&token=' . $this->token .'" method="post" >
			<div align="center"><br />
				<input type="submit" value="&nbsp;'.$this->l('Back').'&nbsp;" class="button" />
			</div>
			</form>';
	}
	
	function recursive_in_array($needle, $haystack) 
	{
		foreach ($haystack as $stalk)
			if ($needle === $stalk || (is_array($stalk) && $this->recursive_in_array($needle, $stalk)))
				return true;

		return false;
	}

}