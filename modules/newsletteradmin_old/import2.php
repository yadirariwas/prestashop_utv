<?php
/* 
 * Importe des fichiers csv et txt et créé les groupes
 * @module newsletteradmin Compatible only PS >= 1.5.x.x
 * @copyright eolia@o2switch.net
 * Version 1.5 - 21/12/2013
*/
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
	$rewrited_url = __PS_BASE_URI__;
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	if (@$_GET['key'] != $Key) die('Bad request...Wrong key.');
	else{
	$filename = 'IMPORT';		
	$table   = _DB_PREFIX_.'mailing_import';

	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
				<title>'.trans('Import CSV File').'</title>
				</head>
					<body>
					<div class="newsfield" >
					<div style="float: right; color:green">Secure key is ok </div>
					<legend><img src="logo.gif"  alt="" title="Import" /> '.trans('Import csv file into your database').' </legend>
					<br/>
					<label><b>'.trans('Writing in database').'...</b><br/><br/></label>
			';

	$group = trim($_POST['group_name']);
	if (empty($group)) { 
			
			echo trans('You must choose a name for this group').' !<br />'.trans('Import stopped').'.
				<center><input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" />
				</center>
				<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
						</div></div>
						</body>
						</html>';
			exit();
			}
	$fichier = dirname(__FILE__).'/temp.txt';
	
	if(file_exists($fichier))
	{
		echo trans('Emails imported to the group').': <b>'.$group.'</b><br/><br/>';
		$fp = fopen("$fichier", "r") ;
		while ($ligne = fgets($fp)) 
		{
			if ($ligne != NULL)
			{
				$ligne = str_replace('"','', $ligne);//compatibilité excel
				$liste = explode( ";",$ligne);
				$email = trim(strtolower($liste[0]));
				$nom = !empty($liste[1]) ? utf8_decode(addslashes(trim($liste[1]))) : '';
                $prenom = !empty($liste[2]) ? utf8_decode(addslashes(trim($liste[2]))) : '';
				if (!Validate::isEmail($email)) 
					echo trans('This address will be not imported because the email syntax is invalid').'! ('.$email.')<br />';
				else
					$text[] = "('".$_GET['id_shop']."','ID','".$email."','".utf8_encode($nom)."','".utf8_encode($prenom)."','".$group."')". PHP_EOL;
			} 
			else  
				echo '<br><font color="red"><b>'.trans('Warning, last lines of your file are empty').'!</b></font><br/>';
		}

		fclose($fp);
		
		if(isset($text))
		{
				try
				{	
					Db::getInstance()->Execute('OPTIMIZE TABLE '.$table);
					Db::getInstance()->Execute("
							INSERT IGNORE INTO `".$table."`(`id_shop`,`ID`,`email`,`lastname`,`firstname`,`group`) 
							VALUES ".implode(',', $text)
							);
					unlink($fichier);
				}
				catch(PrestaShopDatabaseException $e) 
				{
					echo '<center><font color="red">';
					$array = (array)$e;
					if(@preg_grep("/Duplicate/",$array) == true) 
						echo trans('This group name is already in use, please choose another one!');
					else 
						echo trans('Database error').' : '.$e;
						
					echo '<br/>'.trans('Import stopped').'.<br/><br/></font>
						<input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" /></center>
						<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
						</div></div>
						</body>
						</html>';
					exit();
				}

		echo '<br/>
			<center>'.count($text).' '.trans('imported address').'<br /><br /><b>'.trans('IMPORT SUCCESS').'!</b><br/><br/>
			<input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-2)" /></center><br/>
		';
		}
	}
		else 
				echo '<center><font color="red">'.trans('ERROR! No file...').'</font><br/><br/>
			<input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-2)" /></center><br/>';
				
	echo '<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div>
				</div></div>
			</body>
		</html>';
}
?>