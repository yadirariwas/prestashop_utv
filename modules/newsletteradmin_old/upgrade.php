<?php
/* 
 * @Mise � jour auto 
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2013
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
$filename = 'UPGRADE';

	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;	
	
	if (!isset($_GET['step']) || !isset($_GET['key'])) 
		die('Invalid request');
		
	$step = $_GET['step'];
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	
	if (isset($_GET['key']) AND $_GET['key'] == $Key)
	{
			$token = Configuration::get('Newsletter_Token');
			$tokenNews = Configuration::get('tokenNews');
			$ref = Configuration::get('Admin_Index');
			ini_set("user_agent","Mozilla 5.0 for AdminModules");
			$wait = '<script>document.getElementById("message").style.display = "block";</script>';
			$bottom = '<div style="font-size:11px;text-align:right"> Eolia.o2switch.net &copy;</div></div></body>';
								
			$html = '
				<!DOCTYPE html >
					<html>
					<head>
						<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css">
						<link rel="stylesheet" type="text/css" href="views/css/admin.css">
						<title>'.trans('Module Updater').'</title>
					</head>
					<body>
						<div class="newsfield" >
							<legend><img src="logo.gif"  alt="" title="Update" /> <a style="margin: 0;padding: 0.2em 0.5em;font-weight: bold;text-align: left;">'.trans('Updating Newsletteradmin Module').' </a></legend>
							<br/>
								<div id="message" style="display:none;"><p class="center"><img src="../../img/loader.gif" alt="" /><br/> '.trans('Please wait').'...</p><br/>
								</div>';
		if ($step == 0)
				{	
					$html .= $wait.'<label><b>'.trans('Checking the latest version').'...</b><br/><br/></label><br/><br/>'.$bottom;
					$html .= '<meta http-equiv="refresh" content="1;URL='.$_SERVER["PHP_SELF"].'?step=1&key='.$Key.'">';
				}	
				
		$xml_link = 'http://www.eolia.o2switch.net/newsletter/xml/version2.xml';
		if (@fclose(@fopen($xml_link, 'r'))){
			$feed = @simplexml_load_file("http://www.eolia.o2switch.net/newsletter/xml/version2.xml");
			$num = $feed->version->num;
			$name = $feed->version->name;
			$link = $feed->download->link;
			if ($step == 1)
					{				
						if ($name != null) {
								$html .= $wait.'<label><b>'.trans('Last version online').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('Ok, downloading files').'...<br/>
												<br/>
										</center>'.$bottom.'<meta http-equiv="refresh" content="1;URL='.$_SERVER["PHP_SELF"].'?step=2&key='.$Key.'">';
						}
						else
							$html .= '<label><b>'.trans('Last version online').': </b><br/><br/></label><br/><br/><center>'.trans('xml request error! Try later').':<br/>
												<br/>
										</center>'.$bottom.'<meta http-equiv="refresh" content="1;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
					}
						
			if ($step == 2)
					{				
						if($link != null) 
						{ 
							$testurl = 'http://www.eolia.o2switch.net/newsletter/download/newsletteradmin2.zip';
							$url = 'http://www.eolia.o2switch.net/newsletter/download/counter.php?f=newsletteradmin2.zip';
							
							if (@fclose(@fopen($testurl, 'r'))){
								$tempFile = '../upgrade_PSNewsLetter.zip';

								$out = fopen($tempFile , 'wb');
								
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_FILE, $out);
								curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla 5.0 for AdminModules");
								curl_setopt($ch, CURLOPT_HEADER, false);
								curl_setopt($ch, CURLOPT_URL, $url);
								curl_exec($ch);

								$info = curl_getinfo($ch);
								
								
									if($info['http_code'] == 200 ) {
										$files = _PS_MODULE_DIR_.'newsletteradmin';
										$files2 =  _PS_MODULE_DIR_.'newsletteradmin_old';
										smartCopy($files, $files2, $options=array('folderPermission'=>0755,'filePermission'=>0755)) or die('error');
										@deltree($files);
										sleep(1);
										
									
										include(dirname(__FILE__).'/../../tools/pclzip/pclzip.lib.php');
										if (!file_exists($tempFile))
											{
												$html .= $wait.'<label><b>'.trans('Extracting file').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('Error!').':<br/><br/></center>'.$bottom.'<meta http-equiv="refresh" content="2;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
											}
										$archive = new PclZip($tempFile);
										
										if ($archive->extract(PCLZIP_OPT_PATH, '../',
															PCLZIP_OPT_REMOVE_PATH, $tempFile) == 0)
											{
												rename($files2,$files) or die('Error when changing the name of the directory, Please rename '._PS_MODULE_DIR_.'newsletteradmin to '._PS_MODULE_DIR_.'newsletteradmin_old yourself.');
												$html .= '<label><b>'.trans('Extracting file').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('Error!').':'.$archive->errorInfo(true).'<br/>'.trans('Your files have been restored, Update canceled').'.<br/></center>'.$bottom.'<meta http-equiv="refresh" content="3;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
											} else {
												fclose($out);
												$html .= $wait.'<label><b>'.trans('Extracting file').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('files were unpacked in the directory').':<br/>'.$files.'<br/></center>'.$bottom.'<meta http-equiv="refresh" content="3;URL=../newsletteradmin_old/upgrade.php?step=3&key='.$Key.'">';
											}
									}else
										$html .= '<label><b>'.trans('Extracting file').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('Server Error, Please try later').':<br/><br/></center>'.$bottom.'<meta http-equiv="refresh" content="3;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
							}else
								$html .= '<label><b>'.trans('Downloading file').': '.$name.'</b><br/><br/></label><br/><br/><center>'.trans('Zip Archive missing on the server, Please try later').':<br/><br/></center>'.$bottom.'<meta http-equiv="refresh" content="3;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
						}
						else
							$html .= '<label><b>'.trans('Downloading file').': </b><br/><br/></label><br/><br/><center>'.trans('xml request error! Try later').':<br/>
												<br/>
										</center>'.$bottom.'<meta http-equiv="refresh" content="2;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
					}							
						
			if ($_GET['step'] == 3)
				{					
					$reset = $ref.'?controller=AdminModules&token='.$token.'&module_name=newsletteradmin&reset&tab_module=emailing';
					$html .= $wait.'<label><b>'.trans('End of installation').'!</b><br/><br/></label><br/><br/><center>'.trans('Resetting the updated module in progress').'...<br/><br/>
									</center>'.$bottom.'<meta http-equiv="refresh" content="3;URL='.$reset.'">';
				}					
									
						 
						
		}else $html .= '<br/><br/><br/><br/><center>'.trans('xml request error! Try later').':<br/><br/>
						</center>'.$bottom.'<meta http-equiv="refresh" content="2;URL='.$ref.'?controller=AdminNewsletter&token='.$tokenNews.'">';
		echo $html;	
		exit;
	}else 
		die('Invalid request');
	
?>	