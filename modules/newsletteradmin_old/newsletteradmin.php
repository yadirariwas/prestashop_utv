<?php
/**
 * Send a Newsletter from the Prestashop Back-Office
 * @category admin
 * @author  Eolia  27/06/2015  compatible PS >= 1.5.x.x ONLY !
 * @version 5.7
 *
 */
if (!defined('_PS_VERSION_') || !defined('_CAN_LOAD_FILES_'))
	exit;
 		
			
// Checking compatibility with older PrestaShop and fixing it
if (!defined('_MYSQL_ENGINE_'))
	define('_MYSQL_ENGINE_', 'MyISAM');
 
class NewsletterAdmin extends Module
{ 
	
	/******************************************************************/
	/** Construct Method **********************************************/
	/******************************************************************/
	
	public function __construct ()
	{
    $this->name = 'newsletteradmin';
    $this->tab = 'emailing';
    $this->version = '5.7';
    $this->author = 'Eolia';
	$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7');

    parent::__construct();

    $this->confirmUninstall = $this->l('Delete your Newsletter module ?');
    $this->displayName = $this->l('Newsletter for Prestashop');
    $this->description = $this->l('Sending newsletter from the Back-Office');
	$this->description .= $this->ckeckModuleUpgrade();
	$this->addAsTrusted();
	$this->bootstrap = false;
	
	}

	public function install()
	{
		include(dirname(__FILE__).'/sql/install.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
				return false;

		if (!$this->checkDump()) {
			$this->context->controller->errors[] =  $this->l( 'Sorry but your database can\'t be restored, use the backup.sql file or your personal backup and restore it.');
			return false;
        }	
		
		if (!parent::install())
			return false;

		Configuration::updateGlobalValue('Admin_Newsletter_Version', '0.0');

		$tab = new Tab();
		$tab->class_name = 'AdminNewsletter';
		$tab->id_parent = Tab::getIdFromClassName('AdminCustomers');
		$tab->module = $this->name;
		$tab->name[(int)(Configuration::get('PS_LANG_DEFAULT'))] = $this->l('Send a newsletter');
		if(!$tab->add())
			return false;
			
		Tools::redirectAdmin('index.php?controller=AdminNewsletter&token='.Tools::getAdminTokenLite('AdminNewsletter'));
			return true;
	}
	
	public function uninstall()
	{	
		$database = $this->dumpMySQL();
		include(dirname(__FILE__).'/sql/uninstall.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
				return false;

		Configuration::deleteByName('Admin_Newsletter_Version');
		Configuration::deleteByName('NEWSLETTER_KEY_CODE');

		$tab = new Tab(Tab::getIdFromClassName('AdminNewsletter'));
		if(!$tab->delete())
			return false;

		if (file_exists(_PS_ROOT_DIR_.'/track.php')) 
			unlink(_PS_ROOT_DIR_.'/track.php');
		if (file_exists(_PS_ROOT_DIR_.'/unsubscribe.php')) 
			unlink(_PS_ROOT_DIR_.'/unsubscribe.php');

		if (!parent::uninstall())
			return false;

		return true;
	}
  
	private function ckeckModuleUpgrade()
	{
		if (!$this->context->controller instanceof AdminModulesController)
			return;
		ini_set("user_agent","Mozilla 5.0 for ".$_GET['controller']);
		$xml_link = 'http://www.eolia.o2switch.net/newsletter/xml/version2.xml';
		$update = Configuration::get('Admin_Newsletter_Check_Update');
		$updateOk = Configuration::get('Admin_Newsletter_Last_Version');
		if ($updateOk > $this->version)
		{	
			Configuration::updateGlobalValue('Newsletter_Token', Tools::getAdminTokenLite('AdminModules'));
			Configuration::updateGlobalValue('tokenNews', Tools::getAdminTokenLite('AdminNewsletter'));
			Configuration::updateGlobalValue('Admin_Index', $_SERVER["PHP_SELF"]);
			return  '<br></p><div class="hint" style="display: block">
					<span>'.$this->l('New stable release available').'&nbsp: '.$updateOk.'</span>
					<a class="button btn" style="float:right;border:1px solid #BBBBBB;background-color:#6AF4FA;" href="../modules/newsletteradmin/upgrade.php?step=0&key='.Configuration::get('NEWSLETTER_KEY_CODE').'">'.$this->l('Upgrade now').'&nbsp;!</a>
					</div><p><br>';
		}
		if(time() > ($update + 86400))
		{			 
			if(ini_get('allow_url_fopen') && @fclose(@fopen($xml_link,"r")))
			{
				$feed = @simplexml_load_file($xml_link);
				$num = $feed->version->num;
				$name = $feed->version->name;
				$link = $feed->download->link;					
				if ($this->version < $num)
					Configuration::updateGlobalValue('Admin_Newsletter_Last_Version', $num);
				Configuration::updateGlobalValue('Admin_Newsletter_Check_Update', time());	
			}
			else	
				return '<br>'.$this->l('Sorry, No information of new version avalaible!');
		}
		else
			return '<br>'.$this->l('Your version is up to date ');
			
	}

    public function addAsTrusted() {

        if (defined('self::CACHE_FILE_TRUSTED_MODULES_LIST') == true) {
            if (isset($this->context->controller->controller_name) &&
                    $this->context->controller->controller_name == 'AdminModules') {

                $sxe = new SimpleXMLElement('<theme/>');

                $modules = $sxe->addChild('modules');
                $module = $modules->addChild('module');
                $module->addAttribute('action', 'install');
                $module->addAttribute('name', $this->name);

                $trusted = $sxe->saveXML();
                file_put_contents(_PS_ROOT_DIR_ . '/config/xml/themes/' . $this->name . '.xml', $trusted);
                if(is_file(_PS_ROOT_DIR_ . Module::CACHE_FILE_UNTRUSTED_MODULES_LIST))
                    Tools::deleteFile(_PS_ROOT_DIR_ . Module::CACHE_FILE_UNTRUSTED_MODULES_LIST);
            }
        }
    }

	//Registration management 
	private function isCsv($csvEmail)
	{
        if (Db::getInstance()->getRow('SELECT `email` FROM '._DB_PREFIX_.'mailing_import WHERE `email` = \''.pSQL($csvEmail).'\''))
            return 1;
		else
			return 0;
	}  
	
	private function isNewsletterRegistered($Email)
    {
        if (Db::getInstance()->getRow('SELECT `email` FROM '._DB_PREFIX_.'newsletter WHERE `email` = \''.pSQL($Email).'\'AND id_shop = '.$this->context->shop->id))
            return 1;
        if (!$registered = Db::getInstance()->getRow('SELECT `newsletter` FROM '._DB_PREFIX_.'customer WHERE `email` = \''.pSQL($Email).'\' AND id_shop = '.$this->context->shop->id))
            return -1;
        if ($registered['newsletter'] == '1')
            return 2;
        return 0;
    }
  
	public function newsletterRegistration($email)
    {
		global $currentIndex, $cookie;
			
		$email = base64_decode($_GET['SID']);
		$action = $_GET['action'];
				
		if (empty($email) || $action !='0' && $action != '1') 
			header('Location: index.php');
		if (!Validate::isEmail(pSQL($email)))
			return  $this->l('Invalid e-mail address');
					
		/* Unsubscription */
			$mail_dir = _PS_MODULE_DIR_ . 'newsletteradmin/subscription/mails/';
			$var_iso = $cookie->id_lang;
			$language = Language::getLanguage(intval($cookie->id_lang));
			if(!is_dir($mail_dir.$language['iso_code'].'/'))
			{
				if(@rename($mail_dir.'en',$mail_dir.$language['iso_code'])) echo '';
				else $var_iso = 1;
			}
			elseif ($action == '1')
			{
				$csvStatus = $this->isCsv($email);
				$registerStatus = $this->isNewsletterRegistered($email);
				if ($registerStatus < 1 AND $csvStatus < 1)
					return $this->l('This e-mail address').' (<b>'.$email.'</b>) '.$this->l('is not registered in our database').'.';
				/* If the user ins't a customer */
				elseif ($registerStatus == 1)
				{
					if (!Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'newsletter WHERE `email` = \''.pSQL($email).'\' AND id_shop = '.$this->context->shop->id))
						return $this->l('Error during unsubscription').'.';

					Mail::Send(intval($var_iso), 'unsubscribe_conf', Mail::l($this->l('Unsubscribe our Newsletter')), array('{email}' => $email), pSQL($email), NULL, NULL, NULL, NULL, NULL, $mail_dir);						
					return $this->l('You are now unsubscribed from our mailing list').'. '.$this->l('Your e-mail').' (<b>'.$email.'</b>) '.$this->l('has been removed from our database').'. </br></br> '.$this->l('A confirmation mail has been sent').'.';

				}
				/* If the user is a customer */
				elseif ($registerStatus == 2)
				{
					if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer SET `newsletter` = 0 WHERE `email` = \''.pSQL($email).'\' AND id_shop = '.$this->context->shop->id))
						return $this->l('Error during unsubscription').'.';

					Mail::Send(intval($var_iso), 'unsubscribe_conf', Mail::l($this->l('Unsubscribe our Newsletter')), array('{email}' => $email), pSQL($email), NULL, NULL, NULL, NULL, NULL, $mail_dir);							
					return $this->l('You are now unsubscribed from our mailing list').'. '.$this->l('Your e-mail').' (<b>'.$email.'</b>) '.$this->l('has been removed from our database').'. </br></br> '.$this->l('A confirmation mail has been sent').'.';
				}
				/* If the user is in a csv list */
				elseif ($csvStatus == 1)
				{
					if (!Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'mailing_import WHERE `email` = \''.pSQL($email).'\''))
						return trans('Error during unsubscription').'.';

					Mail::Send(intval($var_iso), 'unsubscribe_conf', Mail::l($this->l('Unsubscribe our Newsletter')), array('{email}' => $email), pSQL($email), NULL, NULL, NULL, NULL, NULL, $mail_dir);							
					return $this->l('You are now unsubscribed from our mailing list').'. '.$this->l('Your e-mail').' (<b>'.$email.'</b>) '.$this->l('has been removed from our database').'. </br></br> '.$this->l('A confirmation mail has been sent').'.';
                }
			}
		/* Subscription */
			elseif ($action == '0')
			{
				$registerStatus = $this->isNewsletterRegistered(pSQL($email));
				if ($registerStatus > 0)
					return $this->l('This e-mail address').' (<b>'.$email.'</b>) '.$this->l('is already registered').'.';
				/* If the user ins't a customer */
				elseif ($registerStatus == -1)
				{
					if (!Db::getInstance()->Execute('
								INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
								VALUES('.$this->context->shop->id.','.$this->context->shop->id_shop_group.',\''.pSQL($email).'\',
								NOW(),
								\''.pSQL(Tools::getRemoteAddr()).'\',
								(
									SELECT c.http_referer
									FROM '._DB_PREFIX_.'connections c
									WHERE c.id_guest = '.(int)$this->context->customer->id.'
									ORDER BY c.date_add DESC LIMIT 1
								),1
								)'))
						return $this->l('Error during subscription').'.';
							
					Mail::Send(intval($var_iso), 'newsletter_conf', Mail::l($this->l('Subscribe our Newsletter')), array('{email}' => $email), pSQL($email), NULL, NULL, NULL, NULL, NULL, $mail_dir);
					return $this->l('You are now subscribed to our mailing list').'. '.$this->l('Your e-mail').' (<b>'.$email.'</b>) '.$this->l('was added to our database').'. </br></br> '.$this->l('A confirmation mail has been sent').'.';
				}
				/* If the user is a customer */
				elseif ($registerStatus == 0)
				{
					if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL($_SERVER['REMOTE_ADDR']).'\' WHERE `email` = \''.pSQL($email).'\'AND id_shop = '.$this->context->shop->id))
						return $this->l('Error during subscription').'.';
						  
					Mail::Send(intval($var_iso), 'newsletter_conf', Mail::l($this->l('Subscribe our Newsletter')), array('{email}' => $email), pSQL($email), NULL, NULL, NULL, NULL, NULL, $mail_dir);
					return $this->l('You are now subscribed to our mailing list').'. '.$this->l('Your e-mail').' (<b>'.$email.'</b>) '.$this->l('was added to our database').'. </br></br> '.$this->l('A confirmation mail has been sent').'.';
				}
			}
    }

	private function checkDump() 
	{	
		$backup = _PS_MODULE_DIR_ . 'newsletteradmin/databasebackup';
		if (file_exists($backup))
		{
			$database = unserialize(implode('',file($backup)));
			unlink($backup);
			if(!$this->restoreDb($database))
				return false;
		}
		return true;
	}
		
	private function dumpMySQL()
	{
		$tables = array(_DB_PREFIX_.'mailing_history', _DB_PREFIX_.'mailing_import', _DB_PREFIX_.'mailing_sent', _DB_PREFIX_.'mailing_track');
		$entete  = "-- Eolia NewsletterAdmin SQL DataBase Dump\n";
		$entete .= "-- Server: "._DB_SERVER_." -- Database: "._DB_NAME_." --\n";
		$entete .= "-- Dump of NewsletterAdmin tables on ".date("d-M-Y")."\n";
		$entete .= "\n\n";
		$structure = "";
		
		if (!is_array($tables))
			$tables = array();
		$database = array();	
		foreach($tables as $table)
		{
			$structure .= "--\n";
			$structure .= "-- Table structure of ".$table."\n";
			$structure .= "--\n\n";

			if($structureTable = Db::getInstance()->ExecuteS("show create table ".$table))
			{
				foreach($structureTable as $createTable)
				{
					$structure .= $createTable["Create Table"].";\n\n";
					$columns = Db::getInstance()->ExecuteS("SHOW COLUMNS FROM ".$table);
					$donnees = Db::getInstance()->ExecuteS("SELECT * FROM ".$table);
					$database[$table] = $donnees;
					$insertions = "\n";
					$insertions .= "--\n";
					$insertions .= "-- Insertions in the table ".$table."\n";
					$insertions .= "--\n";
					if(count($donnees) > 0)
					{
						$insertions .= "INSERT INTO `".$table."` "; 
						$fields ='';
						$datas = '';

						foreach($columns as $line)
						{
							$fields .=  "`".addslashes($line['Field'])."`, ";
						}
						$fields = "(".substr($fields,0,-2).") VALUES \n";
						$insertions .= $fields;
						
						foreach($donnees as $line)
						{
							$datas .=  "(";
							foreach ($line as $key => $v) 
							{  
								$datas .=  !is_numeric($v)? "'".addslashes($v)."', " : addslashes($v).", ";
							}
							$datas = substr($datas,0,-2);	
							$datas .=  "),\n";
						}
						$datas = substr($datas,0,-2);
						$datas .=  ";\n\n\n";
						$insertions .= $datas;	
						$structure .= $insertions;
						unset($insertions);
					}
					else
						$insertions = '-- No data to insert --';
				}
			}
			else
				return false;
		}
	 
		if($dumpFile = fopen(_PS_MODULE_DIR_ . 'newsletteradmin/backup.sql', 'wb'))
		{
			fwrite($dumpFile, $entete);
			fwrite($dumpFile, str_replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS",$structure));
			fclose($dumpFile);
		}
		else	
			return false;
			
		$srzed = serialize($database);
		$fh = fopen(_PS_MODULE_DIR_ . 'newsletteradmin/databasebackup','w+');
		fwrite($fh,$srzed);
		fclose($fh);
		return true;
	}
	
	private function restoreDb($database)
	{
		foreach ($database as $table => $nb) {
			
			foreach ($database[$table] as $key => $values) {
			$sql = 'Insert into '.$table.' (';
			foreach ($values as $name=>$value) {
				$sql .= '`'.$name.'`,';
			}
			$sql = substr($sql,0,-1);
			$sql .= ') values (';
			foreach ($values as $name=>$value) {
				$sql .= !is_numeric($value)? "'".addslashes($value)."', " : addslashes($value).", ";
			}
			$sql = substr($sql,0,-2);
			$sql .= ');';
			if(!Db::getInstance()->Execute($sql))
				return false;
			}
		}
		return true;
	}
	
}