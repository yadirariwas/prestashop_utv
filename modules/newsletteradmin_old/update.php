<?php
/* 
 * Enregistrement des traductions internes
 * @module newsletteradmin 
 * @copyright eolia@o2switch.net 2013
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/functions.php');
	if(intval(Configuration::get('PS_REWRITING_SETTINGS')) === 1)
		$rewrited_url = __PS_BASE_URI__;
		
	$filename = 'UPDATE';
	$Key = Configuration::get('NEWSLETTER_KEY_CODE');
	$language = Language::getLanguage(intval(Configuration::get('PS_LANG_DEFAULT')));
	$iso = $language['iso_code'];
	
	if (@$_GET['key'] != $Key) die('Bad request...Wrong key.');
	else 
	{	
		$trans_lang = _PS_MODULE_DIR_.'newsletteradmin/lang/lang_'.$iso.'.php';
		include $trans_lang;
		
		if(file_exists ($trans_lang)) 
		{
			
			global $_LANG;	
				echo '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" type="text/css" href="views/css/newsletteradmin.css"/>
			<title>'.trans('Translations').'</title>
			</head>
			<body>
				<div class="newsfield" >
					<div style="float: right; color:green">Secure key is ok </div>
			<h1><img src="logo.gif"  alt="" title="Newsletter" />&nbsp;'.trans('Edit your files translations').'</h1>
					<h4>'.trans('Record').'...</h4><br/><br/>';
			$a = array_combine($_POST['key'], $_POST['name']);	
		
			foreach($a AS $champ => $value)
			{
				if ($value == '') 
				{
					echo '<center>'.trans('You have not entered all information').': '.$champ.' = ????????????
						<br />
						<input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" /></center>
						<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div></div>
						</html>';
					return ('error');	
				}
			}
			$wadd=fopen($trans_lang, "w");
			$r = chr(13);
			fwrite($wadd, '<?php'.PHP_EOL .PHP_EOL .'$_LANG = array();'.PHP_EOL);
					
			foreach($a AS $champ => $value)
			{	
				if ($champ != 'update')
				{	
					fwrite($wadd,'$_LANG[\''.secureslashes(html_entity_decode ($champ)).'\'] = \''.secureslashes(html_entity_decode ($value)).'\';'.PHP_EOL);
				}
			}
			fwrite($wadd,'?>');
			fclose($wadd);
			echo '<br/><b><center>'.count($a).' '.trans('translations updated!').'</b><br/><br/>
				<input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-2)" /></center>
				<div style="font-size:11px;text-align:right;color:grey"> Eolia.o2switch.net &copy;</div></div>
						</html>';
			
		}else die('<br/><center><br/><center>Your file '.$trans_lang.' not exists!, <br/>please use "Configure" in your module install<br/><input type="button" class="button" value="'.trans('Back').'" onClick="javascript:history.go(-1)" /></center>');
	}
	
?>
