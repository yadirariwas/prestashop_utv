<?php

class OrderDetail extends OrderDetailCore
{
	 
	public function saveTaxCalculator(Order $order, $replace = false)
	{
		// Nothing to save
		if ($this->tax_calculator == null)
			return true;

		if (!($this->tax_calculator instanceOf TaxCalculator))
			return false;

		if (count($this->tax_calculator->taxes) == 0)
			return true;

		if ($order->total_products <= 0)
			return true;

		$ratio = $this->unit_price_tax_excl / $order->total_products;
		$order_reduction_amount = $order->total_discounts_tax_excl * $ratio;
		$discounted_price_tax_excl = $this->unit_price_tax_excl - $order_reduction_amount;

		$values = '';
		foreach ($this->tax_calculator->getTaxesAmount($discounted_price_tax_excl) as $id_tax => $amount)
		{
			$unit_amount = (float)$amount;
			//$unit_amount = number_format($amount, 2, '.', '');
			//$unit_amount = (float)Tools::ps_round($amount, 2);
			$total_amount = $unit_amount * $this->product_quantity;
			$values .= '('.(int)$this->id.','.(float)$id_tax.','.$unit_amount.','.(float)$total_amount.'),';
		}

		if ($replace)
			Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'order_detail_tax` WHERE id_order_detail='.(int)$this->id);
			
		$values = rtrim($values, ',');
		$sql = 'INSERT INTO `'._DB_PREFIX_.'order_detail_tax` (id_order_detail, id_tax, unit_amount, total_amount)
				VALUES '.$values;
		
		return Db::getInstance()->execute($sql);
	}
	
	protected function setDetailProductPrice(Order $order, Cart $cart, $product)
	{
		$this->setContext((int)$product['id_shop']);
		Product::getPriceStatic((int)$product['id_product'], true, (int)$product['id_product_attribute'], 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $specific_price, true, true, $this->context);
		$this->specificPrice = $specific_price;

		$this->original_product_price = Product::getPriceStatic($product['id_product'], false, (int)$product['id_product_attribute'], 6, null, false, false, 1, false, null, null, null, $null, true, true, $this->context);
		$this->original_product_priceII = Product::getPriceStatic($product['id_product'], false, (int)$product['id_product_attribute'], 6, null, false, true, 1, false, null, null, null, $null, true, true, $this->context);
		$this->original_product_priceIII = Product::getPriceStatic($product['id_product'], true, (int)$product['id_product_attribute'], 6, null, false, true, 1, false, null, null, null, $null, true, true, $this->context);
		$this->product_price = $this->original_product_price;
		
		if (isset($_GET['controller']))
		{
			if ($_GET['controller'] == 'AdminOrders')
			{
				$this->unit_price_tax_incl = (float)$product['price_wt'];
				$this->unit_price_tax_excl = (float)$product['price'];
				$this->total_price_tax_incl = (float)$product['total_wt'];
				$this->total_price_tax_excl = (float)$product['total'];
			}
			else
			{
				$this->unit_price_tax_incl = $this->original_product_priceIII;
				$this->unit_price_tax_excl = $this->original_product_priceII;
				$this->total_price_tax_incl = $this->original_product_priceIII * (int)$product['cart_quantity'];
				$this->total_price_tax_excl = $this->original_product_priceII * (int)$product['cart_quantity'];
			}
		}
		else
		{
			
			$this->unit_price_tax_incl = $this->original_product_priceIII;
			$this->unit_price_tax_excl = $this->original_product_priceII;
			$this->total_price_tax_incl = $this->original_product_priceIII * (int)$product['cart_quantity'];
			$this->total_price_tax_excl = $this->original_product_priceII * (int)$product['cart_quantity'];
			
		}
		
		//(float)$product['total'];

        $this->purchase_supplier_price = (float)$product['wholesale_price'];
        if ($product['id_supplier'] > 0)
            $this->purchase_supplier_price = (float)ProductSupplier::getProductPrice((int)$product['id_supplier'], $product['id_product'], $product['id_product_attribute']);

		$this->setSpecificPrice($order, $product);

		$this->group_reduction = (float)(Group::getReduction((int)($order->id_customer)));

		$shop_id = $this->context->shop->id;

		$quantityDiscount = SpecificPrice::getQuantityDiscount((int)$product['id_product'], $shop_id,
			(int)$cart->id_currency, (int)$this->vat_address->id_country,
			(int)$this->customer->id_default_group, (int)$product['cart_quantity'], false, null, null, $null, true, true, $this->context);

		$unitPrice = Product::getPriceStatic((int)$product['id_product'], true,
			($product['id_product_attribute'] ? intval($product['id_product_attribute']) : null),
			2, null, false, true, 1, false, (int)$order->id_customer, null, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $null, true, true, $this->context);
		$this->product_quantity_discount = 0.00;
		if ($quantityDiscount)
		{
			$this->product_quantity_discount = $unitPrice;
			if (Product::getTaxCalculationMethod((int)$order->id_customer) == PS_TAX_EXC)
				$this->product_quantity_discount = Tools::ps_round($unitPrice, 2);

			if (isset($this->tax_calculator))
				$this->product_quantity_discount -= $this->tax_calculator->addTaxes($quantityDiscount['price']);
		}

		$this->discount_quantity_applied = (($this->specificPrice && $this->specificPrice['from_quantity'] > 1) ? 1 : 0);
	}
	
}

