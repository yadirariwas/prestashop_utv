<?php

if (!defined('_PS_VERSION_'))
	exit;

class roundfixpmkt extends Module
{
	function __construct()
 	{
 	 	$this->name = 'roundfixpmkt';
 	 	$this->version = '0.1';
		$this->author = 'PrestaMarketing';
 	 	$this->tab = 'others'; 	 	
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6.0.9');

		parent::__construct();

		$this->displayName = $this->l('Fix Prestashop Round Bug');
		$this->description = $this->l('PrestaMarketing - Fix Prestashop Round Bug');
 		
	}

	public function install()
	{
	 	return (parent::install());
	}

	public function uninstall()
	{
		return (parent::uninstall());
	}
	
	public function getContent()
	{
		$output = '<center>';
		$output .= '<h2 style="width:400px">Si esta aportaci�n te ha sido util y te apetece invitarnos a un caf� o realizar un aporte econ�mico para poder seguir trabajando en m�s como esta, no dudes en donar pulsando en el link que aparece a continuaci�n</h2>';
		$output .= '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input name="cmd" type="hidden" value="_s-xclick" /> <input name="hosted_button_id" type="hidden" value="JYYUHD4JFUBDG" /> <input alt="PayPal. La forma r&aacute;pida y segura de pagar en Internet." name="submit" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" type="image" /> <img src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" alt="" width="1" height="1" border="0" /></form>';
		$output .= '</center>';
		return $output;
	}
}
?>