<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class MyAccountControllerCore extends FrontController
{
	public $auth = true;
	public $php_self = 'my-account';
	public $authRedirection = 'my-account';
	public $ssl = true;

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS(_THEME_CSS_DIR_.'my-account.css');
	}

	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		/*
		$angel["aaya@sunedison.com"]="aaya";
		$angel["c4martell@gmail.com"]="adlo";
		$angel["pindterbertha@gmail.com"]="adlrios";
		$angel["adri1calderon@gmail.com"]="adria";
		$angel["asdz58@hotmail.com"]="adze";
		$angel["mlopez@cpiproductos.com.mx"]="aguaya";
		$angel["gerardjaguar@yahoo.com.mx"]="alaj";
		$angel["alberto@grupofrisa.com"]="alberiv";
		$angel["patricioalcocer@yahoo.com"]="alcopat";
		$angel["alejandro@delgado.com"]="aledel";
		$angel["alejandro.fernandez@matesa.net"]="alefdz";
		$angel["nirvana_heaven@hotmail.com"]="alel";
		$angel["alejandroespinosa@correoinfinitum.com"]="alesp";
		$angel["alesscavazos@gmail.com"]="alessa";
		$angel["rokudf@gmail.com"]="alexam";
		$angel["a_martinez_c@yahoo.com"]="almaco";
		$angel["marinalejandro@mac.com"]="almamor";
		$angel["almuval86@yahoo.com"]="almud";
		$angel["prismav@yahoo.com"]="alrito";
		$angel["asg.1973@gmail.com"]="als";
		$angel["anac.almaguer@hotmail.com"]="amace";
		$angel["javier.amador@me.com"]="amaja";
		$angel["Ro_avendano@hotmail.com"]="amas";
		$angel["alejandro@amrlabogados.mx"]="amon";
		$angel["anita_sada@hotmail.com"]="anasada";
		$angel["antonio.herrero@deltagaming.com.mx"]="anto";
		$angel["antonio.herrero@deltagaming.com.mx"]="antoh";
		$angel["mauro_308@hotmail.com"]="antoro";
		$angel["lo8a61@gmail.com"]="antoro2";
		$angel["adrianaparedeso@hotmail.com"]="aparoj";
		$angel["aperez1105@gmail.com"]="aps";
		$angel["adelarosa0728@yahoo.com"]="arosa";
		$angel["rt@wallakholding.com"]="at";
		$angel["balta@cavazos.com"]="balta";
		$angel["blanca_blanco2007@hotmail.com"]="bb";
		$angel["kps@chelsearf.com"]="bc";
		$angel["lbenjami@hotmail.com"]="befdz";
		$angel["lbenjami@hotmail.com"]="befdz2";
		$angel["cellobene@hotmail.com"]="benem";
		$angel["sarkislara@gmail.com"]="betosa";
		$angel["barbaraizzo@me.com"]="bizzo";
		$angel["bruno_reynoso@hotmail.com"]="brey";
		$angel["liga_te@yahoo.com.mx"]="bru";
		$angel["misaza55@yahoo.com.mx"]="bt3";
		$angel["nizarclemente@gmail.com"]="btower";
		$angel["mariainesprats@gmail.com"]="cahue";
		$angel["martcado@hotmail.com"]="camartin";
		$angel["karlacapote@hotmail.com"]="Capota";
		$angel["carce2000@yahoo.com"]="carce";
		$angel["cfreigva@televisa.com.mx"]="carei";
		$angel["bienmanejado@hotmail.com"]="carlop";
		$angel["Credelag@gmail.com"]="carm";
		$angel["sobando@yahoo.com"]="carov";
		$angel["bibist@hotmail.com"]="cars";
		$angel["taniadiaz777@hotmail.com"]="CAZS88";
		$angel["Ccouttolenc@me.com"]="ccoutt";
		$angel["cecmc@me.com"]="cecmc";
		$angel["queridomario@gmail.com"]="cejas";
		$angel["mankev@me.com"]="cemby";
		$angel["chabatito@yahoo.com.mx"]="chaba";
		$angel["nizarclemente@gmail.com"]="cozumel";
		$angel["dmz.dany@gmail.com"]="dadc";
		$angel["Danespino@gmail.com"]="Danes";
		$angel["cornago@gmail.com"]="danifer";
		$angel["david.solis@condenast.com.mx"]="davisol";
		$angel["Dandelrio@hotmail.com"]="Ddr";
		$angel["diego.fernandez@matesa.net"]="dfdz";
		$angel["teransfam@gmail.com"]="dieter";
		$angel["danperezsalazar@gmail.com"]="dopo2";
		$angel["iarmida@gmail.com"]="dopo3";
		$angel["jokhuysen@gmail.com"]="dopo4";
		$angel["doporto@doporto.mx"]="dopo5";
		$angel["guillermo.lts@gmail.com"]="dopo6";
		$angel["david.perezsalinas@gmail.com"]="dps";
		$angel["direccion@luxorytransfers.com"]="Ebarb";
		$angel["eclavijo@integrapostventa.com"]="edu";
		$angel["edivelazquez@hotmail.com"]="eduve";
		$angel["virues0@gmail.com"]="Efra";
		$angel["virues0@gmail.com"]="efra2";
		$angel["Murrieta.emg@gmail.com"]="ely";
		$angel["fraga.emilio@gmail.com"]="emifra";
		$angel["Esberatala@me.com"]="Esber";
		$angel["mankev@me.com"]="espa";
		$angel["e.o.vieira@gmail.com"]="ev";
		$angel["federicobelden@gmail.com"]="fabs";
		$angel["fabiolahg96@gmail.com"]="fahdz";
		$angel["federicobeldensada@gmail.com"]="febe2";
		$angel["federicosf@gmail.com"]="fedesf";
		$angel["fdeovando@hotmail.com"]="ferova";
		$angel["fidelraul10191963@gmail.com"]="fidera";
		$angel["Fjkm33@gmail.com"]="fjkm";
		$angel["florerubio90@gmail.com"]="flrubio";
		$angel["carlos.noyola@gmail.com"]="FNF";
		$angel["cupcakes.to.go@hotmail.com"]="forra";
		$angel["juan.f.mediavilla@gmail.com"]="frame";
		$angel["fredo.abrantes@gmail.com"]="fre";
		$angel["jdavila10@gmail.com"]="fuadv";
		$angel["gbj007@gmail.com"]="gabar";
		$angel["gcch@eog.mx"]="gcch";
		$angel["gemmaburch1@mac.com"]="gemma";
		$angel["morancastellot@yahoo.com.mx"]="gemo";
		$angel["gildamgarciag@gmail.com"]="ggg";
		$angel["gilberto61c@gmail.com"]="gilc";
		$angel["giocerritelli@me.com"]="gioc";
		$angel["gerardomorancastellot@gmail.com"]="gm";
		$angel["ricardo.kerber@gmail.com"]="godo";
		$angel["moga_16@hotmail.com"]="goyri";
		$angel["gretta27@gmail.com"]="gretta";
		$angel["gflores@me.com"]="guflo";
		$angel["garr231@interhabita.com.mx"]="guianro";
		$angel["garciagus2003@yahoo.com"]="gusga";
		$angel["pitaglezs@yahoo.com.mx"]="gzzg";
		$angel["humbertocavazos@yahoo.com"]="hcc1";
		$angel["humberto@cavazosflores.com"]="hcf";
		$angel["helle_jeppsson@hotmail.com"]="helle";
		$angel["hpaliza@gmail.com"]="hpaliza";
		$angel["hazel_sandoval@hotmail.com"]="hs2";
		$angel["iabiega@yahoo.com"]="ikerab";
		$angel["inigomat@yahoo.com.mx"]="inigo";
		$angel["inigomat@yahoo.com.mx"]="inigo2";
		$angel["jcabrera@jaca.com.mx"]="jaca";
		$angel["jalazvar@yahoo.com.mx"]="jalaz";
		$angel["igrobles@hotmail.com"]="javad";
		$angel["Javier@amrlabogados.mx"]="Javlo";
		$angel["mankev@me.com"]="jb2";
		$angel["mankev@me.com"]="jballesteros";
		$angel["jorgecarlos@inteligencialaboral.com"]="jc2";
		$angel["jorgegcastaneda@gmail.com"]="jcas";
		$angel["fernanda.delafuente@gmail.com"]="jccc2";
		$angel["asuntoarreglado@cavazos.com"]="jccc3";
		$angel["fernanda.delafuente@gmail.com"]="jccc4";
		$angel["jdavila10@gmail.com"]="jesdav";
		$angel["javfolch@mac.com"]="jf2";
		$angel["juan_antoniohernandez@hotmail.com"]="jh";
		$angel["jibarrola@lico.mx"]="jiba";
		$angel["lopez.jorgej@gmail.com"]="jl";
		$angel["genaf@me.com"]="jmatos";
		$angel["jsauquet@gmail.com"]="joan2";
		$angel["rokudf@gmail.com"]="joji";
		$angel["joseroman.garzon@gmail.com"]="josegr";
		$angel["joselbq@yahoo.com"]="joselb";
		$angel["jorgeeduardo15@hotmail.com"]="jovi";
		$angel["jorge@recreoentretenimiento.com"]="jp";
		$angel["jreyes63@mac.com"]="jreyes";
		$angel["jsauquet@gmail.com"]="jsau";
		$angel["japedreroh@gmail.com"]="juanph";
		$angel["jrmoreno@morenolaw.com"]="juro";
		$angel["jcarlosgonzalez2000@hotmail.com"]="jyc";
		$angel["jamessprowls@gmail.com"]="JYM";
		$angel["mestradag18@gmail.com"]="jz";
		$angel["ARQ.KARLARMZ@GMAIL.COM"]="karam";
		$angel["kps@chelsearf.com"]="kimp";
		$angel["igavidal@gmail.com"]="kv";
		$angel["alfonso_mtzr@hotmail.com"]="lamr";
		$angel["Leogarcia7@gmail.com"]="leog";
		$angel["leyrafael@gmail.com"]="leyr";
		$angel["lidibc@hotmail.com"]="lidibc1";
		$angel["bladic@hotmail.com"]="lidibc2";
		$angel["hbladinieres@hotmail.com"]="lidibc3";
		$angel["liliabh29@hotmail.com"]="lilibu";
		$angel["laila.garciag@hotmail.com"]="lmg";
		$angel["lmv@monsalvo.com.mx"]="lmv";
		$angel["lazcarraga88@aol.com"]="loraz1";
		$angel["lazcarraga88@aol.com"]="loraz2";
		$angel["lazcarraga88@aol.com"]="loraz3";
		$angel["lugaudi@gmail.com"]="lugau";
		$angel["lcasamayor@gmail.com"]="luiscm";
		$angel["lgonzalez@inpamex.com"]="luisre";
		$angel["proinsumos01@gmail.com"]="luisya";
		$angel["macahaca@gmail.com"]="macaha";
		$angel["centenomaria@gmail.com"]="mace";
		$angel["melisaromo@gmail.com"]="maelro";
		$angel["mallitaxoxo93@gmail.com"]="magi";
		$angel["lgmartinez@almex.com.mx"]="makar";
		$angel["Mab3067@gmail.com"]="malva";
		$angel["maragarza@gmail.com"]="maraga";
		$angel["mariagaha@me.com"]="marga";
		$angel["mariolozano21@hotmail.com"]="mariol";
		$angel["mariza.mejiag@gmail.com"]="marmej";
		$angel["lavazquez001@hotmail.com"]="maruv";
		$angel["marthagalas@gmail.com"]="matro";
		$angel["matute@hotmail.com"]="matuan";
		$angel["mautvtv@hotmail.com"]="mautv";
		$angel["maytegfigueroa@gmail.com"]="may";
		$angel["manuel.castellanosc@gmail.com"]="mcs";
		$angel["mda@chelsearf.com"]="md";
		$angel["paolatrevino10@gmail.com"]="memob";
		$angel["mgarzah@hotmail.com"]="mg";
		$angel["monicagzzfigueroa@gmail.com"]="mgf";
		$angel["gerenciawtc@teikit.mx"]="michrdz1";
		$angel["mnhayito@hotmail.com"]="michrdz2";
		$angel["monicagr22@hotmail.com"]="mongr22";
		$angel["maytegfigueroa@gmail.com"]="moym";
		$angel["monicasanz@t-visa.com.mx"]="msanz";
		$angel["mtt99@hotmail.com"]="mtt99";
		$angel["mzubiria@mac.com"]="mzub";
		$angel["icastro@adimpacto.com"]="nachoo";
		$angel["nataliagarza@gmail.com"]="nagalo";
		$angel["normacantu@me.com"]="nc";
		$angel["mankev@me.com"]="neo";
		$angel["nizarclemente@gmail.com"]="nizar";
		$angel["nunziarojodelavega@gmail.com"]="nunzia";
		$angel["osalinas@tsigns.com.mx"]="osal";
		$angel["osalinas@tsigns.com.mx"]="osal2";
		$angel["plavalleg@hotmail.com"]="pabl2";
		$angel["plavalleg@hotmail.com"]="pabl3";
		$angel["jflozano@rimmsa.com"]="pancho";
		$angel["patyf2@mac.com"]="patyf2";
		$angel["jdavila10@gmail.com"]="pe";
		$angel["pedro_alvear@hotmail.com"]="pedal";
		$angel["pablozuelos@gmail.com"]="pel";
		$angel["politrevino@hotmail.com"]="politre";
		$angel["ropodo@hotmail.com"]="poo";
		$angel["Garciabazan@hotmail.com"]="pr";
		$angel["alejandroprats90@gmail.com"]="prats";
		$angel["angaha-1001@hotmail.com"]="puma1";
		$angel["rafgonz43@yahoo.com"]="rafgzz";
		$angel["ricardodigo@hotmail.com"]="rdgzz";
		$angel["robertofmiles@gmail.com"]="rfmiles";
		$angel["rherrerias@rhcei.mx"]="riche";
		$angel["rherrerias@rhcei.mx"]="riche2";
		$angel["rlopez66@hotmail.com"]="rlm";
		$angel["drbetochas@gmail.com"]="robng";
		$angel["Roberto.sanchez@consyrsa.com"]="Rosan";
		$angel["torresl.german@gmail.com"]="rp";
		$angel["rtogores@ludicus.com"]="rtogo";
		$angel["jrubenhernandez@hotmail.com"]="ruhdz";
		$angel["claude.mongala@gmail.com"]="sa13";
		$angel["rmatenorio@outlook.com"]="sa14";
		$angel["mrvieira@yahoo.com"]="sa15";
		$angel["undertvsa@gmail.com"]="sa4";
		$angel["rmatenorio@gmail.com"]="sa6";
		$angel["bernard.ruberg@gmail.com"]="sa7";
		$angel["Greg.lang@touchhome.co.za"]="sa8";
		$angel["claude@seragliojewellers.com"]="sa9";
		$angel["salomonmansur@gmail.com"]="salom";
		$angel["mankev@me.com"]="santi";
		$angel["schufani@chufani.com"]="Schu";
		$angel["sergiochedraui@gmail.com"]="seched";
		$angel["visionk6@gmail.com"]="senri";
		$angel["visionk6@gmail.com"]="senri2";
		$angel["caneggle87@gmail.com"]="servc";
		$angel["diego.fernandez@matesa.net"]="sfdz";
		$angel["stephaniemdl@hotmail.com"]="smdl";
		$angel["sofia_mtz5@hotmail.com"]="somtz";
		$angel["sonia_bdm@hotmail.com"]="sonba";
		$angel["pablosozzi@hotmail.com"]="sozzi";
		$angel["Stephaniemdl@hotmail.com"]="Stemu";
		$angel["susre@hotmail.com"]="supare";
		$angel["dondelees@gmail.com"]="tino";
		$angel["mankev@me.com"]="tioze";
		$angel["scullenp@gmail.com"]="tmas";
		$angel["ltostado@hotmail.com"]="tos";
		$angel["Ugow@gaam.com.mx"]="Ugow";
		$angel["affernan@radioformula.com.mx"]="usu1";
		$angel["affernan@radioformula.com.mx"]="usu2";
		$angel["mauro_plantabaja@hotmail.com"]="usu3";
		$angel["mlarregui@hotmail.com"]="usu4";
		$angel["valeriant@gmail.com"]="valent";
		$angel["vela_f@hotmail.com"]="velascof2";
		$angel["rokudf@gmail.com"]="vicval";
		$angel["drvalpuesta@gmail.com"]="vicvalp***";
		$angel["dvillarreal@aceromex.com"]="villad";
		$angel["e.trejo@me.com"]="xmas1";
		$angel["josed_rgc@hotmail.com"]="xmas2";
		$angel["agaskdro@yahoo.com.mx"]="xmas5";
		$angel["amhilairefraga@hotmail.com"]="xmas6";
		$angel["yessics@hotmail.com"]="yess";
		$angel["marioab75@yahoo.com.mx"]="yogzz";
		$angel["mzubiriam@hotmail.com"]="zubm";

		*/

/*
		foreach($angel as $key => $value){
			$angel[strtolower($key)]=$value;
		}
		
		if (isset($angel[strtolower($this->context->customer->email)])){
			$usuario_roku=$angel[strtolower($this->context->customer->email)];
		}
		else{
			$usuario_roku="utv_".$this->context->customer->id;
		}*/
		$usuario_roku='';

		$this->context->smarty->assign(array(
			'has_customer_an_address' => empty($has_address),
			'voucherAllowed' => (int)CartRule::isFeatureActive(),
			'returnAllowed' => (int)Configuration::get('PS_ORDER_RETURN'),
			'usuario_roku' => $usuario_roku
		));
		$this->context->smarty->assign('HOOK_CUSTOMER_ACCOUNT', Hook::exec('displayCustomerAccount'));

		$this->setTemplate(_PS_THEME_DIR_.'my-account.tpl');
	}
}

