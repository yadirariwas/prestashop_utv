<?php

/*

* 2007-2014 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Open Software License (OSL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/osl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*/



class AdminCustomerPagoControllerCore extends AdminController

{

	public function __construct()

	{

		$this->bootstrap = true;

		//$this->className = 'Configuration';

		//$this->table = 'configuration';



		parent::__construct();


		$this->fields_options = array(

			'pago' => array(

				'title' =>	$this->l('Pago OnDemand'),

				'fields' =>	array(

					'method' => array(
						'title' => $this->l('Method'),
						'type' => 'text',
						'label' => $this->l('method'),
						'name' => 'method',
						'col' => '4',
						'required' => false,
						'autocomplete' => false,
						'is_invisible' => true
					),
					'user' => array(
						'title' => $this->l('User'),
						'type' => 'text',
						'label' => $this->l('user'),
						'name' => 'user',
						'col' => '4',
						'required' => false,
						'autocomplete' => false
					),
					'reference' => array(
						'title' => $this->l('Reference'),
						'type' => 'text',
						'label' => $this->l('reference'),
						'name' => 'reference',
						'col' => '4',
						'required' => false,
						'autocomplete' => false
					),
					'token' => array(
						'title' => $this->l('Token'),
						'type' => 'text',
						'label' => $this->l('token'),
						'name' => 'token',
						'col' => '4',
						'required' => false,
						'autocomplete' => false
					),
					'amount' => array(
						'title' => $this->l('Amount'),
						'type' => 'text',
						'label' => $this->l('amount'),
						'name' => 'amount',
						'col' => '4',
						'required' => false,
						'autocomplete' => false
					),





				),

				'submit' => array('title' => $this->l('Save'))

			),

		);

	}







}

