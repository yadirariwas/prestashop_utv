<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class AdminRegisterCustomerControllerCore extends AdminController
{
	protected $delete_mode;
	protected $_defaultOrderBy = 'fecha_creacion';
	protected $_defaultOrderWay = 'DESC';
	protected $can_add_customer = false; //Es para agregar un nuevo cliente

	public function __construct()
	{
		$this->bootstrap = true; //
		$this->required_database = true; //
		$this->required_fields = array('newsletter','optin');
		$this->table = 'customer_roku';
		$this->className = 'CustomerRoku';
		$this->lang = false;
		$this->deleted = false; //
		$this->explicitSelect = false; //
		$this->allow_export = true;
		$this->context = Context::getContext();
		$this->default_form_language = $this->context->language->id;
		$titles_array = array();
		$genders = Gender::getGenders($this->context->language->id);

		foreach ($genders as $gender)
			$titles_array[$gender->id_gender] = $gender->name;
		$this->_select = 'a.email, a.user, a.fecha_creacion, a.fecha_pago';



		$this->fields_list = array(
			'email' => array(
				'title' => $this->l('Email'),
			),			

			'user' => array(
				'title' => $this->l('Usuario'),
			),	

			'fecha_creacion' => array(
				'title' => $this->l('F. CreaciÃ³n'),
				'search' => false
			),	

			'fecha_pago' => array(
				'title' => $this->l('F. Pago'),
				'search' => false
			),	
		);


		$this->shopLinkType = 'shop';
		$this->shopShareDatas = Shop::SHARE_CUSTOMER;

		parent::__construct();

		// Check if we can add a customer
		if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP))
			$this->can_add_customer = false;

	}

	

	public function renderForm()
	{
		if (!($obj = $this->loadObject(true)))
			return;

		$years = Tools::dateYears();
		$months = Tools::dateMonths();
		$days = Tools::dateDays();

		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Customer'),
				'icon' => 'icon-user'
			),

			'input' => array(
				array(
					'type' => 'text',
					'prefix' => '<i class="icon-envelope-o"></i>',
					'label' => $this->l('Correo electrÃ³nico'),
					'name' => 'email',
					'col' => '4',
					'required' => true,
					'autocomplete' => false
				),

				array(
					'type' => 'text',
					'label' => $this->l('Usuario'),
					'name' => 'user',
					'required' => true,
					'col' => '4',
					'hint' => $this->l('Invalid characters:').' 0-9!&lt;&gt;,;?=+()@#"Â°{}_$%:'

				),
			)
		);



		$this->fields_form['submit'] = array(
			'title' => $this->l('Save'),
		);

	}	

}