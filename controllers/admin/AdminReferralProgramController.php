<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminReferralProgramControllerCore extends AdminController
{
	protected $delete_mode;

	protected $_defaultOrderBy = 'date_add';
	protected $_defaultOrderWay = 'DESC';
	protected $can_add_customer = false; //Es para agregar un nuevo cliente

	public function __construct()
	{
		$this->bootstrap = true; //
		$this->required_database = true; //
		$this->required_fields = array('newsletter','optin');
		$this->table = 'referralprogram';
		$this->className = 'Customer';
		$this->lang = false;
		$this->deleted = false; //
		$this->explicitSelect = false; //

		$this->allow_export = true;

		$this->context = Context::getContext();

		$this->default_form_language = $this->context->language->id;

		$titles_array = array();
		$genders = Gender::getGenders($this->context->language->id);

		foreach ($genders as $gender)
			$titles_array[$gender->id_gender] = $gender->name;

		$this->_select = 'a.email,
			DATE_FORMAT(a.date_add,"%d-%m-%Y") as fecha_registro,
			CONCAT(a.lastname, " ", a.firstname) AS cliente';

		//echo $this->_select; exit;
		
		$this->_join = '';
		
		$this->fields_list = array(
			'cliente' => array(
				'title' => $this->l('Cliente'),
				'search' => false
			),			
			'email' => array(
				'title' => $this->l('Email'),
				'search' => false
			),		
			'fecha_registro' => array(
				'title' => $this->l('F. registro'),
				'search' => false
			),		
		);
		
		$this->_group = '';

		$this->shopLinkType = 'shop';
		$this->shopShareDatas = Shop::SHARE_CUSTOMER;

		parent::__construct();
	}


}
